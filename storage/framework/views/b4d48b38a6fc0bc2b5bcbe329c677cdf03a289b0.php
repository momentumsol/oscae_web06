<style>
	/* .promotion-content h3, h2 {
		color: #333;
	}

	.empty-space {
		height: 52.5px;
		width: 100%;
	} */
	.slick-track > [class*=col]{
		height: 460px;
	}
	.product-title{
    white-space: nowrap !important;
    overflow: hidden !important;
    text-overflow: ellipsis !important;
    display: inline-block !important;
    width: 100%;
}

@media (max-width: 400px) { 

.promotion-content .col-lg-4{
	height: 180px !important;
}
.product-content{
	min-height: 110px
}
}

</style>
<?php $__env->startSection('content'); ?>

<div class="promotion-content">
	<div class="container">

		<?php if( count($offers) > 0): ?>
			<section class="vs-blog-wrapper pt-5 space-md-bottom" id="blog">
				<div class="container">
					<div class="section-title text-center wow fadeIn" data-wow-delay="0.3s">
						<h1 class="sec-title1"><?php echo e(__('lang.promotions_members')); ?></h1>
						<p><a href="<?php echo e(url(app()->getLocale().'/all_promotions')); ?>" style="text-decoration: underline"><?php echo e(__('lang.show_all')); ?></a></p>
						
						
					</div>
					<div class="row vs-carousel wow fadeIn" data-wow-delay="0.3s" data-slide-show="3">
						<?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $meal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="col-lg-4">
								<div class="vs-blog blog-grid vs-product-box1 thumb_swap">
									<div class="blog-img image-scale-hover">
										<a href="<?php echo e(url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id)); ?>"><img src="<?php echo e($meal->images['0']->src); ?>" alt="Blog Image" class="w-100"></a>
									</div>
									<div class="blog-content text-center product-content">
										<div class="actions-btn">
											<a href="<?php echo e($meal->PriceUnit == "kg" ? url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id) : ''); ?> " title="<?php echo e($meal->id); ?>" class="<?php echo e($meal->in_stock != 1 ? 'd-none' : ''); ?> <?php echo e($meal->PriceUnit != "kg" ? 'cart' : ''); ?> "><i class="fal fa-cart-plus"></i></a>
											<?php if(Auth::guard('customerForWeb')->user()): ?>
											<a href="#" title="<?php echo e($meal->id); ?>" class="Unlike" style="display: <?php echo e(in_array($meal->id,$pros) ? 'block' : 'none'); ?>"><i style="color:#C53330;" class="fal fa-heart"></i></a>
											<a href="#" title="<?php echo e($meal->id); ?>" class="Wishlist" style="display: <?php echo e(in_array($meal->id,$pros) ? 'none' : 'block'); ?>"><i class="fal fa-heart"></i></a>
											<?php endif; ?>
										</div>
										<div >
											<h4 class=" product-title h5 mb-0"><a href="<?php echo e(url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id)); ?>"><?php echo e(app()->getLocale() =='en' ? $meal->name : $meal->name_ar); ?></a></h4>
											<span class="price font-theme"><strong><span style="text-decoration: <?php echo e($meal->on_sale == '1' ?  'line-through' : ' '); ?> "><?php echo e($meal->regular_price); ?></span>   <?php echo e($meal->on_sale == '1' ? ' '.$meal->discountprice :''); ?> </strong>  <?php echo e(__('lang.EGP')); ?></span>
										
										</div>
										
									</div>
								</div> 
							</div>	
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</section>
		<?php else: ?>
			<div class="row justify-content-center mt-5">
				<div class="col-lg-6 col-md-6 col-sm-6 col-6 text-center">
					<div class="empty-space"></div>
					<h2>No promotions available yet</h2>
					<h3>Please check back later</h3>
					<div class="empty-space"></div>
				</div>
			</div>
		<?php endif; ?>
		
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>