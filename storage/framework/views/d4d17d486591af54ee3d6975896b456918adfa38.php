<style>
.restaurant{
    margin: 30 auto;
}
.restaurant .image-group{
}
.restaurant h1{
    color: #EA0706;
    text-align: center;
    margin: 10px

}

/* .circl1 {
    border-radius: 50%;
} */
.circl1 img {
    width: 100%;
    border-radius: 100%;
    height: 30vh;
    
   
}

/* .circl2 {
    border-radius: 50%;
} */
@media  only screen and (min-width: 600px) {
.circl2 img {
    width: 100%;
    border-radius: 100%;
    height: 30vh;
}
.circl1 img {
    width: 100%;
    border-radius: 100%;
    height: 30vh;
    
   
}

}
.nameOfRestaurant {
    font-size: 25px;
    color: #000;
}

.retaurant a:hover {
    color: #000;
}
@media  only screen and (max-width: 767px) {
 .circl1 img {
    width: 100%;
    border-radius: 100%;
    height: 10vh;
    
   
}

/* .circl2 {
    border-radius: 50%;
} */
.circl2 img {
    width: 100%;
    border-radius: 100%;
    height: 10vh;
}
}
</style>
<?php $__env->startSection('content'); ?>

    <div class="breadcumb-wrapper breadcumb-layout1 bg-fluid pt-200 pb-200" data-bg-src="<?php echo e(asset('assets/img/breadcumb/hero-bg-4-3.jpg')); ?>">
        <div class="container">
            <div class="breadcumb-content text-center">
                <h1 class="breadcumb-title">Restaurants</h1>
                
                <img src="<?php echo e(asset('assets/img/icons/sec-icon-1.png')); ?>" alt="Section Shape">
            </div>
        </div>
    </div>
    <section class="vs-blog-wrapper space-md-bottom mt-5" id="blog">
        <div class="container">
            <div class="row vs-carousel wow fadeIn" data-wow-delay="0.3s" data-slide-show="3">
            <?php $__currentLoopData = $resaurants; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $res): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php   $image=str_replace("public",'storage',$res->image);
                    $menu=str_replace("public",'storage',$res->menu);
            ?>


                <div class="col-lg-4">
                    <div class="vs-blog blog-grid">
                        <div class="blog-img image-scale-hover">
                            <a href="http://oscar.momentum-sol.com/<?php echo $menu; ?>" download="http://oscar.momentum-sol.com/<?php echo $menu; ?>"><img src="http://oscar.momentum-sol.com/<?php echo $image; ?>" alt="Blog Image" class="w-100"></a>
                        </div>
                        <div class="blog-content">
                            
                            
                        </div>
                    </div>
                </div>
               
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               
                    
                    

            </div>
            

           
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
	<script type="text/javascript">


	</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>