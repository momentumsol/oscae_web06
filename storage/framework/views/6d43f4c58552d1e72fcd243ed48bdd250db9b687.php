<?php $__env->startPush('style'); ?>
	<style>
		td{
		text-align:center !important
	}
	</style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

		
	 <!--==============================
    Cart Area
    ==============================-->
    <div class="vs-cart-wrapper  space-top space-md-bottom">
        <div class="container">
            
            
                <table class="cart_table table shopping-summery">
                    <thead>
                        <tr>
							<th><?php echo e(__('lang.product')); ?></th>
                            <th><?php echo e(__('lang.name')); ?></th>
                            <th class="text-center"><?php echo e(__('lang.unite_price')); ?></th>
                            <th class="text-center"><?php echo e(__('lang.quantity')); ?></th>
                            <th class="text-center"><?php echo e(__('lang.total')); ?></th> 
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        
						<tfoot>
							<tr>
								<td colspan="6" class="" style="text-align: right !important;">
									
									
									<ul style="list-style:none">
										<li style="font-weight: bolder;"><?php echo e(__('lang.cart_sub_total')); ?> <span style"" id="total_price">  </span> </li>
<?php if(Auth::guard('customerForWeb')->user()): ?>
<li><a id="setprice" style="z-index:99999;width:100%;" href="<?php echo e(url(app()->getLocale().'/checkout')); ?>" class="vs-btn rounded-1 shadow-none"><?php echo e(__('lang.check_out')); ?> </a></li>
<?php else: ?>
<li><a href="<?php echo e(route('login',app()->getLocale())); ?>" class="vs-btn rounded-1 shadow-none"> <?php echo e(__("lang.You should Login first")); ?></a></li>
<?php endif; ?>

									</ul>
								</td>
							</tr>

						</tfoot>
                    </tbody>
                </table>

            
            
        </div>
		
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

<script type="text/javascript">


// Closure
(function() {
  /**
   * Decimal adjustment of a number.
   *
   * @param  {String}  type  The type of adjustment.
   * @param  {Number}  value The number.
   * @param  {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
   * @returns  {Number} The adjusted value.
   */
  function decimalAdjust(type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }
})();

function addToCartAjax(id,weight,qty)
{
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/addToCart',
	data:{ product_id:id,weight:weight,quantity:parseInt(qty) },

	success:function(data){
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);

	}	
});
}
var total_price=0.00;
var storage=JSON.parse(localStorage.getItem("cart"));
var cartArray = <?php echo json_encode($proArray); ?>;
//  console.log(cartArray,cartArray.length,'cartArray')
if(cartArray.length==0)
{
	$("#setprice").remove();

}
var lang=<?php echo json_encode(app()->getLocale()); ?>;

for (var i = 0; i < cartArray.length; i++){

	var pro=cartArray[i];

	var key=Object.keys(pro)[0];
	var cartProducts = [];
	var id=pro.id;
	cartProducts.push(id);
	var name=pro.name;

	if(lang=='ar')
	{
		name=pro.name_ar;
	}
	var unit=pro.PriceUnit;
	if(unit=='kg')
	{
		name=name+' ('+pro.weight+'g)'
	}

	if(pro.images[0]['src']){
		var src=pro.images[0]['src'];
	}else{
		var src ='';
	}
	var price=pro.regular_price;
	var original_price=pro.regular_price;

	var qty=pro.quantity;
	if(!qty){
		qty=1;
	}
	if(parseInt(pro.discountprice) !==0)
	{
		price=pro.discountprice
		original_price=pro.discountprice
	}
	if(unit=='kg')
	{
		price=price*pro.weight/1000
	}
	var weight=pro.weight
	
	total_price = Number(price*Number(qty).toFixed(2)) + Number(total_price.toFixed(2));
	
 	var table_row='<tr id="'+id+'">' +
					'<td class="image" data-title="<?php echo e(__('lang.image')); ?>"><img width="100" src="'+src+'" alt="#"></td>' +
					'<td class="product-des" data-title="<?php echo e(__('lang.name')); ?>">' +
						'<p class="product-name"><a href="#">'+name+'</a></p>' +
					'</td>'+
					'<td class="price" data-title="<?php echo e(__('lang.unite_price')); ?>"> <span class="regular_price"> '+Number(original_price)+' <?php echo e(__('lang.EGP')); ?></span></td>'+
					'<td class="qty" data-title="<?php echo e(__('lang.quantity')); ?>">'+
						'<div class="input-group" style="display: flex;  justify-content: center;">'+

						'<input type="hidden"  value="'+weight+'" id="'+id+'_weight">'+
											'<button type="button" class="remove btn  btn-number quantity-minus qut-btn"  data-type="minus" data-field="quant[2]" style="padding:5px">'+
												'<i class="far fa-minus" style="line-height: 0;font-size: inherit;"></i>'+
											'</button>'+

											
											'<input type="text" unit="'+unit+'" class="qty" value="'+qty+'" style="width: 20%;border: 0;text-align: center;" readonly>'+
											'<button type="button"  class="add btn quantity-plus qut-btn  btn-number" data-type="plus" data-field="quant[2]" style="padding:5px">'+
												'<i class="far fa-plus" style="line-height: 0;font-size: inherit;"></i>'+
											'</button>'+
						'</div>'+
						'<!--/ End Input Order -->'+
					'</td>'+
					
					'<td class="product_total_pice" data-title="<?php echo e(__('lang.cart_sub_total')); ?>">'+Number(price*Number(qty)).toFixed(2)+'</td>'+
					'<td class="action" data-title="<?php echo e(__('lang.remove')); ?>"><a href="#"><i class="fal fa-trash-alt"></i></a></td>'+
				'</tr>';

				
	
		$('tbody').append(table_row);	

								

}
	$("#total_price").html(Number(total_price.toString().match(/^\d+(?:\.\d{0,2})?/)) +" <?php echo e(__('lang.EGP')); ?>");
	$(".add").click(function(){
		var qty=$(this).prev("input").val();

		var proUnit=$(this).prev("input").attr('unit');
		
			parseInt(qty);
			qty++;
		
		 $(this).prev("input").val(qty);

		$('.remove').prop("disabled", false);
		var product_id=$(this).closest('tr').attr('id');
		var localStorage_key='product_'+product_id;
		var regular_price=$(this).closest('tr').find('.regular_price').text();
		var product_total_pice =$(this).closest('tr').find('.product_total_pice');
		regular_price=regular_price.replace("EGP","");
		let weight =250
		regular_price=parseFloat(regular_price)
		// product_total_pice=regular_price * qty;

		if(proUnit=='kg')
		{
			let weight_id=product_id+'_weight'

			let weight_total=parseInt(document.getElementById(weight_id).value)
			weight=weight_total
			product_total_pice=(regular_price*weight_total/1000) * qty;
			regular_price = regular_price*weight_total/1000
		

		
		}
		else

		{
			product_total_pice = regular_price * qty;
		}
	
		$(this).closest('tr').find('.product_total_pice').html(Math.ceil10(product_total_pice.toFixed(1),-1));
		total_price += parseFloat(regular_price);
		$("#total_price").html(total_price.toFixed(2));
	
		// Get the existing data
		// var existing =JSON.parse(localStorage.getItem(localStorage_key));
      
		 for (var i=0;i<storage.length;i++)
		 {
			let item=storage[i]
			// console.log(Object.keys(item)[0],localStorage_key)
			// console.log('product_'+Object.keys(item)[0],localStorage_key)
        if(Object.keys(item)[0]==localStorage_key)
        {
			// alert('a')
			item[Object.keys(item)[0]]['quantity']=qty
			localStorage.setItem('cart', JSON.stringify(storage));
			
         
        }
		 }
	
		 addToCartAjax(product_id,weight,1)	

	});
	$(".remove").click(function(){
	

		var product_id=$(this).closest('tr').attr('id');
		var localStorage_key='product_'+product_id;
		var qty=$(this).next("input").val();

		var proUnit=$(this).next("input").attr('unit');
		var price=$(this).closest('tr').find('.product_total_pice').html();
		let weight=250;
		var unit_price=$(this).closest('tr').find('.regular_price').text();
		unit_price=unit_price.replace("EGP","");
		unit_price=unit_price.replace("جنيه","");

		unit_price=parseFloat(unit_price)
			if ( qty == 1) {

			$(this).prop("disabled", true);
			}
	
		else if ( qty >= 1) {

			qty--;
			$(this).next("input").val(qty);
			if(proUnit =='kg')
			{
				let weight_id_remove=product_id+'_weight'
				let weight_total_remove=parseInt(document.getElementById(weight_id_remove).value)
				weight=weight_total_remove
				let total_unite_price=parseFloat(unit_price)*(weight_total_remove/1000)
				$(this).closest('tr').find('.product_total_pice').html((price - total_unite_price).toFixed(1));
				total_price -= total_unite_price;
			  $("#total_price").html(total_price.toFixed(1) +" <?php echo e(__('lang.EGP')); ?>");
		

			}
			else
			{
				$(this).closest('tr').find('.product_total_pice').html((price - unit_price).toFixed(1));
				total_price =total_price - unit_price;
				$("#total_price").html(total_price.toFixed(1) +" <?php echo e(__('lang.EGP')); ?>");
			}
		
			// Get the existing data
			for (var i=0;i<storage.length;i++)
		 {
			let item=storage[i]
        if(Object.keys(item)[0]==localStorage_key)
        {
			item[Object.keys(item)[0]]['quantity']=qty

			localStorage.setItem('cart', JSON.stringify(storage));
			
         
        }
		 }

		}
		else {
			$(this).prop("disabled", true);
		}	

		addToCartAjax(product_id,weight,-1)	
	});

	$('.action').click(function (e) {
	
		e.preventDefault();
storage.length
		x=$(this).closest('tr').find('.product_total_pice').html();
		total_price =parseFloat(total_price) - parseFloat(x) ;
		console.log(storage.length,'total_price')
		if(parseFloat(total_price) <= 0)
		{


			$("#total_price").html(0.00+" <?php echo e(__('lang.EGP')); ?>")
		}
		else if(storage.length==1)
		{

			$("#total_price").html(0.00+" <?php echo e(__('lang.EGP')); ?>")
		}
		else
		{
			$("#total_price").html( total_price.toFixed(1) +" <?php echo e(__('lang.EGP')); ?>");
		}
		$(this).closest('tr').remove();
		var product_id=$(this).closest('tr').attr('id');
		$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/removeFromCart',
	data:{ product_id:product_id },
	success:function(data)
	{
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);

	}	


});
		for (let i = 0; i< storage.length; i++) {
			var pro=storage[i];
			var key=Object.keys(pro)[0];
			if(key == 'product_'+product_id){
				storage.splice(i, 1); 
				// delete storage[key]
			}
		}

		var cart =[];
		localStorage.setItem('cart', JSON.stringify(cart));
		localStorage.setItem('cart', JSON.stringify(storage));
		localStorage.removeItem('product_'+product_id);

		var total=JSON.parse(localStorage.getItem("cart"));
		$('.total-count').html(JSON.parse(total.length));
	});


	$('#setprice').click(function(){
			var total= $("#total_price").html();
			localStorage.setItem("total_price", total);

	});

</script>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>