<?php $__env->startPush('style'); ?>

   <link rel="stylesheet" href="<?php echo e(asset('/css/customer-profile.css')); ?>">
    <style>
        .nav-link{
            background-color: #fff !important
        }

        #addresses th,#addresses td{
            padding: 8px
        }
        .button-5 {
            color: #C5171C !important;
            padding: 8px 8px 5px
        }
        h5 .button-5:hover {
            color: #fff !important
        }
        .pay_method img{
          width: 100px;
          margin-right: 20px;
          cursor: pointer;
      }
      .pay_method img:active{
        box-shadow: -1px 1px 14px -4px rgba(0,0,0,0.75);
      }
      [type=radio] { 
        position: absolute;
        opacity: 0;
        width: 0;
        height: 0;
        }

        /* IMAGE STYLES */
        [type=radio] + img {
        cursor: pointer;
        }

        /* CHECKED STYLES */
        [type=radio]:checked + img {
        outline: 2px solid #f00;
        }
        /* Styles FOR MESSEGES */
        .container2 {
            border: 2px solid #dedede;
            background-color: #f1f1f1;
            border-radius: 5px;
            padding: 10px;
            margin: 10px 0;
        }

        .darker {
            border-color: #ccc;
            background-color: #ddd;
        }

        .container2::after {
            content: "";
            clear: both;
            display: table;
        }

        .container2 img {
            float: left;
            max-width: 60px;
            width: 100%;
            margin-right: 20px;
            border-radius: 50%;
        }

        .container2 img.right {
            float: right;
            margin-left: 20px;
            margin-right:0;
        }

        .time-right {
            float: right;
            color: #aaa;
        }

        .time-left {
            float: left;
            color: #999;
        }

        .btn-send {
            border-radius: 5px;
            width: 100% !important;
            padding: 5px 10px;
        }
       
        .aside .nav-tabs .nav-link{
            padding: 0 !important
        }
        .aside .nav-tabs .nav-link:focus, .nav-tabs .nav-link:hover{
            background-color: transparent !important;
            border: 0 !important
        }
        input{
            max-width: 400px;
        }
        form ::placeholder {
            color: #a39f9f !important
        }

        .table-area {
            position: relative;
            z-index: 0;
            margin-top: 50px;
        }
        
        table.responsive-table {
            display: table;
            /* required for table-layout to be used (not normally necessary; included for completeness) */
            table-layout: fixed;
            /* this keeps your columns with fixed with exactly the right width */
            width: 100%;
            /* table must have width set for fixed layout to work as expected */
            height: 100%;
        }
        
        /* table.responsive-table thead {
            position: fixed;
            top: 50px;
            left: 0;
            right: 0;
            width: 100%;
            height: 50px;
            line-height: 3em;
            background: #eee;
            table-layout: fixed;
            display: table;
        } */
        
        table.responsive-table th {
            background: #eee;
        }
        
        table.responsive-table td {
            line-height: 2em;
        }
        
        table.responsive-table tr>td,
        table.responsive-table th {
            text-align: left;
        }
        
        @media  screen and (max-width: 600px) {
            .table-area table {
                border: 0;
            }
            .table-area table caption {
                font-size: 1.3em;
            }
            .table-area table thead {
                border: none;
                clip: rect(0 0 0 0);
                height: 1px;
                margin: -1px;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 1px;
            }
            .table-area table tr {
                border-bottom: 3px solid #ddd;
                display: block;
                margin-bottom: .625em;
            }
            .table-area table td {
                border-bottom: 1px solid #ddd;
                display: block;
                font-size: .8em;
                text-align: right;
            }
            .table-area table td::before {
                /*
    * aria-label has no advantage, it won't be read inside a table
    content: attr(aria-label);
    */
                content: attr(data-label);
                float: left;
                font-weight: bold;
                text-transform: uppercase;
            }
            .table-area table td:last-child {
                border-bottom: 0;
            }
            .table-areatbody tr td {
                text-align: center !important
            }
            .table-area body {
                padding-top: 0px;
            }
            .table-area {
                margin: 0;
            }
        }
        

        

        /* input[type="radio"]~label::before {
        top: 1px;
        left: 0;
        width: 16px;
        height: 16px;
        border: 1px solid var(--border-color);
        background: var(--white-color);
    } */



    /* input[type="radio"]+label:after {
        width: 0;
    }
    
    input[type="radio"]+label:before {
        right: -19px !important;
        left: auto !important;
    } */
    </style>
<?php $lang= app()->getLocale();
if($lang == 'ar'){
    echo'<style> input[type="radio"]+label:after {
        width: 0;
    }
    
    input[type="radio"]+label:before {
        right: -19px !important;
        left: auto !important;
    }</style>';

    echo'<style> table.responsive-table tr>td,
        table.responsive-table th {
            text-align: right;
        }</style>';

}else{
    echo'<style> input[type="radio"]+label:after {
        width: 0;
    }
    
    input[type="radio"]+label:before {
        left: 0px !important;
        top: 1px !important;
    }</style>';

}
?>


<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>





<div class="main mt-5">
    <div class="container">

        <div class="row profile-content" style="justify-content: space-around">

            <div class="col-12 col-sm-12 d-inline-flex d-sm-inline-flex d-md-none d-lg-none d-xl-none" style="white-space: nowrap; overflow-x: scroll; overflow-y: hidden;">

                <ul class="nav justify-content-center" style="display: contents;">
                 
                    <li class="nav-item text-center">
                        <a  class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true" style="display: grid; color: #495057; padding: 0.5rem 1rem;">
                            <i class="fas fa-user" style="font-size: 30px; /* margin-left: 10px; */ color: #303030;"></i>
                            <?php echo e(__('lang.profile')); ?>

                        </a>
                    </li>
                    <li class="nav-item text-center">
                        <a  class="nav-link" id="addresses-tab" data-toggle="tab" href="#addresses" role="tab" aria-controls="addresses" aria-selected="false" style="display: grid; color: #495057; padding: 0.5rem 1rem;">
                            <i class="fas fa-address-card" style="font-size: 30px; /* margin-left: 20px; */ color: #303030;"></i>
                            <?php echo e(__('lang.addresses')); ?>

                        </a>
                    </li>
                    
                    <li class="nav-item text-center">
                        <a class="nav-link" id="loyality-tab" data-toggle="tab" href="#loyality" role="tab" aria-controls="loyality" aria-selected="false" style="display: grid; color: #495057; padding: 0.5rem 1rem;">
                            <!-- <img src="<?php echo e(asset('images/loyality.png')); ?>" alt="" width="30px" style="margin-left: 10px;"> -->
                            <i class="fas fa-hand-holding-heart" style="color: #303030; font-size: 30px;"></i>
                            <?php echo e(__('lang.loyality')); ?>

                        </a>
                    </li>
                    <li class="nav-item text-center" >
                        <a class="nav-link" id="favorite-tab" data-toggle="tab" href="#favorite" role="tab" aria-controls="favorite" aria-selected="false" style="display: grid; color: #495057; padding: 0.5rem 1rem;">
                            <i class="fa fa-heart" aria-hidden="true" style="font-size: 30px; /* margin-left: 15px; */ color: #303030;"></i>
                            <?php echo e(__('lang.wishlist')); ?>

                        </a>
                    </li>
                    <!-- <li class="nav-item text-center">
                        <a  class="nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="payment" aria-selected="false" style="display: grid; color: #495057; padding: 0.5rem 1rem;">
                            <i class="fas fa-credit-card" style="font-size: 30px; /* margin-left: 45px; */ color: #303030;"></i>
                            <?php echo e(__('lang.payment_methods')); ?>

                        </a>
                    </li> -->
                    <li class="nav-item text-center">
                        <a  class="nav-link" id="order-tab" data-toggle="tab" href="#order" role="tab" aria-controls="order" aria-selected="false" style="display: grid; color: #495057; padding: 0.5rem 1rem;">
                            <i class="fa fa-shopping-basket" aria-hidden="true" style="font-size: 30px; /* margin-left: 7px; */ color: #303030;"></i>
                            <?php echo e(__('lang.orders')); ?>

                        </a>
                    </li>
                    <li class="nav-item text-center">
                        <a  class="nav-link" id="message-tab" data-toggle="tab" href="#message" role="tab" aria-controls="message" aria-selected="false" style="display: grid; color: #495057; padding: 0.5rem 1rem;">
                            <i class="fas fa-comment-alt" style="font-size: 30px; /* margin-left: 20px; */ color: #303030;"></i>
                            <?php echo e(__('lang.messages')); ?>

                        </a>
                    </li>
                    <li class="nav-item text-center">
                        <a  class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false" style="display: grid; color: #495057; padding: 0.5rem 1rem;">
                            <i class="fa fa-life-ring" aria-hidden="true" style="font-size: 30px; /* margin-left: 45px; */ color: #303030;"></i>
                            <?php echo e(__('lang.contact_support')); ?>

                        </a>
                    </li>
                    <li class="nav-item text-center">
                        <form action="<?php echo e(route('web.logout', app()->getLocale())); ?>" method="post" style="display:inline">
                                <?php echo csrf_field(); ?>
                            <input name="id" value="<?php echo e(Auth::guard('customerForWeb')->user()->id); ?>" hidden>
                            <button type="submit" style="background: none;border:0; display: grid; font-size: 17px; text-transform: capitalize; color: #495057;" class="btn qut-btn vs-btn shadow-none"><i class="ti-power-off" style="font-size: 28px; margin-top: 9px;"></i><?php echo e(__('lang.logout')); ?></button>
                        </form>
                    </li>
                    
                </ul>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12 mb-lg-5 mb-md-5 d-none d-sm-none d-md-block d-lg-block d-xl-block">
                <div class="aside p-5">
                    <ul class="nav nav-tabs" id="myTab" role="tablist" style="display: inline-block;">
                        <li class="nav-item" style="display: table-cell; width: 93%;">
                            <!-- <div class="float-left mb-3" style="font-size: 50px;color #838282;padding: 15px;">
                                <?php if(auth()->guard('customerForWeb')->user()->images != null): ?>
                                    <img style="width: 70px;border-radius:50%" src="<?php echo e(asset('/images/customers/'.auth()->guard('customerForWeb')->user()->images)); ?>" alt="">
                                <?php else: ?>
                                     <i class="float-left mb-3 fas fa-user-circle"></i>
                                <?php endif; ?> 
                            </div> -->
                            <div class="float-left mb-3 ml-3 mt-2" style="padding-top: 15px;">
                                <h4><?php echo e(auth()->guard('customerForWeb')->user()->name); ?></h4>
                                <span><?php echo e(auth()->guard('customerForWeb')->user()->email); ?></span>
                            </div>
                        </li>
                        <li class="nav-item ">
                            <a  class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">
                                <i class="fas fa-user"></i>
                                <?php echo e(__('lang.profile')); ?>

                            </a>
                        </li>
                        <li class="nav-item ">
                            <a  class="nav-link" id="addresses-tab" data-toggle="tab" href="#addresses" role="tab" aria-controls="addresses" aria-selected="false">
                                <i class="fas fa-address-card"></i>
                                <?php echo e(__('lang.addresses')); ?>

                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" id="loyality-tab" data-toggle="tab" href="#loyality" role="tab" aria-controls="loyality" aria-selected="false">
                                <img src="<?php echo e(asset('images/loyality.png')); ?>" alt="" width="20px">
                                <?php echo e(__('lang.loyality')); ?>

                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="favorite-tab" data-toggle="tab" href="#favorite" role="tab" aria-controls="favorite" aria-selected="false">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                                <?php echo e(__('lang.wishlist')); ?>

                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a  class="nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="payment" aria-selected="false">
                                <i class="fas fa-credit-card"></i>
                                <?php echo e(__('lang.payment_methods')); ?>

                            </a>
                        </li> -->
                        <li class="nav-item">
                            <a  class="nav-link" id="order-tab" data-toggle="tab" href="#order" role="tab" aria-controls="order" aria-selected="false">
                                <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                                <?php echo e(__('lang.orders')); ?>

                            </a>
                        </li>
                        <li class="nav-item">
                            <a  class="nav-link" id="message-tab" data-toggle="tab" href="#message" role="tab" aria-controls="message" aria-selected="false">
                                <i class="fas fa-comment-alt"></i>
                                <?php echo e(__('lang.messages')); ?>

                            </a>
                        </li>
                        <li>
                            <a  class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                                <i class="fa fa-life-ring" aria-hidden="true"></i>
                                <?php echo e(__('lang.contact_support')); ?>

                            </a>
                        </li>
                        <li style="    padding-left: 29px;">
                            <form action="<?php echo e(route('web.logout', app()->getLocale())); ?>" method="post" style="display:inline">
                                <?php echo csrf_field(); ?>
                                <input name="id" value="<?php echo e(Auth::guard('customerForWeb')->user()->id); ?>" hidden>
                                <button type="submit" style="background: none;border:0; font-size: 17px; text-transform: capitalize; color: #495057;"><i class="ti-power-off" style="font-size: 20px; margin-right: 10px;"></i><?php echo e(__('lang.logout')); ?></button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8 col-md-6 col-sm-12 my-5">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <?php echo $__env->make('website.partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                        <h5><?php echo e(__('lang.personal_info')); ?></h5>
                        <hr>
                        <!-- Default form row -->
                        <form class="w-100" action="<?php echo e(url( app()->getLocale().'/profile/'.auth()->guard('customerForWeb')->user()->id)); ?>" method="post" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('put'); ?>
                            <!-- Grd row -->
                            <div class="form-row w-100" style="margin:0; display: block;">
                                <div class="col-5">
                                    <!-- <div class="form-group">
                                        <div  class="dropzone dz-clickable" >
                                            <div class="dz-default dz-message">
                                            <label><?php echo e(__('lang.user_image')); ?></label>
                                                <br>
                                                <input class="box__file" type="file" name="files" id="file" onchange="showImage(this)" value="<?php echo e(auth()->guard('customerForWeb')->user()->images); ?>" hidden  data-multiple-caption="{count} files selected"/ title="disabled">
                                                <label for="file" style="outline: 1px solid; background: #eee;padding:5px 8px;    cursor: pointer;"><strong ><?php echo e(__('lang.choose_file')); ?></strong></label>

                                            </div>
                                        </div>
                                        <div class="upload-wrap">
        
        
                                        </div>
                                    </div> -->
                                </div>
                                <!-- Grid column -->
                                <div class="form-group col-11 col-lg-8 col-md-10 col-sm-8">
                                    
                                    <input name="name"  type="text" class="form-control" value="<?php echo e(auth()->guard('customerForWeb')->user()->name); ?>">
                                </div>
                                <div class="form-group col-11 col-lg-8 col-md-10 col-sm-8">
                                    
                                    <input name="email" type="text" class="form-control" value="<?php echo e(auth()->guard('customerForWeb')->user()->email); ?>">
                                </div>
                                <div class="form-group col-11 col-lg-8 col-md-10 col-sm-8">
                                    
                                    <input name="phone" type="text" class="form-control" value="<?php echo e(auth()->guard('customerForWeb')->user()->phone); ?>">
                                </div>
                                <div class="form-group col-11 col-lg-8 col-md-10 col-sm-8">
                                    
                                    <input style="color: #ccc" name="password" type="password" class="form-control" value="" placeholder="<?php echo e(__('lang.Enter New Password')); ?>">
                                </div> 
                                <div class="form-group col-11 col-lg-8 col-md-10 col-sm-8">
                                    
                                    <input style="color: #ccc" name="password_confirmation" type="password" class="form-control" value="" placeholder="<?php echo e(__('lang.Enter  Password Confirmation')); ?>">
                                </div>  
                                <div class="form-group  col-5">
                                </div> 
                                
                                
                               
                                
                              
                                <div class="col-11 col-lg-5 col-md-10 col-sm-8">
                                    <button type="submit" class="btn mt-5 mb-5 btn qut-btn vs-btn shadow-none" style="background-color: #414742">  <?php echo e(__('lang.update')); ?></button>
                                </div>
                            </div>


                            <!-- Grd row -->
                        </form>
                        <!-- Default form row -->
                    </div>
                    <div class="tab-pane fade"             id="lang" role="tabpanel" aria-labelledby="lang-tab">
                        <h5><?php echo e(__('lang.languages')); ?></h5>
                        <hr>
                        <div class="row text-center my-5">
                            <div class="col-md-8 col-sm-12  mb-1 text-center" >
                                <ul>
                                    <li style="list-style: none">
                                        <?php $lang= app()->getLocale();?>

                                                <!-- Dropdown -->
                                            <a style="    border: 1px solid #ccc;
                                            float: left;
                                            padding: 0px 63px;" class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-bs-toggle="dropdown" aria-expanded="false">
                                            <?php echo e($lang); ?>

                                            </a>
        
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink1">
                                                <li  style="list-style: none ">
                                                    
                                                        <div>
                                                                <?php $__currentLoopData = config('app.available_locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $locale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php
                                                                    if($locale =='ar')
                                                                    {
                                                                        $local_lang='عربي';
        
                                                                    }
                                                                    else
                                                                        {
                                                                            $local_lang='English';
                                                                        }
                                                                    ?>
                                                                    <?php if(is_null(\Illuminate\Support\Facades\Route::currentRouteName())): ?>
                                                                        <?php if(!is_null(\Request::segment(3)) && \Request::segment(3)=='branch'): ?>
                                                                            <?php $id=\Request::segment(4);?>
                                                                    <a class="nav-link"href="<?php echo e(url($locale.'/homeWeb/branch/'.$id)); ?>"
                                                                    <?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>>
                                                                    <?php echo e(strtoupper($local_lang)); ?>

                                                                    </a>
                                                                    <?php elseif(!is_null(\Request::segment(3)) && \Request::segment(3)=='all_products'): ?>
                                                                    <?php $id=\Request::segment(4);?>
                                                                    <a class="nav-link"
                                                                    href="<?php echo e(url($locale.'/homeWeb/all_products/'.$id)); ?>"
                                                                    <?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
        
                                                                    <?php else: ?>
                                                                    <a class="nav-link"
                                                                    href="<?php echo e(route('homeWeb', $locale)); ?>"
                                                                    <?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
                                                                    <?php endif; ?>
                                                                    <?php elseif(\Illuminate\Support\Facades\Route::currentRouteName()=='homeWeb.show'): ?>
                                                                    <a class="nav-link"
                                                                    <?php $slug=\Request::segment(3);
                                                                    $url='homeWeb/'.$slug?>
                                                                    href="<?php echo e(url($locale.'/homeWeb/'.$slug)); ?>"
                                                                    <?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
                                                                    <?php elseif(\Illuminate\Support\Facades\Route::currentRouteName()=='homeWeb.index'): ?>
                                                                    <a class="nav-link"
                                                                    <?php $slug=\Request::segment(4);
                                                                    ?>
                                                                    href="<?php echo e(url($locale.'/homeWeb/branch/'.$slug)); ?>"
                                                                    <?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
                                                                    <?php elseif(!is_null(\Request::segment(3)) && \Request::segment(3)=='search_product'): ?>
                                                                    <?php $search=\Request::query('search')?>
                                                                    <a class="nav-link"
                                                                    href="<?php echo e(url($locale.'/homeWeb/search_product?search='.$search)); ?>"
                                                                    <?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
                                                                    <?php elseif(!is_null(\Request::segment(2)) && \Request::segment(2)=='address' && \Request::segment(4)=='edit'): ?>
                                                                    <?php $id=\Request::segment(3)?>
                                                                    <a class="nav-link"
                                                                    href="<?php echo e(url($locale.'/address/'.$id.'/edit')); ?>"
                                                                    <?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
        
                                                                    <?php else: ?>
                                                                    <a class="nav-link"
                                                                    href="<?php echo e(route(\Illuminate\Support\Facades\Route::currentRouteName(), $locale)); ?>"
                                                                    <?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
                                                                    <?php endif; ?>
        
        
        
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </div>
                                                </li>
                                            </ul>
                                    </li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                    <div class="tab-pane fade" id="addresses" role="tabpanel" aria-labelledby="addresses-tab">
                            <h5><?php echo e(__('lang.addresses')); ?> <a href="<?php echo e(url(app()->getLocale().'/address')); ?>" class="btn button-5 float-right"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo e(__('lang.add')); ?> </a></h5> 

                            <hr>
                           
                        <div >
                                <?php if(count(auth()->guard('customerForWeb')->user()->address) > 0 ): ?>
                                <div class="row text-center my-5 table-area">
                                    <table class="table-bordered table-hover responsive-table table" style="direction:<?php echo e(app()->getLocale() == 'ar' ?'rtl' : 'ltr'); ?>">
                                        <thead>
                                            <tr >
                                                <th scope="col"><?php echo e(__('lang.name')); ?></th>
                                                <th scope="col"><?php echo e(__('lang.phone')); ?></th>
                                                <th scope="col"><?php echo e(__('lang.address')); ?></th>
                                                <th scope="col"><?php echo e(__('lang.city')); ?></th>
                                                <th scope="col"><?php echo e(__('lang.area')); ?></th>
                                                <th scope="col" colspan="2"><?php echo e(__('lang.action')); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $__currentLoopData = auth()->guard('customerForWeb')->user()->address; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$address): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td  data-label="location name: &nbsp;"><?php echo e($address->name); ?></td>
                                                <td  data-label="phone: &nbsp;"><?php echo e($address->phone); ?></td>
                                                <td  data-label="address: &nbsp;"><?php echo e($address->address); ?></td>
                                                <td  data-label="city: &nbsp;"><?php echo e($address->city); ?></td>
                                                <td  data-label="area: &nbsp;"><?php echo e($address->area); ?></td>
                                                <td  data-label="Edit">
                                                    <a class="success" style="color: rgb(123, 223, 105)" href="<?php echo e(url(app()->getLocale().'/address/'.$address->id.'/edit' )); ?>"><i class="far fa-edit"></i></a>
                                                    
                                                </td>
                                                <td  data-label="Dlete">
                                                    <a class="delete warning" style="color: rgb(189, 55, 55)" href=""  id="<?php echo e($address->id); ?>"><i class=" fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>  
                                </div>
                                <?php else: ?>
                                    <h4 class="text-center" style="color: #8a8686"><?php echo e(__('lang.no_address_found')); ?></h4>
                                <?php endif; ?>
                                                                 
                            
                        </div>
                    </div>
                    <div class="tab-pane fade" id="loyality" role="tabpanel" aria-labelledby="loyality-tab">
                        <h5><?php echo e(__('lang.lyality')); ?> </h5>
                        <hr>
                        <div class="row text-center my-5">
                            <div class="col-md-8 col-sm-12  mb-1 text-center" >
                               <h6 ><?php echo e($loyality ?? ' No Loyality Yet'); ?>    </h6>
                               <p><?php echo e(__('lang.loyalty_text')); ?></p>
                            </div>
                            
                        </div>
                    </div>
                    <div class="tab-pane fade" id="favorite" role="tabpanel" aria-labelledby="favorites-tab">
                            <h5><?php echo e(__('lang.wishlist')); ?></h5>
                            <h5><?php echo e(__('lang.cart_message')); ?></h5>
                            <hr>
                        <div style="">
                            <div class="row text-center">
                                <?php if(isset($pros) && count($pros) > 0): ?>

                                <table class="table-bordered table-hover" style="direction:<?php echo e(app()->getLocale() == 'ar' ?'rtl' : 'ltr'); ?>;text-align:<?php echo e(app()->getLocale() == 'ar' ?'right' : 'left'); ?>">
                                    <thead>
                                        <tr >
                                            <th><?php echo e(__('lang.name')); ?></th>
                                            <th><?php echo e(__('lang.image')); ?></th>
                                            
                                            <th><?php echo e(__('lang.description')); ?></th>
                                            <th><?php echo e(__('lang.Price')); ?></th>
                                            <th colspan="2"><?php echo e(__('lang.action')); ?></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $pros; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        
                                        <tr>
                                            <td><?php echo e($lang=='en' ? $pro->name : $pro->name_ar); ?></td>
                                            <td><img width="40" src="<?php echo e($pro->images[0]->src); ?>" alt=""></td>
                                            
                                            <td><?php echo e($lang=='en' ?  $pro->description: $pro->description_ar); ?></td>
                                            <td><?php echo e($pro->regular_price); ?> <?php echo e(__('lang.EGP')); ?></td>
                                            <td>
                                                <a class=" quickcart" onclick="quickcart(this,event)" title="<?php echo e($pro->id); ?>" class="btn"  ><i class=" fa fa-cart-plus" aria-hidden="true"></i></a>
                                            </td>
                                            <td>
                                                <a class=" warning Unlike" style="color: rgb(189, 55, 55)" title="<?php echo e($pro->id); ?>"><i class=" fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>  
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
                        <h5><?php echo e(__('lang.Payment Methods')); ?></h5>
                        <hr>
                        <div class="row text-center">
                            <div class="single-widget get-button" style="margin: 30px auto">
                                <div class="content">
                                    <div class="pay_method">
                                        <label>
                                            <input type="radio" name="pay" value="credit"disabled >
                                            <img src="<?php echo e(asset('images/card.png')); ?>">
                                        </label>
                                        <label>
                                            <input  id="pay_cash" type="radio" name="pay" value="cash" checked>
                                            <img src="<?php echo e(asset('images/cash.jpeg')); ?>">
                                        </label>
                         
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="tab-pane fade" id="order" role="tabpanel" aria-labelledby="order-tab">
                        <h4 class="pt-2 pl-3 "><?php echo e(__('lang.orders')); ?></h4>
                        <hr>
                        <div class="row text-center"> <h6> <?php echo e(__("lang.current orders")); ?> </h6></div>
                        <div class="row text-center">
                            <section class="Orders mb-5 w-100">
                                <div class="container-fluid">
                                    <div class="row justify-content-start align-items-center">
                                        <div class="col-md-12 mt-1 p-0">

                                                    
                                            <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="col-md-12 mt-3 gray-line1 pt-1" style="color:#fff">
                                                    <h5 class="mb-1" style="display: inline;float: left;"><?php echo e(__('lang.Order')); ?># <?php echo e($order->id); ?></h5>
                                                    <div class="card-header" id="heading2" style="background-color: #fff; padding: 0px; border-bottom: 0px;">
                                                        <button class="btn btn-link text-left button-1" style="display: inline;float: right;color:#fff;text-decoration: underline;padding: 0;background: #666666;" type="button" data-toggle="collapse" data-target="#collapse<?php echo e($order->id); ?>" aria-expanded="true" aria-controls="collapse">
                                                                <?php echo e(__('lang.show details')); ?>

                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                                    $date =explode(" ", $order->created_at);
                                                    $items=json_decode($order->OrderEntry);
                                                    $total_price=0;
                                                    foreach ($items as  $item) {
                                                        $total_price+=(floatval ($item->price) );
                                                    }
                                                ?>
                                                <p class="py-3"><span class="float-left"><?php echo e(__('lang.Order Date')); ?>: </span><span class="float-right"><?php echo e($date[0]); ?> </span></p>
                                                <p class="py-3"><span class="float-left"><?php echo e(__('lang.Order Status')); ?>: </span><span class="float-right"><?php echo e($order->order_status ==''? __('lang.pending') : ''); ?> </span></p>
                                                <div class="accordion-<?php echo e($order->id); ?>" id="accordion-<?php echo e($order->id); ?>">
                                                    <div  style="border: none;">
                                                        <div  id="heading<?php echo e($order->id); ?>" style="background-color: #fff; padding: 0px; border-bottom: 0px;">
                                                            
                                                        </div>
                                                        <div id="collapse<?php echo e($order->id); ?>" class="collapse" aria-labelledby="heading<?php echo e($order->id); ?>" data-parent="#accordion-<?php echo e($order->id); ?>">
                                                            <div class="card-body" style="padding: 5px 0px 0px 0px;">
                                                                <p class="py-3" style="text-align: left"><?php echo e(__('lang.Shipping Address')); ?>: <?php echo e($order->CustomerAddress); ?> </p>
                                                                <span  for="" style="float: left !important;font-weight:bold;margin 5px 0;color:#827E8B"><?php echo e(__('lang.Order Items')); ?> :</span>
                                                                <table style="text-align: left">
                                                                
                                                                <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <tr>
                                                                        <td><?php echo e($item->name); ?></td>
                                                                        <td>x<?php echo e($item->quantity); ?></td>
                                                                        <td><?php echo e(floatval ($item->price)); ?> <?php echo e(__('lang.EGP')); ?> </td>
                                                                    </tr>
                                                                   
                                                                
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </table>
                                                                
                                                                <hr>
                                                            </div>
                                                            <p><?php echo e(__('lang.sub_total')); ?>: <?php echo e($total_price); ?> <?php echo e(__('lang.EGP')); ?></p>
                                                            <p> <?php echo e(__('lang.delivery_fee')); ?>: <?php echo e($order->deliver_fee); ?> <?php echo e(__('lang.EGP')); ?></p>

                                                            <p> <?php echo e(__('lang.total')); ?>: <?php echo e($total_price+$order->deliver_fee); ?> <?php echo e(__('lang.EGP')); ?></p>
                                                       
                                                            <button type="button" id="reorder" onclick="reOrder('<?php echo e($order->id); ?>')"><?php echo e(__('lang.Reorder')); ?></button>

                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                
                                        </div>

                                        
                                    </div>
                                </div>
                            </section>
                        </div>
                        <hr>
                        <div class="row text-center"> <h6> <?php echo e(__('lang.past orders')); ?> </h6></div>
                        <div class="row text-center">
                            <section class="Orders mb-5 w-100">
                                <div class="container-fluid">
                                    <div class="row justify-content-start align-items-center">
                                        <div class="col-md-12 mt-1 p-0">

                                                    
                                            <?php $__currentLoopData = $pastorders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="col-md-12 mt-3 gray-line1 pt-1" style="color:#fff">
                                                    <h5 class="mb-1" style="display: inline;float: left;"><?php echo e(__('lang.order')); ?># <?php echo e($order->id); ?></h5>
                                                    <div class="card-header" id="heading2" style="background-color: #fff; padding: 0px; border-bottom: 0px;">
                                                        <button class="btn btn-link text-left button-1" style="display: inline;float: right;color:#fff;text-decoration: underline;padding: 0;background: #666666;" type="button" data-toggle="collapse" data-target="#collapse<?php echo e($order->id); ?>" aria-expanded="true" aria-controls="collapse">
                                                                <?php echo e(__('lang.show details')); ?>

                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                                    $date =explode(" ", $order->created_at);
                                                    $items=json_decode($order->OrderEntry);
                                                    $total_price=0;
                                                    foreach ($items as  $item) {
                                                        $total_price+=(floatval ($item->price) * floatval ($item->quantity));
                                                    }
                                                ?>
                                                <p class="py-3"><span class="float-left"><?php echo e(__('lang.Order Date')); ?>: </span><span class="float-right"><?php echo e($date[0]); ?> </span></p>
                                                <p class="py-3"><span class="float-left"><?php echo e(__('lang.Order Status')); ?>: </span><span class="float-right"><?php echo e($order->order_status ==''? 'pending' : ''); ?> </span></p>
                                                <div class="accordion-<?php echo e($order->id); ?>" id="accordion-<?php echo e($order->id); ?>">
                                                    <div  style="border: none;">
                                                        <div  id="heading<?php echo e($order->id); ?>" style="background-color: #fff; padding: 0px; border-bottom: 0px;">
                                                            
                                                        </div>
                                                        <div id="collapse<?php echo e($order->id); ?>" class="collapse" aria-labelledby="heading<?php echo e($order->id); ?>" data-parent="#accordion-<?php echo e($order->id); ?>">
                                                            <div class="card-body" style="padding: 5px 0px 0px 0px;">
                                                                <p class="py-3" style="text-align: left"><?php echo e(__("lang.Shipping Address")); ?>: <?php echo e($order->CustomerAddress); ?> </p>
                                                                <span  for="" style="float: left !important;font-weight:bold;margin 5px 0;color:#827E8B"><?php echo e(__('lang.Order Items')); ?> :</span>
                                                                <table style="text-align: left">
                                                                
                                                                <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <tr>
                                                                        <td><?php echo e($item->name); ?></td>
                                                                        <td>x<?php echo e($item->quantity); ?></td>
                                                                        <td><?php echo e(floatval ($item->price) * floatval ($item->quantity)); ?> <?php echo e(__('lang.EGP')); ?></td>
                                                                    </tr>
                                                                    
                                                                
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </table>
                                                                
                                                                <hr>
                                                            </div>
                                                            <?php echo e($total_price); ?> <?php echo e(__('lang.EGP')); ?>


                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            <hr class="ml-3 mr-3">
                                                
                                        </div>

                                        
                                    </div>
                                </div>
                            </section>
                        </div>


                    </div>
                    <div class="tab-pane fade" id="message" role="tabpanel" aria-labelledby="message-tab">
                        <h5><?php echo e(__('lang.Messages')); ?></h5>
                        <hr>
                        <div class="row text-center"><?php echo e(__('lang.Please contact us on 16991 or chat to one of our customer service agents on WhatsApp +201050403443')); ?>.</div>
                        <div class="row text-center" style="display: none;">
                            

                            <div class="col-12">
                                <form>
                                    <div class="container2">
                                        <img src="<?php echo e(asset('/images/customers/'.auth()->guard('customerForWeb')->user()->images)); ?>" alt="Profile picture" style="width:100%;">
                                        <p><?php echo e(__('lang.Hello. How are you today?')); ?></p>
                                        <span class="time-right">11:00</span>
                                    </div>

                                    <div class="container2 darker">
                                        <img src="/w3images/avatar_g2.jpg" alt="Avatar" class="right" style="width:100%;">
                                        <p><?php echo e(__("lang.Hey! I'm fine. Thanks for asking!")); ?></p>
                                        <span class="time-left">11:01</span>
                                    </div>

                                    <div class="container2">
                                        <img src="<?php echo e(asset('/images/customers/'.auth()->guard('customerForWeb')->user()->images)); ?>" alt="Profile picture" style="width:100%;">
                                        <p><?php echo e(__('lang.Sweet! So, what do you wanna do today?')); ?></p>
                                        <span class="time-right">11:02</span>
                                    </div>

                                    <div class="container2 darker">
                                        <img src="/w3images/avatar_g2.jpg" alt="Avatar" class="right" style="width:100%;">
                                        <p><?php echo e(__('lang.Nah, I dunno. Play soccer.. or learn more coding perhaps?')); ?></p>
                                        <span class="time-left">11:05</span>
                                    </div>
                                    <div class="container2">
                                        <div class="input-group mb-3 mt-3">
                                            <input type="text" class="form-control" placeholder="Enter your text" aria-label="text" aria-describedby="button-send2" style="max-width: 100%;">
                                            <div class="input-group-append">
                                                <button class="btn mt-5 mb-5 btn qut-btn vs-btn shadow-none" style="background-color: #414742 !important" type="button" id="button-send2"><?php echo e(__('lang.Send')); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                    <div class=" tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <h5><?php echo e(__('lang.contact_support')); ?>

                        </h5>
                        <hr>
                        <h6 class="my-2"><?php echo e(__('lang.how_can_i_help_you')); ?></h6>
                        <form action="<?php echo e(url(app()->getLocale().'/sendemail')); ?>" method="POST" >
                            <?php echo csrf_field(); ?>
                            <!-- Group of default radios - option 1 -->
                            <div class="custom-control custom-radio1">
                                <input type="text" hidden name="support" value="1">
                                <input type="radio" class="custom-control-input" id="defaultGroupExample1"value="order" name="number">
                                <label class="custom-control-label" for="defaultGroupExample1"><?php echo e(__('lang.I have an issue with my order')); ?></label>
                                <div class="radio1"  style="display:none">
                                    <input class="form-control radio1" type="text" name="order" placeholder="<?php echo e(__('lang.# Order Number')); ?>">
                                    <div class="form-group mt-2 shadow-textarea">
                                        <textarea class="form-control z-depth-1" name="order-message" id="exampleFormControlTextarea6" rows="3" placeholder="<?php echo e(__('lang.Please include a detailed description of your customer support issue, and a epresentative will respond to you as soon as possible.')); ?>"></textarea>
                                    </div>
                                </div>
                            </div>

                            <!-- Group of default radios - option 2 -->
                            <div class="custom-control custom-radio2">
                                <input type="radio" class="custom-control-input" id="defaultGroupExample2" value="driver" name="number" >
                                <label class="custom-control-label" for="defaultGroupExample2"><?php echo e(__('lang.I have a comment about my delivery driver')); ?></label>
                                <div class="radio2" style="display:none">
                                    <input class="form-control " type="text" name="driver"  placeholder="<?php echo e(__('lang.DriverName')); ?>">
                                    <div class="form-group mt-2 shadow-textarea">
                                        <textarea class="form-control z-depth-1" name="driver-message" id="exampleFormControlTextarea6" rows="3" placeholder="<?php echo e(__('lang.Please include a detailed description of your customer support issue, and a representative will respond to you as soon as possible.')); ?>"></textarea>
                                    </div>
                                </div>
                            </div>

                            <!-- Group of default radios - option 3 -->
                            <div class="custom-control custom-radio3">
                                <input type="radio" class="custom-control-input" value="other" id="defaultGroupExample3" name="number">
                                <label class="custom-control-label" for="defaultGroupExample3"><?php echo e(__('lang.Other customer support request')); ?></label>
                                <div class="radio3"  style="display:none">
                                    <div class="form-group mt-2 shadow-textarea">
                                        <textarea class="form-control z-depth-1" name="other-message" id="exampleFormControlTextarea6" rows="3" placeholder="<?php echo e(__('lang.Please include a detailed description of your customer support issue, and a representative will respond to you as soon as possible.')); ?>"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-11 col-lg-5 col-md-10 col-sm-8">
                                <button type="submit" class="btn mt-5 btn qut-btn vs-btn shadow-none" style="background-color: #414742"><?php echo e(__('lang.send')); ?> </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>




<?php $__env->stopSection(); ?>




<?php $__env->startPush('scripts'); ?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo e(asset('/js/customer-profile.js')); ?>"></script>

<script >





function reOrder(id){
    $.get(url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/getProducts/'+id, function(data, status){
       
        localStorage.removeItem('cart');
        var cart;
                if (!localStorage['cart']) cart = [];
                else cart = JSON.parse(localStorage['cart']);            
                if (!(cart instanceof Array)) cart = [];
                
                for(var i=0;i<data.length;i++)
            {
                var obj = {};
            var key =data[i].id;
            obj[key] = data[i];
             cart.push(obj);
            // console.log(cart)
                }
            

               localStorage.setItem('cart', JSON.stringify(cart));
               
               window.location.replace("https://oscarstores.com/en/cart");
               

  });
  
  
}
function removeToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/remove/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}
function addToCartAjax(id,weight,qty)
{
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/addToCart',
	data:{ product_id:id,weight:weight,quantity:parseInt(qty)  },

	success:function(data){
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);
	}	
});
}
function addToCart(product) {
            if (localStorage) {
                var cart;
                if (!localStorage['cart']) cart = [];
                else cart = JSON.parse(localStorage['cart']);            
                if (!(cart instanceof Array)) cart = [];
                var key ='product_'+product.id;
                var obj = {};
                obj[key] = product;
                cart.push(obj);
                localStorage.setItem('cart', JSON.stringify(cart));
            } 
        }

function quickcart(elem,event){

event.preventDefault();
var id=elem.title;
var qty= $(elem).parent().parent().find('.quantity').find('input').val();
var storage=JSON.parse(localStorage.getItem("cart"));



$.ajax({
type:'POST',

url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/add-to-cart',

data:{ id:id },
success:function(data){
    
    data['qty']=qty;
    let weight=250

    if(data.PriceUnit=='kg')
	{
	
		data['weight']=250
	}
	else
	{
		data['weight']=1
	}

  
    var res=jQuery.inArray(id,localStorage);
    var key='product_'+data.id;
    var storage=localStorage['cart'];
    let check=false;
    for(var i=0;i<JSON.parse(storage).length;i++)
    {
        let item=JSON.parse(storage)[i]
        console.log(key,Object.keys(item)[0],Object.keys(item)[0]==key)
        if(Object.keys(item)[0]==key)
        {
            check=true
        }
        
        
    }
  
        
        addToCart(data);
        $('#alert-cart').show();
        $('#alert-cart').html(<?php echo json_encode( __('lang.add_to_cart'), 15, 512) ?>);
        setTimeout(function() { 
                $('#alert-cart').fadeOut('fast'); 
        }, 10000);
        $('.total-count').html('');
        var total=JSON.parse(localStorage.getItem("cart"));
        $('.total-count').html(total.length);

    
    addToCartAjax(id,weight,1)

// $(elem).parent().parent().find('.Unlike').click();

}
});
var modal=$(elem).closest(".modal").modal("hide");
}

 function removeDiv(elem){
            $(elem).parent('div').remove();
            $('#file').val('');
        }


function showImage(file){


for (var i = 0; i < file.files['length']; i++) {

var img='<div class="upload-image float-left" id="upload-image" style="display:inline-block">'+
        "<img id='img_src' width=50 src="+"'" +window.URL.createObjectURL(file.files[i])+"'"+">"+
        '<a onClick="removeDiv(this)" href="javascript:void(0);" class="btn  btn-danger btn-sm" style="padding:3px"><i class="far fa-trash-alt"></i></a>'+
    '</div>';

$(".upload-wrap").append(img);

$('.upload-image').css('display','block');
};
};

$.ajaxSetup({

headers: {

    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

}

});

$(".delete").click(function(e){
    var select = $(this);
    var id =select.attr('id');
    e.preventDefault();
    
    '/<?php echo e(app()->getLocale()); ?>/address/'

    $.ajax({
    type:'DELETE',
    url:url+'/<?php echo e(app()->getLocale()); ?>/address/'+id,
  

    success:function(data){
           
        select.closest('tr').remove();

     
        
        }   
    });


});



$(".Unlike").click(function(e){
            e.preventDefault();
            
    
            var id = $(this).attr("title");
            $.ajax({
               type:'POST',
               url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/unLike',
    
               data:{ id:id },

               success:function(data){
                removeToWhishList(id)
                //    console.log(data);

            }
    
            });
            
            $(this).closest('tr').remove();
            var count=$('.wishlist-total-count').html();
            $('.wishlist-total-count').html(Number(count)-Number(1));
        });
    
var hash = document.location.hash;
if (hash =='#favorite') {

    $('#favorite-tab').tab('show');
}




</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>