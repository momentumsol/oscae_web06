<style>

.vs-product-box1 .actions-btn{
    right: auto !important;
    position: absolute !important;
}

h4.product-title.h5.mb-0{
	padding-top: 20px !important;
}

@media  only screen and (max-width:500px){
    .vs-product-box1 .actions-btn{
          transform: translateX(-50%) scale(.8) !important;
    }   
}



.department.product-area  .image{
    z-index: -1;
    position: absolute;
    left: 0;
    top: -100px;
}
.department .content .content-overlay {
/* background: rgba(0,0,0,0.7);
position: absolute;*/
/* height: 100%;  */
width: 92%;
/* left: 15px; */
/* top: 0; */
bottom: 0;
/* right: 0; */
opacity: 1;
-webkit-transition: all 0.4s ease-in-out 0s;
-moz-transition: all 0.4s ease-in-out 0s;
transition: all 0.4s ease-in-out 0s;
}

/* .content:hover .content-overlay{
opacity: 1;
} */

.department .content-image{
min-width: 100%;
}

/* .department .content-details {
position: absolute;
text-align: center;
padding-left: 1em;
padding-right: 1em;
width: 100%;
top: 50%;
left: 50%;
opacity: 0;
-webkit-transform: translate(-50%, -50%);
-moz-transform: translate(-50%, -50%);
transform: translate(-50%, -50%);
-webkit-transition: all 0.3s ease-in-out 0s;
-moz-transition: all 0.3s ease-in-out 0s;
transition: all 0.3s ease-in-out 0s;
} */


.department .content-details h3{
color: rgb(204, 19, 19);
font-weight: 500;
letter-spacing: 0.15em;
margin-bottom: 0.5em;
text-transform: uppercase;
}

.department .content-details p{
color: #fff;
font-size: 0.8em;
}

.department .fadeIn-bottom{
top: 80%;
}


/* .fade.show {
margin-top: 0px !important;
} */



.department .fadeIn-right{
left: 80%;
}

.pagination {
	justify-content: center !important
}
.pagination li{
	color: var(--body-color) !important;
}
.product-area img:not([draggable]), embed, object, video {
	/* max-height: 200px; */
    background: white;
	background-repeat: no-repeat;
	background-size: contain;
}
/* .Wishlist i{
	color: #858585;
} */
.sub-cat:hover .image {
  opacity: 0.3;
}
.sub-cat:hover .middle {
  opacity: 1;
}

.text {
	background:linear-gradient(to bottom, rgba(245, 246, 252, 0.52), rgba(219, 212, 217, 0.73));
	color: #000 !important;
	font-size: 16px;
	padding: 16px 32px;
}
.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.links ul li .page-link{
	color:#19408C !important;
}
.page-item.active .page-link{
	background:#19408C !important;
	color:#fff !important
}
.product-area img:not([draggable]), embed, object, video{
background: white;

	max-height:274px !important;
	background-size:cover;
}
.product-title{
    white-space: nowrap !important;
    overflow: hidden !important;
    text-overflow: ellipsis !important;
    display: inline-block !important;
    width: 100%;
}
.widget_nav_menu .widget-title a{
	padding: 5px !important;
}
/* .sticky{
	top:18px !important;
} */

@media  screen and ( max-width: 520px ){

li.page-item {

	display: none;
}

.page-item:first-child,
.page-item:nth-last-child(2),
/* .page-item:nth-first-child(2), */
.page-item:last-child,
.page-item.active {

	display: block;
}
}
.vs-product-box1 .actions-btn{
	right: auto !important;
	position: absolute;
}

</style>
<?php $__env->startSection('content'); ?>

<?php $lang= app()->getLocale();?>


<div class="product-area section">
	<?php if(isset($category)): ?>
		<!--==============================
			Breadcumb
		============================== -->
		<div class="breadcumb-wrapper breadcumb-layout1 bg-fluid pt-100 pb-100" style="  background:linear-gradient(to bottom, rgba(245, 246, 252, 0.52), rgba(219, 212, 217, 0.73)),url('<?php echo e($category->image['src']); ?>')" >
			<div class="container">
				<div class="breadcumb-content text-center">
		
					<h1 class="breadcumb-title"><?php echo e($category->name->$lang); ?></h1>
			
					
				</div>
			</div>
		</div>

		
		<div class="row my-5"  style="margin-top: 200px;display: flex !important;justify-content: center !important;"><!--cats section-->
			<div class="department ">
				<div class="container" >
					<div class="">
						<div class="row">
							<?php $__currentLoopData = $category->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

							<div class="col-md-4 col-sm-6 col-xs-12 mb-2 sub-cat"   style="padding-right: 10px;position: relative;">
								<a class="content w-100" href="<?php echo e(url($lang.'/homeWeb/all_products/'. $cat->id )); ?>" >
								
					
									
									<div class="content-details w-100  text-center " style="width: 90% !important">
										<h3 class="text"><?php echo e($cat->name->$lang); ?></h3>
									</div>
								</a>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					
						</div>
					
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<section class="vs-shop-wrapper position-relative  space-md-bottom mt-5" id="products-content">
        <div class="container">
			<?php if(isset($res) && count($res) >0): ?>
				<div class="row">
					<?php $__currentLoopData = $res; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="col-sm-6 col-xl-3 col-6">
							<div class="vs-product-box1 thumb_swap">
								<div class="product-tag1" style="display: <?php echo e($product->on_sale == 0 ?'none' : 'block'); ?>">sale</div>
								<?php if(isset($category)): ?>
								<a href="<?php echo e(url(app()->getLocale().'/show_product/'.$product->id.'/'.$category->id)); ?>">	

								<?php else: ?>
										<a href="<?php echo e(url(app()->getLocale().'/show_product/'.$product->id.'/'.$product->category_id)); ?>">	
											<?php endif; ?>	
									<div class="" style="background-image: url(<?php echo e(asset('images/not_found_image.jpeg')); ?>); background-size: contain;background-repeat: no-repeat;background-position: center;" >
										
									<img  style="background: white;" src="<?php echo e($product->images[0]->src); ?>"  onerror="this.src='https://oscarstores.com/images/not_found_image.jpeg'">
								</div>
								</a>
								<div class="product-content">
									<div class="actions-btn">
										<?php if(Auth::guard('customerForWeb')->user()): ?>
										<?php if(isset($category)): ?>
										<a href="<?php echo e($product->PriceUnit == "kg" ? url(app()->getLocale().'/show_product/'.$product->id.'/'.$category->id) : ''); ?>" title="<?php echo e($product->id); ?>" class="<?php echo e($product->in_stock != 1 ? 'd-none' : ''); ?>  <?php echo e($product->PriceUnit != "kg" ? 'cart' : ''); ?>"><i class="fal fa-cart-plus"></i></a>
										<?php else: ?>
										<a href="<?php echo e($product->PriceUnit == "kg" ? url(app()->getLocale().'/show_product/'.$product->id.'/'.$product->category_id) : ''); ?>" title="<?php echo e($product->id); ?>" class="<?php echo e($product->in_stock != 1 ? 'd-none' : ''); ?>  <?php echo e($product->PriceUnit != "kg" ? 'cart' : ''); ?>"><i class="fal fa-cart-plus"></i></a>

										<?php endif; ?>
										<?php if($product->in_stock != 0 && Auth::guard('customerForWeb')->check()): ?>
										
										<a href="#" title="<?php echo e($product->id); ?>" class="Unlike" style="display: <?php echo e(in_array($product->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'block' : 'none'); ?>"><i style="color:#C53330;" class="fal fa-heart"></i></a>
										<a href="#" title="<?php echo e($product->id); ?>" class="Wishlist" style="display: <?php echo e(in_array($product->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'none' : 'block'); ?>"><i  class="fal fa-heart"></i></a>
										<?php endif; ?>
										<?php endif; ?>
										
									<?php if($product->in_stock != 1 && Auth::guard('customerForWeb')->check()): ?>

<a href="#" title="<?php echo e($product->id); ?>" class="Unlike" style="display: <?php echo e(in_array($product->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'block' : 'none'); ?>"><i style="color:#C53330;" class="fal fa-heart"></i></a>
<a href="#" title="<?php echo e($product->id); ?>" class="Wishlist" style="display: <?php echo e(in_array($product->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'none' : 'block'); ?>"><i  class="fal fa-heart"></i></a>

										<h6 style="color:#bd0505;    margin-top: 11%;" class="product-title mb-0"><?php echo e(__('lang.out_of_stock')); ?></h6>
										<?php endif; ?>
</div>
                                  <?php if(isset($category)): ?>

									<h4 class="product-title h5 mb-0"><a href="<?php echo e(url(app()->getLocale().'/show_product/'.$product->id.'/'.$category->id)); ?>"><?php echo e($product->name); ?></a></h4>
									<?php else: ?>
									<h4 class="product-title h5 mb-0"><a href="<?php echo e(url(app()->getLocale().'/show_product/'.$product->id.'/'.$product->category_id)); ?>"><?php echo e($product->name); ?></a></h4>

									<?php endif; ?>
									<span class="price font-theme"><strong><span style="text-decoration: <?php echo e($product->on_sale == '1' ?  'line-through' : ' '); ?> "><?php echo e($product->regular_price); ?> <?php echo e(__('lang.EGP')); ?></span>   <?php echo e($product->on_sale == '1' ? ' '.$product->discountprice.__('lang.EGP') :''); ?> </strong> </span>
									
								</div>
							</div>
						</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<div class=" links"  style="justify-content:center;" >
						<?php echo e($res->appends(request()->query())->links()); ?>

					</div>
				</div>
			<?php else: ?>
			<div class="warning">
				<h1 class="text-center"><?php echo e(__('lang.No results found')); ?></h1>
			</div>
			
			<?php endif; ?>
		</div>
	</section>
						
</div>
	
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">

	$('.pagination .active span').addClass('vs-btn')	
	$('.pagination .active span').css({
		'padding' : '.375rem .75rem',
		'border-radius' : '0px',
		'box-shadow':'none'
	})
		

		// let url='';


		$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		}

		});
function addToCartAjax(id,weight,qty)
{
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/addToCart',
	data:{ product_id:id,weight:weight,quantity:parseInt(qty)  },

	success:function(data){
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);

	}	
});
}
function addToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/add/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}
function removeToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/remove/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}	
		
	$(".Wishlist").click(function(e){
			e.preventDefault();
			
			
	
			var id = $(this).attr("title");
			var wishlistLink =this;
			$.ajax({
			   type:'POST',
			   url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/wishList',
				
			   data:{ id:id },
	
			   success:function(data){

			
					$('#alert-wishlist').show();
					$('#alert-wishlist').html(<?php echo json_encode(__('lang.added to wishlist succssfully'), 15, 512) ?>);
					addToWhishList(id)
					setTimeout(function() { 
							$('#alert-wishlist').fadeOut('fast'); 
						}, 2000);
	
				}	
			});
			// if(! $(this).hasClass('quick')){

				$(this).parent().find('.Wishlist').css('display','none');
				$(this).parent().find('.Unlike').css('display','block');

				

			// }else{
				// $(this).removeClass('Wishlist');
				// $(this).addClass('Wishlist');
			// }

			

			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)+Number(1));

		});

		$(".Unlike").click(function(e){
			e.preventDefault();



			
			$('#alert-wishlist').show();
			$('#alert-wishlist').html(<?php echo json_encode(__('lang.removed from wishlist succssfully'), 15, 512) ?>);
			
			setTimeout(function() { 
                    $('#alert-wishlist').fadeOut('fast'); 
                }, 2000);
	
			var id = $(this).attr("title");
	
			$.ajax({
			   type:'POST',
			   url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/unLike',
	
			   data:{ id:id },

			   success:function(data){
				removeToWhishList(id)
				//    console.log(data);
				// $('li#'+data).remove();
			}
	
			});
			$(this).parent().find('.Wishlist').css('display','block');
			$(this).parent().find('.Unlike').css('display','none');
			// $(this).parent().find('.Unlike i').css('color','#858585');

			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)-Number(1));

			// $(this).removeClass('Unlike');
			// $(this).addClass('Wishlist');
		});

		var products= [];
		var local_storage=[];
		
		if( !JSON.parse(localStorage.getItem("cart"))){
			var cart =[];
			localStorage.setItem('cart', JSON.stringify(cart));
		}
		$(".cart").click(function(e){
			
			e.preventDefault();

			var id = $(this).attr("title");
			var unit=$(this).attr('unit');
			$.ajax({
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type:'POST',
			
			url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/add-to-cart',

			data:{ id:id },
			success:function(data){
				let weight=250;
					if(unit == 'kg'){
						let weight_id=id+'_weights'
					 weight=document.getElementById(weight_id).value
					data['weight']=weight;
						data['quantity']=1;
					}else{
					data['weight']=1;
					data['quantity']=1;
					}
			
					var res=jQuery.inArray(id,localStorage);
					var key='product_'+data.id;
					var storage=localStorage['cart'];
					let qty =1;
				
					if(storage.includes(key)){

						let storage_keys=JSON.parse(storage)
						for(var i in storage_keys){
							let item=storage_keys[i]
							let key_check=Object.keys(storage_keys[i])[0]
		if (Object.keys(storage_keys[i])[0]== 'product_'+id) {
			 qty=storage_keys[i][key_check]['quantity'];
			qty=qty+1
			item[Object.keys(item)[0]]['quantity']=qty
		
			localStorage.setItem('cart', JSON.stringify(storage_keys));

			// storage.splice(i, 1); 
	
		}
		}
	
						var in_Cart=	<?php echo json_encode( __('lang.updated to cart succssfully'), 15, 512) ?>;
				var lang=<?php echo json_encode(app()->getLocale(), 15, 512) ?>;
				var name=data.name;
				
				if(lang =='ar'){
					name=data.name_ar;
				}
				$('#alert-cart').html(name+' '+in_Cart);
						setTimeout(function() { 
								$('#alert-cart').fadeOut('fast'); 
						}, 2000);
					}else{
						addToCart(data);
						$('#alert-cart').show();
						$('#alert-cart').html(<?php echo json_encode( __('lang.added to cart succssfully'), 15, 512) ?>);
						setTimeout(function() { 
								$('#alert-cart').fadeOut('fast'); 
						}, 2000);
						$('.total-count').html('');
						var total=JSON.parse(localStorage.getItem("cart"));
						$('.total-count').html(JSON.parse(total.length));
					}
		addToCartAjax(id,weight,1)
				}	
			});
			
		});



		function addToCart(product) {
			if (localStorage) {
				var cart;
				if (!localStorage['cart']) cart = [];
				else cart = JSON.parse(localStorage['cart']);            
				if (!(cart instanceof Array)) cart = [];
				var key ='product_'+product.id;
				var obj = {};
				obj[key] = product;
				cart.push(obj);
				localStorage.setItem('cart', JSON.stringify(cart));
			} 
		}

	$(".add").click(function(){


		var product_id=$(this).attr('id');
		var localStorage_key='product_'+product_id;
		var qty=$(this).parent().parent().find("input").val();
		var total=0;
		var total_price=parseInt($(this).parent().parent().parent().parent().find("#total_price").html());

	
		
		
	});

	function quickcart(elem,event){

		event.preventDefault();
		var id=elem.title;
		var qty= $(elem).parent().parent().find('.quantity').find('input').val();
		var storage=JSON.parse(localStorage.getItem("cart"));

		for (let i = 0; i< storage.length; i++) {
			var pro=storage[i];
			var key=Object.keys(pro);
			if(key== 'product_'+id){
				storage.splice(i, 1); 
				var cart =[];
				localStorage.setItem('cart', JSON.stringify(cart));
				localStorage.setItem('cart', JSON.stringify(storage));
				localStorage.removeItem(key);
			}
		}
		$.ajax({
		type:'POST',

		url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/add-to-cart',

		data:{ id:id },
		success:function(data){

			data['quantity']=qty;
	if(data.PriceUnit=='kg')
	{
		let weights_id=id+'_weights'
		data['weight']=document.getElementById(weights_id).value
	}
	else
	{
		data['weight']=1
	}
			var res=jQuery.inArray(id,localStorage);
			var key='product_'+data.id;
			var storage=localStorage['cart'];
			if(storage.includes(key)){
				$('#alert-cart').show();
				$('#alert-cart').html(data.name+' already in cart ');
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
			}else{
		
				addToCart(data);
				$('#alert-cart').show();
				$('#alert-cart').html(<?php echo json_encode( __('lang.added to cart succssfully'), 15, 512) ?>);
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
				$('.total-count').html('');
				var total=JSON.parse(localStorage.getItem("cart"));
				$('.total-count').html(total.length);

			}
		}
	});
	// $(elem).parent().parent().find('.quantity').find('input').val(1)
	var modal=$(elem).closest(".modal").modal("hide");

	}

</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>