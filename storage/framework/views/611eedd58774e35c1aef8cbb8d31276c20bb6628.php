<style>
  .menu-item-has-children i{
    margin: 0 8px 0 0;

  }
.sub-cat:hover .image {
  opacity: 0.3;
}
.sub-cat:hover .middle {
  opacity: 1;
}

.text {
	background:linear-gradient(to bottom, rgba(245, 246, 252, 0.52), rgba(219, 212, 217, 0.73));
	color: #000 !important;
	font-size: 16px;
	padding: 16px 32px;
}
.middle {
  transition: .5s ease;
  /* opacity: 0; */
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
</style>
<?php $__env->startSection('content'); ?>
<?php $lang= app()->getLocale();?>
    <div class="department my-5">
        <div class="container pl-0 pr-0">
            <div class=" my-5">
                <div class="row" style="text-align: center">
                    <?php if(isset($main_categories)): ?>
                      <?php $__currentLoopData = $main_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        

                        <div class="col-md-4 col-sm-6 col-xs-12 mb-2 sub-cat"   style="padding-right: 10px;position: relative;">
                          <a class="content" href="<?php echo e(url($lang.'/homeWeb/all_products/'. $cat->id )); ?>" target="_blank">
                          
                    
                            <img class="content-image" style="background-image: url(http://34.105.27.34/html/oscar_web/public/images/image.png); height: 100px; background-size: 103%; background-repeat: no-repeat; background-position: center;" src="<?php echo e($cat->image['src']); ?>">
                            <div class="content-details w-100  middle ">
                              <h3 class="text"><?php echo e($cat->name->$lang); ?></h3>
                            </div>
                          </a>
                        </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 mb-2 sub-cat">
                      <a class="content" href="<?php echo e(url('homeWeb/all_products/'. $cat->id )); ?>" >
                          <div class="content-overlay"> <h3 style="color:#fff; margin-top:15px; font-weight: 500;  letter-spacing: 0.15em;margin-bottom: 0.5em;text-transform: uppercase;"><?php echo e($category->name->$lang); ?></h3></div>

                          <img class="content-image" style="background-image: url(http://34.105.27.34/html/oscar_web/public/images/image.png); height: 100px; background-size: 103%; background-repeat: no-repeat; background-position: center;" src="<?php echo e($cat->image['src']); ?>">
                          <div class="content-details w-100  middle ">
                            <h3 class="text"><?php echo e($cat->name->$lang); ?></h3>
                          </div>
                      </a>
                  </div>
                    <?php endif; ?>
                </div>
               
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
	<script type="text/javascript">


	</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>