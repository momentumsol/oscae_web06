<style>
	
</style>
<?php $__env->startSection('content'); ?>
<?php 
$lang= app()->getLocale();
$category=\App\Models\Category::find($catId);
?>


    <!--==============================
    Breadcumb
============================== -->
<div class="breadcumb-wrapper breadcumb-layout1 bg-fluid pt-200 pb-200" style="  background:linear-gradient(to bottom, rgba(245, 246, 252, 0.52), rgba(219, 212, 217, 0.73)),url('<?php echo e($category->image['src']); ?>')">
	<div class="container">
		<div class="breadcumb-content text-center">
			<h1 class="breadcumb-title"><?php echo e($category->name->$lang); ?></h1>
			
		</div>
	</div>
	
</div>

 <!--==============================
    Shop Details testww
    ==============================-->
    <section class="vs-shop-wrapper shop-details space-top space-md-bottom">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-6 col-xl-5 mb-30 mb-md-0">
                    <div class="product-big-img" >
					<img class="" src="<?php echo e($product->images[0]->src); ?>"   style="background: white;"  onerror="this.src='https://oscarstores.com/images/not_found_image.jpeg'">

                    </div>

                </div>
                <div class="col-md-6 col-lg-4" style=" display: flex;align-items: center;">
                    <div class="product-content">
                        <h3 class="product-title mb-1"><?php echo e($lang == 'en' ? $product->name : $product->name_ar); ?></h3>
						<?php if($product->PriceUnit=='kg'): ?>
						<span class="price font-theme"><strong style="font-size:26px;"><?php echo e($product->regular_price); ?> <?php echo e(__('lang.EGP')); ?>/ <?php echo e(__('lang.kg')); ?></strong></span>

						<?php else: ?>
                        <span class="price font-theme"><strong style="font-size:26px;"><?php echo e($product->regular_price); ?> <?php echo e(__('lang.EGP')); ?></strong></span>
						<?php endif; ?>
                        <div class="mt-2">

                        </div>
                        <p class="fs-xs my-4"><?php echo e($product->description); ?></p>
                        <div class="mt-2 link-inherit fs-xs" style="height: 40px">
                            <p>
								<strong class="text-title me-3 font-theme fl-left"><?php echo e(__('lang.Availability')); ?> :</strong>
								<span  class="fl-right" style="line-height: 1"><?php echo $product->in_stock == 1 ? '<i class="far fa-check-square me-2" style="cursor: auto !important;"></i>'.__('lang.In Stock') : '<i class="icon-check-empty" style="cursor: auto !important;"></i>'.__('lang.Out Stock'); ?></span></p>
                        </div>
                        <div class="actions mb-4 pb-2" >
                            <div class="quantity style2" >
                                <input type="number" class="qty-input" id="qty'"value="1" min="1" max="99">
                                <button class="quantity-minus qut-btn remove"><i class="far fa-chevron-down"></i></button>
                                <button class="quantity-plus qut-btn add"><i class="far fa-chevron-up"></i></button>

                            </div>
                            <?php if($product->PriceUnit=='kg' && $product->in_stock == 1): ?>
							<div>
								<select name="weight" id="weights">
                                    <option value="250" selected>250<?php echo e(__('lang.g')); ?></option>
                                    <option value="500">500<?php echo e(__('lang.g')); ?></option>
                                    <option value="750">750<?php echo e(__('lang.g')); ?></option>
                                    <option value="1000">1.00<?php echo e(__('lang.kg')); ?></option>
                                    <option value="1250">1.25<?php echo e(__('lang.kg')); ?></option>
                                    <option value="1500">1.50<?php echo e(__('lang.kg')); ?></option>
                                    <option value="1750">1.75<?php echo e(__('lang.kg')); ?></option>
                                    <option value="2000">2.00<?php echo e(__('lang.kg')); ?></option>
                                    <option value="2250">2.25<?php echo e(__('lang.kg')); ?></option>
                                    <option value="2500">2.50<?php echo e(__('lang.kg')); ?></option>
								</select>
							</div>
							<?php endif; ?>
                           
                        </div>
						<!-- <h1><?php echo e(in_array($product->id,$pros)); ?></h1> -->
                        <?php if(Auth::guard('customerForWeb')->user()): ?>
						<?php if( $product->in_stock==1): ?>
						<a style="cursor: pointer;" unit=<?php echo e($product->PriceUnit); ?>  title="<?php echo e($product->id); ?> "  class="cart mb-2 vs-btn shadow-none <?php echo e($product->in_stock != 1 ? 'd-none' : ''); ?>"><?php echo e(__('lang.Add To Cart')); ?></a>
<?php else: ?>
<a style="cursor: pointer;"  class="mb-2 vs-btn shadow-none "><?php echo e(__('lang.out_of_stock')); ?></a>

<?php endif; ?>
                        <a  unit=<?php echo e($product->PriceUnit); ?>  title="<?php echo e($product->id); ?> " style="cursor: pointer;display: <?php echo e(in_array($product->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'none' : 'block'); ?>"  class="Wishlist  vs-btn shadow-none <?php echo e($product->in_stock != 1 ? 'd-none' : ''); ?>"><?php echo e(__('lang.Add To WishList')); ?></a>
                        <a  unit=<?php echo e($product->PriceUnit); ?>  title="<?php echo e($product->id); ?> " style="cursor: pointer;display: <?php echo e(in_array($product->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'block' : 'none'); ?>"  class=" Unlike  vs-btn shadow-none <?php echo e($product->in_stock != 1 ? 'd-none' : ''); ?>"><?php echo e(__('lang.Remove From WishList')); ?></a>
                        <?php endif; ?>
                        <div class="product_meta mt-2">
                            
                            <span class="posted_in"><?php echo e(__('lang.Category')); ?>: <a style="cursor: pointer;"  href="<?php echo e(url(app()->getLocale().'/homeWeb/all_products/'. $category->id )); ?>" rel="tag"><?php echo e($category->name->$lang); ?></a> </span>
                        </div>
                    </div>
                </div>

            </div>
            
        </div>
    </section>


<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
	<script type="text/javascript">
	let id=''
	function addToCartAjax(id,weight,qty)
{
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/addToCart',
	data:{ product_id:id,weight:weight,quantity:parseInt(qty) },

	success:function(data)
	{
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);

	}	
});
}
function addToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/add/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}
function removeToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/remove/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}	
$(".cart").click(function(e){
	

	e.preventDefault();
	var unit=$(this).attr('unit');

	 id = $(this).attr("title");
	id=id.replace(/\s+/g, '')
	$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		}

	});
	$.ajax({

	type:'POST',
	
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/add-to-cart',

	data:{ id:id },
	success:function(data){
		let weight=250;
			if(unit == 'kg'){
				 weight=document.getElementById('weights').value
				data['weight']=weight;
				data['quantity']=parseInt(document.getElementsByClassName('qty-input')[0].value);
			}else{
				data['weight']=1;
				data['quantity']=parseInt(document.getElementsByClassName('qty-input')[0].value);
			}
			var res=jQuery.inArray(id,localStorage);
			var key='product_'+data.id;
			var storage=localStorage['cart'];
        let existed_qty=0
	
		
			if(storage.includes(key)){
		let storage_keys=JSON.parse(storage)
		for(var i in storage_keys){
		if (typeof(storage_keys[i][key]) !== "undefined") {
			 existed_qty=storage_keys[i][key]['quantity']
			
	
		}
		}
			}
		
	// console.log(existed_qty, parseInt(document.getElementsByClassName('qty-input')[0].value),'l',id+'_qty')
			if(storage.includes(key)  ){
				if(existed_qty==parseInt(document.getElementsByClassName('qty-input')[0].value))
				{

				
				$('#alert-cart').show();
				var in_Cart=	<?php echo json_encode( __('lang.already in cart'), 15, 512) ?>;
				var lang=<?php echo json_encode(app()->getLocale(), 15, 512) ?>;
				var name=data.name;
				
				if(lang =='ar'){
					name=data.name_ar;
				}
				$('#alert-cart').html(name+' '+in_Cart);

				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
			}
			else
			{
				
		var qty=parseInt(document.getElementsByClassName('qty-input')[0].value);;
		var storage=JSON.parse(localStorage.getItem("cart"));

		for (let i = 0; i< storage.length; i++) {
			var pro=storage[i];
			var key=Object.keys(pro);
			if (typeof(storage[i][key]) !== "undefined") {
				storage.splice(i, 1); 
				var cart =[];
				console.log(storage)
				localStorage.setItem('cart', JSON.stringify(cart));
				localStorage.setItem('cart', JSON.stringify(storage));
				localStorage.removeItem(key);
				addToCart(data)

			}

			
		}
	

				var add_Cart=	<?php echo json_encode( __('lang.updated to cart succssfully'), 15, 512) ?>;

$('#alert-cart').show();
$('#alert-cart').html(add_Cart);
setTimeout(function() { 
		$('#alert-cart').fadeOut('fast'); 
}, 2000);
;
			}
			}else{
				var add_Cart=	<?php echo json_encode( __('lang.added to cart succssfully'), 15, 512) ?>;
				qty=parseInt(document.getElementsByClassName('qty-input')[0].value);
				addToCart(data);
				$('#alert-cart').show();
				$('#alert-cart').html(add_Cart);
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
				$('.total-count').html('');
				var total=JSON.parse(localStorage.getItem("cart"));
				$('.total-count').html(JSON.parse(total.length));
				
			}
		
		addToCartAjax(id,weight,qty)	
		
		}
		
	});
	
});

// let url='';


		$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		}

		});
		// if(! $(this).hasClass('quick')){

	$(".Wishlist").click(function(e){
			e.preventDefault();
			
			
	
			var id = $(this).attr("title");
			var wishlistLink =this;
			$.ajax({
			   type:'POST',
			   url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/wishList',
				
			   data:{ id:id },
	
			   success:function(data){

				
					$('#alert-wishlist').show();
					$('#alert-wishlist').html(<?php echo json_encode(__('lang.added to wishlist succssfully'), 15, 512) ?>);
					addToWhishList(id)
					setTimeout(function() { 
							$('#alert-wishlist').fadeOut('fast'); 
						}, 2000);
	
				}	
			});
			// if(! $(this).hasClass('quick')){

				$(this).parent().find('.Wishlist').css('display','none');
				$(this).parent().find('.Unlike').css('display','block');

				

			// }else{
				// $(this).removeClass('Wishlist');
				// $(this).addClass('Wishlist');
			// }

			

			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)+Number(1));

		});

		$(".Unlike").click(function(e){
			e.preventDefault();



			
			$('#alert-wishlist').show();
			$('#alert-wishlist').html(<?php echo json_encode(__('lang.removed from wishlist succssfully'), 15, 512) ?>);
			
			setTimeout(function() { 
                    $('#alert-wishlist').fadeOut('fast'); 
                }, 2000);
	
			var id = $(this).attr("title");
	
			$.ajax({
			   type:'POST',
			   url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/unLike',
	
			   data:{ id:id },

			   success:function(data){
				removeToWhishList(id)
			
			}
	
			});
			$(this).parent().find('.Wishlist').css('display','block');
			$(this).parent().find('.Unlike').css('display','none');
			// $(this).parent().find('.Unlike i').css('color','#858585');

			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)-Number(1));

			// $(this).removeClass('Unlike');
			// $(this).addClass('Wishlist');
		});

		var products= [];
		var local_storage=[];
		
		if( !JSON.parse(localStorage.getItem("cart"))){
			var cart =[];
			localStorage.setItem('cart', JSON.stringify(cart));
		}
	



// $('.add').each(function () {
//     $(this).on('click', function (e) {
//       e.preventDefault();
//       var $qty = $(this).siblings(".qty-input");
	  
//       var currentVal = parseInt($qty.val());
	
//       if (!isNaN(currentVal)) {
//         $qty.val(currentVal + 1);
//       }
	  
//     })
//   });
  
//   $('.remove').each(function () {
//     $(this).on('click', function (e) {
//       e.preventDefault();
//       var $qty = $(this).siblings(".qty-input");
//       var currentVal = parseInt($qty.val());
//       if (!isNaN(currentVal) && currentVal > 1) {
//         $qty.val(currentVal - 1);
//       }
//     });
//   })
  

function addToCart(product) {
	if (localStorage) {
		var cart;
		if (!localStorage['cart']) cart = [];
		else cart = JSON.parse(localStorage['cart']);            
		if (!(cart instanceof Array)) cart = [];
		var key ='product_'+product.id;
		var obj = {};
		obj[key] = product;
		cart.push(obj);
		localStorage.setItem('cart', JSON.stringify(cart));
	} 
}

	</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>