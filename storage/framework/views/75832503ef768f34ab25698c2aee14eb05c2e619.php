<style>
p{
  color: black  !important;
}
	.contact .col-md-3{
        /* background: #C5171C;
        color: #fff; */
		padding: 4%;
		border-top-left-radius: 0.5rem;
		border-bottom-left-radius: 0.5rem;
	}
	.contact-info{
		margin-top:10%;
	}
	.contact-info img{
		margin-bottom: 15%;
	}
	.contact-info h2{
		margin-bottom: 10%;
	}
	.contact .col-md-9{
		background: #fff;
		padding: 3%;
		border-top-right-radius: 0.5rem;
		border-bottom-right-radius: 0.5rem;
	}
	.contact-form label{
		font-weight:600;
	}
	.contact-form button{
		background: #25274d;
		color: #fff;
		font-weight: 600;
		width: 25%;
	}
	.contact-form button:focus{
		box-shadow:none;
	}
</style>

<?php $__env->startSection('content'); ?>



<?php $lang= app()->getLocale();?>
	<?php if($lang == 'en'): ?>
		<?php echo $__env->make('website.about-us_en', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php else: ?>    
		<?php echo $__env->make('website.about-us_ar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php endif; ?>
    
<?php $__env->stopSection(); ?>


<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>