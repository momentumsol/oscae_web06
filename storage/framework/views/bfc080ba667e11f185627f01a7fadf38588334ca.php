<?php $__env->startPush('style'); ?>
<style>
    .login{
    border-top: 1px solid #C5C5C5;
    border-bottom: 1px solid #C5C5C5;
    background-color: #FAFAFA;
    overflow: hidden;
    padding: 10px;
    color: #535353
}
.form{

    border: 1px solid #C53330;
    margin:  0 auto;
    padding: 20px 20px 30px
}

/* @media  only screen and (min-width: 800px) {
    .form{
    width: 40%;
    }
}
@media  only screen and (min-width: 1200px) {
    .form{
    width: 20%;
    }
} */
/* .form-control{
    width: 80%;
    margin:  0 auto;
    margin-top: 20px;
    margin-bottom: 20px;

}
button{
    border: 1px solid   #414742 !important;
    color:   #414742 !important;
    margin:  0 auto;
}
.forget_pss{
    color: #aaaeb3;
    text-decoration: underline
}
.create_new_account{
    text-align: center;
    margin-bottom: 40px;
    margin-top: 25px
}
.create_new_account a{
    color:   #414742;
    text-decoration: underline

}
.text-center{
    font-size: 12px;
    padding: 20px
}
.text-center a{
    color: #aaaeb3;
    text-decoration: underline
} */



</style>

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>


<div>
    <div class="row  justify-content-center my-5" style="margin: 0">
        <div class="col-lg-4 col-md-6 col-sm-10 " style="padding: 0">
            <?php echo $__env->make('website.partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
            <div class="form">
                <form action="<?php echo e(route('web.login', app()->getLocale())); ?>" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-group">
                        <input class="form-control" name="email" type="text" placeholder="<?php echo e(__('lang.email')); ?>" style="width: 90%;margin-bottom: 8px">
                        <input class="form-control" name="password" type="password" placeholder="<?php echo e(__('lang.password')); ?>" style="width: 90%">
                        <a  href="<?php echo e(route('web.forgot_password', app()->getLocale())); ?>" class="forget_pss float-right mt-2" style="font-size: 14px"><?php echo e(__('lang.forget_password')); ?></a>
                    </div>
                    <div class="row" style="justify-content: center;margin:0" >
                        <button type="submit" class="btn qut-btn vs-btn shadow-none " style="width: 85%"> <?php echo e(__('lang.login')); ?></button>
                    </div>
                </form>
                <hr>
        
                <div class="create_new_account">
                    <span><?php echo e(__('lang.oscarnew')); ?></span> <a style="color: blue;text-decoration:underline" href="<?php echo e(route('register',app()->getLocale())); ?>" > <?php echo e(__('lang.newaccount')); ?></a>
                </div>
                <p class="mt-2" style="line-height: 1.5;"><?php echo e(__('lang.login-policy')); ?> <a style="color: blue;text-decoration:underline" href="<?php echo e(url (app()->getLocale().'/return_policy')); ?>"><?php echo e(__('lang.Refund & Returns Policy')); ?></a> <?php echo e(__('lang.and')); ?> <a style="color: blue;text-decoration:underline" href="<?php echo e(url (app()->getLocale().'/privacy_policy')); ?>"><?php echo e(__('lang.Privacy Policy')); ?></a>.</p>
            </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>


<?php $__env->startPush('script'); ?>
<script src="<?php echo e(asset('/front/js/dish.js')); ?>"></script>


<script>
    $( document ).ready(function() {
        $('.carousel-item').first().addClass( "active" );
});

</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>