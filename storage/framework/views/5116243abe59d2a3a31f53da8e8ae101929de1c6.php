<style>
	/* @media  only screen and (min-width: 768px) {
		.tab-height img{
			height: 450 !important;
		}
		.banner1 img{
			height: 900 !important;

		}
	}

	@media  only screen and (max-width: 992px) {
		.tab-height img{
			height: 100 !important;
		}
		.banner1 img{
			height: 300 !important;
		}
	}



	@media  only screen and (min-width: 992px) {
		.banner{
			height: 700px;
			margin:  0 auto;
			width: 100%;
			object-fit: fill;
		
		}

	} */
	.ti-heart:hover{
color: red !important
}

.slick-slide{
	height: auto !important;
}
.ad-box-area .gx-10{
	margin: 20px !important
}

</style>

<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">



<?php $__env->startSection('content'); ?>

<?php if(isset($error)): ?>
	<h1 class="modal_error"><?php echo e($error); ?></h1>
<?php endif; ?>

<!-- <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<a href="<?php echo e($slider->link); ?>">
<div class="row">
	<img loading="lazy"class="w-100 banner" src="<?php echo e($slider->image ?? ''); ?>" alt="">
	
</div>
</a>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> -->


<?php $lang= app()->getLocale();?>





        <!--==============================
      Hero Area
    ==============================-->
	<section class="vs-hero-wrapper hero-layout4  ">
		<div class="vs-hero-carousel" data-height="650" data-container="1800" data-slidertype="fullwidth">

			<?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php
				$link=explode('/',$slider->link);
				$link=array_pop($link);
			?>
			
					<div class="ls-slide" data-ls="duration: 8000; transition2d: 106, 5, 111;">
						
						<a class="ls-link" href="<?php echo e($slider->link); ?>"></a>

						<img loading="lazy"class="ls-bg" onclick="sliderLink(this)" src="<?php echo e($slider->image ?? ''); ?>" alt="..." >
						

					</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		</div>
	</section>

    <!--==============================
    Featured Bar
    ==============================-->
    <section class="featured-bar-wrapper  ">
        <div class="container">
            <div class="featured-bar">
                <div class="row justify-content-around justify-content-lg-between">
                    <div class="col-4 feature-bar-box d-lg-flex mb-30">
                        
                        
                            
                        
						<a href="<?php echo e(url (app()->getLocale().'/aboutus')); ?>"><img loading="lazy"class="w-100" src="<?php echo e(asset('Oscar stores logo copy.webp')); ?>" alt=""></a>

						
                    </div>
                    <div class="col-4  feature-bar-box d-lg-flex mb-30">
                        
						<a href="<?php echo e(url (app()->getLocale().'/aboutus')); ?>"><img loading="lazy"class="w-100" src="<?php echo e(asset('Oscar final logo-1.webp')); ?>" alt=""></a>

                    </div>
                    <div class="col-4  feature-bar-box d-lg-flex mb-30">
                        
						<a href="<?php echo e(url (app()->getLocale().'/aboutus')); ?>"><img loading="lazy"class="w-100" src="<?php echo e(asset('Oscar signature.webp')); ?>" alt=""></a>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--==============================
    Advertise Box
    ==============================-->

    <!--==============================
    Advertise Box
    ==============================-->
	
    <div class="ad-box-area">
        <div class="container">
            <div class="row gx-10 my-5" style="margin-right: 0px !important;margin-left: 0 !important;">
				
                <div class=" col-4 mb-10">
                    <div class="vs-ad-box3 mega-hover">
						<?php if($banners[0]['link']): ?>
                        	<a   href="<?php echo e($banners[0]['link']); ?>" class="overlay"><span class="sr-only">hole div linkup</span></a>	
						<?php endif; ?>
						
                        <img loading="lazy"src="<?php echo e($banners[0]['image']); ?>"  class="w-100 d-none d-md-block d-lg-block d-xl-block  h-100">
                        <img loading="lazy"src="<?php echo e($bannersMob[0]['image']); ?>"  class="w-100 d-block  d-sm-block d-md-none h-100" >
                         <div class="ad-content">
                            
                        </div> 
                    </div>
                </div>
                <div class="col-4 mb-10">
                    <div class="vs-ad-box4 mega-hover">
						<?php if($banners[1]['link']): ?>
						
                        <a href="<?php echo e($banners[1]['link']); ?>" class="overlay"><span class="sr-only">hole div linkup  </span></a>
						<?php endif; ?>
                        
						<img loading="lazy"src="<?php echo e($banners[1]['image']); ?>" class=" w-100 d-none d-md-block d-lg-block d-xl-block   h-100">
                        <img loading="lazy"src="<?php echo e($bannersMob[1]['image']); ?>"  class="w-100 d-block  d-sm-block d-md-none h-100">
                        <div class="ad-content">
                            
                        </div> 
                    </div>
                </div>
                <div class="col-4 mb-10">
                    <div class="vs-ad-box5 mega-hover">
						<?php if($banners[2]['link']): ?>

                        <a href="<?php echo e($banners[2]['link']); ?>" class="overlay"><span class="sr-only">hole div linkup</span></a>
						<?php endif; ?>
                       
						<img loading="lazy"src="<?php echo e($banners[2]['image']); ?>" class="w-100 d-none d-md-block d-lg-block d-xl-block   h-100">
                        <img loading="lazy"src="<?php echo e($bannersMob[2]['image']); ?>"class="w-100 d-block  d-sm-block d-md-none h-100">
                        <div class="ad-content">
                            
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>




	    <!--==============================
    Shop Categories
    ==============================-->
	<?php if(isset( $main_categories)): ?>
		<?php if(count($main_categories->toArray()) > 0): ?>
			<section class="shop-categories-wrapper  " data-bg-src="<?php echo e(asset('shop-cat-bg.webp')); ?>">
				<div class="container">
					<div class="section-title text-center  wow fadeIn" >
						<h2 class="sec-title1"><?php echo e(__('lang.Shop By Categories')); ?></h2>
						<img loading="lazy" style="  margin: 0 auto;" src="<?php echo e(asset('assets/img/icons/sec-icon-1.png')); ?>" alt="Section Icon">
					</div>
					<div class="row vs-carousel  wow fadeIn" data-wow-delay="0.3s" data-slide-show="4">

						<?php $__currentLoopData = $main_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="col-xl-3">
								<div class="vs-cat-card mb-30">
									<div class="cat-img">
										<a href="<?php echo e(url(app()->getLocale().'/homeWeb/all_products/'. $category->id )); ?>"><img loading="lazy" style="background-image: url(<?php echo e(asset('/image.webp')); ?>);  background-size: cover;background-repeat: round;height:150px" src="<?php echo e($category->image['src']); ?>" class="w-100" alt="Cat Image"></a>
									</div>
									<div class="cat-content">
										<h3 class="h5 cat-title"><a href="<?php echo e(url(app()->getLocale().'/homeWeb/all_products/'. $category->id )); ?>"><?php echo e($category->name->$lang); ?></a></h3>
									</div>
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					
					
					</div>
				</div>
			</section>
		<?php endif; ?>
	<?php endif; ?>

    <!--==============================
        Blog Area
    ==============================-->
<?php if(isset( $readyMeals)): ?>
	<?php if(count($readyMeals->toArray()) > 0): ?>
		<section class="vs-blog-wrapper " id="blog">
			<div class="container">
				<div class="section-title text-center" style="margin-bottom: 0">
					<h1 class="sec-title1"><?php echo e(__('lang.Ready Meals')); ?></h1>
					<h2 class="sec-title2"><?php echo e(__('lang.from Oscar')); ?></h2>
					<img loading="lazy"style="margin: 0 auto" src="<?php echo e(asset('assets/img/icons/sec-icon-1.png')); ?>" alt="Section Shape">
				</div>
				<div class="row vs-carousel wow fadeIn" data-slide-show="3">
					<?php $__currentLoopData = $readyMeals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $meal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="col-lg-4">
							<div class="vs-blog blog-grid vs-product-box1 thumb_swap">
								<div class="blog-img image-scale-hover">
									<a href="<?php echo e(url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id)); ?>"><img src="<?php echo e($meal->images['0']->src); ?>" alt="Blog Image" class="w-100"></a>
								</div>
								<div class="blog-content text-center product-content">
									<div class="actions-btn">
										<?php if(Auth::guard('customerForWeb')->user()): ?>
									
                                       <?php if($meal->in_stock == 1): ?>
									   <a  title="<?php echo e($meal->id); ?>" class="cart <?php echo e($meal->PriceUnit != "kg" ? 'cart' : ''); ?> "><i class="fal fa-cart-plus"></i></a>
									   <?php endif; ?>
										<a href="#" title="<?php echo e($meal->id); ?>" class="Unlike" style="display: <?php echo e(in_array($meal->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'block' : 'none'); ?>"><i style="color:#C53330;" class="fal fa-heart"></i></a>
										<a href="#" title="<?php echo e($meal->id); ?>" class="Wishlist" style="display: <?php echo e(in_array($meal->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'none' : 'block'); ?>"><i class="fal fa-heart"></i></a>
									
										<?php endif; ?>
									</div>
									<?php if($meal->in_stock != 1): ?>
										<h4 class="product-title h5 mb-0"><?php echo e(__('lang.out_of_stock')); ?></h4>
										<?php endif; ?>
									<div class="cat-content" >
										<h4 class="cat-title"><a href="<?php echo e(url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id)); ?>"><?php echo e(app()->getLocale() =='en' ? $meal->name : $meal->name_ar); ?></a></h4>
									</div>
								
								</div>
							</div>
						</div>	
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</section>
	<?php endif; ?>
<?php endif; ?>


<?php if(Auth::guard('customerForWeb')->user()): ?>

	</div>
</div>
<?php endif; ?>




	<!-- Modal -->
	<?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="modal fade" id="exampleModal<?php echo e($offer->id); ?>" tabindex="-1" role="dialog" style="margin-top: 50px">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close"
								aria-hidden="true"></span></button>
					</div>
					<div class="modal-body">
						<div class="row no-gutters">
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
								<!-- Product Slider -->
								<div class="product-gallery">
									<div class="quickview-slider-active">
										<div class="single-slider">
											<img loading="lazy"class="w-100   "   src="<?php echo e($offer->images[0]->src); ?>"  style="background-image: url(<?php echo e(asset('image.webp')); ?>); height:247px;background-size: contain;background-repeat:no-repeat" alt="#">
										</div>
										
									</div>
								</div>
								<!-- End Product slider -->
							</div>
							<?php $category=App\Models\Category::where('id',$offer->category_id)->first()?>
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="background-color: #ccc">
								<div class="quickview-content">
									<h2 style="margin-top: 100px;font-size: large;"><?php echo e($offer->name); ?></h2>
										<h5 style=" margin-top: 20px;font-size: medium;"><span style="font-weight:bold;font-size:small"><?php echo e(__('lang.category')); ?> :</span><?php echo e($category->name->$lang); ?></h5>
									
										<div class="product-price" style="font-size: small">
											<span style="font-weight:bold;font-size:small"><?php echo e(__('lang.unite_price')); ?> : </span>
											<span class="old" style="text-decoration: line-through"><?php echo e($offer->regular_price); ?> <?php echo e(__('lang.EGP')); ?></span>
											<span ><?php echo e($offer->discountprice); ?> <?php echo e(__('lang.EGP')); ?></span>
										</div>
										<?php if(!is_null($offer->discription)): ?>
											<div class="quickview-peragraph">
												<p><span style="font-weight:bold;font-size:small">><?php echo e(__('lang.description')); ?> : </span> <?php echo e($offer->discription); ?>.</p>
											</div>
										<?php endif; ?>
									
									
									<div class="quantity my-3">
										<!-- Input Order -->
										<div class="input-group">
											<div class="button minus" id="Quantity">
												<button type="button" class="btn  btn-number" disabled="disabled"
													data-type="minus" data-field="quant[1]" style="height:33px">
													<i class="ti-minus"></i>
												</button>
											</div>
											<input type="text" name="quant[1]" class="input-number qty" data-min="<?php echo e($offer->PriceUnit =='Quantity'?'1' : '0.1'); ?>"
											data-max="9000" value="<?php echo e($offer->PriceUnit =='Quantity'?'1' : '0.1'); ?>" style="border:0">
										<div class="button plus" id="Quantity">
											<button id="<?php echo e($offer->id); ?>" type="button" class="btn add  btn-number"   data-type="plus"
												data-field="quant[1]" style="height:33px" data=<?php echo e($offer); ?>>
												<i class="ti-plus"></i>
											</button>
										</div>
										</div>
										<!--/ End Input Order -->
									</div>
									<div class="add-to-cart mt-2" style="display: flex;justify-content: space-between;align-items: center;">
										<?php if(auth()->guard('customerForWeb')->user()): ?>
										<a class="quickcart" onclick="quickcart(this,event)" title="<?php echo e($offer->id); ?>" href="#" class="btn" style="background-color: #333;color:#fff;padding: 10px 42px;" ><?php echo e(__('lang.add_to_cart')); ?></a>

											<a  class="Wishlist" title="<?php echo e($offer->id); ?>" href="#" class="btn min"  style="display: <?php echo e(in_array($offer->id,auth('customerForWeb')->user()->wishlist->pluck('product_id')->toArray()) ? 'none' : 'block'); ?>;background-color: #333;color:#fff;padding:0 10px"><i class="ti-heart" style="font-weight: bold;color: #fff;margin:auto; line-height: 43px"></i></a>
											<a  class="Unlike" title="<?php echo e($offer->id); ?>" href="#" class="btn min"  style="display: <?php echo e(in_array($offer->id,auth('customerForWeb')->user()->wishlist->pluck('product_id')->toArray()) ? 'block' : 'none'); ?>;background-color: #333;color:#fff;padding:0 10px"><i class="ti-heart" style="font-weight: bold;color: #fff;margin:auto; line-height: 43px"></i></a>
										<?php endif; ?>
										
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- Modal end -->
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 
<?php $__env->stopSection(); ?>


<?php $__env->startPush('scripts'); ?>

<script>

if($(window).width() <= 767){
	$('.mega-hover').css('height','100px')
}else if($(window).width() <= 500){
	$('.mega-hover').css('height','70px')

}
else{
	$('.mega-hover').css('height','300px')

}
</script>

	<script type="text/javascript">
function addToCartAjax(id,weight,qty)
{
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/addToCart',
	data:{ product_id:id,weight:weight,quantity:parseInt(qty)  },

	success:function(data){
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);
	}	
});
}
function addToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/add/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}
function removeToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/remove/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}	
		// function sliderLink(link){
		// 	alert(link)
		// }
	
	// let url='';

		$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		}

		});

		$(".Wishlist").click(function(e){
		
			e.preventDefault();
			
			$('#alert-wishlist').show();
			$('#alert-wishlist').html(<?php echo json_encode(__('lang.added to wishlist succssfully'), 15, 512) ?>);
			
			setTimeout(function() { 
                    $('#alert-wishlist').fadeOut('fast'); 
                }, 2000);
			var id = $(this).attr("title");
			
			$.ajax({
			   type:'POST',
			   url: url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/wishList',
	
			   data:{ id:id },
	
			   success:function(data){
				 addToWhishList(id)
				
	
				}	
			});
			
		
				$(this).parent().find('.Wishlist').css('display','none');
			
			$(this).parent().find('.Unlike').css('display','block');

			

			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)+Number(1));
	
		});

		$(".Unlike").click(function(e){
			e.preventDefault();
			e.preventDefault();
			$('#alert-wishlist').show();
			$('#alert-wishlist').html(<?php echo json_encode(__('lang.removed from wishlist succssfully'), 15, 512) ?>);
			
			setTimeout(function() { 
                    $('#alert-wishlist').fadeOut('fast'); 
                }, 2000);
	
	
			var id = $(this).attr("title");
	
			$.ajax({
			   type:'POST',
			   url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/unLike',
	
			   data:{ id:id },

			   success:function(data){
				//    console.log(data);
				$('li#'+data).remove();
				removeToWhishList(id)
			}
	
			});
			
			$(this).parent().find('.Wishlist').css('display','block');
			$(this).parent().find('.Unlike').css('display','none');
			
			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)-Number(1));
		});
		if( !JSON.parse(localStorage.getItem("cart"))){
			var cart =[];
			localStorage.setItem('cart', JSON.stringify(cart));
		}
		var products= [];
		var local_storage=[];



		$(".cart").click(function(e){
	

			e.preventDefault();
			var unit=$(this).attr('unit');

			var id = $(this).attr("title");
			$.ajaxSetup({

				headers: {

					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

				}

			});
			$.ajax({

			type:'POST',
			
			url:'/<?php echo e(app()->getLocale()); ?>/homeWeb/add-to-cart',

			data:{ id:id },
			success:function(data){
				let weight=250;
					if(unit == 'kg'){
						 weight=document.getElementById('weights').value
						data['weight']=weight;
								data['quantity']=1;
					}else{
						data['weight']=1;
						data['quantity']=1;
					}
					var res=jQuery.inArray(id,localStorage);
					var key='product_'+data.id;
					var storage=localStorage['cart'];
					let qty =1;
					if(storage.includes(key)){

						let storage_keys=JSON.parse(storage)
						for(var i in storage_keys){
							let item=storage_keys[i]
							let key_check=Object.keys(storage_keys[i])[0]
		if (Object.keys(storage_keys[i])[0]== 'product_'+id) {
			 qty=storage_keys[i][key_check]['quantity'];
			qty=qty+1
			item[Object.keys(item)[0]]['quantity']=qty
		
			localStorage.setItem('cart', JSON.stringify(storage_keys));

			// storage.splice(i, 1); 
	
		}
		}
						
						var in_Cart=	<?php echo json_encode( __('lang.updated to cart succssfully'), 15, 512) ?>;
				var lang=<?php echo json_encode(app()->getLocale(), 15, 512) ?>;
				var name=data.name;
				
				if(lang =='ar'){
					name=data.name_ar;
				}
				$('#alert-cart').html(name+' '+in_Cart);
						setTimeout(function() { 
								$('#alert-cart').fadeOut('fast'); 
						}, 2000);
					}else{
						addToCart(data);
						$('#alert-cart').show();
						$('#alert-cart').html('added to cart succssfully');
						setTimeout(function() { 
								$('#alert-cart').fadeOut('fast'); 
						}, 2000);
						$('.total-count').html('');
						var total=JSON.parse(localStorage.getItem("cart"));
						$('.total-count').html(JSON.parse(total.length));
					}
					console.log(id,weight,qty,'l')
					addToCartAjax(id,weight,qty)

				}
			

			});
			
		});



		function addToCart(product) {
			if (localStorage) {
				var cart;
				if (!localStorage['cart']) cart = [];
				else cart = JSON.parse(localStorage['cart']);            
				if (!(cart instanceof Array)) cart = [];
				var key ='product_'+product.id;
				var obj = {};
				obj[key] = product;
				cart.push(obj);
				localStorage.setItem('cart', JSON.stringify(cart));
			} 
		}


		function quickcart(elem,event){

		event.preventDefault();
		var id=elem.title;
		var qty= $(elem).parent().parent().find('.quantity').find('input').val();
		var storage=JSON.parse(localStorage.getItem("cart"));

		for (let i = 0; i< storage.length; i++) {
			var pro=storage[i];
			var key=Object.keys(pro);
			if(key== 'product_'+id){
				storage.splice(i, 1); 
				var cart =[];
				localStorage.setItem('cart', JSON.stringify(cart));
				localStorage.setItem('cart', JSON.stringify(storage));
				localStorage.removeItem(key);
			}
		}

		
		$.ajax({
		type:'POST',

		url:url+'/<?php echo e(app()->getLocale()); ?>/homeWeb/add-to-cart',

		data:{ id:id },
		success:function(data){
			
			data['quantity']=qty;
			var res=jQuery.inArray(id,localStorage);
			var key='product_'+data.id;
			var storage=localStorage['cart'];
			if(storage.includes(key)){
				// console.log(JSON.parse(storage));

				
				$('#alert-cart').show();
				$('#alert-cart').html(data.name+' already in cart ');
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
			}else{
				
				addToCart(data);
				$('#alert-cart').show();
				$('#alert-cart').html('added to cart succssfully');
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
				$('.total-count').html('');
				var total=JSON.parse(localStorage.getItem("cart"));
				$('.total-count').html(total.length);

			}
		}
	});
	var modal=$(elem).closest(".modal").modal("hide");
	}





				
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>