<?php $__env->startPush('style'); ?>
<style>
.register{
    border-top: 1px solid #C5C5C5;
    border-bottom: 1px solid #C5C5C5;
    background-color: #FAFAFA;
    overflow: hidden;
    padding: 10px;
    color: #535353
}
.form .form-control{
    margin:  0 auto;
    margin-top: 20px;
    margin-bottom: 20px;
    margin-bottom: 8px

}
.form{
    border: 1px solid #C53330;
margin:  0 auto;
padding: 20px 20px 30px
}
/* 
@media  only screen and (min-width: 800px) {
    .form{
    width: 40%;

}}
@media  only screen and (min-width: 1200px) {
    .form{
    width: 20%;

}}



button{
    border: 1px solid   #414742 !important;
    color:   #414742 !important;
    margin:  0 auto;
}
.forget_pss{
    color: #aaaeb3;
    text-decoration: underline
}
.create_new_account{
    text-align: center;
    margin-bottom: 40px;
    margin-top: 25px
}
.create_new_account a{
    color:   #414742;
    text-decoration: underline

}
.text-center{
    font-size: 12px;
    padding: 20px
}
.text-center a{
    color: #aaaeb3;
    text-decoration: underline
} */
</style>

<?php $__env->stopPush(); ?>


<?php $__env->startSection('content'); ?>




<div>
    <div class="row my-5 justify-content-center" style="margin: 0">
        <div class="col-lg-4 col-md-6 col-sm-10"  style="padding: 0">
            <?php echo $__env->make('website.partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="form">
                <form action="<?php echo e(route('web.register', app()->getLocale())); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                        <input class="form-control" name="name" type="text" placeholder="<?php echo e(__('lang.name')); ?>" value="<?php echo e(old('name')); ?>">
                        <input class="form-control" name="email" type="text" placeholder="<?php echo e(__('lang.email')); ?>"  value="<?php echo e(old('email')); ?>">
                        <input class="form-control"name="phone" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "12" placeholder="<?php echo e(__('lang.phone')); ?>" value="<?php echo e(old('phone')); ?>">
                        
                            
                        
                        <input class="form-control" name="password" type="password" placeholder="<?php echo e(__('lang.password')); ?>">
                        <input class="form-control" name="password_confirmation" type="password" placeholder="<?php echo e(__('lang.confirm_password')); ?>">
                    </div>
                    <div class="row mt-5" style="justify-content: center">
                        <button type="submit" class="btn qut-btn vs-btn shadow-none"  style="width: 85%"> <?php echo e(__('lang.register')); ?></button>
                    </div>
                </form>
    
    
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('script'); ?>
<script src="<?php echo e(asset('/front/js/dish.js')); ?>"></script>


<script>
    $( document ).ready(function() {
        $('.carousel-item').first().addClass( "active" );
});

</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('website.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>