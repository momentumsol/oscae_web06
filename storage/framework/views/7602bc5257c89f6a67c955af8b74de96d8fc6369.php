<!DOCTYPE html>
<html lang="en">

<head>

	<!-- Meta Tag -->
	<meta charset="utf-8">
	<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
	<meta http-equiv=“Content-Security-Policy” content=“default-src ‘self’ gap://ready file://* *; style-src ‘self’ ‘unsafe-inline’; script-src ‘self’ ‘unsafe-inline’ ‘unsafe-eval’”/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Title Tag  -->
	<title>Oscar</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="https://oscarstores.com/images/favicon.png">




	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"rel="stylesheet">
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtBC-vEWQokUP6yPiNXQOD-LPk2ru10W4&callback=initMap&libraries=&v=weekly"defer></script>


	<!-- Favicons - Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('assets/img/favicons/apple-icon-57x57.png')); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo e(asset('assets/img/favicons/apple-icon-60x60.png')); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('assets/img/favicons/apple-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('assets/img/favicons/apple-icon-76x76.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('assets/img/favicons/apple-icon-114x114.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('assets/img/favicons/apple-icon-120x120.png')); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('assets/img/favicons/apple-icon-144x144.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('assets/img/favicons/apple-icon-152x152.png')); ?>">
    <link rel="apple-touch-icon" sizes="170x170" href="<?php echo e(asset('assets/img/favicons/apple-icon-170x170.png')); ?>">
	    <link rel="shortcut icon" sizes="96x96" href="<?php echo e(asset('assets/img/favicons/favicon-96x96.png')); ?>"  type="image/x-icon">
		<link  rel="icon" type="image/x-icon" sizes="96x96" href="<?php echo e(asset('assets/img/favicons/favicon-96x96.png')); ?>">

		<link rel="apple-touch-icon-precompose" sizes="60x60" href="<?php echo e(asset('assets/img/favicons/apple-icon-60x60.png')); ?>">

		
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo e(asset('assets/img/favicons/android-icon-192x192.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('assets/img/favicons/favicon-32x32.png')); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(asset('assets/img/favicons/favicon-96x96.png')); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('assets/img/favicons/favicon-16x16.png')); ?>">
    <link rel="manifest" href="<?php echo e(asset('assets/img/favicons/manifest.json')); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo e(asset('assets/img/favicons/ms-icon-144x144.png')); ?>">
    <meta name="theme-color" content="#ffffff">

    <!--==============================
	    All CSS File
	============================== -->
    <!-- Bootstrap -->
    
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">

    <!-- Fontawesome Icon -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/fontawesome.min.css')); ?>">
    <!-- Flaticon -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/flaticon.min.css')); ?>">
    <!-- Layerslider -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/layerslider.min.css')); ?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/jquery.datetimepicker.min.css')); ?>">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/magnific-popup.min.css')); ?>">
    <!-- Slick Slider -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/slick.min.css')); ?>">
    <!-- Animation CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/animate.min.css')); ?>">
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/style.css')); ?>">
    <!-- Theme Color CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/theme-color1.css')); ?>">


	<link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet'>

	<!-- StyleSheet -->

	
	
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css">




<style>
	.fa, .fab, .fad, .fal, .far, .fas{
		line-height: inherit;
		font-size: 20px;
		cursor: pointer;
		margin: 0 3px
	}
	.icon-btn.bg3,.icon-btn.bg2{
		background-color: #ccc;
		cursor: pointer;

	}
	.box-nav-popup ul {
		display: block !important
	}

.mega-menu-wrap{
	max-width:1000px;
}
.actions-btn{
	display: flex !important
}

.head-top-links {
	/* display: flex !important; */
    align-items: center !important
}

.head-top-links .icon-btn{
	width: 40px;
	height: 40px;
	line-height: 42px
}

.fa-shopping-cart{
margin-top: -2px
}
.vs-menu-wrapper i,.header6-inner i{
	font-size: 24px !important
}

.fa-user{
	/* color: var(--body-color2); */
}
.menu-item-has-children a i{
	margin-right: 8 !important
}


.vs-product-box1:hover .actions-btn{
	opacity: 1;
    visibility: visible;
    margin-top: 0;
    width: 100px;
    display: flex !important;
    align-items: center;
    justify-content: center;
}
.actions-btn a {
	font-size: 25px !important;
	width: 40px !important;
	height: 40px !important;
}
.actions-btn i{
	font-size: 25px !important;
	/* width: 40px; */
	line-height: 1.8 !important

}
.actions-btn a:hover i{
/* padding: 10px !important */
} 
.product-img img{
	max-height: 375px;

}
.department .content-image{
	width: 100% !important;
}
.bg-theme{
	height: 60px;
}
.vs-mobile-menu{
	max-height: 100% !important;
}
.vs-menu-area{
	height: 100%;
	overflow: auto
}
.menu-item-has-children img{
	display: inline !important;
}
.vs-mobile-menu ul li a{
	border:none;
	padding: 5px 0 !important;
}
.box-nav-popup {
	margin: 5px
}
.main-menu a{
	border: 0
}

body,h1,h2,h3,h4,h5,h6,p,span,button,a{
	font-family: 'Oswald' !important
}

@media (max-width: 768px) { 
    .featured-bar{
		z-index: 0;
	}

}
.vs-mobile-menu ul .vs-item-has-children > a:after{
	content:'+';
	box-shadow:none;
}
.vs-mobile-menu ul .vs-item-has-children.vs-active > a:after{
		content:'-';

}
.close-search{
	display: inline;
    /* padding: 8px 0 0 10px; */
    color: #fff;
    /* line-height: 1.5; */
    font-size: 30px;
}
.head-top-links > ul > li::after{
	content: ''
}

.palceholder-search{
	margin: 0 0 0 15px;
}
.featured-bar{
	z-index: 40;
}
.main-menu ul.sub-menu li ul{
	margin: 0
}
.box-nav-popup {
    padding: 20px 0px 0px 15px;

}
.main-menu ul.sub-menu li.menu-item-has-children>a:after{
	font-family: 'Font Awesome 5 Pro';
}
.main-menu ul.sub-menu li.menu-item-has-children>a::before{
	font-family: 'Font Awesome 5 Pro';
}
.mega-menu-wrap {
	/* width: 700px !important; */
}
.main-menu ul.sub-menu li.menu-item-has-children>a:after {
	margin-left: 60px
}

.product-title a{
	font-size: 16px !important
}
.vs-mobile-menu ul li a:before{
	content: ''
}
.widget_title:before{
	left: 50%;
	width: 0;
}
.head-top-links  .fa-user{
	line-height: 1.5 !important;
}
.ms-2 {
        margin: 0 .5rem!important;
    }
</style>
<style>
	.fa-user:hover{
		color:#fff !important
	}
	.head-top-links>ul>li{
		margin:  0 7px !important
	}
	/* #cart-count2{
		top: -9px;
    right: -8px
	} */
	.fa-heart{
		font-size: 21px !important;
	}

</style>
<?php $lang= app()->getLocale();
			if($lang == 'ar'){
				echo'<link rel="stylesheet" href="'.asset('css/ar.css').'">';

			}else{
				echo'<link rel="stylesheet" href="'.asset('css/en.css').'">';

			}
	?>
<style>
	.widget-title{
		display: flex;
		/* width: fit-content; */
	}
	.widget-title a{
    white-space: nowrap !important;
    overflow: hidden !important;
    text-overflow: ellipsis !important;
    display: inline-block !important;
    width: 100%;
}
.vs-mobile-enu{
	max-height: auto !important;
}
/* .sub-menu{
	height: 100%;
} */
/* div.sticky{
  position: sticky;
  top: 0 !important;
} */
.product-area img:not([draggable]), embed, object, video{
	background-size: contain !important;
}
.row{
	margin:0
}
.sub-menu{
	z-index: 99 !important;
}




@media (max-width: 768px) { 

	.product-img{
		height: 50%;
	}
}
.widget_title{
	margin-bottom: 0 !important
}
.section-title{
	margin:20px 0 !important ;
	padding-top:20px !important ;
}
@media (max-width: 10000px){
	.featured-bar {
		margin-top:-60px
	}

}
.fa-shopping-cart{
		height: 1.9 !important;
		margin-top: 0;
	}

@media (max-width: 400px) { 
	.product-img{
		height: 35%;
	}
	.product-area .col-sm-6 {
		height: max-content;
	}
	.fa-shopping-cart{
		height: 1.9 !important;
	}
	.header-search{
		width: 33px !important; 
    height: 33px !important;
	top: -2px !important;
    right: -2px !important;
	}
	.product-area img:not([draggable]), embed, object, video {
    	max-height: 130px !important;

	}
	.bg-theme{
	display: flex;
	align-items: center
	}
	.header-search.style2{
		min-width: fit-content !important;
	}
	.bg-theme  form  .icon-btn{
		width: 35px !important;
		height: inherit !important;
		line-height: 0;
		top: 0px;
		right: 0px;
	}
	#icons-header{
		display: flex !important
	}

}

@media (max-width: 550px) { 
.product-area img:not([draggable]), embed, object, video {
		height: 130px !important;
    	max-height: 130px !important;
		-webkit-max-height: 130px !important;
	}
	.product-area img{
		height: 130px !important;
		max-height: 130px !important;
		-webkit-max-height: 130px !important;

	}
}

@media (max-width: 400px) { 
.product-area img:not([draggable]), embed, object, video {
		height: 100px !important;
    	max-height: 100px !important;
		-webkit-max-height: 100px !important;

	}
	.product-area img{
		height: 100px !important;
		max-height: 100px !important;
		-webkit-max-height: 100px !important;

	}
}


.fa-search{
	line-height: 0 !important
}
</style>

<?php echo $__env->yieldPushContent('style'); ?>
<?php echo $__env->yieldPushContent('top_script'); ?>
</head>

<body class="js">
	<?php
	$logo='';
	if($branch->store_id == '02'){
		$logo='branches/Oscar final logo-1.png';
	}elseif($branch->store_id == '01' || $branch->store_id == '05'){
		$logo='branches/Oscar stores logo copy.png';
	
	}else{
		$logo='branches/Oscar signature.png';
	
	}
	?>

	    <!--==============================
		Preloader
	==============================-->
	 <!-- <div class="preloader  ">
		
		<div class="preloader-inner">
			<div class="loader-logo">
				<img src="<?php echo e(asset($logo)); ?>" alt="Oscar Grand Stores" width="170">
			</div>
			<div class="loader-wrap pt-4">
				<span class="loader"></span>
			</div>
		</div>
	</div> -->
	


	
	<?php $lang= app()->getLocale();
			$arr=[];
			if(auth()->guard('customerForWeb')->user())
			{
				
		
				$store_id=$branch->store_id;
				$stockId='in_stock_';
				switch($store_id)
				{
				case 01:$stockId.=1;
				break;
				case 02:$stockId.=2;
				break;
				case 04:$stockId.=4;
				break;
				case 05:$stockId.=5;
		
				}
				$auth=count(auth()->guard('customerForWeb')->user()->cart->where($stockId,1)->unique('barcode')->values() );

			}
			else
			{
				$auth=0;
			}
	?>


<!--==============================
        Header Top
    ==============================-->
	
    <div class="header-top-wrapper header-top-v3">
        <div class="container">
            <div class="row justify-content-center justify-content-md-between align-items-center">
                <div class="">
                    <div class="head-top-links">
                        
                        <ul class="w-100">
							<li class="fl-left">
							
                               		 <!-- Dropdown -->
									<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-bs-toggle="dropdown" aria-expanded="false">
									<i class="fas fa-globe"></i>
									<?php echo e(strtoupper($lang)); ?>

									
									</a>

									<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink1">
										<li >
											
												<div>
														<?php $__currentLoopData = config('app.available_locales'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $locale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<?php
															if($locale =='ar')
															{
																$local_lang='ع';

															}
															else
																{
																	$local_lang='EN';
																}
															?>
															<?php if(is_null(\Illuminate\Support\Facades\Route::currentRouteName())): ?>
																<?php if(!is_null(\Request::segment(3)) && \Request::segment(3)=='branch'): ?>
																	<?php $id=\Request::segment(4);?>
															<a class="nav-link"href="<?php echo e(url($locale.'/homeWeb/branch/'.$id)); ?>"
															<?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>>
															<?php echo e(strtoupper($local_lang)); ?>

															</a>
															<?php elseif(!is_null(\Request::segment(3)) && \Request::segment(3)=='all_products'): ?>
															<?php $id=\Request::segment(4);?>
															<a class="nav-link"
															href="<?php echo e(url($locale.'/homeWeb/all_products/'.$id)); ?>"
															<?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>

															<?php else: ?>
															<a class="nav-link"
															href="<?php echo e(route('homeWeb', $locale)); ?>"
															<?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
															<?php endif; ?>
															<?php elseif(\Illuminate\Support\Facades\Route::currentRouteName()=='homeWeb.show'): ?>
															<a class="nav-link"
															<?php $slug=\Request::segment(3);
															$url='homeWeb/'.$slug?>
															href="<?php echo e(url($locale.'/homeWeb/'.$slug)); ?>"
															<?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
															<?php elseif(\Illuminate\Support\Facades\Route::currentRouteName()=='homeWeb.index'): ?>
															<a class="nav-link"
															<?php $slug=\Request::segment(4);
															?>
															href="<?php echo e(url($locale.'/homeWeb/branch/'.$slug)); ?>"
															<?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
															<?php elseif(!is_null(\Request::segment(3)) && \Request::segment(3)=='search_product'): ?>
															<?php $search=\Request::query('search')?>
															<a class="nav-link"
															href="<?php echo e(url($locale.'/homeWeb/search_product?search='.$search)); ?>"
															<?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
															<?php elseif(!is_null(\Request::segment(2)) && \Request::segment(2)=='address' && \Request::segment(4)=='edit'): ?>
															<?php $id=\Request::segment(3)?>
															<a class="nav-link"
															href="<?php echo e(url($locale.'/address/'.$id.'/edit')); ?>"
															<?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>

															<?php else: ?>
															<a class="nav-link"
															href="<?php echo e(route(\Illuminate\Support\Facades\Route::currentRouteName(), $locale)); ?>"
															<?php if(app()->getLocale() == $locale): ?> style="font-weight: bold; text-decoration: underline" <?php endif; ?>><?php echo e(strtoupper($local_lang)); ?></a>
															<?php endif; ?>



														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</div>
										</li>
									</ul>
							</li>
							<li class="fl-right">
								<!-- Dropdown -->
								<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink2" data-bs-toggle="dropdown" aria-expanded="false">
									<i class="fas fa-map-marker-alt mr-2"></i><?php echo e($branch['title']->$lang); ?>

								</a>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">

									<?php for($i = 0; $i < count($branches); $i++): ?>
									
										<?php if($branches[$i]->store_id != $branch['store_id']): ?>
											<li><a id="<?php echo e($branches[$i]->store_id); ?>" onclick="set_branch(this)" href="<?php echo e(url(app()->getLocale().'/homeWeb/branch/'.$branches[$i]->store_id)); ?>"  ><?php echo e($branches[$i]->title->$lang); ?></a></li>
										<?php endif; ?>
									<?php endfor; ?>
								</ul>
							</li>	
									
					
									
						</ul>			
					</div>
					
				</div>
			</div>
		</div>
	</div>


    <!--========================
    Sticky Header
	========================-->
	<div class="sticky-header-wrap sticky-header py-2 py-lg-1">
		<div class="container position-relative">
			<div class="row align-items-center">
				<div class="col-5 col-md-3">
					<div class="logo">
						<a href="<?php echo e(url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id)); ?>"><img src="<?php echo e(asset($logo)); ?>" alt="Oscar" width="100"></a>
					</div>
				</div>
				<div class="col-7 col-md-9 text-end position-static">
					<nav class="main-menu menu-sticky1 d-none d-lg-block link-inherit">
						<ul>
							<li class="menu-item-has-children">
								<a href="<?php echo e(url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id)); ?>"><?php echo e(__('lang.home')); ?></a>
							</li>
							<li > <a href="<?php echo e(url(app()->getLocale().'/categoties')); ?>"  data-content="Link Hover" aria-hidden="true"><?php echo e(__('lang.Categories')); ?></a></li>

							
							
							<li class="<?php echo e(Request::segment(1) =='promotions' ? 'active' : ' '); ?>">
								<a href="<?php echo e(url(app()->getLocale().'/promotions')); ?>"><?php echo e(__('lang.promotions')); ?></a>
							</li>
						</ul>
					</nav>
					<button class="vs-menu-toggle d-inline-block d-lg-none "><i class="far fa-bars"></i></button>
				</div>
			</div>
		</div>
	</div>
    <!--==============================
    Mobile Menu
  ============================== -->
  	<div class="vs-menu-wrapper">
		<div class="vs-menu-area">
			<button class="vs-menu-toggle "><i class="fal fa-times"></i></button>
			<div class="mobile-logo">
				<a href="<?php echo e(url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id)); ?>"><img src="<?php echo e(asset($logo)); ?>" width="120px" alt="Oscar"></a>
			</div>
			<div class="vs-mobile-menu link-inherit">


			</div>
			<!-- Menu Will Append With Javascript -->
		</div>
	</div>
    <!--==============================
        Header Area
    ==============================-->
    <header class="header-wrapper">
        <div class="header6-inner" style="padding:15px 0">
            <div class="container position-relative">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto">
                        <div class="header-logo">
							
                            <a href="<?php echo e(url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id)); ?>"><img width="170" src="<?php echo e(asset($logo)); ?>" alt="Oscar"></a>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="header-address d-none d-md-block">
                            <p class="font-theme fs-xs fw-bold tmb-1">
								
								<a href="tel:16991" class="text-reset" ><i class="far fa-phone"></i> <span style="font-size: 30px">16991</span></a></p>
                        </div>
                    </div>
					<div class="col-auto">
                        <button type="button" class="vs-menu-toggle d-inline-block d-lg-none" id="ToggleBTN"><i class="far fa-bars"></i></button>
                        <div class="head-top-links text-body2 d-none d-lg-block">
							<span class="icon-btn bg3" style="margin:0"><a   style="display: block !important" href="<?php echo e(Auth::guard('customerForWeb')->user() ? url(app()->getLocale().'/profile') : route('login',$lang)); ?>"><i class="fal fa-user" style="color: var(--body-color2)" ></i></a></span>
                            <ul>
								<?php if(!Auth::guard('customerForWeb')->check() ): ?>
									
									<li><a href="<?php echo e(route('login',$lang)); ?>" ><?php echo e(__('lang.login')); ?></a></li>
									<li><a href="<?php echo e(route('register',$lang)); ?>"><?php echo e(__('lang.register')); ?></a></li>
								<?php endif; ?>
                            </ul>
							<div class="modal fade" id="modalLRFormmobile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="    z-index: 999999;" aria-hidden="true">
								<div class="modal-dialog cascading-modal" role="document">
									<!--Content-->
									<div class="modal-content">
					
										<!--Modal cascading tabs-->
										<div class="modal-c-tabs">
					
											<!-- Nav tabs -->
											<ul class="nav nav-tabs md-tabs tabs-2  darken-3" role="tablist">
												<li class="nav-item" >
													<a class="nav-link active" id="active"  data-toggle="tab" href="#panel7mobile"
													role="tab"><i class="fas fa-user mr-1"></i>
													<?php echo e(__('lang.login')); ?></a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#panel8mobile" role="tab"><i
														class="fas fa-user-plus mr-1"></i>
													<?php echo e(__('lang.register')); ?></a>
												</li>
											</ul>
					
											<!-- Tab panels -->
											<div class="tab-content">
												
					
												<!--Panel 7-->
												<div class="tab-pane fade in show active" id="panel7mobile" role="tabpanel">
					
													<!--Body-->
													<div class="modal-body mb-1">
														<form action="<?php echo e(route('web.login', app()->getLocale())); ?>" method="post" class="needs-validation form-search" novalidate>
															<?php echo e(csrf_field()); ?>

															<div class="md-form form-sm ">
																<i class="fas fa-envelope prefix"></i>
																<input type="email" id="validationCustom032" name="email" placeholder="Your email"
																class="form-control form-control-sm validate" required>
					
																
															</div>
					
															<div class="md-form form-sm mb-4">
																<i class="fas fa-lock prefix"></i>
																<input type="password" id="modalLRInput11" name="password" placeholder="Your password"
																class="form-control form-control-sm validate" required>
					
																
															</div>
															<div class="text-center form-sm mt-2">
																<button class="btn btn-info" type="submit"><?php echo e(__('lang.login')); ?> <i
																	class="fas fa-sign-in ml-1"></i></button>
																</div>
														</form>
													</div>
													<!--Footer-->
													<div class="modal-footer">
														<div class="options text-center text-md-right mt-1">
															<p> <a href="<?php echo e(route('web.forgot_password', app()->getLocale())); ?>" class="blue-text"><?php echo e(__('lang.forget_password')); ?>?</a></p>
														</div>
														<button type="button" style="padding: 10px"
														class="btn btn-outline-info waves-effect ml-auto"
														data-dismiss="modal"><?php echo e(__('lang.close')); ?></button>
													</div>
					
												</div>
													<!--/.Panel 7-->
					
													<!--Panel 8-->
												<div class="tab-pane fade" id="panel8mobile" role="tabpanel">
					
													<!--Body-->
													<div class="modal-body">
														<form action="<?php echo e(route('web.register', app()->getLocale())); ?>" method="post" class="needs-validation" novalidate>
															<?php echo e(csrf_field()); ?>

															<div class="md-form form-sm mb-1">
																<i class="fas fa-user prefix"></i>
																<input type="text" name="name"
																class="form-control form-control-sm validate" placeholder="Your Name" value="<?php echo e(old('name')); ?>">
																
															</div>
															<div class="md-form form-sm mb-1">
																<i class="fas fa-mobile prefix"></i>
																<input type="number" name="phone" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
																type = "number"
																maxlength = "12"
																class="form-control form-control-sm validate" placeholder="Your Phone" value="<?php echo e(old('phone')); ?>">
																
															</div>
					
															<div class="md-form form-sm mb-1">
																<i class="fas fa-envelope prefix"></i>
																<input type="email" id="email" name="email"
																class="form-control form-control-sm validate" placeholder="Your email" required value="<?php echo e(old('email')); ?>">
																
															</div>
					
															<div class="md-form form-sm mb-1">
																<i class="fas fa-lock prefix"></i>
																<input type="password" name="password"
																class="form-control form-control-sm validate" placeholder="Your password">
																
															</div>
					
															<div class="md-form form-sm mb-1">
																<i class="fas fa-lock prefix"></i>
																<input type="password"  name="password_confirmation"
																class="form-control form-control-sm validate" placeholder="Repeat password">
																
															</div>
					
															<div class="text-center form-sm mt-2">
																<button class="btn btn-info" type="submit">Sign up <i
																	class="fas fa-sign-in ml-1"></i></button>
																</div>
															</form>
					
													</div>
													<!--Footer-->
													<div class="modal-footer" style="padding-right:1rem !important;padding-left:1rem !important">
					
														<button type="button"
														class="btn btn-outline-info waves-effect ml-auto"
														data-dismiss="modal">Close</button>
													</div>
												</div>
													<!--/.Panel 8-->
											</div>
					
										</div>
									</div>
									<!--/.Content-->
								</div>
							</div>
							<?php if(Auth::guard('customerForWeb')->check() ): ?>
                            	<a href="<?php echo e(url(app()->getLocale().'/get_wishlist')); ?>" class="icon-btn has-badge bg3 ms-3"><i style="font-size: 21px !important" class="fal fa-heart"></i><span id="wishlist-count"  class="badge wishlist-total-count"><?php echo e(count(auth()->guard('customerForWeb')->user()->wishlist->unique('barcode')->values() )); ?></span></a>
							<?php endif; ?>
							<?php if(Auth::guard('customerForWeb')->check() ): ?>
							<a href="<?php echo e(route('cart', app()->getLocale())); ?>" class="icon-btn has-badge bg2 ms-2 "><i class="fal fa-shopping-cart"></i><span id="cart-count2" class="badge total-count cart-count2"><?php echo e(count(auth()->guard('customerForWeb')->user()->cart->where($stockId,1)->unique('barcode')->values() )); ?></span></a>

					<a style="display: block !important;float: right;margin-left: 15px !important;" class="icon-btn has-badge bg2 ms-2 " href="<?php echo e(url(app()->getLocale().'/all_promotions')); ?>"><img  src="<?php echo e(asset('images/promotions.svg')); ?>" alt=""></a>					
							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </header>
	<div class="bg-theme  sticky-header " >
		<div class="container ">
			<div class="position-relative">
				<h1 class="close-search" style="display: none;">x</h1>
				<div class="text-body2 d-lg-none d-md-block fl-left" id="icons-header" style="margin: 2.5% 0;display: flex ;align-items: center;">
					<span class="icon-btn bg3"><a   style="display: block !important" href="<?php echo e(Auth::guard('customerForWeb')->user() ? url(app()->getLocale().'/profile') : route('login',$lang)); ?>"><i class="fal fa-user" ></i></a></span>
					<ul style="display: none">
						<?php if(Auth::guard('customerForWeb')->check() ): ?>
							<li><a id="account" href="<?php echo e(url(app()->getLocale().'/profile')); ?>"><?php echo e(__('lang.my_account')); ?></a></li>
						<?php else: ?>
							<li><a href="<?php echo e(route('login',$lang)); ?>" >Login</a></li>
							<li><a href="<?php echo e(route('register',$lang)); ?>">Register</a></li>
						<?php endif; ?>

					</ul>
					<div class="modal fade" id="modalLRFormmobile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="    z-index: 999999;" aria-hidden="true">
						<div class="modal-dialog cascading-modal" role="document">
							<!--Content-->
							<div class="modal-content">
			
								<!--Modal cascading tabs-->
								<div class="modal-c-tabs">
			
									<!-- Nav tabs -->
									<ul class="nav nav-tabs md-tabs tabs-2  darken-3" role="tablist">
										<li class="nav-item" >
											<a class="nav-link active" id="active"  data-toggle="tab" href="#panel7mobile"
											role="tab"><i class="fas fa-user mr-1"></i>
											<?php echo e(__('lang.login')); ?></a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#panel8mobile" role="tab"><i
												class="fas fa-user-plus mr-1"></i>
											<?php echo e(__('lang.register')); ?></a>
										</li>
										
									</ul>
			
									<!-- Tab panels -->
									<div class="tab-content">
										
			
										<!--Panel 7-->
										<div class="tab-pane fade in show active" id="panel7mobile" role="tabpanel">
			
											<!--Body-->
											<div class="modal-body mb-1">
												<form action="<?php echo e(route('web.login', app()->getLocale())); ?>" method="post" class="needs-validation" novalidate>
													<?php echo e(csrf_field()); ?>

													<div class="md-form form-sm ">
														<i class="fas fa-envelope prefix"></i>
														<input type="email" id="validationCustom032" name="email" placeholder="Your email"
														class="form-control form-control-sm validate" required>
			
														
													</div>
			
													<div class="md-form form-sm mb-4">
														<i class="fas fa-lock prefix"></i>
														<input type="password" id="modalLRInput11" name="password" placeholder="Your password"
														class="form-control form-control-sm validate" required>
			
														
													</div>
													<div class="text-center form-sm mt-2">
														<button class="btn btn-info" type="submit"><?php echo e(__('lang.login')); ?> <i
															class="fas fa-sign-in ml-1"></i></button>
														</div>
												</form>
											</div>
											<!--Footer-->
											<div class="modal-footer">
												<div class="options text-center text-md-right mt-1">
													<p> <a href="<?php echo e(route('web.forgot_password', app()->getLocale())); ?>" class="blue-text"><?php echo e(__('lang.forget_password')); ?>?</a></p>
												</div>
												<button type="button" style="padding: 10px"
												class="btn btn-outline-info waves-effect ml-auto"
												data-dismiss="modal"><?php echo e(__('lang.close')); ?></button>
											</div>
			
										</div>
											<!--/.Panel 7-->
			
											<!--Panel 8-->
										<div class="tab-pane fade" id="panel8mobile" role="tabpanel">
			
											<!--Body-->
											<div class="modal-body">
												<form action="<?php echo e(route('web.register', app()->getLocale())); ?>" method="post" class="needs-validation" novalidate>
													<?php echo e(csrf_field()); ?>

													<div class="md-form form-sm mb-1">
														<i class="fas fa-user prefix"></i>
														<input type="text" name="name"
														class="form-control form-control-sm validate" placeholder="Your Name" value="<?php echo e(old('name')); ?>">
														
													</div>
													<div class="md-form form-sm mb-1">
														<i class="fas fa-mobile prefix"></i>
														<input type="number" name="phone" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
														type = "number"
														maxlength = "12"
														class="form-control form-control-sm validate" placeholder="Your Phone" value="<?php echo e(old('phone')); ?>">
														
													</div>
			
													<div class="md-form form-sm mb-1">
														<i class="fas fa-envelope prefix"></i>
														<input type="email" id="email" name="email"
														class="form-control form-control-sm validate" placeholder="Your email" required value="<?php echo e(old('email')); ?>">
														
													</div>
			
													<div class="md-form form-sm mb-1">
														<i class="fas fa-lock prefix"></i>
														<input type="password" name="password"
														class="form-control form-control-sm validate" placeholder="Your password">
														
													</div>
			
													<div class="md-form form-sm mb-1">
														<i class="fas fa-lock prefix"></i>
														<input type="password"  name="password_confirmation"
														class="form-control form-control-sm validate" placeholder="Repeat password">
														
													</div>
			
													<div class="text-center form-sm mt-2">
														<button class="btn btn-info" type="submit">Sign up <i
															class="fas fa-sign-in ml-1"></i></button>
														</div>
													</form>
			
											</div>
											<!--Footer-->
											<div class="modal-footer" style="padding-right:1rem !important;padding-left:1rem !important">
			
												<button type="button"
												class="btn btn-outline-info waves-effect ml-auto"
												data-dismiss="modal">Close</button>
											</div>
										</div>
											<!--/.Panel 8-->
									</div>
			
								</div>
							</div>
							<!--/.Content-->
						</div>
					</div>
					<?php if(Auth::guard('customerForWeb')->check() ): ?>
						<a href="<?php echo e(url(app()->getLocale().'/get_wishlist')); ?>" class="icon-btn has-badge bg3 mx-2"><i style="font-size: 21px !important" class="fal fa-heart"></i><span id="wishlist-count"  class="badge wishlist-total-count"><?php echo e(count(auth()->guard('customerForWeb')->user()->wishlist->unique('barcode')->values())); ?></span></a>
						<a href="<?php echo e(route('cart', app()->getLocale())); ?>" class="icon-btn has-badge bg2 ms-2 "><i class="fal fa-shopping-cart" style="color: var(--body-color2)"></i><span id="cart-count2" class="badge total-count cart-count2"><?php echo e(count(auth()->guard('customerForWeb')->user()->cart->where($stockId,1)->unique('barcode')->values())); ?></span></a>

					<?php endif; ?>
					<a style="color: #fff;margin-left:20px;line-height:2;padding:3px" class="icon-btn has-badge bg2 ms-2 " href="<?php echo e(url(app()->getLocale().'/all_promotions')); ?>"><img  src="<?php echo e(asset('images/promotions.svg')); ?>" alt=""></a>					
				</div>
				<div class="col-lg-auto fl-right" id="search-header" >
					<form action="<?php echo e(route('search',app()->getLocale())); ?>" method="GET" class="header-search style2">
						<input type="text" name="search" placeholder="<?php echo e(__('lang.search_here')); ?>" class="form-control palceholder-search pleft-5" style="">
						<button type="submit" class="icon-btn "  ><i class="far fa-search" ></i></button>

					</form>
					<button class="icon-btn search-btn-mobile-view" style="display: none ;background:#ccc;width: 30px; height:30px" ><i class="far fa-search" style="margin: 0 auto"></i></button>
				</div>
				<div class="col-auto fl-left">
					
					<nav class="main-menu menu-style3 mobile-menu-active menu-style2 text-white">
						<ul>
							
							<li class="">
								<a href="#"><i class="fas fa-bars mr-2"> </i>  <?php echo e(__('lang.Categories')); ?></a>
								<ul class="sub-menu" style="z-index: 99 !important">
									
									<?php if(isset($main_categories)): ?>
									

									
									<?php $__currentLoopData = $main_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php
										$i+=1;
									?>
									
									<li class=" menu-item-has-children">
										<a style="" href="<?php echo e(url( app()->getLocale().'/homeWeb/all_products/'. $category->id )); ?>"> <img style="    margin: 0 8px 0 0 !important; display:inline !important" src="<?php echo e(asset($category->icon_image )); ?>" width="20px" alt="" > <?php echo e($category->name->$lang); ?> </a>
										
										<?php $__currentLoopData = $category->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub1_child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										
											<?php if(isset($sub1_child->children) ): ?>
												<ul class="mega-menu-wrap sub-menu">
													<li class="menu-item-has-children">
														<div class="box-nav-popup" data-bg-src="assets/img/bg/nav-bg-1.png">
															<div class="row" id="removeRow" style="margin: 0">
																<ul style="    margin: 0 0 15px;">
																	<li style="text-decoration: underline;padding:0;border:0" ><a href="<?php echo e(url( app()->getLocale().'/homeWeb/all_products/'. $category->id )); ?>"><?php echo e(__('lang.Show-all')); ?></a> </li>

																</ul>

																<?php $__currentLoopData = $category->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub1_child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																	<?php if(count($sub1_child->children) > 1): ?>
																		<ul class="col-md-12 col-lg-4  ">
																			
																			<div class="widget widget_nav_menu">
																				<h3 class="widget-title" style="align-items: center;   "><a href="<?php echo e(url( app()->getLocale().'/homeWeb/all_products/'. $sub1_child->id )); ?>" style=" display: inline;">  <?php echo e($sub1_child->name->$lang); ?> </a> <i class="fa fa-plus  show_sub" style="padding: 0 2px 0;font-size: 10px !important;" ></i> </h3>
																				<div class="vs-box-nav" style="display: none">
																					<ul>
																						<li ><a href="<?php echo e(url( app()->getLocale().'/homeWeb/all_products/'. $sub1_child->id )); ?>"><?php echo e(__('lang.Show-all')); ?></a> </li>
																						<?php $__currentLoopData = $sub1_child->children; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub2_child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																							
																							<?php if(count($sub2_child->children) > 1): ?>
																							<li >
																								<a href="<?php echo e(url( app()->getLocale().'/homeWeb/all_products/'. $sub2_child->id )); ?>"><?php echo e($sub2_child->name->$lang); ?> </a></li>
																							</li>
																							 <?php else: ?>
																							<li ><a href="<?php echo e(url( app()->getLocale().'/homeWeb/all_products/'.$sub2_child->id)); ?>"><?php echo e($sub2_child->name->$lang); ?></a></li>
																							<?php endif; ?> 
																						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																					</ul>
																				</div>
																			</div>
																		</ul>
																	<?php else: ?>
																		<ul class="col-md-12 col-lg-4 ">
																			<div class="widget widget_nav_menu">
																				<h3 class="widget-title" style="align-items: center;   "><a href="<?php echo e(url( app()->getLocale().'/homeWeb/all_products/'. $sub1_child->id )); ?>" style=" display: inline;">  <?php echo e($sub1_child->name->$lang); ?> </a>
																					
																				</h3>
																				<div class="vs-box-nav" style="display: none">
																					
																				</div>
																				
																				
																			</div>
																		</ul>
																	<?php endif; ?> 
																<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
																
															</div>
															 
														</div>
													</li>
												</ul>
											
												
											<?php endif; ?>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
								</ul>
							</li>
							
						</ul>
					</nav>
				</div>
			   
			</div>
		</div>
	</div>

	<?php if(session('reset')): ?>

	<div id="alert-reset" class="alert alert-success" style="position: fixed ;z-index:9999" >

		<p>Check Your Email.</p>
		
	</div>


	<?php endif; ?>
	<?php if(session('email')): ?>

	<div id="alert-reset" class="alert alert-success" style="position: fixed ;z-index:9999" >

		<p> <?php echo e(Session::get('email')); ?></p>
		
	</div>


	<?php endif; ?>

	
	<div id="alert-cart" class="alert alert-success" style="position: fixed ;display:none;z-index:9999" ></div>
	<div id="alert-wishlist" class="alert alert-danger" style="position: fixed ;display:none;z-index:9999" ></div>
	<?php echo $__env->yieldContent('content'); ?>



    <!--==============================
			Footer Area
	==============================-->
    <footer style="text-align:<?php echo e($lang == 'ar' ? 'right' : 'left'); ?>" class="footer-wrapper footer-layout1 pt-5" data-bg-src="<?php echo e(asset('assets/img/shape/footer-4-bg.jpg')); ?>">
        <div class="container">
			
			<div class="widget-area">
                <div class="row align-items-start justify-content-between">
                    <div class="col-sm-6 col-lg-4 col-xl-4">
                        <div class="widget  ">
                            <div class="vs-widget-about">
                                <div class="widget-about-logo mb-30">
                                    <a href="<?php echo e(url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id)); ?>" id="home_link">
                                        <img src="<?php echo e(asset($logo)); ?>" alt="Oscar" width="170">
                                    </a>
                                </div>
                                <p class="widget-about-text mb-20"><?php echo e(__('lang.welcome')); ?></p>
                                <div class="social-btn">
                                    <ul>
                                        <li><a href="https://facebook.com/oscargrandstores/"><i class="fab fa-facebook-f"></i></a></li>
                                        
                                        <li><a href="https://instagram.com/oscargrandstores"><i class="fab fa-instagram"></i></a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 col-xl-2">
                        <div class="widget widget_nav_menu  footer-widget  ">
                            <h3 class="widget_title"><?php echo e(__('lang.Useful Links')); ?></h3>
                            <div class="menu-all-pages-container">
                                <ul class="menu">
                                    <li><a href="<?php echo e(url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id)); ?>"><?php echo e(__('lang.Home')); ?></a></li>
                                    <li > <a href="<?php echo e(url(app()->getLocale().'/categoties')); ?>"  data-content="Link Hover" aria-hidden="true"><?php echo e(__('lang.Categories')); ?></a></li>
									
									<li><a href="<?php echo e(url (app()->getLocale().'/aboutus')); ?>"  data-content="Link Hover" aria-hidden="true"><?php echo e(__('lang.about_us')); ?></a></li>
									<li ><a href="<?php echo e(url(app()->getLocale().'/contact')); ?>"><?php echo e(__('lang.contact_us')); ?></a></li>
                                    <li><a href="<?php echo e(url (app()->getLocale().'/return_policy')); ?>" ><?php echo e(__('lang.Refund & Returns Policy')); ?></a></li>
                                    <li><a href="<?php echo e(url (app()->getLocale().'/privacy_policy')); ?>" ><?php echo e(__('lang.Privacy Policy')); ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 col-xl-2">
                        <div class="widget  footer-widget  ">
                            <h3 class="widget_title"><?php echo e(__('lang.Download App')); ?></h3>
                            <div class="footer-social-links">
                                <ul>
                                    <li><a href="https://play.google.com/store/apps/details?id=com.msolapps.oscar"><img src="<?php echo e(asset('android.png')); ?>" alt=""></a></li>
                                    <li><a href="https://apps.apple.com/eg/app/oscar-grand-stores/id1474983542"><img style="width: 152px !important;height: 47px;" src="<?php echo e(asset('apple.png')); ?>" alt=""></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4 col-xl-3">
                        <div class="widget footer-widget  ">
                            <h3 class="widget_title"><?php echo e(__('lang.contact_us')); ?></h3>
                            <div class="vs-widget-about">
                                <!-- <p class="fs-md"><?php echo e(__('lang.Address: 105 Omar Ibn El Khatab street, Heliopolis, Cairo')); ?>.</p> -->
                                <p class="mb-1 link-inherit"><i class="fas fa-paper-plane me-3"></i><a href="mailto:info@oscargrandstores.com"> info@oscargrandstores.com</a></p>
                                <p class="mb-0 link-inherit"><i class="fal fa-fax me-3"></i><a href="tel:16991"><?php echo e(__('lang.Hotline')); ?> <?php echo e(__('lang.16991')); ?></a></p>
								<p class="mb-0 link-inherit"><i class="fab fa-whatsapp me-3"></i><a href="https://wa.me/+201050403443"><?php echo e(__("lang.whatsapp")); ?></a></p>
                                <!-- <p class="fs-md" style="margin:0"><?php echo e(__('lang.Got Question? Call us')); ?> 24/7</p> -->
                                <p class="fs-md"><?php echo e(__("lang.open")); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright text-center pb-35">
                <p class="mb-0 link-inherit font-theme">&copy; <?php echo e(__('lang.2021')); ?> <a href="<?php echo e(url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id)); ?>">Oscar </a><?php echo e(__('lang.By')); ?>  <a href="http://momentum-sol.com/"> Momentum Solutions. </a><?php echo e(__('lang.All Rights Reserved')); ?> .</p>
            </div>
        </div>
    </footer>
				



<!-- Jquery -->
<script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery-migrate-3.0.0.js')); ?>"></script>
<script src="<?php echo e(asset('js/jquery-ui.min.js')); ?>"></script>
<!-- Popper JS -->
<script src="<?php echo e(asset('js/popper.min.js')); ?>"></script>
<!-- Bootstrap JS -->
<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>


 <!--==============================
        All Js File
    ============================== -->
    <!-- Jquery -->
    <script src="<?php echo e(asset('assets/js/vendor/jquery-1.12.4.min.js')); ?>"></script>
    <!-- Slick Slider -->
    <script src="<?php echo e(asset('assets/js/slick.min.js')); ?>"></script>
    <!-- Layerslider -->
    <script src="<?php echo e(asset('assets/js/layerslider.utils.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/layerslider.transitions.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/layerslider.kreaturamedia.jquery.js')); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo e(asset('assets/js/bootstrap.min.js')); ?>"></script>
    <!-- Date Picker -->
    <script src="<?php echo e(asset('assets/js/jquery.datetimepicker.min.js')); ?>"></script>
    <!-- Filter -->
    <script src="<?php echo e(asset('assets/js/imagesloaded.pkgd.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/isotope.pkgd.min.js')); ?>"></script>
    <!-- Magnific Popup -->
    <script src="<?php echo e(asset('assets/js/jquery.magnific-popup.min.js')); ?>"></script>
    <!-- Jquery UI -->
    <script src="<?php echo e(asset('assets/js/jquery-ui.min.js')); ?>"></script>
    <!-- WOW JS (Animation JS) -->
    <script src="<?php echo e(asset('assets/js/wow.min.js')); ?>"></script>
    <!-- Custom Carousel -->
    <script src="<?php echo e(asset('assets/js/vscustom-carousel.min.js')); ?>"></script>
    <!-- Mobile Menu -->
    <script src="<?php echo e(asset('assets/js/vsmenu.min.js')); ?>"></script>
    <!-- Form Js -->
    <script src="<?php echo e(asset('assets/js/ajax-mail.js')); ?>"></script>
    <!-- Main Js File -->
    <script src="<?php echo e(asset('assets/js/main.js')); ?>"></script>


<script>

$(document).scroll(function(){
        // if($(this).scrollTop() >= 50) {
		// 	$('.bg-theme').css('top',0)
        // } else{
		// 	$('.bg-theme').css('top',124)

		// }
		// if(screen.width <=400){
		// 	// $('.sticky-header-wrap').addClass('active')
		// 	if($(this).scrollTop() >= 50) {
		// 	$('.bg-theme').css('top',0)
		// 	} else{
		// 		$('.bg-theme').css('top',120)

		// 	}
		// }

    });

	$('#ToggleBTN').click(function(){
		// $('.vs-mobile-menu ul .menu-item-has-children').click();
		$('.vs-mean-expand').first().hide();
		// $('.vs-mobile-menu ul li').next('.vs-mean-expand').click()
		// $('.vs-submenu').show();
		$('.vs-mean-expand').first().click();

		// var link=$('#home_link').attr('href');
		// var home='<span style="font-size:18px ; margin-left:33px"><a style="    margin-left: 32px !important;" href="'+link+'" >Home</a></span>';
		// $('.vs-mobile-menu').append(home);
	})
let=url='';


	$('.show_sub').click(function(){
		if($(this).hasClass('fa-plus')){
			$(this).removeClass('fa-plus');
			$(this).addClass('fa-minus');
			$(this).parent().next().css('display','block');

		}else{
			$(this).removeClass('fa-minus');
			$(this).addClass('fa-plus');
			$(this).parent().next().css('display','none');
		}
	})

	// var link=$('#home_link').attr('href');
	
	// var home='<li style="font-size:18px"><a style="    margin-left: 32px !important;" href="'+link+'" >Home</a><a/li>';
	// $('.vs-mobile-menu ul').append(home);
	// $(document).scroll(function() {


	// var y = $(this).scrollTop();
	// if (y > 50) {
	// 	$('.slicknav_nav').fadeOut();
	// } else {
	// 	$('.slicknav_nav').fadeIn();
	// }
	// });

	function set_branch(){
			
		var cart =[];
		localStorage.setItem('cart', JSON.stringify(cart));
	}

	$( document ).ready(function() {

		$('body').on('hidden.bs.modal', function () {

			$('.input-number').val(1);
		});
		var storage=JSON.parse(localStorage.getItem("cart"));
		var  cartArray=[];
var cartArray=<?php echo json_encode($auth); ?>;
	

	
		

		if(storage == null ){
			var cart =[];
		localStorage.setItem('cart', JSON.stringify(cart));
		}
		if(storage ==null){
			
			$('.cart-count2').css('background', 'transparent');
			$('.cart-count2').hide();
			// $('#cart-count1').css('background', 'transparent');
			// $('#cart-count1').hide();

		}else{
			$('.cart-count2').html(cartArray.length);
			// $('#cart-count1').html(storage.length);


		}

			// $('#cartandwishlist').hide()

			if ($(window).width() < 442) {
				$('#logout').hide();
			}
			if($(window).width() <402){
				$('#account').hide();
			}
			// if($(window).width() <768){
				
			// 	$('#cartandwishlist').show()
			// }
			getLocation();
			// var x = document.getElementById("demo");
			function getLocation() {
			if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(showPosition);


			} 
			// else {
			// 	x.innerHTML = "Geolocation is not supported by this browser.";
			// }

			}
			function showPosition(position) {
				setCookie(position.coords.latitude,position.coords.longitude);

			}			
			function setCookie(lat, long, exdays) {
				var d = new Date();
				d.setTime(d.getTime() + (exdays*24*60*60*1000));
				var expires = "expires="+ d.toUTCString();
				document.cookie = 'lat' + "=" + lat + ";" + expires + ";path=/";
				document.cookie = 'long' + "=" + long + ";" + expires + ";path=/";
			}


		});
	$( document ).ready(function() {

		setTimeout(function() { 
			$('#error').fadeOut('fast'); 
		}, 6000);
		setTimeout(function() { 
			$('#alert-reset').fadeOut('fast'); 
		}, 6000);

		
	});
</script>
<script>
	
	if(screen.width <=991)
		{
			// $('#removeRow').removeClass('row');
			$('#removeRow ul').each(function(){
				$(this).css('width' ,'100%')
			})
		}
		if(screen.width <=445){

			$('.bg-theme ').addClass('sticky');
			$('.sticky-header').removeClass('sticky-header');
			$('.sticky').css({
				'position':"sticky",
				'top':'0',
				'z-index':'99999'
			});
			// var y = $(window).scrollTop();
  			// if (y >100) {
			// 	$('.sticky').css('top','18px')
			// }else{
			// 	$('.sticky').css('top','0')
			// } 
			
			// $('.links .row').css({
			// 	'width':'90%',
			// 	'margin': '6px'
			// });
			// $('.pagination').addClass('col-9')
			// $('.pagination li').css('width':'12%')
			$('.vs-hero-wrapper ').css('margin-top:50px')

			// if($(this).scrollTop() >= 50) {
			// 	$('.bg-theme').css('top','50px')
        	// }
			// $('.bg-theme').css({
			// 	'position': 'fixed','width': '100%'
			// })
			$('.header-search').hide();
			$('.search-btn-mobile-view').show();
			$('#search-header').css({'margin':'2.5% 0' })
		}else{
			$('.header-search').show();
			$('.search-btn-mobile-view').hide();
		}

		$('.search-btn-mobile-view').click(function(){
			$(this).hide();
			$('.close-search').show();
			$('.header-search').show();
			$('#search-header').css('margin','0');
			$('.position-relative .text-body2').hide()
			$('#search-header').css({'margin':'0', 'width':'70%' })
			$('#icons-header').attr("style", "display: none !important");

		});
		if(screen.width <=445){
		$('.close-search').click(function(e){
			$('.close-search').hide();
			

				$('.position-relative .text-body2').show()

				$('.header-search').hide();
				$('.search-btn-mobile-view').show()
				$('#search-header').css({'margin':'2.5% 0','width':'auto' })
				$('#icons-header').attr("style", "display: flex !important")
				$('.text-body2').css('margin-top','10px')
			
		});
	}


	if(screen.width <=350){
		$('.text-body2 .icon-btn').css({
			'width':'30px',
			'height':'30px',
		});
		$('.text-body2 .icon-btn i').css({
			'line-height':'1.5',
		});
		$('.text-body2').css('margin','3% 0');
		$('.bg-theme').css('height','47px');
		$('.header-search').css('height','30px');
		$('.header-search button').css({
			'width':'30px',
			'height':'30px',
		});

		$('.fa-search').css({
			'line-height':'1.5',
		});

	}	
	if(screen.width <=400){
			// $('.sticky-header-wrap').css('opacity',1)
	}
</script>

<?php echo $__env->yieldPushContent('scripts'); ?>
</body>



</html>
