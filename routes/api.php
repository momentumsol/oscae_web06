<?php

use Illuminate\Http\Request;
use Rainwater\Active\Active;
include('oscar_api.php');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::post('/verify_user', function (Request $request) {
	$url = 'http://41.33.238.100/RR_OSCAR/RR_Services.asmx/VerifyUser';
	$body = [
		'Email' => $request->get('email'),
		'Phone' => $request->get('phone'),
		'CardNumber' => $request->get('cardnumber'),
	];

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($body));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec($ch);
	curl_close($ch);

	return $server_output;
});

Route::post('/check_points', function (Request $request) {
	$url = 'http://41.33.238.100/RR_OSCAR/RR_Services.asmx/CheckPoints';
	$body = [
		'FromDate' => $request->get('FromDate'),
		'ToDate' => $request->get('ToDate'),
		'CardNumber' => $request->get('CardNumber'),
		'Token' => $request->get('Token'),
	];

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($body));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec($ch);
	curl_close($ch);

	return $server_output;
});

Route::post('/update_avatar', function (Request $request) {
	if ($request->has('avatar')) {
		$file = $request->file('avatar');
		if (is_file($file)) {
			$file = \Storage::put('public/avatars', $file, 'public');
			$file = url(\Storage::url($file));
		}
	}
	$result = [
		'file' => $file,
		'user' => json_decode($request->get('user', '')),
		'data' => $request->all()
	];
	\Log::info(json_encode($result));
	return response()->json($result);
});

Route::post('/login', 'Auth\LoginController@login')->name('login');

Route::middleware('jwt.auth')->post('/send_pn', function (Request $request) {
	$request->validate([
		'card' => 'required',
		'balance' => 'required|integer',
		'transaction' => 'required'
	]);
	$result = [
		'success' => true,
		'data' => $request->all(),
		'message' => 'Message sent successfully'
	];
	return response()->json($result);
});

Route::middleware('jwt.auth')->get('/user', function (Request $request) {
	return $request->user();
});

// Route::group(['middleware' => ['jwt.auth']], function () {
Route::resource('users', 'UserAPIController');
Route::resource('roles', 'RoleAPIController');
Route::resource('permissions', 'PermissionAPIController');
Route::resource('page_items', 'PageItemAPIController');
Route::get('qrCode/{code}', '\App\Http\Controllers\HomeController@readQrcode')->name('qrCode.show');
Route::resource('devices', 'DeviceAPIController');

// });

Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
	Route::resource('page_types', 'PageTypeAPIController');
	Route::resource('pages', 'PageAPIController');
	Route::resource('categories', 'CategoryAPIController');
	Route::resource('page_contents', 'PageContentAPIController');
	Route::resource('restaurants', 'RestaurantAPIController');
	Route::resource('promotions', 'PromotionAPIController');
	Route::resource('on_boardings', 'OnBoardingAPIController');
	Route::resource('on_boarding_values', 'OnBoardingValueAPIController');
	
});

Route::resource('contact_forms', 'ContactFormAPIController');
Route::resource('branches', 'BranchAPIController');
Route::resource('kitchens', 'KitchenAPIController');
Route::resource('promo_videos', 'PromoVideoAPIController');
Route::post('promo_videos/{promoVideo}/watch', 'PromoVideoAPIController@watch')->name('promoVideos.watch');
Route::resource('imageSlider', 'imageSliderApiController');



Route::post('customer/register', 'APIRegisterController@register');
Route::post('customer/login', 'APIRegisterController@login');
Route::post('customer/forgot_password', 'APIForgotPasswordController@forgot_password');

Route::group(['middleware' => 'auth:customers'], function () {

	Route::get('customer/logout', 'APIRegisterController@logout');
	Route::post('/address/add', 'AddressController@add')->middleware('verified');
	Route::post('/address/update/{id}', 'AddressController@update')->middleware('verified');
	Route::post('/address/delete/{id}', 'AddressController@delete')->middleware('verified');
	Route::get('/address/get', 'AddressController@get')->middleware('verified');
	Route::post('/devices/addDevice', 'DeviceAPIController@addDevice')->middleware('verified');
	Route::get('/customers', 'APIRegisterController@getCustomer')->middleware('verified');
	Route::post('/orders', 'APIOrderController@saveOrder')->middleware('verified');
	Route::get('/orders/get', 'APIOrderController@getOrder')->middleware('verified');
	Route::post('get_messages', 'ChatsController@fetchMessages');
	Route::post('messages', 'ChatsController@sendMessage');
	Route::post('messages_client', 'ChatsController@messages_client');

   Route::post('online_now', 'ChatsController@online_now');
   Route::get('get_users', 'ChatsController@get_users');
   Route::post('end_chat', 'ChatsController@end_chat');
   Route::get('get_online', 'ChatsController@get_online');
   Route::post('get_channelid', 'ChatsController@get_channelid');
   Route::post('get_client_name', 'ChatsController@get_client_name');
   Route::post('get_client_info', 'ChatsController@get_client_info');

   Route::post('leaving_user', 'ChatsController@leaving_user');
   Route::post('change_status', 'ChatsController@change_status');
   
   Route::post('/broadcasting/auth', function (Request $request) {
	$app_id = '1034097';
	$app_key ='e61957c04b0a82143f57';
	$app_secret ='8db3ddc52e699c1d8d7f';
	$app_cluster = 'eu';
	$pusher = new Pusher\Pusher( $app_key, $app_secret, $app_id, array('cluster' => $app_cluster) );
	$auth = $pusher->presence_auth($request->get('channel_name'),$request->get('socket_id'),\Auth::guard('customers')->user()->id,\Auth::guard('customers')->user());
	return response($auth, 200)->header('Content-Type', 'application/json');
});
  
});


