<?php
use App\Models\Branch;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Product;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Use App\Models\Customer;
// Route::get('/', 'HomeController@index');
// Route::get('/log_out_chat/{id}', 'LoginChatController@logout');
// Route::get('/customer/verify/{token}', 'API\APIRegisterController@verifyCustomer')->name('customer.verify');
// Route::get('/customer/reset/{token}', 'API\APIForgotPasswordController@showForgetForm');
// Route::post('/customer/forget', 'API\APIForgotPasswordController@forget_password_save');

// Route::get('login_chat', 'LoginChatController@index');
// Route::post('login_chat', 'LoginChatController@login');
// Route::get('chat/{id}/{customer}/{client_id}', function() {
// $customer=Customer::find( Route::current()->customer);

// $token = auth('customers')->login($customer);
// 			return view('chat.users_chat');
// 		});
// 		Route::get('users_chat/{id}', function() {
// 			$customer=Customer::find( Route::current()->id);
	
// 			$token = auth('customers')->login($customer);
// 						return view('chat.users_chat');
// 					});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::post('/sendPN', 'HomeController@sendPN');
// Route::post('/upload', 'MediaController@upload');
// Route::get('import', 'PromotionController@import');
// Route::post('import', 'PromotionController@import');
// Route::get('cache_clear', 'ProductController@cache');


// Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
// 	Route::resource('pages', 'PageController');
// 	Route::resource('categories', 'CategoryController');
// 	Route::resource('pageContents', 'PageContentController');
// 	Route::resource('restaurants', 'RestaurantController');
// 	Route::resource('promotions', 'PromotionController');
// 	Route::delete('promotions_all', 'PromotionController@destroyAll')->name('promotions.destroy_all');
// 	Route::resource('onBoardings', 'OnBoardingController');
// 	Route::resource('onBoardingValues', 'OnBoardingValueController');
// });

// Route::resource('users', 'UserController');
// Route::resource('roles', 'RoleController');
// Route::resource('permissions', 'PermissionController');
// Route::resource('pageTypes', 'PageTypeController');
// Route::resource('pageItems', 'PageItemController');
// Route::get('productCategories/import', 'ProductCategoryController@import');
// Route::post('productCategories/import', 'ProductCategoryController@import');
// Route::any('/productCategories/selectCategory', 'ProductCategoryController@selectCategory')->name('productCategories.selectCategory');

// Route::any('/productCategories/syncAPI', 'ProductCategoryController@syncAPI')->name('productCategories.syncAPI');
// Route::resource('productCategories', 'ProductCategoryController');
// Route::resource('notifications', 'NotificationController');
// Route::resource('imageSlider', 'ImageSliderController');
// Route::resource('customerService', 'CustomerServiceController');
// Route::get('products/import', 'ProductController@import');
// Route::post('products/import', 'ProductController@import');
// Route::get('/downloadProductSample', 'ProductController@getDownload');
// Route::resource('products', 'ProductController');
// Route::post('products/search', 'ProductController@search');
// Route::any('/products/syncAPI', 'ProductController@syncAPI')->name('products.syncAPI');
// Route::post('/products/selectImages', 'ProductController@selectImages')->name('products.selectImages');
// Route::get('/products/selectImages/{filter}', 'ProductController@getselectImages')->name('products.selectImages');

// Route::get('qrCode', 'HomeController@qrcode')->name('qrCode.index');

// Route::resource('devices', 'DeviceController');
// Route::resource('contactForms', 'ContactFormController');
// Route::resource('media', 'MediaController');
// Route::resource('orders', 'OrderController');
// Route::post('/order/status', 'OrderController@changeStatus')->name('order.status');
// // MediaManager
// // ctf0\MediaManager\MediaRoutes::routes();

// Route::resource('branches', 'BranchController');
// Route::resource('kitchens', 'KitchenController');
// Route::resource('promoVideos', 'PromoVideoController');
// Route::post('promoVideos/{promoVideo}/activate', 'PromoVideoController@activate')->name('promoVideos.activate');

// Route::get('uploads', function() {
// 	$files = \Storage::allFiles("public/media");
// 	return view('uploads.index')->with('files', $files);
// })->name('uploads.index');

// // Auth::routes(['verify' => true]);


// Route::get('lang/{lang}', function ($lang) {
//     App::setlocale($lang);

//     return redirect()->back();
// });
Route::get('/', function () {

    return redirect('/en/homeWeb');
});
Route::get('/homeWeb', function () {

    return redirect(app()->getLocale());
});
Route::post('forgot', 'Web\LoginController@forgot')->name('web.forgot');

Route::group(['prefix' => '{locale}','namespace'=>'Web'  ,'where' => ['locale' => '[a-zA-Z]{2}'], 
'middleware' => 'setlocale'],function(){

	Route::get('homeWeb/','HomeController@redirect')->name('homeWeb');
	Route::any('homeWeb/search_product','HomeController@search')->name('search');
	Route::resource('homeWeb','HomeController',['except'=> ['index']]);
	Route::get('homeWeb/branch/{id}','HomeController@index')->name('homeWeb.index');
	Route::get('/show_product/{id}/{catId}', 'HomeController@show_product');

    Route::get('/show_product/{id}', 'HomeController@show_product_mobile');


	Route::post('homeWeb/wishList','HomeController@wishList');
	Route::post('homeWeb/unLike','HomeController@unLike');
	Route::get('homeWeb/all_products/{slug}','HomeController@showallproducts');
	Route::get('homeWeb/show/{slug}','HomeController@show');
	Route::post('login','LoginController@login')->name('web.login');
	Route::post('register','RegisterController@register')->name('web.register');
	Route::get('/customer/verify/{token}', 'RegisterController@verifyCustomer')->name('customer.verify');
	Route::post('logout','LoginController@logout')->name('web.logout');
	Route::get('forgot_password', 'LoginController@forgot_password')->name('web.forgot_password');

	Route::get('cart', 'HomeController@cart')->name('cart');
	Route::get('login', 'HomeController@login')->name('login');
	Route::get('register', 'HomeController@register')->name('register');
	Route::post('homeWeb/add-to-cart', 'HomeController@addToCart');
    Route::post('homeWeb/addToCart', 'CartController@add_cart');
    Route::post('homeWeb/removeFromCart', 'CartController@remove_cart');

	Route::get('checkout', 'HomeController@checkout')->name('checkout');
	Route::get('contact', 'HomeController@contact')->name('contact');
	Route::get('aboutus', 'HomeController@aboutus')->name('aboutus');
    Route::get('privacy_policy', 'HomeController@privacy_policy')->name('privacy_policy');
    Route::get('return_policy', 'HomeController@return_policy')->name('return_policy');
    Route::post('homeWeb/add/wishlist', 'WishlistController@add');
    Route::post('homeWeb/remove/wishlist', 'WishlistController@remove');


	Route::resource('profile', 'CustomerController')->middleware('auth:customerForWeb');
	Route::get('profile/wishlist', 'CustomerController@wishlist')->name('wishlist');

	Route::get('get_wishlist', 'CustomerController@get_wishlist')->name('get_wishlist');

    
	Route::post('sendemail', 'CustomerController@email')->name('email');

	Route::resource('restaurant', 'RestaurantController');
	Route::resource('categoties', 'DepartmentController');
	Route::resource('address', 'AddressController');
	Route::get('address/{id}', 'AddressController@delete')->name('addresss.delete');
	Route::post('address/getaddress','AddressController@getAddress')->middleware('auth:customerForWeb');

	Route::get('homeWeb/getProducts/{id}','HomeController@getProducts');

	Route::get('/all_promotions','HomeController@all_promosion')->name('all_promosion');
	
	Route::get('/promotions', function()  {
		$id = \Cookie::get('Branch_id');
		$id = \Cache::get('Branch_id');

        $all=collect();
    
    
            // dd(isset(($_COOKIE['lat'])),$_COOKIE);
            if(isset($_COOKIE['lat'])){
            $lat1= $_COOKIE['lat'];
                
            }else{
                $lat1=   30.027679;
            }
            if(isset($_COOKIE['long'])){
            $lon1=$_COOKIE['long'];
    
            }else{
                $lon1=   31.486457;
    
            }
            $branches=Branch::where('status','open')->get();
    
            foreach($branches as $branch)
            {
                $lat2=$branch['latitudes'];
                $lon2=$branch['longitudes'];
                $theta = $lon1 - $lon2;
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $km = $miles * 1.609344;
                $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
            }
            $all->sortBy('km')->values()->toArray();
        
            $nearst=$all[0];
    
            if($id){
                $nearst['store_id']=$id;
            }
            $branch=Branch::where('status','open')->where('store_id',$nearst['store_id'])->first();
            $store_id=$nearst['store_id'];
            if(is_null($store_id))
            {
                $store_id=01;
            }
            $countId='count_';
            switch($store_id)
            {
            case 01:$countId.=1;
            break;
            case 02:$countId.=2;
            break;
            case 04:$countId.=4;
    
            }$cat=[];
    		$showId='show_';
					switch($store_id)
					{
					case 01:$showId.=1;
					break;
					case 02:$showId.=2;
					break;
					case 04:$showId.=4;

					}
              
            if($store_id !='04'){
                $main_categories=Category::where($showId,'!=',0)->where('id','!=','5637168576')->whereNull('parent')->orderBy('order')->get();
            }else{

                $main_categories=Category::where($showId,'!=',0)->where('id','=','5637168576')->whereNull('parent')->orderBy('order')->first();
            }
       $customer=null;
       $pros=array();
        if(auth()->guard('customerForWeb')->user()){
            $customer=Customer::where('id',auth()->guard('customerForWeb')->user()->id)->first();
           
            foreach($customer->products as $pro){

                array_push($pros,$pro->id);
            }
        }
        $lang=LaravelLocalization::getCurrentLocale();
		$offers=Product::where('on_sale' , '1')
        ->where($store_id , 'true')
        ->get();
        if($lang=='ar')
        {
            $offers=Product::where('on_sale' , '1')
            ->where($store_id , 'true')->select(['*','name_ar as name','description_ar as description'])->get();
        }
        $offers=$offers->unique('barcode');
		return view('website.promotions',compact('branches','branch','offers','cat','main_categories','pros','customer'));
	});
	


	Route::post('/flags','HomeController@flags');
    Route::get('/get_category','CategoryController@get_category');


});


