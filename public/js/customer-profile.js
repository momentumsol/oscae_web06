$(document).ready(function(){
    // Add arrow icon for collapse element which is open by default
    $(".collapse.show").each(function(){
        $(this).prev(".card-header").find(".fa").addClass("fa-angle-right").removeClass("fa-angle-down");
    });

    // Toggle arrow right to down icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-angle-right").addClass("fa-angle-down");
    }).on('hide.bs.collapse', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-angle-down").addClass("fa-angle-right");
    });
});

$('.nav-tabs').on('click', 'li', function() {
    $('.nav-tabs li.active').removeClass('active');
    $(this).addClass('active');
});
$( "li:has('.active')" ).addClass( "active" );

$('.custom-radio1').click(function(){
    $('.radio1').css('display','block');
    $('.radio2').css('display','none');
    $('.radio3').css('display','none');


    $("label").css('color','#000');
    $(this).find("label[for =defaultGroupExample1]").css( 'color','#F4774F')
});
$('.custom-radio2').click(function(){
    $('.radio2').css('display','block');
    $('.radio1').css('display','none');
    $('.radio3').css('display','none');


    $("label").css('color','#000');
    $(this).find("label[for =defaultGroupExample2]").css( 'color','#F4774F' )

});

$('.custom-radio3').click(function(){
    $('.radio3').css('display','block');
    $('.radio1').css('display','none');
    $('.radio2').css('display','none');


    $("label").css('color','#000');
    $(this).find("label[for =defaultGroupExample3]").css( 'color','#F4774F' )

});


