<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryPageItemsTable extends Migration
{

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('category_page_item', function (Blueprint $table) {
      $table->integer('category_id')->unsigned()->default(0);
      $table->integer('page_item_id')->unsigned()->default(0);
      $table->foreign('category_id')->references('id')->on('categories');
      $table->foreign('page_item_id')->references('id')->on('page_items');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('category_page_item');
  }
}
