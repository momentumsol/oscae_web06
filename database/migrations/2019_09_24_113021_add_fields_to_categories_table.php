<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->text('name')->nullable()->after('id');
            $table->text('slug')->nullable()->after('id');
            $table->longText('description')->nullable()->after('id');
            $table->longText('image')->nullable()->change();
            $table->integer('parent')->nullable()->after('slug');
            $table->string('display')->default('default')->after('parent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            //
        });
    }
}
