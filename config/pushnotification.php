<?php
/**
 * @see https://github.com/Edujugon/PushNotification
 */

return [
    'gcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'My_ApiKey',
    ],
    'fcm' => [
        'priority' => 'high',
        'dry_run' => false,
        'apiKey' => 'AIzaSyDDPX2diDRveXRAhdyGsKbO6HEcnFeiXL8',
    ],
    'apn' => [
        'certificate' => __DIR__ . '/iosCertificates/new.pem',
        'passPhrase' => 'secret', //Optional
        'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
        'dry_run' => true,
    ],
];
