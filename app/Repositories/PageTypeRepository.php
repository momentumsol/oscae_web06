<?php

namespace App\Repositories;

use App\Models\PageType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PageTypeRepository
 * @package App\Repositories
 * @version March 4, 2018, 4:34 pm UTC
 *
 * @method PageType findWithoutFail($id, $columns = ['*'])
 * @method PageType find($id, $columns = ['*'])
 * @method PageType first($columns = ['*'])
*/
class PageTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PageType::class;
    }
}
