<?php

namespace App\Repositories;

use App\Models\Customer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ImageSliderRepository
 * @package App\Repositories
 * @version February 26, 2019, 4:33 pm UTC
 *
 * @method ImageSlider findWithoutFail($id, $columns = ['*'])
 * @method ImageSlider find($id, $columns = ['*'])
 * @method ImageSlider first($columns = ['*'])
*/
class CustomerServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
    
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Customer::class;
    }
}
