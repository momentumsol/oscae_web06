<?php

namespace App\Repositories;


use App\Models\OrderHistory;
use InfyOm\Generator\Common\BaseRepository;


/**
 * Class KitchenRepository
 * @package App\Repositories
 * @version September 25, 2019, 12:55 pm UTC
 *
 * @method Kitchen findWithoutFail($id, $columns = ['*'])
 * @method Kitchen find($id, $columns = ['*'])
 * @method Kitchen first($columns = ['*'])
*/
class OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'OrderNumber', 'CustomerPhone', 'CustomerAccount','NotesPeriod','OrderEntry',
        'customer_id','CustomerAddress','Signature','express_shipping','order_status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderHistory::class;
    }
}
