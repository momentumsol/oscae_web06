<?php

namespace App\Repositories;

use App\Models\PromoVideo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PromoVideoRepository
 * @package App\Repositories
 * @version October 7, 2019, 2:52 pm UTC
 *
 * @method PromoVideo findWithoutFail($id, $columns = ['*'])
 * @method PromoVideo find($id, $columns = ['*'])
 * @method PromoVideo first($columns = ['*'])
*/
class PromoVideoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PromoVideo::class;
    }
}
