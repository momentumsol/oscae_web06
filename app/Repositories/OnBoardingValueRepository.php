<?php

namespace App\Repositories;

use App\Models\OnBoardingValue;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OnBoardingValueRepository
 * @package App\Repositories
 * @version March 28, 2018, 11:23 am UTC
 *
 * @method OnBoardingValue findWithoutFail($id, $columns = ['*'])
 * @method OnBoardingValue find($id, $columns = ['*'])
 * @method OnBoardingValue first($columns = ['*'])
*/
class OnBoardingValueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OnBoardingValue::class;
    }
}
