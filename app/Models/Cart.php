<?php


namespace App\Models;

use Eloquent as Model;



class Cart extends Model
{
    //
    
    public $table = 'cart_customers';
    protected $fillable = [
        'product_id', 'customer_id', 'status','details'
    ];
  
	
  
}