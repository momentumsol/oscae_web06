<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Media
 * @package App\Models
 * @version February 26, 2019, 4:33 pm UTC
 *
 * @property string name
 * @property string url
 */
class Media extends Model
{
		use SoftDeletes;

		public $table = 'media';


		protected $dates = ['deleted_at'];


		public $fillable = [
				'name',
				'url'
		];

		/**
		 * The attributes that should be casted to native types.
		 *
		 * @var array
		 */
		protected $casts = [
				'name' => 'string',
				'url' => 'string'
		];

		/**
		 * Validation rules
		 *
		 * @var array
		 */
		public static $rules = [
				'url' => 'required|image'
		];

		public function setUrlAttribute($file) {
			$url = $file;
			if (is_file($file)) {
				$url = 'public/media/'. $file->getClientOriginalName();
				\Storage::put($url , file_get_contents($file), 'public');
			}
			$this->attributes['url'] = $url;
		}

		public function getUrlAttribute($file) {
			$url = '';
			if (\Storage::exists($this->attributes['url'])) {
				$url = url(\Storage::url($this->attributes['url']));
			}
			return $url;
		}
}
