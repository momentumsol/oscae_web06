<?php

namespace App\Models;

use Eloquent as Model;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use OpenApi\Annotations\Examples;

/**
 * Class Category
 * @package App\Models
 * @version March 5, 2018, 12:02 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection categoryPageItem
 * @property string title
 * @property string image
 */
class Category extends Model
{
	use SoftDeletes;


	// use \igaster\TranslateEloquent\TranslationTrait {
	// 	boot as bootTranslations;
	// }

	// protected static function boot()
	// {
	// 	parent::boot();
	// 	self::bootTranslations();
	// }

	// protected static $translatable = [
	// 	'm_title',
	// 	'm_image'
	// ];

	public $table = 'categories';



	protected $dates = ['deleted_at'];

	// protected $appends = ['display_name', 'count'];
	protected $appends = ['display_name'];

	public $fillable = [
		'id',
		'name',
		'slug',
		'image',
		'parent',
		'description',
		'count'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'name' => 'string',
		'slug' => 'string',
		'parent' => 'integer',
		'image' => 'string',
	];

	

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'name' => 'required',
		// 'slug' => 'required|unique:categories,slug' ,
		'image' => 'image',
	];

	public function getDisplayNameAttribute()
	{
		return $this->name->en;
	}



	public function setImageAttribute($file)
	{
		
   try{
	if (is_file($file)) {
		$file = \Storage::put('public/categories', $file, 'public');
	}

	$this->m_image = $this->id;
	$jsonstring = array();
	$jsonstring['src'] = url(\Storage::url($file));
	$jsonstring['title'] = $this->attributes['name'];
	$jsonstring['alt'] = '';
	$this->attributes['image'] = json_encode($jsonstring);
}
catch(Exception $e)
{
if(isset($file[0]))
{
	$file=$file[0];
	$this->m_image = $this->id;
	$jsonstring = array();
	$jsonstring['src'] =$file;
	$jsonstring['title'] = '';
	$jsonstring['alt'] = '';

	$this->attributes['image'] = json_encode($jsonstring);
}
else
{

	$this->m_image = $this->id;
	$jsonstring = array();
	$jsonstring['src'] =$file['src'];
	$jsonstring['title'] = $file['title'];
	$jsonstring['alt'] = '';

	$this->attributes['image'] = json_encode($jsonstring);
}
	
}
		
	}

	public function getImageAttribute($json)
	{

	
		if ($json) {
			$url = json_decode($json);
			if (!\Request::isJson()) {
				$img['src'] = $url->src;
				return $img;
			}
			return $url;
		}
	}

	public function getParentAttribute($id)
	{
		return $id ? intval($id) : 0;
	}

	public function setTitleAttribute($data)
	{
		$this->m_title = $data;
		$this->attributes['title'] = $data;
	}

	public function getTitleAttribute($data)
	{
		return empty($this->m_title) ? $this->attributes['title'] : $this->m_title;
	}

	public function setNameAttribute($data)
	{
		$json = json_encode($data);
		$this->attributes['name'] = $json;
	}

	public function setDescriptionAttribute($data)
	{
		$json = json_encode($data);
		$this->attributes['description'] = $json;
	}

	public function getNameAttribute($data)
	{
		$name = json_decode($data);
		if (!$name) {
			$name = (object) [
				"en" => $data,
				"ar" => $data
			];
		}
		return $name;
	}

	public function getDescriptionAttribute($data)
	{
		$description = json_decode($data);
		if (!$description) {
			$description = (object) [
				"en" => $data,
				"ar" => $data
			];
		}
		return $description;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 **/
	public function pageItems()
	{
		return $this->belongsToMany(\App\Models\PageItem::class, 'category_page_item', 'category_id', 'page_item_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 **/
	public function promotions()
	{
		return $this->belongsToMany(\App\Models\Promotion::class, 'category_promotion', 'category_id', 'promotion_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\hasMany
	 **/
	public function children()
	{
		return $this->hasMany(\App\Models\Category::class, 'parent', 'id');
	}
	public function sub_children()
    {
        return $this->hasMany(\App\Models\Category::class, 'parent')->with('children');
    }
	public function getname()
	{
	foreach($this->name as $app) {
		
		$en=$app;
		return $en;
	}
	}


}
