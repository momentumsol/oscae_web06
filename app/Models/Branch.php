<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Branch
 * @package App\Models
 * @version September 23, 2019, 12:26 pm UTC
 *
 * @property string title
 * @property string image
 * @property string location
 */
class Branch extends Model
{
	use SoftDeletes;

	public $table = 'branches';


	protected $dates = ['deleted_at'];


	public $fillable = [
		'title',
		'image',
		'logo',
		'location',
		'latitudes',
		'longitudes',
		'store_id',
		'category',
		'status',
		'hide',
		'message',
		'show_prices',
		'header_logo'

	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'title' => 'string',
		'image' => 'string',
		'location' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'title' => 'required',
		'image' => 'file|image'
	];

	public function setTitleAttribute($data) {
		$this->attributes['title'] = json_encode($data);
	}

	public function getTitleAttribute($data) {
		$title = json_decode($data);
		if(!$title) {
			$title = (object) [
				'en' => $data,
				'ar' => $data
			];
		}
		return $title;
	}

	public function setImageAttribute($file)
	{
		if (is_file($file)) {
			$file = \Storage::put('public/branches', $file, 'public');
		}
		$this->attributes['image'] = $file;
	}

	public function getImageAttribute($file)
	{
		$url = '';
		if (\Storage::exists($file)) {
			$url = url(\Storage::url($file));
		}
		return $url;
	}
	public function setLogoAttribute($file)
	{
		if (is_file($file)) {
			$file = \Storage::put('public/branches/logo', $file, 'public');
		}
		$this->attributes['logo'] = $file;
	}

	public function getLogoAttribute($file)
	{
	
		$url = '';
		if (\Storage::exists($file)) {
			$url = url(\Storage::url($file));
		}
		return $url;
	}

	public function setHeaderLogoAttribute($file)
	{
	
		if (is_file($file)) {
			$file = \Storage::put('public/branches/header_logo', $file, 'public');
		}
		$this->attributes['header_logo'] = $file;
	}

	public function getHeaderLogoAttribute($file)
	{
	
		$url = '';
		if (\Storage::exists($file)) {
			$url = url(\Storage::url($file));
		}
		return $url;
	}


	public function getLocationAttribute($location)
	{
		return $location? $location: 'http://oscarstores.com';
	}
	public function gettitle()
	{
		
	foreach($this->title as $app) {

		$en=$app;
		return $en;
	}
	}
}