<?php


namespace App\Models;

use Eloquent as Model;



class OrderHistory extends Model
{
    //
    
    public $table = 'order_history';
    protected $fillable = [
        'OrderNumber', 'CustomerPhone', 'CustomerAccount','NotesPeriod','OrderEntry',
        'customer_id','CustomerAddress','Signature','express_shipping','order_status','deliver_fee'
    ];

    public static $rules = [
        'order_status' => 'required',
    ];
    public function user()
	{
		return $this->belongsTo(\App\Models\Customer::class, 'customer_id', 'id');
	}
	
  
}