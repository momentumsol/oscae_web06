<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 * @version March 5, 2018, 12:02 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection categoryPageItem
 * @property string title
 * @property string image
 */
class ImageSlider extends Model
{



	

	public $table = 'images_slider';




	public $fillable = [
		'image',
        'order',
		'type',
		'link'
	];

	

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
        'image' => 'required',
        'order' => 'required',
	];


	


}
