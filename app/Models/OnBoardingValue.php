<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OnBoardingValue
 * @package App\Models
 * @version March 28, 2018, 11:23 am UTC
 *
 * @property \App\Models\OnBoarding onBoarding
 * @property integer question_id
 * @property string value
 */
class OnBoardingValue extends Model
{
	use SoftDeletes;
	//use \igaster\TranslateEloquent\TranslationTrait;

	// use \igaster\TranslateEloquent\TranslationTrait {
	// 	boot as bootTranslations;
	// }

	// protected static function boot() {
	// 	parent::boot();
	// 	self::bootTranslations();
	// }

	protected static $translatable = ['m_value'];

	public $table = 'on_boarding_values';

	protected $dates = ['deleted_at'];

	public $fillable = [
		'question_id',
		'value',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'question_id' => 'integer',
		'value' => 'string',
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'value' => 'required',
	];

	public function setValueAttribute($data) {
		$this->m_value = $data;
		$this->attributes['value'] = $data;
	}

	public function getValueAttribute($data) {
		return empty($this->m_value)? $this->attributes['value'] : $this->m_value;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function onBoarding()
	{
		return $this->belongsTo(\App\Models\OnBoarding::class, 'question_id', 'id');
	}
}
