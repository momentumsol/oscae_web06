<?php


namespace App\Models;

use Eloquent as Model;



class Address extends Model
{
    //
    
    public $table = 'address';
    protected $fillable = [
        'name', 'address', 'area','city','phone','customer_id','coordinates','apartment_number','floor_number','building_number'
    ];


    public function user()
	{
		return $this->belongsTo(\App\Models\Customer::class, 'customer_id', 'id');
	}

	
  
}
