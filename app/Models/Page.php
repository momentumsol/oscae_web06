<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Page
 * @package App\Models
 * @version March 4, 2018, 4:41 pm UTC
 *
 * @property \App\Models\PageType pageType
 * @property string title
 * @property string tags
 * @property integer page_type_id
 */
class Page extends Model
{
	use SoftDeletes;
	use \igaster\TranslateEloquent\TranslationTrait;
	// use \igaster\TranslateEloquent\TranslationTrait {
	// 	boot as bootTranslations;
	// }

	// protected static function boot() {
	// 	parent::boot();
	// 	self::bootTranslations();
	// }
	

	protected static $translatable = ['m_title'];

	public $table = 'pages';

	protected $dates = ['deleted_at'];

	public $fillable = [
		'title',
		'tags',
		'page_type_id',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'title' => 'string',
		'tags' => 'string',
		'page_type_id' => 'integer',
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'title' => 'required',
	];

	public function getTitleAttribute() {
		return empty($this->m_title)? $this->attributes['title'] : $this->m_title;
	}

	public function setTitleAttribute($title) {
		$this->m_title = $title;
		$this->attributes['title'] = $title;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function pageType()
	{
		return $this->belongsTo(\App\Models\PageType::class, 'page_type_id', 'id');
	}

	public function pageContent()
	{
		return $this->hasMany(\App\Models\PageContent::class, 'page_id', 'id')->orderBy('order');
	}
}
