<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 * @version March 5, 2018, 12:02 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection categoryPageItem
 * @property string title
 * @property string image
 */
class Notification extends Model
{



	

	public $table = 'notifications';




	public $fillable = [
		'title',
		'message',
		'os',
		'language',
		'image',
		'published_at'
	];

	

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
        'title' => 'required',
        'message' => 'required',
        'language' => 'required',
        'os' => 'required',
		'image' => 'image',
	];


}
