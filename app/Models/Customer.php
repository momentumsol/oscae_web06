<?php

namespace App;

namespace App\Models;

use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
class Customer extends   Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','default_address','status','type','channel_id','token','images'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function address()
	{
		return $this->hasMany(\App\Models\Address::class, 'customer_id');
    }
    
    public function orders()
	{
		return $this->hasMany(\App\Models\OrderHistory::class, 'customer_id')->orderBy('created_at','desc');
    }
    public function verifyCustomer()
    {
        return $this->hasOne(\App\Models\VerifyCustomer::class);
    }

    public function messages()
    {
    return $this->hasMany(Message::class);
    }

    public function channel()
    {
    return $this->hasMany(Channel::class,'client_id');
    }

    public function products(){
        return $this->belongsToMany(\App\Models\Product::class,'customer_product','customer_id','product_id');
    }
    public function wishlist()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'wishlist','customer_id','product_id');
    }
    public function cart()
{
    return $this->belongsToMany(\App\Models\Product::class, 'cart_customers','customer_id','product_id');
}
}
