<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PageItem
 * @package App\Models
 * @version March 5, 2018, 12:02 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection categoryPageItem
 * @property string title
 * @property string description
 * @property string image
 * @property string location
 */
class PageItem extends Model
{
  use SoftDeletes;

  public $table = 'page_items';

  protected $dates = ['deleted_at'];

  public $fillable = [
    'title',
    'description',
    'image',
    'location',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
    'title' => 'string',
    'description' => 'string',
    'image' => 'string',
    'location' => 'string',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
    'title' => 'required',
    'image' => 'file|image',
  ];

  public function setImageAttribute($file)
  {
    if (is_file($file)) {
      $file = \Storage::put('public/page_items', $file, 'public');
    }
    $this->attributes['image'] = $file;
  }

  public function getImageAttribute($file)
  {
    $url = '';
    if (\Storage::exists($file)) {
      $url = url(\Storage::url($file));
    }
    return $url;
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   **/
  public function categories()
  {
    return $this->belongsToMany(\App\Models\Category::class, 'category_page_item', 'category_id', 'page_item_id');
  }
}
