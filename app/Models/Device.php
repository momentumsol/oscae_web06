<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Device
 * @package App\Models
 * @version April 1, 2018, 10:16 pm UTC
 *
 * @property string os
 * @property string device_token
 * @property string language
 * @property string topics
 */
class Device extends Model
{
    use SoftDeletes;

    public $table = 'devices';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'os',
        'device_token',
        'language',
        'topics'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'os' => 'string',
        'device_token' => 'string',
        'language' => 'string',
        'topics' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'os' => 'required',
        'device_token' => 'required',
        'language' => 'required'
    ];

    
}
