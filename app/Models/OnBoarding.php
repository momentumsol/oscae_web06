<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OnBoarding
 * @package App\Models
 * @version March 28, 2018, 11:04 am UTC
 *
 * @property integer order
 * @property string question
 * @property string answers
 */
class OnBoarding extends Model
{
	use SoftDeletes;
	

	// use \igaster\TranslateEloquent\TranslationTrait {
	// 	boot as bootTranslations;
	// }

	// protected static function boot() {
	// 	parent::boot();
	// 	self::bootTranslations();
	// }

	// protected static $translatable = [
	// 	'm_question',
	// 	'm_answers'
	// ];

	public $table = 'on_boardings';

	protected $dates = ['deleted_at'];
	
	protected $appends = ['answers_decoded'];

	public $fillable = [
		'order',
		'question',
		'answers',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'order' => 'integer',
		'question' => 'string',
		'answers' => 'string',
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'order' => 'integer',
		'question' => 'required',
		'answers' => 'required',
	];

	public function setQuestionAttribute($data) {
		$this->m_question = $data;
		$this->attributes['question'] = $data;
	}

	public function getQuestionAttribute($data) {
		return empty($this->m_question)? $this->attributes['question'] : $this->m_question;
	}
	
	public function setAnswersAttribute($data) {
		$this->m_answers = $data;
		$this->attributes['answers'] = $data;
	}

	public function getAnswersAttribute($data) {
		return empty($this->m_answers)? $this->attributes['answers'] : $this->m_answers;
	}
	
	public function getAnswersDecodedAttribute() {
		$answers = empty($this->m_answers)? $this->attributes['answers'] : $this->m_answers;
		return json_decode($answers);
	}

	public function values() {
		return $this->hasMany(\App\Models\OnBoardingValue::class, 'question_id', 'id');
	}

}
