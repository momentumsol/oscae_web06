<?php 
namespace App\Models;

use Eloquent as Model;



class VerifyCustomer extends Model
{
    //

    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }
}
