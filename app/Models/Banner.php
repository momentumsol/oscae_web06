<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 * @version March 5, 2018, 12:02 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection categoryPageItem
 * @property string title
 * @property string image
 */
class Banner extends Model
{



	

	public $table = 'banners';




	public $fillable = [
		'image',
		'order',
		'banner_name',
		'type',
		'order'
	];

	

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
        'image' => 'required',
        'order' => 'required',
	];


}
