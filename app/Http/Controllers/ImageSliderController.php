<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
// use App\Repositories\ProductRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ImageSliderRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Importer;
use App\Models\Device;



class ImageSliderController extends AppBaseController
{
	/** @var  ProductRepository */
	// private $productRepository;
	private $imageSliderRepository;

	public function __construct(ImageSliderRepository $imageSliderRepo)
	{
		// $this->productRepository = $productRepo;
		$this->imageSliderRepository = $imageSliderRepo;
	}

	/**
	 * Display a listing of the Product.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{

        $this->imageSliderRepository->pushCriteria(new RequestCriteria($request));
        $images = $this->imageSliderRepository->orderBy('order')->paginate(10);

		return view('image_slider.index')
			->with('images', $images);
	}

	/**
	 * Show the form for creating a new Product.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->authorize('notifications.create');

		return view('image_slider.create');
	}

	

	public function store(Request $request)
	{
        $input = $request->all();
   
		if ($request->file('image')) {
			$file = \Storage::put('public/image_slider', $request->file('image'), 'public');
			$image=str_replace("http", "https",  url(\Storage::url($file)));
		
			$input['image'] =  $image
			 ;
			 
		}
	
		
		
 
// Display replaced string
 

		$image = $this->imageSliderRepository->create($input);


        Flash::success('image slider saved successfully.');

        return redirect(route('imageSlider.index'));
		}
	

	/**
	 * Display the specified Product.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	// public function show($id)
	// {
	// 	$this->authorize('notifications.create');
	// 	$notification  = $this->notificationRepository->findWithoutFail($id);

	// 	if (empty($notification)) {
	// 		Flash::error('Notification not found');

	// 		return redirect(route('notifications.index'));
	// 	}

	// 	return view('notifications.show')->with('notification', $notification );
	// }

	/**
	 * Show the form for editing the specified Product.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$this->authorize('notifications.create');
        $image = $this->imageSliderRepository->findWithoutFail($id);

		if (empty($image)) {
			Flash::error('image slider not found');

			return redirect(route('imageSlider.index'));
		}

		return view('image_slider.edit')->with('image',$image );
	}

	/**
	 * Update the specified Product in storage.
	 *
	 * @param int $id
	 * @param UpdateProductRequest $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$this->authorize('notifications.create');
		$input = $request->all();
		$image = $this->imageSliderRepository->findWithoutFail($id);

		if (empty($image)) {
			Flash::error('image not found');

			return redirect(route('imageSlider.index'));
		}
		if ($request->file('image')) {
			$file = \Storage::put('public/notification', $request->file('image'), 'public');
			$input['image'] =  url(\Storage::url($file))
;
		}
		
		$image = $this->imageSliderRepository->update($input, $id);

		Flash::success('image slider updated successfully.');

		return redirect(route('imageSlider.index'));
	}

	/**
	 * Remove the specified Product from storage.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{

		$this->authorize('notifications.delete');
        $image = $this->imageSliderRepository->findWithoutFail($id);

		if (empty($image)) {
			Flash::error('image slider not found');

			return redirect(route('imageSlider.index'));
		}
         $this->imageSliderRepository->delete($id);

		Flash::success('image slider deleted successfully.');

		return redirect(route('imageSlider.index'));
	}



	public function import(Request $request)
	{
		$arr = [];
		$products = \Cache::get('products', []);

		if ($request->hasFile('file')) {
			$rules = [
				'file' => ["required", ''],
			];
			$messages = [
				'mimes' => 'The :attribute field is must be a file of type: xlsx.',
			];

			$this->validate($request, $rules, $messages);
			$excel = Importer::make('Excel');
			$excel->load($request->file('file'));
			$collection = $excel->getCollection();
			$collection = $collection->forget($collection->shift());
			foreach ($collection as $record) {
				if ($record[0] || $record[1]) {
					$ids = [];
					$randomNumber = rand(1, 1000000000000);
					foreach ($products as $p) {
						array_push($ids, $p['id']);
					}
					if ($this->checkIfExistId($ids, $randomNumber)) {
						$product = [
							'id' => $randomNumber,
							'name' => '',
							'description' => '',
							'sale_price' => '',
							'on_sale' => false,
							'regular_price' => '',
							'reviews_allowed' => false,
							'in_stock' => true,
							'categories' => [],

						];

						$product['name'] = $record[0];
						$product['description'] = $record[2];
						$product['name_ar'] = $record[1];
						$product['description_ar'] = $record[3];
						$product['sale_price'] = $record[5];
						$product['on_sale'] = !empty($product['sale_price']);
						$product['regular_price'] = $record[4];
						$product['hot_price'] = ($record[6] == 'yes') ? true : false;

						if ($record[7]) {
							$categories = explode(',', $record[7]);
							$selectedCategories = array();
							foreach ($categories as $cat) {
								$cats = $this->categoryRepository->findByField('slug', $cat)->toArray();
								if (!empty($cats))
									array_push($selectedCategories, $cats[0]);
							}

							$product['categories'] = $selectedCategories;
						}
						$img[0]['src'] = url(\Storage::url("media/$record[8]"));
						$product['images'] = $img;
						array_push($products, $product);
					}
				}
			}
			\Cache::forever('products', $products);

			Flash::success('Product imported successfully.');
			return redirect(route('products.index'));
			//dd($products);
		}
		return view('products.import')->with('array', $arr);
	}
}
