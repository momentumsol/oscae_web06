<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Customer;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;
use Auth;

use function GuzzleHttp\json_decode;

class CartController extends AppBaseController
{


    public function add_cart(Request $request)
    {

        if (\Cache::get('Branch_id')) {
            $store_id = \Cache::get('Branch_id');
        } else {
            $store_id = '01';
        }

        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
                break;
            case 07:
                $stockId .= 7;
        }

        $product = Product::where('id', $request->product_id)->first();

        $customer = auth()->guard('customerForWeb')->user();

        $weight = 1;

        $cart = Cart::where('product_id', $request->product_id)->where('customer_id', $customer->id)->first();
        if (!is_null($cart)) {
            $data = json_decode($cart->details);
            if ($request->quantity == 1) {
                $data = json_decode($cart->details);
                $data->quantity = $data->quantity + 1;
            }
            if ($request->quantity == -1) {

                $data->quantity = $data->quantity - 1;
            }
            if ($request->quantity > 1) {
                $data = json_decode($cart->details);
                $data->quantity = $data->quantity + $request->quantity;
            }

            if ($data->quantity == 0 || $request->quantity == 0) {
                $cart = Cart::where('product_id', $request->product_id)->where('customer_id', $customer->id)->delete();
            } else {
                if ($product->PriceUnit == 'kg') {
                    if ($product->discountprice == "0") {

                        $data->total = $product->regular_price * $data->quantity * (250 / 1000);
                    } else {
                        $data->total = $product->discountprice * $data->quantity * (250 / 1000);
                    }
                } else {
                    if ($product->discountprice == "0") {
                        $data->total = $product->regular_price * (float)$data->quantity;
                    } else {
                        $data->total = $product->discountprice * (float)$data->quantity;
                    }
                }

                Cart::where('product_id', $request->product_id)->where('customer_id', $customer->id)->update(['details' => json_encode($data)]);
            }


            return response()->json([
                'success' => true,
                'count' => count(auth()->guard('customerForWeb')->user()->cart->where($stockId, 1)->unique('barcode')->values()),
                'message' => 'Product  Updated To Cart.',
            ], 200);
        }
        if (!is_null($product)) {





            if ($product->PriceUnit == 'kg') {
                if (isset($request->weight)) {
                    $weight = $request->weight;
                    if ($product->discountprice == "0") {

                        $total = $product->regular_price * $request->quantity * ($request->weight / 1000);
                    } else {
                        $total = $product->discountprice * $request->quantity * ($request->weight / 1000);
                    }
                } else {


                    $weight = 250;
                    if ($product->discountprice == "0") {

                        $total = $product->regular_price * $request->quantity * (250 / 1000);
                    } else {
                        $total = $product->discountprice * $request->quantity * (250 / 1000);
                    }
                }

                $product = collect($product)->put('total', $total);
                $product = collect($product)->put('quantity', $request->quantity);
                $product = collect($product)->put('weight', $request->weight);
            } else {

                if ($product->discountprice == "0") {

                    $total = $product->regular_price * $request->quantity;
                } else {
                    $total = $product->discountprice * $request->quantity;
                }
                // $total=$product->regular_price *(float)$request->quantity;
                $product = collect($product)->put('total', $total);
                $product = collect($product)->put('quantity', $request->quantity);
                $product = collect($product)->put('weight', $weight);
            }

            Cart::create(['customer_id' => $customer->id, 'product_id' => $product->first(), 'status' => 'available', 'details' => json_encode($product)]);
            return response()->json([
                'success' => true,
                'count' => count(auth()->guard('customerForWeb')->user()->cart->where($stockId, 1)->unique('barcode')->values()),

                'message' => 'Product Added To Cart Successfully',
            ], 200);
        }
    }

    public function remove_cart(Request $request)
    {

        $product = Product::where('id', $request->product_id)->first();
        $customer = auth('customerForWeb')->user();
        if (\Cache::get('Branch_id')) {
            $store_id = \Cache::get('Branch_id');
        } else {
            $store_id = '01';
        }
        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
                break;
            case 07:
                $stockId .= 7;
        }


        $cart = \DB::table('cart_customers')->where('product_id', $request->product_id)->where('customer_id', $customer->id)->delete();
        $carts = \DB::table('cart_customers')->where('customer_id', $customer->id)->count();
        return response()->json([
            'success' => true,
            'message' => 'Product Deleted  From Cart Successfully',
            'count' => count(auth()->guard('customerForWeb')->user()->cart->where($stockId, 1)->unique('barcode')->values()),


        ], 200);
    }

    public function get_cart(Request $request)
    {
        $customer = auth('customers')->user();
        $cart = Cart::where('customer_id', $customer->id)->get();
        if (is_null($cart)) {
            return response()->json([
                'success' => true,

                'message' => 'Cart is empty',
            ], 200);
        }
        $store_id = $request->store_id;
        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
                break;
            case 07:
                $stockId .= 7;
        }
        $proArray = [];
        foreach ($cart as $productData) {
            $product = json_decode($productData->details);

            $pro = Product::where("id", "LIKE", "%" . $product->id . "%")->first();

            $status = 'available';


            if (is_null($pro)) {
                $status = 'available';
            }
            if ($pro->$store_id == 'false') {
                $status = 'unavailable';
            }
            if (isset($product->weight)) {
                $weight =  $product->weight;
            } else {
                $weight =  0;
            }

            array_push($proArray, array(

                "id" => $pro->id,
                "name" => $pro->name,
                "description" => $pro->description,
                "name_ar" => $pro->name_ar,
                "description_ar" => $pro->description_ar,
                "on_sale" => $pro->on_sale,
                "regular_price" => $pro->regular_price,
                "hot_price" => $pro->hot_price,
                "discountFrom" => $pro->regular_price,
                "discountto" => $pro->discountto,
                "discountprice" => $pro->discountprice,
                "barcode" => $pro->barcode,
                "PriceUnit" =>  $pro->PriceUnit,
                "weight" => (string)$weight,
                "in_stock" =>  $pro->in_stock,
                "quantity" => $product->quantity,
                "category_id" => $pro->category_id,
                "images" => $pro->images,
                "01" => $pro->$check_01,
                "02" => $pro->$check_02,
                "04" => $pro->$check_04,
                "limit" => $pro->limit,
                "categories" => $pro->categories,
                "created_at" => $pro->created_at,
                "updated_at" => $pro->updated_at,
                "sale_price" => $pro->sale_price,
                "status" => $status,
                "total" => $product->total



            ));
        }

        return response()->json([
            'success' => true,
            'data' => $proArray,
            'message' => 'Cart Returned Successfully',
        ], 200);
    }


    public function get_cart_new(Request $request, $lang)
    {
        $customer = auth('customers')->user();
        $cart = Cart::where('customer_id', $customer->id)->get();
        if (is_null($cart)) {
            return response()->json([
                'success' => true,

                'message' => 'Cart is empty',
            ], 200);
        }
        $store_id = '01';
        if ($request->has('store_id')) {
            $store_id = $request->store_id;
        }

        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
                break;
            case 07:
                $stockId .= 7;
        }
        $check_01 = '01';
        $check_02 = '02';
        $check_04 = '04';
        $proArray = [];

        foreach ($cart as $productData) {
            $product = json_decode($productData->details);

            $pro = Product::where("id", "LIKE", "%" . $product->id . "%")->where($stockId, 1)->first();

            $status = 'available';


            if (is_null($pro)) {
                $status = 'available';
            }
            if (!is_null($pro)) {
                if ($pro->$store_id == 'false') {
                    $status = 'unavailable';
                }
            } else {
                $status = 'unavailable';
            }

            if (isset($product->weight)) {
                $weight =  $product->weight;
            } else {
                $weight =  0;
            }
            if ($lang == 'ar' && !is_null($pro)) {
                array_push($proArray, array(

                    "id" => $pro->id,
                    "name" => $pro->name_ar,
                    "description" => $pro->description_ar,
                    "name_ar" => $pro->name_ar,
                    "description_ar" => $pro->description_ar,
                    "on_sale" => $pro->on_sale,
                    "regular_price" => $pro->regular_price,
                    "hot_price" => $pro->hot_price,
                    "discountFrom" => $pro->regular_price,
                    "discountto" => $pro->discountto,
                    "discountprice" => $pro->discountprice,
                    "barcode" => $pro->barcode,
                    "PriceUnit" =>  $pro->PriceUnit,
                    "weight" => (string)$weight,
                    "in_stock" =>  $pro->in_stock,
                    "quantity" => $product->quantity,
                    "category_id" => $pro->category_id,
                    "images" => $pro->images,
                    "01" => $pro->$check_01,
                    "02" => $pro->$check_02,
                    "04" => $pro->$check_04,
                    "limit" => $pro->limit,
                    "categories" => $pro->categories,
                    "created_at" => $pro->created_at,
                    "updated_at" => $pro->updated_at,
                    "sale_price" => $pro->sale_price,
                    "status" => $status,
                    "total" => $product->total



                ));
            } elseif ($lang == 'en' && !is_null($pro)) {
                array_push($proArray, array(

                    "id" => $pro->id,
                    "name" => $pro->name,
                    "description" => $pro->description,
                    "name_ar" => $pro->name_ar,
                    "description_ar" => $pro->description_ar,
                    "on_sale" => $pro->on_sale,
                    "regular_price" => $pro->regular_price,
                    "hot_price" => $pro->hot_price,
                    "discountFrom" => $pro->regular_price,
                    "discountto" => $pro->discountto,
                    "discountprice" => $pro->discountprice,
                    "barcode" => $pro->barcode,
                    "PriceUnit" =>  $pro->PriceUnit,
                    "weight" => (string)$weight,
                    "in_stock" =>  $pro->in_stock,
                    "quantity" => $product->quantity,
                    "category_id" => $pro->category_id,
                    "images" => $pro->images,
                    "01" => $pro->$check_01,
                    "02" => $pro->$check_02,
                    "04" => $pro->$check_04,
                    "limit" => $pro->limit,
                    "categories" => $pro->categories,
                    "created_at" => $pro->created_at,
                    "updated_at" => $pro->updated_at,
                    "sale_price" => $pro->sale_price,
                    "status" => $status,
                    "total" => $product->total



                ));
            }
        }

        return response()->json([
            'success' => true,
            'data' => $proArray,
            'message' => 'Cart Returned Successfully',
        ], 200);
    }

    public function empty_cart(Request $request)
    {
        $customer = auth('customers')->user();
        $cart = Cart::where('customer_id', $customer->id)->delete();
        return response()->json([
            'success' => true,

            'message' => 'Cart Deleted Successfully',
        ], 200);
    }
}
