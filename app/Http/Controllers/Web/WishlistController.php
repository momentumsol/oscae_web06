<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Customer;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

use function GuzzleHttp\json_decode;
use DB;

class WishlistController extends AppBaseController
{


        public function add(Request $request)
        {
                $id = $request->product_id;
                $product = Product::where('id', $id)->first();

                $customer = auth('customerForWeb')->user();


                $wishlist_found = DB::table('wishlist')->where('customer_id', $customer->id)->where('product_id', $id)->first();
                if (!is_null($wishlist_found)) {

                        return response()->json([
                                'success' => true,
                                'count' => count(auth('customerForWeb')->user()->wishlist->unique('barcode')->values()),

                                'message' => 'Product  Added To Wishlist Successfully.',
                        ], 200);
                } else {
                        DB::table('wishlist')->insert(['customer_id' => $customer->id, 'product_id' => $id]);
                        return response()->json([
                                'success' => true,
                                'count' => count(auth('customerForWeb')->user()->wishlist->unique('barcode')->values()),
                                'message' => 'Product  Added To Wishlist Successfully.',
                        ], 200);
                }
        }


        public function remove(Request $request)
        {
                // $product=Product::where('id',$id)->first();
                $id = $request->product_id;
                $customer = auth('customerForWeb')->user();





                DB::table('wishlist')->where('customer_id', $customer->id)->where('product_id', $id)->delete();
                return response()->json([
                        'success' => true,
                        'count' => count(auth('customerForWeb')->user()->wishlist->unique('barcode')->values()),

                        'message' => 'Product  Removed To Wishlist Successfully.',
                ], 200);
        }

        public function get(Request $request, $lang)
        {
                $store_id = '01';
                if ($request->has('store_id')) {
                        $store_id = $request->store_id;
                }

                $stockId = 'in_stock_';
                switch ($store_id) {
                        case 01:
                                $stockId .= 1;
                                break;
                        case 02:
                                $stockId .= 2;
                                break;
                        case 04:
                                $stockId .= 4;
                                break;
                        case 05:
                                $stockId .= 5;
                                break;
                        case 06:
                                $stockId .= 6;
                                break;
                        case 07:
                                $stockId .= 7;
                }
                $customer = auth('customers')->user();
                if ($lang == 'ar') {
                        $wishlist = Customer::where('id', $customer->id)->with(["wishlist" => function ($q) use ($stockId) {
                                $q->select('products.*', 'products.name_ar as name', '' . $stockId . ' as in_stock');
                        }])->get();
                } else {
                        $wishlist = Customer::where('id', $customer->id)->with(["wishlist" => function ($q)  use ($stockId) {
                                $q->select('products.*', 'products.name as name', '' . $stockId . ' as in_stock');
                        }])->get();
                }


                return response()->json([
                        'success' => true,
                        'message' => 'Products Wishlist Successfully.',
                        'data' => $wishlist->first()->wishlist->unique('id')->values()
                ], 200);
        }


        public function remove_all(Request $request, $id)
        {
                $customer = auth('customers')->user();

                DB::table('wishlist')->where('customer_id', $customer->id)->delete();


                return response()->json([
                        'success' => true,
                        'message' => 'Wishlist  Cleared Successfully.',

                ], 200);
        }



        public function add_cart_form_wish(Request $request)
        {
                $customer = auth('customers')->user();
                $weight = 1;
                $store_id = '01';

                foreach ($request->id as $id) {

                        $product = Product::where('id', $id)->first();

                        $cart = Cart::where('product_id', $id)->where('customer_id', $customer->id)->first();

                        if (!is_null($cart)) {
                                $data = json_decode($cart->details);
                                $data->quantity = $data->quantity + 1;

                                if ($product->PriceUnit == 'kg') {
                                        if ($product->discountprice == "0") {

                                                $data->total = $product->regular_price * $data->quantity * (250 / 1000);
                                        } else {
                                                $data->total = $product->discountprice * $data->quantity * (250 / 1000);
                                        }
                                } else {
                                        if ($product->discountprice == "0") {
                                                $data->total = $product->regular_price * (float)$data->quantity;
                                        } else {
                                                $data->total = $product->discountprice * (float)$data->quantity;
                                        }
                                }



                                Cart::where('product_id', $id)->where('customer_id', $customer->id)->update(['details' => json_encode($data)]);
                        } else {

                                if ($product->PriceUnit == 'kg') {


                                        $weight = 250;
                                        if ($product->discountprice == "0") {

                                                $total = $product->regular_price * 1 * (250 / 1000);
                                        } else {
                                                $total = $product->discountprice * 1 * (250 / 1000);
                                        }

                                        $product = collect($product)->put('total', $total);
                                        $product = collect($product)->put('quantity', 1);
                                        $product = collect($product)->put('weight', 250);
                                } else {
                                        $total = $product->regular_price * 1;
                                        $product = collect($product)->put('total', $total);
                                        $product = collect($product)->put('quantity', 1);
                                        $product = collect($product)->put('weight', 0);
                                }




                                Cart::create(['customer_id' => $customer->id, 'product_id' => $product->first(), 'status' => 'available', 'details' => json_encode($product)]);
                        }
                }

                return response()->json([
                        'success' => true,
                        'message' => 'Product Added To Cart Successfully',
                ], 200);
        }
}
