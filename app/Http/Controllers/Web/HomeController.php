<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Banner;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Customer;
use App\Models\ImageSlider;
use App\Models\Message;
use App\Models\OrderHistory;
use App\Models\Product;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\Models\Cart;
// use Illuminate\Contracts\Validation\Validator;

use \Validator;

class HomeController extends Controller

{

    public function branch()
    {
        $all = collect();

        if (isset($_COOKIE['lat'])) {
            $lat1 = $_COOKIE['lat'];
        } else {
            $lat1 = 30.027679;
        }
        if (isset($_COOKIE['long'])) {
            $lon1 = $_COOKIE['long'];
        } else {
            $lon1 = 31.486457;
        }
        $branches = Branch::where('status', 'open')->get();
        foreach ($branches as $branch) {
            $lat2 = $branch['latitudes'];
            $lon2 = $branch['longitudes'];
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km = $miles * 1.609344;
            $all->push(['id' => $branch['id'], 'km' => round($km, 2), 'image' => $branch['image'], 'location' => $branch['location'], 'store_id' => $branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();
        if (isset($_COOKIE['current_branch_id'])) {
            // dd('a');
            // dd($_COOKIE['current_branch_id'] , $_COOKIE['Branch_id']);

            // if($_COOKIE['current_branch_id'] != $_COOKIE['Branch_id']){
            $store_id = $_COOKIE['current_branch_id'];
            // }else{

            // }
            // dd($_COOKIE['current_branch_id']);

        } else {
            // dd('b');
            $store_id = $all[0]['store_id'];
        }
        // dd($store_id);
        return  $store_id;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function redirect()
    {

        $id = \Cache::get('Branch_id');

        if (is_null($id)) {
            return $this->index(app()->getLocale(), '01');
        } else {
            return $this->index(app()->getLocale(), $id);
        }
    }

    public function show_product($lang, $id_product, $catId)
    {


        if (\Cache::get('Branch_id')) {
            $id = \Cache::get('Branch_id');
        } else {
            $id = '01';
        }

        $nearst['store_id'] = $this->branch();

        $branches = Branch::where('status', 'open')->get();


        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }

        $store_id = $branch->store_id;
        if (is_null($store_id)) {
            $store_id = 01;
        }
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
                break;
            case 07:
                $showId .= 7;
        }

        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
                break;
            case 07:
                $stockId .= 7;
        }

        $cat = [];

        if ($store_id != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }


        $customer = null;
        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        $product = Product::where('id', $id_product)->select(['*', '' . $stockId . ' as in_stock'])->first();
        $offers = Product::where('id', '!=', $id_product)->where('category_id', $catId)->select(['*', '' . $stockId . ' as in_stock'])->limit('20')->get();
        if ($lang == 'ar') {
            $product = Product::where('id', $id_product)->select(['*', 'name_ar as name', 'description_ar as description', '' . $stockId . ' as in_stock'])->first();
            $offers = Product::where('id', '!=', $id_product)->where('category_id', $catId)->select(['*', 'name_ar as name', 'description_ar as description', '' . $stockId . ' as in_stock'])->limit('20')->get();
        }

        return view('website.show_product', compact('product', 'catId', 'branches', 'branch', 'cat', 'main_categories', 'pros', 'customer', 'offers'));
    }
    public function index($lang, $id)
    {

        if (isset($_COOKIE['Branch_id'])) {

            setcookie("current_branch_id", $_COOKIE['Branch_id'], time() + 36000, '/');

            setcookie("Branch_id", $_COOKIE['Branch_id'], time() + 36000, '/');
        } else {
            setcookie("current_branch_id", '02', time() + 36000, '/');

            setcookie("Branch_id", '02', time() + 36000, '/');
        }



        \Cache::forever('Branch_id', $id);
        \Cache::forever('current_branch_id', $id);



        $sliders = ImageSlider::where('type', 'web')->get();
        $banners = Banner::where('type', 'web')->get();
        $bannersMob = Banner::where('type', 'webMobile')->get();





        $nearst['store_id'] = $this->branch();


        $customer = null;
        $pros = array();

        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }

        if (request()->segment(4)) {
            $store = request()->segment(4);

            if (request()->segment(4) == 01 || request()->segment(4) == 02 || request()->segment(4) == 03 || request()->segment(4) == 05 || request()->segment(4) == 04  || request()->segment(4) == 06) {
                $nearst['store_id'] = $store;
            }
        }

        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        // dd($branch);
        $store_id = $nearst['store_id'];

        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }

        $store_id = $branch->store_id;

        $cat = [];
        $main_categories = [];
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
                break;
            case 07:
                $showId .= 7;
        }

        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
                break;
            case 07:
                $stockId .= 7;
        }

        //  dd($nearst['store_id']);
        if ($store_id != '04') {

            $main_categories = Category::where($showId, '!=', 0)
                ->whereNull('parent')
                ->where('id', '!=', '5637168576')
                ->orderBy('order')
                ->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)
                ->whereNull('parent')
                ->where('id', '!=', '5637168576')
                ->orderBy('order')
                ->get();
        }


        $readymeal = Category::where('slug', 'ready-meals')->first();
        if ($readymeal) {
            $readymeal_id = Category::where('slug', 'ready-meals')->first()->id;

            $readyMeals = Product::where('category_id', $readymeal_id)
                ->where($store_id, 'true')
                ->get();





            $children = Category::where('parent', $readymeal_id)->get()->pluck('id')->ToArray();
            $subChildern = Category::whereIn('parent', $children)->get()->pluck('id')->ToArray();
            $last = Category::whereIn('parent', $subChildern)->get()->pluck('id')->ToArray();
            $final = Category::whereIn('parent', $last)->get()->pluck('id')->ToArray();
            array_push($children, $readymeal_id);
            $total = array_merge($children, $subChildern, $last, $final);

            $products_data = Product::whereIn('category_id', $total)->select(['*', '' . $stockId . ' as in_stock'])
                ->get();



            $result = $products_data->values()->unique('barcode');;
            $readyMeals = $result->take(6);
            // dd(  $readyMeals);
        } else {
            $readyMeals = collect();
        }

        $offers = Product::where('on_sale', '1')
            ->where($store_id, 'true')
            ->limit('20')->get();
        if ($lang == 'ar') {
            $offers = Product::where('on_sale', '1')
                ->where($store_id, 'true')->select(['*', 'name_ar as name', 'description_ar as description'])->limit('20')->get();
        }
        $offers = $offers->unique('barcode');

        $branches = Branch::where('status', 'open')->get();

        $category = Category::where('id', '=', $offers->first()->category_id)->get();


        return view('website.index', compact('offers', 'main_categories', 'cat', 'readyMeals', 'customer', 'branches', 'bannersMob', 'branch', 'sliders', 'banners', 'pros', 'nearst'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function getProducts($lang, $id)
    {

        $products = OrderHistory::where('id', $id)->first();
        $proArray = [];
        $check_01 = '01';
        $check_02 = '02';
        $check_04 = '04';
        $check_05 = '05';
        $check_06 = '06';
        $check_07 = '07';
        foreach (json_decode($products->OrderEntry) as $product) {
            $pro = Product::where("id", "LIKE", "%" . $product->id . "%")->first();

            if (!is_null($pro)) {
                if (isset($product->weight)) {
                    $weight =  $product->weight;
                } else {
                    $weight =  0;
                }

                array_push($proArray, array(

                    "id" => $pro->id,
                    "name" => $pro->name,
                    "description" => $pro->description,
                    "name_ar" => $pro->name_ar,
                    "description_ar" => $pro->description_ar,
                    "on_sale" => $pro->on_sale,
                    "regular_price" => $pro->regular_price,
                    "hot_price" => $pro->hot_price,
                    "discountFrom" => $pro->regular_price,
                    "discountto" => $pro->discountto,
                    "discountprice" => $pro->discountprice,
                    "barcode" => $pro->barcode,
                    "PriceUnit" =>  $pro->PriceUnit,
                    "weight" => $weight,
                    "in_stock" =>  $pro->in_stock,
                    "quantity" => $product->quantity,
                    "category_id" => $pro->category_id,
                    "images" => $pro->images,
                    "01" => $pro->$check_01,
                    "02" => $pro->$check_02,
                    "04" => $pro->$check_04,
                    "limit" => $pro->limit,
                    "categories" => $pro->categories,
                    "created_at" => $pro->created_at,
                    "updated_at" => $pro->updated_at,
                    "sale_price" => $pro->sale_price



                ));
            }
        }
        return response()->json($proArray);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lang = app()->getLocale();
        if ($request->coordinates == null) {
            if (app()->getLocale() == 'ar') {
                return response()->json([
                    'error' => true,
                    'message' => 'يجب عليك تمكين اكتشاف الموقع'
                ], 500);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'You Must Enable Location Services for Your Browser'
                ], 500);
            }
        }
        // else
        // {
        // $request->coordinates='30.0653269,31.4707391';
        $validator = Validator::make($request->all(), [
            'local_storage' => 'required',
            'total_price' => 'required',
            'name' => 'required',
            'email' => 'required',
            'number' => 'required|regex:/(01)[0-9]{9}/|numeric',
            'city' => 'required',
            'area' => 'required',
            'coordinates' => 'required',

            'address' => 'required',
            'apartment_no' => 'required',
            'floor_no' => 'required',
            'building_number' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            // dd('aa');
            $cords = explode(",", $request->coordinates);
            $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/oscar-a5ff2-firebase-adminsdk-xokaz-083871fea4.json');
            $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->withDatabaseUri('https://oscarnewfinal-default-rtdb.firebaseio.com')
                ->create();
            $database = $firebase->getDatabase();
            $getUser = $database
                ->getReference('users')
                ->orderByChild('email')
                ->equalTo('hadeel.mostafa@momentum-sol.com')
                ->getValue();
            $user = array_keys($getUser);
            $getZones = $database
                ->getReference('/users/' . $user[0] . '/geoFences')

                ->getValue();

            $zones = (array_values($getZones));
            $arr_check = [];
            foreach ($zones as $zo) {
                $arr_x = [];
                $arr_y = [];
                foreach ($zo['cords'] as $zone) {
                    array_push($arr_x, $zone['lat']);
                    array_push($arr_y, $zone['lng']);
                }
                $points_polygon = count($arr_x) - 1;  // number vertices - zero-based array

                $check = $this->is_in_polygon($points_polygon, $arr_x, $arr_y, $cords[0], $cords[1]);

                if ($check) {
                    array_push($arr_check, $zo);
                }
            }
            // dd(count($arr_check) ,$arr_check ,$cords[0],$cords[1]);
            if ($arr_check == []) {
                if (app()->getLocale() == 'en') {
                    return response()->json(['errors' => ['message' => 'Out Of Zone']]);
                } else {
                    return response()->json(['errors' => ['message' => 'خارج المنطقة']]);
                }
            }


            $data = $request->all();
            // dd('aa');
            if (\Cache::get('Branch_id')) {
                $id = \Cache::get('Branch_id');
            } else {
                $id = '01';
            }
            $proArray = [];
            $nearst['store_id'] = $this->branch();
            if (\Cache::get('Branch_id')) {
                $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
            } else {
                $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
            }

            $store_id = $branch->store_id;
            $client = new Client();
            $stockId = 'in_stock_';
            switch ($store_id) {
                case 01:
                    $stockId .= 1;
                    break;
                case 02:
                    $stockId .= 2;
                    break;
                case 04:
                    $stockId .= 4;
                    break;
                case 05:
                    $stockId .= 5;
                    break;
                case 06:
                    $stockId .= 6;
                    break;
                case 07:
                    $stockId .= 7;
            }
            $stockId = 'in_stock_';
            switch ($store_id) {
                case 01:
                    $stockId .= 1;
                    break;
                case 02:
                    $stockId .= 2;
                    break;
                case 04:
                    $stockId .= 4;
                    break;
                case 05:
                    $stockId .= 5;
                    break;
                case 06:
                    $stockId .= 6;
                    break;
                case 07:
                    $stockId .= 7;
            }
            $check_01 = '01';
            $check_02 = '02';
            $check_04 = '04';
            $cart = Cart::where('customer_id', auth()->guard('customerForWeb')->user()->id)->get();
            $proArray = [];
            foreach ($cart as $productData) {
                $product = json_decode($productData->details);

                $pro = Product::where("id", "LIKE", "%" . $product->id . "%")->where($stockId, 1)->first();

                $status = 'available';


                if (is_null($pro)) {
                    $status = 'available';
                }

                if (isset($product->weight)) {
                    $weight =  $product->weight;
                } else {
                    $weight =  0;
                }
                if (!is_null($pro)) {
                    if ($product->weight != 1) {
                        array_push($proArray, array(

                            "product_id" => $pro->id,
                            "price_unit" =>  $pro->PriceUnit,
                            "weight" => (string)$weight,
                            "quantity" => $product->quantity,



                        ));
                    } else {
                        array_push($proArray, array(

                            "product_id" => $pro->id,
                            "price_unit" =>  $pro->PriceUnit,

                            "quantity" => $product->quantity,



                        ));
                    }
                }
            }




            if ($request->has('delivery_date') && $request->delivery_date != null) {
                $date = strtotime($request->delivery_date);
                $date = date('d/m/Y h:m a', $date);
            } else {
                $date = date('d/m/Y h:m a');
            }


            $order_request = $client->request('POST', 'http://34.105.27.34/oscar/public/api/orders', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    "token" => \Auth::guard('customerForWeb')->user()->token,
                    "set_paid" => false,
                    "address" => [
                        "name" => $request['name'],
                        "address" => $request['address'],
                        "city" => $request['city'],
                        "area" => $request['area'],


                    ],
                    "user" => [
                        "apartment_no" => $request['apartment_no'],
                        "floor_no" => $request['floor_no'],
                        "building_no" => $request['building_number']
                    ],
                    "coordinates_new" => $request->coordinates,
                    "notes" => $request->notes,
                    "phone" => $request->number,
                    "payment_method" => $request->pay,
                    "line_items" => $proArray,
                    "cost" => $request->cost,
                    "flag" => $request->flag,
                    "delivery_date" => $date,
                    "store_id" => $id,
                ],
            ]);

            $data = json_decode($order_request->getBody());
            // dd($data);
            if (true) {
                $cart = Cart::where('customer_id', \Auth::guard('customerForWeb')->user()->id)->delete();
                $url = $lang;
                // dd('/'.$lang.'/homeWeb/branch/'.$id);
                return response()->json(['success' => true, 'message' => 'Order Sent Successfully', 'redirect' => '/' . $lang . '/homeWeb/branch/' . $store_id]);
            } else {
                return response()->json(['error' => true, 'message' => 'Order Sent Failed']);
            }
        }
    }


    public function flags(Request $request)
    {




        $cords = explode(",", $request->coordinates);
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/oscar-a5ff2-firebase-adminsdk-xokaz-083871fea4.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://oscarnewfinal-default-rtdb.firebaseio.com')
            ->create();
        $database = $firebase->getDatabase();
        $getUser = $database
            ->getReference('users')
            ->orderByChild('email')
            ->equalTo('hadeel.mostafa@momentum-sol.com')
            ->getValue();
        $user = array_keys($getUser);
        $getZones = $database
            ->getReference('/users/' . $user[0] . '/geoFences')

            ->getValue();

        $zones = (array_values($getZones));
        $arr_check = [];
        foreach ($zones as $zo) {
            $arr_x = [];
            $arr_y = [];
            foreach ($zo['cords'] as $zone) {
                array_push($arr_x, $zone['lat']);
                array_push($arr_y, $zone['lng']);
            }
            $points_polygon = count($arr_x) - 1;  // number vertices - zero-based array

            $check = $this->is_in_polygon($points_polygon, $arr_x, $arr_y, $cords[0], $cords[1]);

            if ($check) {
                array_push($arr_check, $zo);
            }
        }
        // dd(count($arr_check) ,$arr_check ,$cords[0],$cords[1]);
        // dd(count($arr_check) < 1);
        if (count($arr_check) < 1) {
            if (app()->getLocale() == 'en') {
                return response()->json(['errors' => ['message' => 'Out Of Zone']]);
            } else {
                return response()->json(['errors' => ['message' => 'خارج المنطقة']]);
            }
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Get Orders successfully.',
                'data' => ['flags' => array_values($arr_check[0]['flags'])],
            ], 200);
        }
        // dd($arr_check);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang, $slug)
    {


        $id = $_COOKIE['Branch_id'];

        // $all=collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        // if(isset($_COOKIE['lat'])){
        // $lat1= $_COOKIE['lat'];

        // }else{
        //     $lat1= 31.3655877;
        // }
        // if(isset($_COOKIE['long'])){
        // $lon1=$_COOKIE['long'];

        // }else{
        //     $lon1= 31.3655877;

        // }
        $branches = Branch::where('status', 'open')->get();

        // foreach($branches as $branch)
        // {
        // 	$lat2=$branch['latitudes'];
        // 	$lon2=$branch['longitudes'];
        // 	$theta = $lon1 - $lon2;
        // 	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        // 	$dist = acos($dist);
        // 	$dist = rad2deg($dist);
        // 	$miles = $dist * 60 * 1.1515;
        // 	$km = $miles * 1.609344;
        //     $all->push(['id'=>$branch['id'],'km'=>round($km, 2),'image'=>$branch['image'],'location'=>$branch['location'],'store_id'=>$branch['store_id']]);
        // }
        // $all->sortBy('km')->values()->toArray();

        // $nearst=$all[0];

        // if($id){
        //     $nearst['store_id']=$id;
        // }
        $nearst['store_id'] = $this->branch();

        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        $store_id = $nearst['store_id'];
        if (is_null($store_id)) {
            $store_id = 01;
        }
        // $countId='count_';
        // switch($store_id)
        // {
        // case 01:$countId.=1;
        // break;
        // case 02:$countId.=2;
        // break;
        // case 04:$countId.=4;
        // break;
        // case 05:$countId.=5;
        // }
        $cat = [];
        $showId = 'show_';
        switch ($nearst['store_id']) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        $customer = null;
        $pros = array();
        if ($nearst['store_id'] != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {
                array_push($pros, $pro->id);
            }
        }


        $categories = Category::all(); //to show all categories in category list
        $category = Category::where('slug', $slug)->first(); //to show selected category info
        if ($category) {
            $products = Product::where('category_id', $category->id)->paginate(36); //to show children products of these category

        } else {
            $products = [];
        }

        return view('website.products', compact('main_categories', 'cat', 'categories', 'products', 'customer', 'pros', 'branches', 'branch'));
    }




    public function showallproducts(Request $request, $lang, $id) //to show all children products of these category
    {

        $branches = Branch::where('status', 'open')->get();


        $nearst['store_id'] = $this->branch();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }


        $store_id = $branch->store_id;

        $cat = [];
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
                break;
            case 07:
                $showId .= 7;
        }
        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
                break;
            case 07:
                $stockId .= 7;
        }
        if ($store_id != '04') {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        $products = Product::select(['*', '' . $stockId . ' as in_stock'])->get();

        if ($lang == 'ar') {
            $products = Product::select(['*', 'name_ar as name', 'description_ar as description', '' . $stockId . ' as in_stock'])->get();
        }
        $category = Category::find($id);

        $res = collect($products)->filter(function ($p) use ($category, $id) {

            $categories = collect($p['categories']);

            $subCategories = $category->children->pluck('id')->toArray();

            foreach ($subCategories as $subCat) {

                if ($categories->contains('id', $subCat)) return true;
                if ($categories->contains('parent', $subCat)) return true;
            }

            return $categories->contains('id', $id);
        })->values();

        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        if (!isset($request->page)) {
            $request->page = 1;
        }
        $res = $this->paginate($res, $category->id, $request->page);


        return view('website.products', compact('category', 'cat', 'main_categories', 'res', 'branches', 'branch', 'pros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function paginate($items, $cat, $page)
    {
        $perPage = 36;
        $options = ['path' => '/' . app()->getLocale() . '/homeWeb/all_products/' . $cat, 'pageName' => 'page'];
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    public function promotionpaginate($items, $page)
    {
        $perPage = 36;
        $options = ['path' => '/' . app()->getLocale() . '/all_promotions/', 'pageName' => 'page'];
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);

        ($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }


    public function wishList(Request $request)
    {

        $product = Product::where('id', $request->id)->first();
        $customer_id = auth()->guard('customerForWeb')->user()->id;
        $customer = Customer::where('id', $customer_id)->first();
        $customer->products()->attach($product);

        return response()->json($product);
    }

    public function unLike(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        $customer_id = auth()->guard('customerForWeb')->user()->id;
        $customer = Customer::where('id', $customer_id)->first();
        $customer->products()->detach($product);

        return response()->json($product->id);
    }



    public function search(Request $request, $lang)
    {



        if (\Cache::get('Branch_id')) {
            $cookie_id = \Cache::get('Branch_id');
        } else {
            $cookie_id = '01';
        }


        $branches = Branch::where('status', 'open')->get();


        $nearst['store_id'] = $this->branch();

        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }


        $store_id = $branch->store_id;


        $cat = [];
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
        }
        if ($store_id != '04') {
            // dd($showId);
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }




        $search = explode('?', $request->get('search'))[0];
        if ($lang == 'ar') {
            $products = Product::where(function ($q) use ($search) {

                $q->where("name", "LIKE", "%" . $search . "%")
                    ->orWhere("description", "LIKE", "%" . $search . "%")
                    ->orWhere("name_ar", "LIKE", "%" . $search . "%")
                    ->orWhere("description_ar", "LIKE", "%" . $search . "%");
            })->select(['*', 'name_ar as name', 'description_ar as description', '' . $stockId . ' as in_stock'])->get();
        } else {
            $products = Product::where(function ($q) use ($search) {

                $q->where("name", "LIKE", "%" . $search . "%")
                    ->orWhere("description", "LIKE", "%" . $search . "%")
                    ->orWhere("name_ar", "LIKE", "%" . $search . "%")
                    ->orWhere("description_ar", "LIKE", "%" . $search . "%");
            })->select(['*', '' . $stockId . ' as in_stock'])->get();
        }
        $products = $products->unique('barcode');
        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }

        if (!isset($request->page)) {
            $request->page = 1;
        }
        $perPage = 36;
        $options = ['path' => '/' . app()->getLocale() . '/homeWeb/search_product?', 'pageName' => 'page'];
        $page = $request->page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $products instanceof Collection ? $products : Collection::make($products);
        $res = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        // dd($items);
        return view('website.products', compact('main_categories', 'cat', 'branch', 'branches', 'res', 'pros'));
    }




    public function cart()
    {
        $pros = array();

        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();
            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        if (\Cache::get('Branch_id')) {
            $cookie_id = \Cache::get('Branch_id');
        } else {
            $cookie_id = '01';
        }



        $branches = Branch::where('status', 'open')->get();


        $nearst['store_id'] = $this->branch();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }
        $store_id = $branch->store_id;
        if (is_null($store_id)) {
            $store_id = 01;
        }

        $cat = [];
        $showId = 'show_';
        switch ($nearst['store_id']) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        if ($nearst['store_id'] != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        if (auth()->guard('customerForWeb')->user()) {
            $customer = auth()->guard('customerForWeb')->user();
            $cart = Cart::where('customer_id', $customer->id)->get();
        } else {
            $car = collect();
        }


        $store_id = $store_id;
        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
        }
        $check_01 = '01';
        $check_02 = '02';
        $check_04 = '04';
        $proArray = [];
        foreach ($cart as $productData) {
            $product = json_decode($productData->details);

            $pro = Product::where("id", "LIKE", "%" . $product->id . "%")->where($stockId, 1)->first();

            $status = 'available';


            if (is_null($pro)) {
                $status = 'available';
            }
            if (!is_null($pro)) {
                if ($pro->$store_id == 'false') {
                    $status = 'unavailable';
                }
            } else {
                $status = 'unavailable';
            }
            if (isset($product->weight)) {
                $weight =  $product->weight;
            } else {
                $weight =  0;
            }
            if (!is_null($pro)) {
                array_push($proArray, array(

                    "id" => $pro->id,
                    "name" => $pro->name,
                    "description" => $pro->description,
                    "name_ar" => $pro->name_ar,
                    "description_ar" => $pro->description_ar,
                    "on_sale" => $pro->on_sale,
                    "regular_price" => $pro->regular_price,
                    "hot_price" => $pro->hot_price,
                    "discountFrom" => $pro->regular_price,
                    "discountto" => $pro->discountto,
                    "discountprice" => $pro->discountprice,
                    "barcode" => $pro->barcode,
                    "PriceUnit" =>  $pro->PriceUnit,
                    "weight" => (string)$weight,
                    "in_stock" =>  $pro->in_stock,
                    "quantity" => $product->quantity,
                    "category_id" => $pro->category_id,
                    "images" => $pro->images,
                    "01" => $pro->$check_01,
                    "02" => $pro->$check_02,
                    "04" => $pro->$check_04,
                    "limit" => $pro->limit,
                    "categories" => $pro->categories,
                    "created_at" => $pro->created_at,
                    "updated_at" => $pro->updated_at,
                    "sale_price" => $pro->sale_price,
                    "status" => $status,
                    "total" => $product->total



                ));
            }
        }

        return view('website.cart', compact('proArray', 'main_categories', 'cat', 'branch', 'branches', 'pros'));
    }

    public function addToCart(Request $request)
    {

        $product = Product::where('id', $request->id)->first();

        return response()->json($product);
    }


    public function checkout()
    {

        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();
            $pros = array();
            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }

        if (\Cache::get('Branch_id')) {
            $id = \Cache::get('Branch_id');
        } else {
            $id = '01';
        }

        $branches = Branch::where('status', 'open')->get();


        $nearst['store_id'] = $this->branch();

        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        $store_id = $nearst['store_id'];
        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }


        $store_id = $branch->store_id;

        if (is_null($store_id)) {
            $store_id = 01;
        }

        $cat = [];
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        if ($store_id != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();
        $products = Product::all();
        $addresses = Address::where('customer_id', $customer->id)->get();

        return view('website.checkout', compact('main_categories', 'cat', 'products', 'customer', 'branches', 'addresses', 'branch', 'pros'));
    }

    public function contact()
    {


        $branches = Branch::where('status', 'open')->get();


        $nearst['store_id'] = $this->branch();

        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }


        $store_id = $branch->store_id;

        if (is_null($store_id)) {
            $store_id = 01;
        }

        $cat = [];
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        if ($store_id != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }

        return view('website.contact', compact('main_categories', 'cat', 'branch', 'branches', 'pros'));
    }
    public function aboutus()
    {
        $id = $_COOKIE['Branch_id'];


        $branches = Branch::where('status', 'open')->get();


        $nearst['store_id'] = $this->branch();

        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }


        $store_id = $branch->store_id;

        if (is_null($store_id)) {
            $store_id = 01;
        }

        $cat = [];
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        if ($store_id != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        $about = \DB::table('pages_website')->first();


        return view('website.about-us', compact('about', 'main_categories', 'cat', 'branch', 'branches', 'pros'));
    }
    public function  privacy_policy()

    {
        $id = $_COOKIE['Branch_id'];


        $branches = Branch::where('status', 'open')->get();


        $nearst['store_id'] = $this->branch();

        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }


        $store_id = $branch->store_id;

        if (is_null($store_id)) {
            $store_id = 01;
        }
        $countId = 'count_';

        $cat = [];
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        if ($store_id != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        return view('website.privacy', compact('main_categories', 'cat', 'branch', 'branches', 'pros'));
    }


    public function  return_policy()

    {
        $id = $_COOKIE['Branch_id'];


        $branches = Branch::where('status', 'open')->get();


        $nearst['store_id'] = $this->branch();

        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }


        $store_id = $branch->store_id;

        if (is_null($store_id)) {
            $store_id = 01;
        }

        $cat = [];
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        if ($store_id != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        return view('website.return_policy', compact('main_categories', 'cat', 'branch', 'branches', 'pros'));
    }





    public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
    {
        $i = $j = $c = 0;
        for ($i = 0, $j = $points_polygon; $i < $points_polygon; $j = $i++) {
            if ((($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
                ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i])))
                $c = !$c;
        }
        return $c;
    }




    public function login()
    {
        if (\Cache::get('Branch_id')) {
            $id = \Cache::get('Branch_id');
        } else {
            $id = '01';
        }


        $branches = Branch::where('status', 'open')->get();


        $nearst['store_id'] = $this->branch();

        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }


        $store_id = $branch->store_id;

        if (is_null($store_id)) {
            $store_id = 01;
        }

        $cat = [];
        $showId = 'show_';
        switch ($nearst['store_id']) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        if ($nearst['store_id'] != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        return view('website.login', compact('main_categories', 'cat', 'branch', 'branches', 'pros'));
    }
    public function register()
    {
        if (\Cache::get('Branch_id')) {
            $id = \Cache::get('Branch_id');
        } else {
            $id = '01';
        }


        $branches = Branch::where('status', 'open')->get();


        $nearst['store_id'] = $this->branch();

        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        $store_id = $nearst['store_id'];
        if (is_null($store_id)) {
            $store_id = 01;
        }

        $cat = [];
        $showId = 'show_';
        switch ($nearst['store_id']) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        if ($nearst['store_id'] != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        return view('website.register', compact('main_categories', 'cat', 'branch', 'branches', 'pros'));
    }


    public  function all_promosion()
    {
        $branches = Branch::where('status', 'open')->get();

        $nearst['store_id'] = $this->branch();
        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }


        $store_id = $branch->store_id;

        if (is_null($store_id)) {
            $store_id = 01;
        }

        $store_id = $nearst['store_id'];
        $cat = [];
        $showId = 'show_';
        // dd($nearst['store_id']);
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }
        // dd($nearst['store_id']);
        if ($store_id != '04') {
            // dd( $showId);
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        $products = Product::where($store_id, 'true')->get();

        $offers = Product::where('on_sale', '1')
            ->where($store_id, 'true')
            ->get();
        $lang = app()->getLocale();
        if ($lang == 'ar') {
            $offers = Product::where('on_sale', '1')
                ->where($store_id, 'true')->select(['*', 'name_ar as name', 'description_ar as description'])->get();
        }
        $offers = $offers->unique('barcode');

        $pros = array();

        if (!isset(request()->page)) {
            request()->page = 1;
        }
        $res = $this->promotionpaginate($offers, request()->page);
        $category = '';
        // dd($res);
        return view('website.products', compact('cat', 'main_categories', 'res', 'branches', 'branch', 'pros'));
    }


    public function show_product_mobile($lang, $id_product)
    {


        if (\Cache::get('Branch_id')) {
            $id = \Cache::get('Branch_id');
        } else {
            $id = '01';
        }

        $nearst['store_id'] = $this->branch();

        $branches = Branch::where('status', 'open')->get();


        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }

        $store_id = $branch->store_id;
        if (is_null($store_id)) {
            $store_id = 01;
        }
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }

        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
        }

        $cat = [];

        if ($store_id != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }


        $customer = null;
        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        $product = Product::where('id', $id_product)->select(['*', '' . $stockId . ' as in_stock'])->first();
        $catId =  $product->category_id;
        $offers = Product::where('id', '!=', $id_product)->where('category_id', $catId)->select(['*', '' . $stockId . ' as in_stock'])->limit('20')->get();
        if ($lang == 'ar') {
            $product = Product::where('id', $id_product)->select(['*', 'name_ar as name', 'description_ar as description', '' . $stockId . ' as in_stock'])->first();
            $offers = Product::where('id', '!=', $id_product)->where('category_id', $catId)->select(['*', 'name_ar as name', 'description_ar as description', '' . $stockId . ' as in_stock'])->limit('20')->get();
        }

        return view('website.show_product', compact('product', 'catId', 'branches', 'branch', 'cat', 'main_categories', 'pros', 'customer', 'offers'));
    }
}
