<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Customer;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Session;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Cache::get('Branch_id')) {
            $id = \Cache::get('Branch_id');
        } else {
            $id = '01';
        }

        $all = collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        if (isset($_COOKIE['lat'])) {
            $lat1 = $_COOKIE['lat'];
        } else {
            $lat1 =   31.486457;
        }
        if (isset($_COOKIE['long'])) {
            $lon1 = $_COOKIE['long'];
        } else {
            $lon1 =   31.486457;
        }
        $branches = Branch::where('status', 'open')->get();

        foreach ($branches as $branch) {
            $lat2 = $branch['latitudes'];
            $lon2 = $branch['longitudes'];
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km = $miles * 1.609344;
            $all->push(['id' => $branch['id'], 'km' => round($km, 2), 'image' => $branch['image'], 'location' => $branch['location'], 'store_id' => $branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();

        $nearst = $all[0];

        if ($id) {
            $nearst['store_id'] = $id;
        }
        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }
        $store_id = $branch->store_id;
        if (is_null($store_id)) {
            $store_id = 01;
        }
        $countId = 'count_';
        switch ($store_id) {
            case 01:
                $countId .= 1;
                break;
            case 02:
                $countId .= 2;
                break;
            case 04:
                $countId .= 4;
                break;
            case 05:
                $countId .= 5;
                break;
            case 06:
                $countId .= 6;
                break;
            case 07:
                $countId .= 7;
        }
        $cat = [];
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
                break;
            case 07:
                $showId .= 7;
        }

        $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->get();





        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        \Cache::forget('pre_url');
        \Cache::forever('pre_url', url()->previous());


        return view('website.create_address', compact('main_categories', 'branches', 'pros', 'branch', 'cat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $lang)
    {


        if ($request->coordinates == null) {
            // \Session::flash('message', 'You Must Enable Location Services for Your Browser'); 
            // return back();
            $request->coordinates = '30.033333,31.233334';
        }
        $validate = $request->validate([
            'phone' => 'required',
            'name' => 'required',
            'address' => 'required',
            'area' => 'required',
            'city' => 'required',
            'coordinates' => 'required',
            'apartment_number' => 'required',
            'floor_number' => 'required',
            'building_number' => 'required'
        ]);

        $address = Address::Create([
            'name' => $request->name,
            'address' => $request->address,
            'area' => $request->area,
            'city' => $request->city,
            'customer_id' => auth()->guard('customerForWeb')->user()->id,
            'phone' => $request->phone,
            'coordinates' => $request->coordinates,
            'apartment_number' => $request->apartment_number,
            'floor_number' => $request->floor_number,
            'building_number' => $request->building_number
        ]);
        // dd(\Cache::get('pre_url'));
        if (\Cache::get('pre_url')) {
            $url = \Cache::get('pre_url');
            return redirect()->to($url);
        } else {
            return redirect($lang . '/profile');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, $id)
    {
        $address = Address::find($id);
        $coordinates = json_encode($address->coordinates);
        if (\Cache::get('Branch_id')) {
            $id = \Cache::get('Branch_id');
        } else {
            $id = '01';
        }
        $all = collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        if (isset($_COOKIE['lat'])) {
            $lat1 = $_COOKIE['lat'];
        } else {
            $lat1 =   31.486457;
        }
        if (isset($_COOKIE['long'])) {
            $lon1 = $_COOKIE['long'];
        } else {
            $lon1 =   31.486457;
        }
        $branches = Branch::where('status', 'open')->get();

        foreach ($branches as $branch) {
            $lat2 = $branch['latitudes'];
            $lon2 = $branch['longitudes'];
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km = $miles * 1.609344;
            $all->push(['id' => $branch['id'], 'km' => round($km, 2), 'image' => $branch['image'], 'location' => $branch['location'], 'store_id' => $branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();

        $nearst = $all[0];

        if ($id) {
            $nearst['store_id'] = $id;
        }
        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }
        $store_id = $branch->store_id;

        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
        }


        $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->get();




        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        // $cords=explode(",", str_replace('"', '', $coordinates));
        // $cords=json_encode($cords);
        return view('website.update_address', compact('pros', 'address', 'branch', 'main_categories', 'branches', 'coordinates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lang, $id)
    {
        $address = Address::find($id);
        $validate = $request->validate([
            'phone' => 'required|unique:address,id,' . $address->id,
            'name' => 'required',
            'address' => 'required',
            'area' => 'required',
            'city' => 'required',
            'coordinates' => 'required',
            'apartment_number' => 'required',
            'floor_number' => 'required',
            'building_number' => 'required'
        ]);
        $address = $address->Update([
            'name' => $request->name,
            'address' => $request->address,
            'area' => $request->area,
            'city' => $request->city,
            'customer_id' => auth()->guard('customerForWeb')->user()->id,
            'phone' => $request->phone,
            'coordinates' => $request->coordinates,
            'apartment_number' => $request->apartment_number,
            'floor_number' => $request->floor_number,
            'building_number' => $request->building_number
        ]);

        return redirect($lang . '/profile')->with('update', 'data updated successfully');
    }


    public function getAddress(Request $request)
    {
        $address = Address::find($request->id);


        $cords = explode(",",  $address->coordinates);

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/oscar-a5ff2-firebase-adminsdk-xokaz-083871fea4.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://oscarnewfinal-default-rtdb.firebaseio.com')
            ->create();
        $database = $firebase->getDatabase();
        $getUser = $database
            ->getReference('users')
            ->orderByChild('email')
            ->equalTo('hadeel.mostafa@momentum-sol.com')
            ->getValue();
        $user = array_keys($getUser);
        $getZones = $database
            ->getReference('/users/' . $user[0] . '/geoFences')

            ->getValue();

        $zones = (array_values($getZones));
        $arr_check = [];
        foreach ($zones as $zo) {
            $arr_x = [];
            $arr_y = [];
            foreach ($zo['cords'] as $zone) {
                array_push($arr_x, $zone['lat']);
                array_push($arr_y, $zone['lng']);
            }
            $points_polygon = count($arr_x) - 1;  // number vertices - zero-based array

            $check = $this->is_in_polygon($points_polygon, $arr_x, $arr_y, $cords[0], $cords[1]);

            if ($check) {
                array_push($arr_check, $zo);
            }
        }
        // dd(count($arr_check) ,$arr_check ,$cords[0],$cords[1]);
        if ($arr_check == []) {

            if (app()->getLocale() == 'en') {
                return response()->json(['errors' => ['message' => 'Out Of Zone']]);
            } else {
                return response()->json(['errors' => ['message' => 'خارج المنطقة']]);
            }
        }

        return response()->json([
            'success' => true,
            'message' => 'Get Orders successfully.',
            'data' => ['flags' => array_values($arr_check[0]['flags']), 'address' => $address],
        ], 200);



        // return response()->json($address);
    }




    public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
    {
        $i = $j = $c = 0;
        for ($i = 0, $j = $points_polygon; $i < $points_polygon; $j = $i++) {
            if ((($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
                ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i])))
                $c = !$c;
        }
        return $c;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang, $id)
    {
        $address = Address::find($id);
        $address->delete();
        return response()->json($address);
    }
}
