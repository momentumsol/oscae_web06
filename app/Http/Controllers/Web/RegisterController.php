<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\VerifyCustomer;
use App\Notifications\VerifyCustomer as Verify;
use App\Models\Branch;
use App\Models\Category;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        if (\Cache::get('Branch_id')) {

            $id = \Cookie::get('Branch_id');
        } else {
            $id = '01';
        }

        $all = collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        if (isset($_COOKIE['lat'])) {
            $lat1 = $_COOKIE['lat'];
        } else {
            $lat1 =   31.486457;
        }
        if (isset($_COOKIE['long'])) {
            $lon1 = $_COOKIE['long'];
        } else {
            $lon1 =   31.486457;
        }
        $branches = Branch::where('status', 'open')->get();

        foreach ($branches as $branch) {
            $lat2 = $branch['latitudes'];
            $lon2 = $branch['longitudes'];
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km = $miles * 1.609344;
            $all->push(['id' => $branch['id'], 'km' => round($km, 2), 'image' => $branch['image'], 'location' => $branch['location'], 'store_id' => $branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();

        $nearst = $all[0];

        if ($id) {
            $nearst['store_id'] = $id;
        }
        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        $store_id = $nearst['store_id'];
        if (is_null($store_id)) {
            $store_id = 01;
        }
        $countId = 'count_';
        switch ($store_id) {
            case 01:
                $countId .= 1;
                break;
            case 02:
                $countId .= 2;
                break;
            case 04:
                $countId .= 4;
                break;
            case 05:
                $countId .= 5;
                break;
            case 06:
                $countId .= 6;
                break;
            case 07:
                $countId .= 7;
        }

        if ($store_id != '04') {
            $categories = Category::where($countId, '!=', 0)->get();
        } else {
            $cat = Category::where('id', '5637168576')->first();
        }

        $validatedData = [
            'name' => 'required',
            'email' => 'required|email|unique:customers',
            'phone' => 'required|unique:customers|regex:/(01)[0-9]{9}/',
            'password' => 'required|min:8',
        ];
        $cat = [];
        $pros = [];
        $request->validate($validatedData);
        $token = str_random(32);
        $customer = Customer::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone' => $request->phone,
            'token' => $token
        ]);
        $customer = Customer::find($customer->id);


        $VerifyCustomer = VerifyCustomer::create([
            'customer_id' => $customer->id,
            'token' => $token
        ]);
        $customer->notify(new Verify($customer));
        $customer_integrationFromApi = $this->customer_integration($customer);
        \Auth::guard('customerForWeb')->loginUsingId($customer->id);
        return  view('website.checkverifyemail', compact('branches', 'branch', 'cat', 'categories', 'pros', 'customer'));
    }
    public function customer_integration($customer)
    {

        $url = 'http://41.33.238.100/RR_Oscar_API/api/Customer/CreateCustomer';
        $body = [
            'CustomerAccount' => '',
            'MobileNumber' => $customer->phone,
            'Email' => $customer->email,
            'FirstName' => $customer->name,
            'LastName' => 'Customer',
            "Birthday" => "10/11/1985"


        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($body));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $data = json_decode($server_output);
        Customer::where('id', $customer->id)->update([
            'account_no' => $data
        ]);
        return true;
    }

    public function verifyCustomer($lang, $token)
    {

        $VerifyCustomer = VerifyCustomer::where('token', $token)->first();

        if (isset($VerifyCustomer)) {
            $customer = $VerifyCustomer->customer;

            if (!$customer->verified) {
                $VerifyCustomer->customer->verified = 1;
                $VerifyCustomer->customer->save();
                return redirect('/en/homeWeb');
            } else {
                return redirect('/en/homeWeb');
            }
        } else {
            return view('/not_verified');
        }
    }
}
