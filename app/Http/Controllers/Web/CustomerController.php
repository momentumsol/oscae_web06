<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Customer;
use Illuminate\Support\Facades\Hash;
use App\Models\OrderHistory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $id = \Cookie::get('Branch_id');
        $all = collect();

        if (isset($_COOKIE['lat'])) {
            $lat1 = $_COOKIE['lat'];
        } else {
            $lat1 =   31.486457;
        }
        if (isset($_COOKIE['long'])) {
            $lon1 = $_COOKIE['long'];
        } else {
            $lon1 =   31.486457;
        }
        $branches = Branch::where('status', 'open')->get();
        foreach ($branches as $branch) {
            $lat2 = $branch['latitudes'];
            $lon2 = $branch['longitudes'];
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km = $miles * 1.609344;
            $all->push(['id' => $branch['id'], 'km' => round($km, 2), 'image' => $branch['image'], 'location' => $branch['location'], 'store_id' => $branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();
        if (isset($_COOKIE['current_branch_id'])) {
            // dd('a');
            // dd($_COOKIE['current_branch_id'] , $_COOKIE['Branch_id']);

            // if($_COOKIE['current_branch_id'] != $_COOKIE['Branch_id']){
            $store_id = $_COOKIE['current_branch_id'];
            // }else{

            // }
            // dd($_COOKIE['current_branch_id']);

        } else {
            // dd('b');
            $store_id = $all[0]['store_id'];
        }
        $nearst['store_id'] = $store_id;
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }
        $store_id = $branch->store_id;
        if (is_null($store_id)) {
            $store_id = 01;
        }
        $showId = 'show_';
        switch ($nearst['store_id']) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
                break;
            case 07:
                $showId .= 7;
        }
        $cat = [];

        if ($nearst['store_id'] != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }
        $loyality = '';
        $url = 'http://41.33.238.100/RR_OSCAR/RR_Services.asmx/VerifyUserNew';
        $body = [
            'EMail' => auth()->guard('customerForWeb')->user()->email,
            'Phone' => auth()->guard('customerForWeb')->user()->phone,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($body));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        if (isset(json_decode($server_output)->data)) {
            if (is_null(json_decode($server_output)->data)) {
                $loyality = '';
            } else {
                $loyality = json_decode($server_output)->data[0]->AccountNum;
            }
        }

        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
                break;
            case 07:
                $stockId .= 7;
        }
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            $wishlist = Customer::where('id', $customer->id)->with(["wishlist" => function ($q) use ($stockId) {
                $q->select('products.*', 'products.name_ar as name', '' . $stockId . ' as in_stock');
            }])->get();
            $pros = $wishlist->first()->wishlist->unique('id')->values();
        }
        $orders = OrderHistory::where('customer_id', auth()->guard('customerForWeb')->user()->id)
            ->Where('order_status', '=', '')
            ->orderBy('id', 'DESC')
            ->get();

        $pastorders = OrderHistory::where('customer_id', auth()->guard('customerForWeb')->user()->id)
            ->Where('order_status', '!=', '')
            ->orderBy('id', 'DESC')
            ->get();


        return view('website.customer-profile', compact('main_categories', 'pastorders', 'branch', 'branches', 'loyality', 'orders', 'pros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    public function email(Request $request)
    {

        if ($request->contact == '1') {
            $request->validate([
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required',
                // 'comment'=>'required'
            ]);
        } else {
            if ($request->order != null) {
                $request->validate([
                    'order' => 'required',
                    'order-message' => 'required'
                ]);
            } elseif ($request->driver != null) {
                $request->validate([
                    'driver' => 'required',
                    'driver-message' => 'required'
                ]);
            } else {
                $request->validate([
                    'other-message' => 'required'
                ]);
            }
        }

        Mail::to('info@oscargrandstores.com')
            ->send(new \App\Mail\ContactSupport(['data' => $request->all()]));
        if (app()->getLocale() == 'ar') {
            return redirect()->to('en/homeWeb')->with(session()->flash('email', 'شكرا لك على الاتصال بنا'));;
        } else {
            return redirect()->to('en/homeWeb')->with(session()->flash('email', 'Thank you for contacting us'));;
        }
    }

    public function wishlist()
    {

        $orders = OrderHistory::where('customer_id', auth()->guard('customerForWeb')->user()->id)->orderBy('id', 'DESC')->get();



        $branches = Branch::where('status', 'open')->get();
        $categories = Category::all();

        return view('website.customer-profile', compact('categories', 'branches', 'loyality', 'orders'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lang, $id)
    {
        $customer = Customer::find($id);

        $validate = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:customers,id,' . $customer->id,
            'phone' => 'required|unique:customers,id,' . $customer->id,
            'password' => 'confirmed'
        ]);

        if ($request->file('files')) {
            $ext = $request->file('files')->getClientOriginalExtension();
            $image_name = time() . '.' . $ext;
            $path = 'images/customers';
            $request->file('files')->move($path, $image_name);
            if ($customer->images != '1606647695.jpg') {
                Storage::disk('customers')->delete($customer->images);
            }
        } else {
            $image_name = $customer->images;
        }
        $customer->update(
            [
                'images' => $image_name,
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => bcrypt($request->password),
            ]
        );




        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function get_wishlist()
    {


        $id = \Cookie::get('Branch_id');
        $all = collect();

        if (isset($_COOKIE['lat'])) {
            $lat1 = $_COOKIE['lat'];
        } else {
            $lat1 =   31.486457;
        }
        if (isset($_COOKIE['long'])) {
            $lon1 = $_COOKIE['long'];
        } else {
            $lon1 =   31.486457;
        }
        $branches = Branch::where('status', 'open')->get();
        foreach ($branches as $branch) {
            $lat2 = $branch['latitudes'];
            $lon2 = $branch['longitudes'];
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km = $miles * 1.609344;
            $all->push(['id' => $branch['id'], 'km' => round($km, 2), 'image' => $branch['image'], 'location' => $branch['location'], 'store_id' => $branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();
        if (isset($_COOKIE['current_branch_id'])) {
        } else {

            $store_id = $all[0]['store_id'];
        }

        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }
        $store_id = $branch->store_id;

        if (is_null($store_id)) {
            $store_id = 01;
        }
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
                break;
            case 07:
                $showId .= 7;
        }
        $cat = [];

        if ($store_id != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }
        $loyality = '';
        $url = 'http://41.33.238.100/RR_OSCAR/RR_Services.asmx/VerifyUserNew';
        $body = [
            'EMail' => auth()->guard('customerForWeb')->user()->email,
            'Phone' => auth()->guard('customerForWeb')->user()->phone,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($body));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        if (isset(json_decode($server_output)->data)) {
            if (is_null(json_decode($server_output)->data)) {
                $loyality = '';
            } else {
                $loyality = json_decode($server_output)->data[0]->AccountNum;
            }
        }
        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
                break;
            case 07:
                $stockId .= 7;
        }
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            $wishlist = Customer::where('id', $customer->id)->with(["wishlist" => function ($q) use ($stockId) {
                $q->select('products.*', 'products.name_ar as name', '' . $stockId . ' as in_stock');
            }])->get();
            $pros = $wishlist->first()->wishlist->unique('id')->values();
        }

        $orders = OrderHistory::where('customer_id', auth()->guard('customerForWeb')->user()->id)
            ->Where('order_status', '=', '')
            ->orderBy('id', 'DESC')
            ->get();

        $pastorders = OrderHistory::where('customer_id', auth()->guard('customerForWeb')->user()->id)
            ->Where('order_status', '!=', '')
            ->orderBy('id', 'DESC')
            ->get();


        return view('website.wishlist', compact('main_categories', 'pastorders', 'branch', 'branches', 'loyality', 'orders', 'pros'));
    }
}
