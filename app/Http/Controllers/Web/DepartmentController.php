<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Customer;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = \Cookie::get('Branch_id');
        $id = \Cache::get('Branch_id');
        $all = collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        if (isset($_COOKIE['lat'])) {
            $lat1 = $_COOKIE['lat'];
        } else {
            $lat1 =   31.486457;
        }
        if (isset($_COOKIE['long'])) {
            $lon1 = $_COOKIE['long'];
        } else {
            $lon1 =   31.486457;
        }
        $branches = Branch::where('status', 'open')->get();

        foreach ($branches as $branch) {
            $lat2 = $branch['latitudes'];
            $lon2 = $branch['longitudes'];
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km = $miles * 1.609344;
            $all->push(['id' => $branch['id'], 'km' => round($km, 2), 'image' => $branch['image'], 'location' => $branch['location'], 'store_id' => $branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();

        $nearst = $all[0];

        if ($id) {
            $nearst['store_id'] = $id;
        }
        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        $store_id = $nearst['store_id'];
        if (is_null($store_id)) {
            $store_id = 01;
        }
        $countId = 'count_';
        switch ($store_id) {
            case 01:
                $countId .= 1;
                break;
            case 02:
                $countId .= 2;
                break;
            case 04:
                $countId .= 4;
                break;
            case 05:
                $countId .= 5;
                break;
            case 06:
                $countId .= 6;
                break;
            case 07:
                $countId .= 7;
        }
        $cat = [];

        $main_categories = [];
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
            case 07:
                $showId .= 7;
        }

        if ($store_id != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->get();
        }
        $customer = null;
        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        return view('website.department', compact('branches', 'branch', 'cat', 'main_categories', 'pros', 'customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
