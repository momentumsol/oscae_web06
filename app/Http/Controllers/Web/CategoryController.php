<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Customer;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Models\Branch;
use App\Models\Category;
use Cache;
use function GuzzleHttp\json_decode;

class CategoryController extends AppBaseController
{
    public function branch()
    {
        $all = collect();

        if (isset($_COOKIE['lat'])) {
            $lat1 = $_COOKIE['lat'];
        } else {
            $lat1 = 30.027679;
        }
        if (isset($_COOKIE['long'])) {
            $lon1 = $_COOKIE['long'];
        } else {
            $lon1 = 31.486457;
        }
        $branches = Branch::where('status', 'open')->get();
        foreach ($branches as $branch) {
            $lat2 = $branch['latitudes'];
            $lon2 = $branch['longitudes'];
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km = $miles * 1.609344;
            $all->push(['id' => $branch['id'], 'km' => round($km, 2), 'image' => $branch['image'], 'location' => $branch['location'], 'store_id' => $branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();
        if (isset($_COOKIE['current_branch_id'])) {
            // dd('a');
            // dd($_COOKIE['current_branch_id'] , $_COOKIE['Branch_id']);

            // if($_COOKIE['current_branch_id'] != $_COOKIE['Branch_id']){
            $store_id = $_COOKIE['current_branch_id'];
            // }else{

            // }
            // dd($_COOKIE['current_branch_id']);

        } else {
            // dd('b');
            $store_id = $all[0]['store_id'];
        }
        // dd($store_id);
        return  $store_id;
    }

    public function  get_category()
    {
        $lang = app()->getLocale();

        $html = '';
        $nearst['store_id'] = '02';
        if (\Cache::get('Branch_id')) {
            $branch = Branch::where('status', 'open')->where('store_id', \Cache::get('Branch_id'))->first();
        } else {
            $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        }

        $store_id = $branch->store_id;
        $stockId = 'in_stock_';
        switch ($store_id) {
            case 01:
                $stockId .= 1;
                break;
            case 02:
                $stockId .= 2;
                break;
            case 04:
                $stockId .= 4;
                break;
            case 05:
                $stockId .= 5;
                break;
            case 06:
                $stockId .= 6;
                break;
            case 07:
                $stockId .= 7;
        }
        $showId = 'show_';
        switch ($store_id) {
            case 01:
                $showId .= 1;
                break;
            case 02:
                $showId .= 2;
                break;
            case 04:
                $showId .= 4;
                break;
            case 05:
                $showId .= 5;
                break;
            case 06:
                $showId .= 6;
            case 07:
                $showId .= 7;
        }
        if ($store_id != '04') {
            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        } else {

            $main_categories = Category::where($showId, '!=', 0)->whereNull('parent')->where('id', '!=', '5637168576')->orderBy('order')->get();
        }

        if (is_null(\Cache::get("res"))) {


            foreach ($main_categories as  $i => $category) {
                $i += 1;
                $url = app()->getLocale() . '/homeWeb/all_products/' . $category->id;
                $html .= '<li class=" menu-item-has-children">
        <a  href="url(' . $url . ' ) "> <img style="margin: 0 8px 0 0 !important; display:inline !important" src="' . asset($category->icon_image) . '" width="20px" alt="" >' .  $category->name->$lang  . '</a>';
                foreach ($category->children as $sub1_child) {
                    if (isset($sub1_child->children)) {
                        $html .= '<ul class="mega-menu-wrap sub-menu">
                <li class="menu-item-has-children">
                    <div class="box-nav-popup" data-bg-src="assets/img/bg/nav-bg-1.png">
                        <div class="row" id="removeRow" style="margin: 0">
                            <ul style="    margin: 0 0 15px;">
                                <li style="text-decoration: underline;padding:0;border:0" ><a href=" url(' . $url . ')"> ' . __('lang.Show-all') . ' </a> </li>
                    </ul>';
                        foreach ($category->children as $sub1_child) {
                            if (count($sub1_child->children) > 1) {

                                $html .=    '<ul class="col-md-12 col-lg-4  ">
																			
                            <div class="widget widget_nav_menu">
                                <h3 class="widget-title" style="align-items: center;   "><a href="url( ' . app()->getLocale() . '/homeWeb/all_products/' . $sub1_child->id . ') " style=" display: inline;">  ' . $sub1_child->name->$lang . '</a> <i class="fa fa-plus  show_sub" style="padding: 0 2px 0;font-size: 10px !important;" ></i> </h3>
                                <div class="vs-box-nav" style="display: none">
                                    <ul>
                                        <li ><a href=" url( ' . app()->getLocale() . '/homeWeb/all_products/' . $sub1_child->id . ') "> ' . __('lang.Show-all') . ' </a> </li>';
                                foreach ($sub1_child->children as $sub2_child) {
                                    if (count($sub2_child->children) > 1) {
                                        $html .= '<li >
                                                <a href=" url( app()->getLocale()/homeWeb/all_products/$sub2_child->id ) ">' . $sub2_child->name->$lang  . ' </a></li>
                                            </li>';
                                    } else {
                                        $html .= '<li ><a href=" url( app()->getLocale()/homeWeb/all_products/$sub2_child->id) ">' . $sub2_child->name->$lang . '</a></li>';
                                    }
                                }
                                $html .= '</ul>
                                        </div>
                                    </div>
                                </ul>';
                            } else {
                                $html .= '<ul class="col-md-12 col-lg-4 ">
                                            <div class="widget widget_nav_menu">
                                                <h3 class="widget-title" style="align-items: center;   "><a href=" url( app()->getLocale()/homeWeb/all_products/$sub1_child->id ) " style=" display: inline;"> ' . $sub1_child->name->$lang . ' </a>
                                                </h3>
                                                <div class="vs-box-nav" style="display: none">
                                                    
                                                </div>
                                                
                                            </div>
                                        </ul>';
                            }
                        }
                    }
                    $html .= '</div>
															
                    </div>
                </li>
            </ul> ';
                }
                $html .= '</li>';
            }

            Cache::put("res", $html, 5);
        }
        $html = Cache::get("res");
        $sections = view("website.partials.categories_section", compact('html'))->render();


        return response()->json(['sections' => $sections]);
    }
}
