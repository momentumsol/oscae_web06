<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use GuzzleHttp\Client;

class LoginController extends Controller
{

    public function login(Request $request)
    {
        if (\Cache::get('Branch_id')) {
            $store_id = \Cache::get('Branch_id');
        } else {
            $store_id = '01';
        }
        $validatedData = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        try {

            $client = new Client();

            $login_request = $client->request('POST', 'http://34.105.27.34/oscar/public/api/customer/login', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    "email" => $request->email,
                    "password" =>  $request->password,
                ],
            ]);

            $data = json_decode($login_request->getBody());
            $token = $data->token;
            $user = Auth::guard('customerForWeb')->attempt(['email' => $request->email, 'password' => $request->password]);
            //  dd($user);
            // if($user->verified !=1){
            //     return view('not_verified');
            // }
            $user = Customer::find($user);
            // $token = auth('customerForWeb')->login($user);
            // dd($user);
            $user->update([
                'token' => $token
            ]);
        } catch (\Exception $e) {
            //  dd($e);
            return redirect()->back()->with('errs', 'email or password is invalid');
        }

        return redirect('/en/homeWeb');
    }

    public function logout(Request $request)
    {
        Auth::guard('customerForWeb')->logout();

        return redirect('/en/homeWeb');
    }


    public function forgot_password()
    {


        $id = \Cookie::get('Branch_id');

        $all = collect();


        // dd(isset(($_COOKIE['lat'])),$_COOKIE);
        if (isset($_COOKIE['lat'])) {
            $lat1 = $_COOKIE['lat'];
        } else {
            $lat1 = 31.3655877;
        }
        if (isset($_COOKIE['long'])) {
            $lon1 = $_COOKIE['long'];
        } else {
            $lon1 = 31.3655877;
        }
        $branches = Branch::where('status', 'open')->get();

        foreach ($branches as $branch) {
            $lat2 = $branch['latitudes'];
            $lon2 = $branch['longitudes'];
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km = $miles * 1.609344;
            $all->push(['id' => $branch['id'], 'km' => round($km, 2), 'image' => $branch['image'], 'location' => $branch['location'], 'store_id' => $branch['store_id']]);
        }
        $all->sortBy('km')->values()->toArray();

        $nearst = $all[0];

        if ($id) {
            $nearst['store_id'] = $id;
        }
        $branch = Branch::where('status', 'open')->where('store_id', $nearst['store_id'])->first();
        $store_id = $nearst['store_id'];
        if (is_null($store_id)) {
            $store_id = 01;
        }
        $countId = 'count_';
        switch ($store_id) {
            case 01:
                $countId .= 1;
                break;
            case 02:
                $countId .= 2;
                break;
            case 04:
                $countId .= 4;
                break;
            case 05:
                $countId .= 5;
                break;
            case 06:
                $countId .= 6;
                break;
            case 07:
                $countId .= 7;
        }

        $cat = [];
        $customer = '';
        if ($store_id != '04') {
            $categories = Category::where($countId, '!=', 0)->get();
        } else {
            $categories = Category::where($countId, '!=', 0)->where('id', '5637168576')->get();
        }
        $pros = array();
        if (auth()->guard('customerForWeb')->user()) {
            $customer = Customer::where('id', auth()->guard('customerForWeb')->user()->id)->first();

            foreach ($customer->products as $pro) {

                array_push($pros, $pro->id);
            }
        }
        return view('auth.passwords.forget', compact('branches', 'branch', 'cat', 'categories', 'pros', 'customer'));
    }


    public function forgot(Request $request)
    {

        $validatedData = $request->validate([
            'email' => 'required',
        ]);
        try {

            $client = new Client();

            $forgetPass_request = $client->request('POST', 'https://cms.oscarstoresapp.com//api/customer/forgot_password', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    "email" => $request->email,
                ],
            ]);

            $data = json_decode($forgetPass_request->getBody());
        } catch (\Exception $e) {
            return back()->with(['error' => 'invalid email']);
        }


        return redirect('/en/homeWeb')->with(session()->flash('reset', 'Check Your Email.'));
    }
}
