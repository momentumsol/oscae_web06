<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePromoVideoRequest;
use App\Http\Requests\UpdatePromoVideoRequest;
use App\Repositories\PromoVideoRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\PromoVideo;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PromoVideoController extends AppBaseController
{
	/** @var  PromoVideoRepository */
	private $promoVideoRepository;

	public function __construct(PromoVideoRepository $promoVideoRepo)
	{
		$this->promoVideoRepository = $promoVideoRepo;
	}

	/**
	 * Display a listing of the PromoVideo.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->promoVideoRepository->pushCriteria(new RequestCriteria($request));
		$promoVideos = $this->promoVideoRepository->all();

		return view('promo_videos.index')
			->with('promoVideos', $promoVideos);
	}

	/**
	 * Show the form for creating a new PromoVideo.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('promo_videos.create');
	}

	/**
	 * Store a newly created PromoVideo in storage.
	 *
	 * @param CreatePromoVideoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePromoVideoRequest $request)
	{
		$input = $request->all();
		$input['is_active'] = $request->get('is_active', 0);

		$promoVideo = $this->promoVideoRepository->create($input);

		Flash::success('Promo Video saved successfully.');

		return redirect(route('promoVideos.index'));
	}

	/**
	 * Display the specified PromoVideo.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$promoVideo = $this->promoVideoRepository->findWithoutFail($id);

		if (empty($promoVideo)) {
			Flash::error('Promo Video not found');

			return redirect(route('promoVideos.index'));
		}

		return view('promo_videos.show')->with('promoVideo', $promoVideo);
	}

	/**
	 * Show the form for editing the specified PromoVideo.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$promoVideo = $this->promoVideoRepository->findWithoutFail($id);

		if (empty($promoVideo)) {
			Flash::error('Promo Video not found');

			return redirect(route('promoVideos.index'));
		}

		return view('promo_videos.edit')->with('promoVideo', $promoVideo);
	}

	/**
	 * Update the specified PromoVideo in storage.
	 *
	 * @param  int              $id
	 * @param UpdatePromoVideoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatePromoVideoRequest $request)
	{
		$promoVideo = $this->promoVideoRepository->findWithoutFail($id);

		if (empty($promoVideo)) {
			Flash::error('Promo Video not found');
			return redirect(route('promoVideos.index'));
		}
		$inputs = $request->all();
		$inputs['is_active'] = $request->get('is_active', 0);
		dd($inputs);
		$promoVideo = $this->promoVideoRepository->update($inputs, $id);

		Flash::success('Promo Video updated successfully.');

		return redirect(route('promoVideos.index'));
	}

	/**
	 * Activate the specified PromoVideo in storage.
	 *
	 * @param  int              $id
	 * @param ActivatePromoVideoRequest $request
	 *
	 * @return Response
	 */
	public function activate($id, Request $request)
	{
		$promoVideo = $this->promoVideoRepository->findWithoutFail($id);

		if (empty($promoVideo)) {
			Flash::error('Promo Video not found');
			return redirect(route('promoVideos.index'));
		}
		$inputs['is_active'] = !$promoVideo->is_active;
		if($inputs['is_active']) {
			PromoVideo::query()->update(['is_active' => 0]);
		}
		$promoVideo = $this->promoVideoRepository->update($inputs, $id);

		Flash::success('Promo Video updated successfully.');

		return redirect(route('promoVideos.index'));
	}

	/**
	 * Remove the specified PromoVideo from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$promoVideo = $this->promoVideoRepository->findWithoutFail($id);

		if (empty($promoVideo)) {
			Flash::error('Promo Video not found');

			return redirect(route('promoVideos.index'));
		}

		$this->promoVideoRepository->delete($id);

		Flash::success('Promo Video deleted successfully.');

		return redirect(route('promoVideos.index'));
	}
}
