<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePageContentAPIRequest;
use App\Http\Requests\API\UpdatePageContentAPIRequest;
use App\Models\PageContent;
use App\Repositories\PageContentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PageContentController
 * @package App\Http\Controllers\API
 */

class PageContentAPIController extends AppBaseController
{
    /** @var  PageContentRepository */
    private $pageContentRepository;

    public function __construct(PageContentRepository $pageContentRepo)
    {
        $this->pageContentRepository = $pageContentRepo;
    }

    /**
     * Display a listing of the PageContent.
     * GET|HEAD /pageContents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pageContentRepository->pushCriteria(new RequestCriteria($request));
        $this->pageContentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pageContents = $this->pageContentRepository->all();

        return $this->sendResponse($pageContents->toArray(), 'Page Contents retrieved successfully');
    }

    /**
     * Store a newly created PageContent in storage.
     * POST /pageContents
     *
     * @param CreatePageContentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePageContentAPIRequest $request)
    {
        $input = $request->all();

        $pageContents = $this->pageContentRepository->create($input);

        return $this->sendResponse($pageContents->toArray(), 'Page Content saved successfully');
    }

    /**
     * Display the specified PageContent.
     * GET|HEAD /pageContents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PageContent $pageContent */
        $pageContent = $this->pageContentRepository->findWithoutFail($id);

        if (empty($pageContent)) {
            return $this->sendError('Page Content not found');
        }

        return $this->sendResponse($pageContent->toArray(), 'Page Content retrieved successfully');
    }

    /**
     * Update the specified PageContent in storage.
     * PUT/PATCH /pageContents/{id}
     *
     * @param  int $id
     * @param UpdatePageContentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePageContentAPIRequest $request)
    {
        $input = $request->all();

        /** @var PageContent $pageContent */
        $pageContent = $this->pageContentRepository->findWithoutFail($id);

        if (empty($pageContent)) {
            return $this->sendError('Page Content not found');
        }

        $pageContent = $this->pageContentRepository->update($input, $id);

        return $this->sendResponse($pageContent->toArray(), 'PageContent updated successfully');
    }

    /**
     * Remove the specified PageContent from storage.
     * DELETE /pageContents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PageContent $pageContent */
        $pageContent = $this->pageContentRepository->findWithoutFail($id);

        if (empty($pageContent)) {
            return $this->sendError('Page Content not found');
        }

        $pageContent->delete();

        return $this->sendResponse($id, 'Page Content deleted successfully');
    }
}
