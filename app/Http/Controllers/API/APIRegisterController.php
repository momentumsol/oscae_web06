<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Customer;
use App\Models\VerifyCustomer;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Mail\VerifyMail;
use App\Notifications\VerifyCustomer as Verify;

class APIRegisterController extends AppBaseController
{
	public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'required|string|email|max:255|unique:customers',
			'name' => 'required',
			'password' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json($validator->errors());
		}
		$customer_id = Customer::create([
			'name' => $request->get('name'),
			'email' => $request->get('email'),
			'password' => bcrypt($request->get('password')),
			'phone' => $request->get('phone'),
		]);
		$customer = Customer::find($customer_id->id);
		$token = auth('customers')->login($customer);
		$customer->where('id',$customer->id)->update([
         'token' => $token
        ]);
		$VerifyCustomer = VerifyCustomer::create([
            'customer_id' => $customer->id,
            'token' => $token
		]);
		
		// Mail::to($customer->email)->send(new VerifyMail($customer));
        $customer->notify(new Verify($customer));

		return $this->respondWithToken($token);
	
	}
	protected function respondWithToken($token)
	{
		return response()->json([
			'success' => true,
			'message' => 'Your account has been created successfully.',
			'token' => $token,
			'type'   => 'bearer',
			'expires'   => auth('customers')->factory()->getTTL() * 360
		], 200);
	}

	public function login(Request $request)
	{
		// get email and password from request
		$credentials = request(['email', 'password']);

		// try to auth and get the token using api authentication
		if (!$token = auth('customers')->attempt($credentials)) {
			// if the credentials are wrong we send an unauthorized error in json format
			return response()->json(['error' => 'Unauthorized'], 401);
		}
		$customer=auth('customers')->user();
		if(!$customer->verified)
		{
			return response()->json(['error' => 'Not Verified'], 401);
		}
		$customer->where('id',$customer->id)->update([
			'token' => $token
		   ]);
		return response()->json([
			'success' => true,
			'message' => 'User Logged successfully.',
			'token' => $token,
			'type' => 'bearer', // you can ommit this
			'expires' => auth('customers')->factory()->getTTL() * 360, // time to expiration

		], 200);
	}

	public function logout(Request $request)
	{
		\Auth::guard('customers')->logout();


		return response()->json([
			'status' => true,
			'message' => 'User Loggedout successfully.'
		], 200);
	}
	public function getCustomer(Request $request)
	{
		$customer = auth('customers')->user();

		return response()->json([
			'status' => true,
			'message' => 'User data',
			'data' =>  $customer
		], 200);
	}

	public function verifyCustomer($token)
    {
	
		$VerifyCustomer = VerifyCustomer::where('token', $token)->first();
		
        if(isset($VerifyCustomer) ){
            $customer = $VerifyCustomer->customer;
            if(!$customer->verified) {
                $VerifyCustomer->customer->verified = 1;
				$VerifyCustomer->customer->save();
				return view('/verified');
			}else{
				return view('/verified');
            }
        }else{
			return view('/not_verified');
            
        }

    }
}
