<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePromoVideoAPIRequest;
use App\Http\Requests\API\UpdatePromoVideoAPIRequest;
use App\Models\PromoVideo;
use App\Repositories\PromoVideoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PromoVideoController
 * @package App\Http\Controllers\API
 */

class PromoVideoAPIController extends AppBaseController
{
	/** @var  PromoVideoRepository */
	private $promoVideoRepository;

	public function __construct(PromoVideoRepository $promoVideoRepo)
	{
		$this->promoVideoRepository = $promoVideoRepo;
	}

	/**
	 * Display a listing of the PromoVideo.
	 * GET|HEAD /promoVideos
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->promoVideoRepository->pushCriteria(new RequestCriteria($request));
		$this->promoVideoRepository->pushCriteria(new LimitOffsetCriteria($request));
		$promoVideos = $this->promoVideoRepository->all();

		return $this->sendResponse($promoVideos->toArray(), 'Promo Videos retrieved successfully');
	}

	/**
	 * Store a newly created PromoVideo in storage.
	 * POST /promoVideos
	 *
	 * @param CreatePromoVideoAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePromoVideoAPIRequest $request)
	{
		$input = $request->all();

		$promoVideos = $this->promoVideoRepository->create($input);

		return $this->sendResponse($promoVideos->toArray(), 'Promo Video saved successfully');
	}

	/**
	 * Display the specified PromoVideo.
	 * GET|HEAD /promoVideos/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var PromoVideo $promoVideo */
		$promoVideo = $this->promoVideoRepository->findWithoutFail($id);

		if (empty($promoVideo)) {
			return $this->sendError('Promo Video not found');
		}

		return $this->sendResponse($promoVideo->toArray(), 'Promo Video retrieved successfully');
	}

	/**
	 * Update the specified PromoVideo in storage.
	 * PUT/PATCH /promoVideos/{id}
	 *
	 * @param  int $id
	 * @param UpdatePromoVideoAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatePromoVideoAPIRequest $request)
	{
		$input = $request->all();

		/** @var PromoVideo $promoVideo */
		$promoVideo = $this->promoVideoRepository->findWithoutFail($id);

		if (empty($promoVideo)) {
			return $this->sendError('Promo Video not found');
		}

		$promoVideo = $this->promoVideoRepository->update($input, $id);

		return $this->sendResponse($promoVideo->toArray(), 'PromoVideo updated successfully');
	}

	/**
	 * Watch the specified PromoVideo in storage.
	 *
	 * @param  int              $id
	 * @param WatchPromoVideoRequest $request
	 *
	 * @return Response
	 */
	public function watch($id, Request $request)
	{
		$promoVideo = $this->promoVideoRepository->findWithoutFail($id);

		if (empty($promoVideo)) {
			return $this->sendError('Promo Video not found');
		}
		if ($promoVideo->is_active) {
			$inputs = [
				'views' => ++$promoVideo->views
			];
			$promoVideo = $this->promoVideoRepository->update($inputs, $id);
		}

		return $this->sendResponse($promoVideo->toArray(), 'PromoVideo updated successfully');
	}

	/**
	 * Remove the specified PromoVideo from storage.
	 * DELETE /promoVideos/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var PromoVideo $promoVideo */
		$promoVideo = $this->promoVideoRepository->findWithoutFail($id);

		if (empty($promoVideo)) {
			return $this->sendError('Promo Video not found');
		}

		$promoVideo->delete();

		return $this->sendResponse($id, 'Promo Video deleted successfully');
	}
}
