<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\OrderHistory;
use App\Models\Product;
use App\Models\Branch;
use Illuminate\Support\Facades\Validator;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;


class APIOrderController extends AppBaseController
{
    //
   
    public function saveOrder(Request $request)
    {
		$branch=Branch::where('store_id','04')->first();

		$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/logista-b796f05ac902.json');
		$firebase = (new Factory)
		->withServiceAccount($serviceAccount)
		->withDatabaseUri('https://logista-282218.firebaseio.com/')
		->create();
		$database = $firebase->getDatabase();
		

		//get value of all the unique ID
		$getUser = $database
			   ->getReference('users')
			   ->orderByChild('email')
			   ->equalTo('hadeel.mostafa@momentum-sol.com')
			   ->getValue();
			   

        
       	$customer = auth('customers')->user();
		$url = 'http://41.33.238.100/RR_Oscar_API/api/Order/CreateOrder';
		$expressNotes=" ";
		$express=0;
		$proArray=[];
		if($request->has('express_shipping'))
		{
		 if($request->get('express_shipping')==true)
		 {
			 $express=1;
			 $expressNotes=$customer->name.','.'Express';
		 }
		
		}
		else
		{
			$expressNotes=$customer->name;

		}
		foreach($request->get('line_items') as $line)
		{

			$res =Product::all();
			$res = collect($res)->filter(function ($p) use ($line) {
			
				if ($p->id ==  $line['product_id']) {
					return $p;
				}
			})->values();
			$pro=$res->first();
			// return 'dcsd';
			$searchterm =$line['product_id'];
	
			$result =   preg_replace("/[^0-9]/", "",$searchterm );
		
			array_push($proArray,array(
			
				    "id" => $result,
					"quantity" => $line['quantity'],
					"name" =>$pro['name'],
					"price" => $pro['regular_price'],
					
				
			));
		}
	
		$order=OrderHistory::create(
			['CustomerAddress'=>$request->get('address')['address'].','.$request->get('address')['city'].','.$request->get('address')['area'].','.$request->coordinates,
        'CustomerPhone' => $request->get('phone'),
		'CustomerAccount' => $customer->id,
		'CustomerName' => $customer->name,
		'Signature'  => 1, 
		'OrderNumber'  => 0,
        'NotesPeriod'  => '',                           
        'OrderEntry' =>json_encode($proArray),
		'customer_id'=>$customer->id,
		'express_shipping' => $express,
		'order_status' => '',
		]);
		$order->update(['OrderNumber'=>$order->id]);
	
		$body = [
			"OrderNumber"=>$order->OrderNumber,
			"CustomerAddress" => $request->get('address')['address'].','.$request->get('address')['city'].','.$request->get('address')['area'].','.$request->coordinates,
			"CustomerPhone" => $request->get('phone'),
			"CustomerAccount" => $customer->id,
			"Signature"  => 0,
			"NotesPeriod"  =>$expressNotes,
			'Store'=>"Oscar",
			"OrderEntry" => collect($request->get('line_items'))->map(function ($item) use($proArray,$order) {
				   $res =Product::all();
					$res = collect($res)->filter(function ($p) use ($item) {
						if ($p->id ==  $item['product_id']) {
							return $p;
						}
					})->values();
					$pro=$res->first();
				if($pro['PriceUnit']=="kg")
				{
					$Weight=1;
				}
				else
				{
					$Weight=0;

				}
				$searchterm = $pro['id'];
	
				$result =   preg_replace("/[^0-9]/", "",$searchterm );
			
				return [
					"OrderNumber"=>$order->OrderNumber,
					"ItemID" => $result,
					"ItemBarcode" => $pro['barcode'],
					"QuantityOrderd" => $item['quantity'],
					"ItemName" =>$pro['name_ar'],
					"Price" => $pro['regular_price'],
					"IsWeight" => $Weight
				];
				
				
			}),

		];
		$user=array_keys($getUser);
		$newOrder = $database
		->getReference('/users/'.$user[0].'/tasks')
		->push(	
			[
				'deliver'=>['address'=>['lat'=>$branch->latitudes,'lng'=>$branch->longitudes,'name'=>$request->get('address')['address'].','.$request->get('address')['city'].','.$request->get('address')['area'].','.$request->coordinates],
				'date' => strtotime($order->created_at->format('Y-m-d H:i:s')),
				'description' => json_encode($proArray),
				'email'=>$customer->email,
				'name' => $customer->name,
				'orderId' => $order->OrderNumber,
				'phone' =>  $customer->phone],
				'distance'=>'',
				'driverId'=>'',
				'estTime'=>'',
				'pickup'=>['address'=>['lat'=>$branch->latitudes,'lng'=>$branch->longitudes,'name'=>$branch->title->en.','.$request->coordinates],
				'date' => strtotime($order->created_at->format('Y-m-d H:i:s')),
				'description' => json_encode($proArray),
				'email'=>$customer->email,
				'name' => $customer->name,
				'orderId' => $order->OrderNumber,
				'phone' =>  $customer->phone],
				'status'=>'-1',
				'type'=>'pickupOnDelivery'
			]);
	
				
		// $newOrder->getKey(); // => -KVr5eu8gcTv7_AHb-3-
		// $newOrder->getUri(); // => https://my-project.firebaseio.com/blog/posts/-KVr5eu8gcTv7_AHb-3-
		// $newOrder->getChild('CustomerPhone')->set('Changed post title');
		// $newOrder->getValue(); // Fetches the data from the realtime database
		// $newOrder->remove();
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec($ch);
		

   if(json_decode($server_output)=='Order Sent Successfully')
       {
		 
		return response()->json(['success' => true, 'message' => 'Order Sent Successfully']);
		}
		else{
			$order->delete();
			return response()->json(['error' => true, 'message' => 'Order Sent Failed']);

		}
		
}

	public function getOrder(Request $request)
    {
	
		$customer = auth('customers')->user();
		$orders = $customer->orders;
		if ($request->get('lang', null) === 'ar') {
		
			$orders = collect($orders)->map(function ($order) {
			
				if ($order->order_status=="open")
				$order->order_status = "مفتوح";
				if ($order->order_status=="close")
				$order->order_status = "مغلق";
				if(strlen($order->order_status)==0)
				$order->order_status = "مفتوح";
				return $order;
			});
		}
		else
		{
			$orders = collect($orders)->map(function ($order) {
			
				if ($order->order_status=="open")
				$order->order_status = "Open";
				if ($order->order_status=="Close")
				$order->order_status = "close";
				if(strlen($order->order_status)==0)
				$order->order_status = "Open";
				return $order;
			});
		
		}
	
		
			
		
		return response()->json([
			'success' => true,
			'message' => 'Get Orders successfully.',
			'data' => $orders,
		], 200);

	}
	

  

  
}
