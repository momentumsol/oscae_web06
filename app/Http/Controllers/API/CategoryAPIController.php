<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateCategoryAPIRequest;
use App\Http\Requests\API\UpdateCategoryAPIRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Carbon\Carbon;

/**
 * Class CategoryController
 * @package App\Http\Controllers\API
 */

class CategoryAPIController extends AppBaseController
{
	/** @var  CategoryRepository */
	private $categoryRepository;

	public function __construct(CategoryRepository $categoryRepo)
	{
		$this->categoryRepository = $categoryRepo;
	}

	/**
	 * Display a listing of the Category.
	 * GET|HEAD /categories
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		ini_set('max_execution_time', 5000);
		ini_set('memory_limit', '-1');

		$this->categoryRepository->pushCriteria(new RequestCriteria($request));
		$this->categoryRepository->pushCriteria(new LimitOffsetCriteria($request));

		$categories = $this->categoryRepository->get()->load('children');
		$categories = collect($categories->toArray())->values();
		if ($request->has('parent')) {
			if ($request->get('parent') == 1) {
				$categories = $categories->filter(function ($cat) use ($request) {
					if (isset($cat['parent']) && $cat['parent'] == 0) {
						$lang = $request->get('lang', 'en');
						$cat['name'] = $cat['name']->$lang;

						if (is_null($cat['description']->$lang)) {
							$cat['description'] = '';
						} else {
							$cat['description'] = $cat['description']->$lang;
						}
						// unset($cat['children']);
						return $cat;
					}
				})->values();
			}
		}

		$categories = $categories->map(function ($cat) use ($request) {
			$lang = $request->get('lang', 'en');
			$cat['name'] = $cat['name']->$lang;
			if (is_null($cat['description']->$lang)) {
				$cat['description'] = '';
			} else {
				$cat['description'] = $cat['description']->$lang;
			}
		
			return $cat;
		});

		return $this->sendResponse($categories, 'Categories retrieved successfully');
	}

	/**
	 * Store a newly created Category in storage.
	 * POST /categories
	 *
	 * @param CreateCategoryAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCategoryAPIRequest $request)
	{
		$input = $request->all();

		$categories = $this->categoryRepository->create($input);

		return $this->sendResponse($categories->toArray(), 'Category saved successfully');
	}

	/**
	 * Display the specified Category.
	 * GET|HEAD /categories/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var Category $category */
		$category = $this->categoryRepository->findWithoutFail($id)->load('promotions');

		if (empty($category)) {
			return $this->sendError('Category not found');
		}

		return $this->sendResponse($category->toArray(), 'Category retrieved successfully');
	}

	/**
	 * Update the specified Category in storage.
	 * PUT/PATCH /categories/{id}
	 *
	 * @param  int $id
	 * @param UpdateCategoryAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCategoryAPIRequest $request)
	{
		$input = $request->all();

		/** @var Category $category */
		$category = $this->categoryRepository->findWithoutFail($id);

		if (empty($category)) {
			return $this->sendError('Category not found');
		}

		$category = $this->categoryRepository->update($input, $id);

		return $this->sendResponse($category->toArray(), 'Category updated successfully');
	}

	/**
	 * Remove the specified Category from storage.
	 * DELETE /categories/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var Category $category */
		$category = $this->categoryRepository->findWithoutFail($id);

		if (empty($category)) {
			return $this->sendError('Category not found');
		}

		$category->delete();

		return $this->sendResponse($id, 'Category deleted successfully');
	}
}
