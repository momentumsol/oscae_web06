<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePageTypeAPIRequest;
use App\Http\Requests\API\UpdatePageTypeAPIRequest;
use App\Models\PageType;
use App\Repositories\PageTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PageTypeController
 * @package App\Http\Controllers\API
 */

class PageTypeAPIController extends AppBaseController
{
    /** @var  PageTypeRepository */
    private $pageTypeRepository;

    public function __construct(PageTypeRepository $pageTypeRepo)
    {
        $this->pageTypeRepository = $pageTypeRepo;
    }

    /**
     * Display a listing of the PageType.
     * GET|HEAD /pageTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pageTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->pageTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pageTypes = $this->pageTypeRepository->all();

        return $this->sendResponse($pageTypes->toArray(), 'Page Types retrieved successfully');
    }

    /**
     * Store a newly created PageType in storage.
     * POST /pageTypes
     *
     * @param CreatePageTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePageTypeAPIRequest $request)
    {
        $input = $request->all();

        $pageTypes = $this->pageTypeRepository->create($input);

        return $this->sendResponse($pageTypes->toArray(), 'Page Type saved successfully');
    }

    /**
     * Display the specified PageType.
     * GET|HEAD /pageTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PageType $pageType */
        $pageType = $this->pageTypeRepository->findWithoutFail($id);

        if (empty($pageType)) {
            return $this->sendError('Page Type not found');
        }

        return $this->sendResponse($pageType->toArray(), 'Page Type retrieved successfully');
    }

    /**
     * Update the specified PageType in storage.
     * PUT/PATCH /pageTypes/{id}
     *
     * @param  int $id
     * @param UpdatePageTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePageTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var PageType $pageType */
        $pageType = $this->pageTypeRepository->findWithoutFail($id);

        if (empty($pageType)) {
            return $this->sendError('Page Type not found');
        }

        $pageType = $this->pageTypeRepository->update($input, $id);

        return $this->sendResponse($pageType->toArray(), 'PageType updated successfully');
    }

    /**
     * Remove the specified PageType from storage.
     * DELETE /pageTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PageType $pageType */
        $pageType = $this->pageTypeRepository->findWithoutFail($id);

        if (empty($pageType)) {
            return $this->sendError('Page Type not found');
        }

        $pageType->delete();

        return $this->sendResponse($id, 'Page Type deleted successfully');
    }
}
