<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRestaurantAPIRequest;
use App\Http\Requests\API\UpdateRestaurantAPIRequest;
use App\Models\Restaurant;
use App\Repositories\RestaurantRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RestaurantController
 * @package App\Http\Controllers\API
 */

class RestaurantAPIController extends AppBaseController
{
	/** @var  RestaurantRepository */
	private $restaurantRepository;

	public function __construct(RestaurantRepository $restaurantRepo)
	{
		$this->restaurantRepository = $restaurantRepo;
	}

	/**
	 * Display a listing of the Restaurant.
	 * GET|HEAD /restaurants
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->restaurantRepository->pushCriteria(new RequestCriteria($request));
		$this->restaurantRepository->pushCriteria(new LimitOffsetCriteria($request));
		$restaurants = $this->restaurantRepository->all();

		$restaurants = collect($restaurants->toArray());

		$restaurants = $restaurants->map(function ($rest) use ($request) {
			$lang = $request->get('lang', 'en');
			$rest['title'] = $rest['title']->$lang;
			return $rest;
		});

		return $this->sendResponse($restaurants->toArray(), 'Restaurants retrieved successfully');
	}

	/**
	 * Store a newly created Restaurant in storage.
	 * POST /restaurants
	 *
	 * @param CreateRestaurantAPIRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateRestaurantAPIRequest $request)
	{
		$input = $request->all();

		$restaurants = $this->restaurantRepository->create($input);

		return $this->sendResponse($restaurants->toArray(), 'Restaurant saved successfully');
	}

	/**
	 * Display the specified Restaurant.
	 * GET|HEAD /restaurants/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		/** @var Restaurant $restaurant */
		$restaurant = $this->restaurantRepository->findWithoutFail($id);

		if (empty($restaurant)) {
			return $this->sendError('Restaurant not found');
		}

		return $this->sendResponse($restaurant->toArray(), 'Restaurant retrieved successfully');
	}

	/**
	 * Update the specified Restaurant in storage.
	 * PUT/PATCH /restaurants/{id}
	 *
	 * @param  int $id
	 * @param UpdateRestaurantAPIRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateRestaurantAPIRequest $request)
	{
		$input = $request->all();

		/** @var Restaurant $restaurant */
		$restaurant = $this->restaurantRepository->findWithoutFail($id);

		if (empty($restaurant)) {
			return $this->sendError('Restaurant not found');
		}

		$restaurant = $this->restaurantRepository->update($input, $id);

		return $this->sendResponse($restaurant->toArray(), 'Restaurant updated successfully');
	}

	/**
	 * Remove the specified Restaurant from storage.
	 * DELETE /restaurants/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var Restaurant $restaurant */
		$restaurant = $this->restaurantRepository->findWithoutFail($id);

		if (empty($restaurant)) {
			return $this->sendError('Restaurant not found');
		}

		$restaurant->delete();

		return $this->sendResponse($id, 'Restaurant deleted successfully');
	}
}
