<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreatePageRequest;
use App\Http\Requests\UpdatePageRequest;
use App\Repositories\PageRepository;
use App\Repositories\PageTypeRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PageController extends AppBaseController
{
  /** @var  PageRepository */
  private $pageRepository;

  public function __construct(PageRepository $pageRepo, PageTypeRepository $pageTypeRepo)
  {
		$this->middleware('auth');
    $this->pageRepository = $pageRepo;
    $this->pageTypeRepository = $pageTypeRepo;
  }

  /**
   * Display a listing of the Page.
   *
   * @param Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    $this->pageRepository->pushCriteria(new RequestCriteria($request));
    $pages = $this->pageRepository->all();

    return view('pages.index')
      ->with('pages', $pages);
  }

  /**
   * Show the form for creating a new Page.
   *
   * @return Response
   */
  public function create()
  {
    $page_types = $this->pageTypeRepository->pluck('title', 'id');
    return view('pages.create')->with('page_types', $page_types);
  }

  /**
   * Store a newly created Page in storage.
   *
   * @param CreatePageRequest $request
   *
   * @return Response
   */
  public function store(CreatePageRequest $request)
  {
    $input = $request->all();

    $page = $this->pageRepository->create($input);

    Flash::success('Page saved successfully.');

    return redirect(route('pages.index'));
  }

  /**
   * Display the specified Page.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
    $page = $this->pageRepository->findWithoutFail($id);

    if (empty($page)) {
      Flash::error('Page not found');

      return redirect(route('pages.index'));
    }

    return view('pages.show')->with('page', $page);
  }

  /**
   * Show the form for editing the specified Page.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
    $page = $this->pageRepository->findWithoutFail($id);
		$page_types = $this->pageTypeRepository->pluck('title', 'id');

    if (empty($page)) {
      Flash::error('Page not found');

      return redirect(route('pages.index'));
    }

    return view('pages.edit')->with('page', $page)->with('page_types', $page_types);
  }

  /**
   * Update the specified Page in storage.
   *
   * @param  int              $id
   * @param UpdatePageRequest $request
   *
   * @return Response
   */
  public function update($id, UpdatePageRequest $request)
  {
    $page = $this->pageRepository->findWithoutFail($id);

    if (empty($page)) {
      Flash::error('Page not found');

      return redirect(route('pages.index'));
    }

    $page = $this->pageRepository->update($request->all(), $id);

    Flash::success('Page updated successfully.');

    return redirect(route('pages.index'));
  }

  /**
   * Remove the specified Page from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $page = $this->pageRepository->findWithoutFail($id);

    if (empty($page)) {
      Flash::error('Page not found');

      return redirect(route('pages.index'));
    }

    $this->pageRepository->delete($id);

    Flash::success('Page deleted successfully.');

    return redirect(route('pages.index'));
  }
}
