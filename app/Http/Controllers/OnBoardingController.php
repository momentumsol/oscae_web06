<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOnBoardingRequest;
use App\Http\Requests\UpdateOnBoardingRequest;
use App\Repositories\OnBoardingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class OnBoardingController extends AppBaseController
{
    /** @var  OnBoardingRepository */
    private $onBoardingRepository;

    public function __construct(OnBoardingRepository $onBoardingRepo)
    {
			$this->middleware('auth');
        $this->onBoardingRepository = $onBoardingRepo;
    }

    /**
     * Display a listing of the OnBoarding.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->onBoardingRepository->pushCriteria(new RequestCriteria($request));
        $onBoardings = $this->onBoardingRepository->all();

        return view('on_boardings.index')
            ->with('onBoardings', $onBoardings);
    }

    /**
     * Show the form for creating a new OnBoarding.
     *
     * @return Response
     */
    public function create()
    {
        return view('on_boardings.create');
    }

    /**
     * Store a newly created OnBoarding in storage.
     *
     * @param CreateOnBoardingRequest $request
     *
     * @return Response
     */
    public function store(CreateOnBoardingRequest $request)
    {
        $input = $request->all();

        $onBoarding = $this->onBoardingRepository->create($input);

        Flash::success('On Boarding saved successfully.');

        return redirect(route('onBoardings.index'));
    }

    /**
     * Display the specified OnBoarding.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $onBoarding = $this->onBoardingRepository->findWithoutFail($id);

        if (empty($onBoarding)) {
            Flash::error('On Boarding not found');

            return redirect(route('onBoardings.index'));
        }

        return view('on_boardings.show')->with('onBoarding', $onBoarding);
    }

    /**
     * Show the form for editing the specified OnBoarding.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $onBoarding = $this->onBoardingRepository->findWithoutFail($id);

        if (empty($onBoarding)) {
            Flash::error('On Boarding not found');

            return redirect(route('onBoardings.index'));
        }

        return view('on_boardings.edit')->with('onBoarding', $onBoarding);
    }

    /**
     * Update the specified OnBoarding in storage.
     *
     * @param  int              $id
     * @param UpdateOnBoardingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOnBoardingRequest $request)
    {
        $onBoarding = $this->onBoardingRepository->findWithoutFail($id);

        if (empty($onBoarding)) {
            Flash::error('On Boarding not found');

            return redirect(route('onBoardings.index'));
        }

        $onBoarding = $this->onBoardingRepository->update($request->all(), $id);

        Flash::success('On Boarding updated successfully.');

        return redirect(route('onBoardings.index'));
    }

    /**
     * Remove the specified OnBoarding from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $onBoarding = $this->onBoardingRepository->findWithoutFail($id);

        if (empty($onBoarding)) {
            Flash::error('On Boarding not found');

            return redirect(route('onBoardings.index'));
        }

        $this->onBoardingRepository->delete($id);

        Flash::success('On Boarding deleted successfully.');

        return redirect(route('onBoardings.index'));
    }
}
