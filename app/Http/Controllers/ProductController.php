<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
// use App\Repositories\ProductRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Importer;
use App\Models\Category;
use App\Models\Product;
class ProductController extends AppBaseController
{
	/** @var  ProductRepository */
	// private $productRepository;
	private $categoryRepository;

	public function __construct(CategoryRepository $categoryRepo)
	{
		// $this->productRepository = $productRepo;
		$this->categoryRepository = $categoryRepo;
	}

	/**
	 * Display a listing of the Product.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->authorize('products.index');
		ini_set('memory_limit', '1024M');
	// 	$products = \Cache::get('products', []);
	
	// 	$paginate = 10;
	// 	$page = $request->get('page');
	//    if(is_null($page))
	//    {
	//       $page=1;
	//   }
	// 	$option=['path' => './products','pageName'=>'page'];
	// 	$offSet = ($page * $paginate) - $paginate;

	// 	$itemsForCurrentPage = array_slice($products, $offSet, $paginate, true);
	// 	$products= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($products), $paginate, $page,$option);
	$products=Product::paginate(10);

		return view('products.index')
			->with('products', $products);
	}

	/**
	 * Show the form for creating a new Product.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->authorize('products.create');
		$categories = $this->categoryRepository->all()->pluck('display_name', 'id')->toArray();

		return view('products.create')->with('categories', $categories);
	}

	/**
	 * Store a newly created Product in storage.
	 *
	 * @param CreateProductRequest $request
	 *
	 * @return Response
	 */
	public function checkIfExistId($ids, $num)
	{
		ini_set('max_execution_time', 5000);
		ini_set('memory_limit', '1024M');
		if (!in_array($num, $ids)) {
			return $num;
		} else {
			$randomNumber = rand(1, 1000000000000);
			$this->checkIfExistId($ids, $randomNumber);
		}
	}

	public function store(Request $request)
	{
		ini_set('max_execution_time', 5000);
		ini_set('memory_limit', '1024M');
		$this->authorize('products.create');
		$products = \Cache::get('products', []);
		$ids = [];
		$randomNumber = rand(1, 1000000000000);
		foreach ($products as $p) {
			array_push($ids, $p['id']);
		}
		if ($this->checkIfExistId($ids, $randomNumber)) {
			$product = [
				'id' => $randomNumber,
				'name' => '',
				'description' => '',
				'sale_price' => '',
				'on_sale' => false,
				'regular_price' => '',
				'reviews_allowed' => false,
				'in_stock' => true,
				'categories' => [],

			];

			$product['name'] = $request->get('name', '');
			$product['description'] = $request->get('description', '');
			$product['name_ar'] = $request->get('name_ar', '');
			$product['barcode'] = $request->get('barcode');
			$product['description_ar'] = $request->get('description_ar', '');
			$product['sale_price'] = $request->get('sale_price', '');
			$product['on_sale'] = !empty($product['sale_price']);
			$product['regular_price'] = $request->get('regular_price', '');
			$product['in_stock'] = $request->get('in_stock', '');
			$product['hot_price'] = $request->get('hot_price', false);
			// $img[0]['src'] = 'https://sekka.local.momentum-sol.com/pic/'.$request->get('image');
			// $product['images'] = $img;
			if ($request->file('image')) {
				$file = \Storage::put('public/prodcuts', $request->file('image'), 'public');
				$product['images'] = [
					[
						'src' => url(\Storage::url($file))
					]
				];
			}


			if ($request->has('categories')) {
				$categories = $request->get('categories');
				
				$selectedCategories = array();
				foreach ($categories as $cat) {
					$arr = $this->categoryRepository->find($cat, ['id', 'name', 'slug'])->toArray();
					array_push($selectedCategories, $arr);
				}
				$product['categories'] = $selectedCategories;
			}
			
			
			//array_push($products, $product);

			Product::create(['id'=>$request->id,'name'=>$request->name,'description'=>$request->description,
	'name_ar'=> $request->name_ar,'description_ar'=>$request->description_ar,'sale_price'=>$request->sale_price,'on_sale'=>!empty($product['sale_price']),
	'regular_price'=>$request->regular_price,'hot_price'=>false,'discountFrom'=>'','discountto'=>'','discountprice'=>'',
	'barcode'=>$request->barcode,'PriceUnit'=>'','in_stock' =>  $request->get('in_stock', ''),'category_id'=>$categories[0],'images'=>json_encode($product['images']),
	'01'=>true,'02'=>true,'04'=>true,'categories'=>json_encode($selectedCategories[0])]);
			// \Cache::forever('products', $products);

			Flash::success('Product added successfully.');

			return redirect(route('products.index'));
		}
	}

	/**
	 * Display the specified Product.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		ini_set('max_execution_time', 5000);
		ini_set('memory_limit', '1024M');
		$this->authorize('products.show');
		$product=Product::find($id);
		return view('products.show')->with('product', $product);
	}

	/**
	 * Show the form for editing the specified Product.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		ini_set('max_execution_time', 5000);
		ini_set('memory_limit', '1024M');
		$this->authorize('products.edit');
		$categories = $this->categoryRepository->all()->pluck('display_name', 'id')->toArray();

		$product=Product::find($id);


		return view('products.edit')->with('product', $product)->with('categories', $categories);
	}

	/**
	 * Update the specified Product in storage.
	 *
	 * @param int $id
	 * @param UpdateProductRequest $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		ini_set('max_execution_time', 5000);
		ini_set('memory_limit', '1024M');
		$this->authorize('products.edit');
		$products = \Cache::get('products', []);
		$product = [];
		
		$pro=Product::find($id);
		if (empty($pro)) {
			Flash::error('Product not found');
			return redirect(route('products.index'));
		}

if(is_null($request->get('hot_price')))
{
	$hot_price=false;
}
else
{
	$hot_price=true;

}
if(is_null($request->get('in_stock')))
{
	$in_stock=false;
}
else
{
	$in_stock=true;

}

		

		//$product['images'] = 'https://162.241.210.96/App_emg/'.$request->get('image', '');

		// if ($request->has('categories')) {
		// 	$categories = $request->get('categories');

		// 	$selectedCategories = array();
		// 	foreach ($categories as $cat) {
		// 		$arr = $this->categoryRepository->find($cat)->toArray();
		// 		array_push($selectedCategories, $arr);
		// 	}
		// 	$product['categories'] = $selectedCategories;
		// }
		
		if(!is_null($request->get('signature')))
{
	$pro=Product::where('id',$id)->first();
	$categories=$pro->categories;

	$signature =  $this->categoryRepository->findByField('slug','signature')->toArray();

	array_push($categories,$signature[0]);
	Product::where('id',$id)->update(['categories'=>json_encode($categories),'category_id'=>$signature[0]['id']]);
}

		if ($request->file('image')) {
			$file = \Storage::put('public/prodcuts', $request->file('image'), 'public');
			$product['images'] = [
				[
					'src' => url(\Storage::url($file))
				]
			];
		}
	if(is_null($request->description))
	{
		$des='';
	}else
	{
		$des=$request->description;
	}
	if(is_null($request->description_ar))
	{
		$des_ar='';
	}else
	{
		$des_ar=$request->description_ar;
	}

if(!$request->has($request->file('image')))
{
	Product::where('id',$id)->update(['id'=>$request->id,'name'=>$request->name,'description'=>$des,
	'name_ar'=> $request->name_ar,'description_ar'=>$des_ar,'on_sale'=>!empty($product['sale_price']),
	'regular_price'=>$request->regular_price,'hot_price'=>false,'discountFrom'=>'','discountto'=>'','discountprice'=>'',
	'barcode'=>$request->barcode,'PriceUnit'=>'','in_stock' =>  $in_stock,
	'01'=>true,'02'=>true,'04'=>true,'limit'=>$request->get('limit')]);
}
else
{
		Product::where('id',$id)->update(['id'=>$request->id,'name'=>$request->name,'description'=>$des,
	'name_ar'=> $request->name_ar,'description_ar'=>$des_ar,'on_sale'=>!empty($product['sale_price']),
	'regular_price'=>$request->regular_price,'hot_price'=>false,'discountFrom'=>'','discountto'=>'','discountprice'=>'','images'=>json_encode($product['images']),
	'barcode'=>$request->barcode,'PriceUnit'=>'','in_stock' =>  $in_stock,
	'01'=>true,'02'=>true,'04'=>true,'limit'=>$request->get('limit')]);
	
}
		



		Flash::success('Product updated successfully.');

		return redirect(route('products.index'));
	}

	/**
	 * Remove the specified Product from storage.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		ini_set('max_execution_time', 5000);
		ini_set('memory_limit', '1024M');
		$this->authorize('products.delete');
		// $product = $this->productRepository->findWithoutFail($id);

	
Product::where('id',$id)->delete();
		

		Flash::success('Product deleted successfully.');

		return redirect(route('products.index'));
	}

	public function getDownload()
	{
		$file = public_path() . "/download/ProductsSample.xlsx";
		$headers = array('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',);
		return Response::download($file, 'ProductsSample.xlsx', $headers);
	}

	public function import(Request $request)
	{
		
		$arr = [];
		$products = \Cache::get('products', []);

		if ($request->hasFile('file')) {
			$rules = [
				'file' => ["required", ''],
			];
			$messages = [
				'mimes' => 'The :attribute field is must be a file of type: xlsx.',
			];

			$this->validate($request, $rules, $messages);
			$excel = Importer::make('Excel');
			$excel->load($request->file('file'));
			$collection = $excel->getCollection();
			$collection = $collection->forget($collection->shift());

			foreach ($collection as $record) {
				if(!is_numeric($record[10]) ||strlen($record[10]) != 6)
				{
					Flash::error('Product number is not vaild');
			return redirect(route('products.index'));
				}
				
				$product = [];
				foreach ($products as $p) {
					
					if ($p['pro_number'] == $record[10]) {
						$product = $p;
					}
				}
			if(empty($product))
			{

			
				if ($record[0] || $record[1]) {
					$ids = [];
					$randomNumber = rand(1, 1000000000000);
					foreach ($products as $p) {
						array_push($ids, $p['pro_number']);
					}
					if ($this->checkIfExistId($ids, $randomNumber)) {
						$product = [
							'id' => $randomNumber,
							'name' => '',
							'description' => '',
							'sale_price' => '',
							'on_sale' => false,
							'regular_price' => '',
							'reviews_allowed' => false,
							'in_stock' => true,
							'categories' => [],

						];

						$product['name'] = $record[0];
						$product['description'] = $record[2];
						$product['name_ar'] = $record[1];
						$product['description_ar'] = $record[3];
						$product['sale_price'] = $record[5];
						$product['on_sale'] = !empty($product['sale_price']);
						$product['regular_price'] = $record[4];
						$product['hot_price'] = ($record[6] == 'yes') ? true : false;
						$product['barcode']=$record[9];
						$product['pro_number']=$record[10];

						if ($record[7]) {
							$categories = explode(',', $record[7]);
							$selectedCategories = array();
							foreach ($categories as $cat) {
								$cats = $this->categoryRepository->findByField('slug', $cat)->toArray();
								if (!empty($cats))
									array_push($selectedCategories, $cats[0]);
							}

							$product['categories'] = $selectedCategories;
						}
						
						$img[0]['src'] = 'https://oscar-test.momentum-host.com/test/'.$record[8];
						$product['images'] = $img;
						
						array_push($products, $product);
					
					}
				}
			}
			else
			{

				$product['name'] = $record[0];
				$product['description'] = $record[2];
				$product['name_ar'] = $record[1];
				$product['description_ar'] = $record[3];
				$product['sale_price'] = $record[5];
				$product['on_sale'] = !empty($product['sale_price']);
				$product['regular_price'] = $record[4];
				$product['hot_price'] = ($record[6] == 'yes') ? true : false;
				$product['barcode']=$record[9];
				$product['pro_number']=$record[10];

				if ($record[7]) {
					$categories = explode(',', $record[7]);
					$selectedCategories = array();
					foreach ($categories as $cat) {
						$cats = $this->categoryRepository->findByField('slug', $cat)->toArray();
						if (!empty($cats))
							array_push($selectedCategories, $cats[0]);
					}

					$product['categories'] = $selectedCategories;
				}
				
				$img[0]['src'] = 'https://oscar-test.momentum-host.com/test/'.$record[8];
				$product['images'] = $img;


foreach ($products as $key => $p) {
	if ($p['pro_number'] == $record[10]) {
		$products[$key] = $product;
	}
}


			}
			}
			
			
			\Cache::forever('products', $products);

			Flash::success('Product imported successfully.');
			return redirect(route('products.index'));
			//dd($products);
		}
		return view('products.import')->with('array', $arr);
	}
	public function cache()
	{
		 \Artisan::call('cache:clear');
		 \DB::table('products')->delete();
		 \DB::table('categories')->update(['count'=>0]);
		 Flash::success('Products deleted successfully.');
		 return redirect(route('products.index'));
	}

	public function search(Request $request)
	{
		ini_set('max_execution_time', 5000);
		ini_set('memory_limit', '1024M');
		$this->authorize('products.index');
		$products =Product::all();
		$search=$request->get('search');
		if(is_null($search))
		{
			$products = collect($products);
		}
		else
		{
		$products = collect($products)->filter(function ($p) use ($search) {
	
			if (stripos($p['id'], $search) !== false ||
			   stripos($p['barcode'], $search) !== false || stripos($p['name'], $search) !== false ||
			   (isset($p['name_ar']) && stripos($p['name_ar'], $search) !== false)) {
				return $p;
			}
		})->values();
	}
		$paginate = 10;
		$page = $request->get('page');
	   if(is_null($page))
	   {
	      $page=1;
	    }
		$option=['path' => '/products','pageName'=>'page'];
		$offSet = ($page * $paginate) - $paginate;

		$itemsForCurrentPage = array_slice($products->toArray(), $offSet, $paginate, true);
		$products= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($products), $paginate, $page,$option);
		return view('products.index')
			->with('products', $products);
	}

	public function selectImages(Request $request)
	{
		$this->authorize('products.index');
		$products =Product::all();
		$product_image_filter=$request->get('product_image_filter');
	
		if(is_null($product_image_filter) || $product_image_filter=='all')
		{
			$products = collect($products);
		}
		elseif($product_image_filter==0)
		{
		$products = collect($products)->filter(function ($p)  {
	
			if ($p['images'][0]['src']=='http://oscar.momentum-sol.com/images/image.png' 
			) {
			 	return $p;
			}
		})->values();
	}
	elseif($product_image_filter==1)
		{
		$products = collect($products)->filter(function ($p)  {
			
			
			if ($p['images'][0]['src']!='http://oscar.momentum-sol.com/images/image.png') {
			 	return $p;
			}
		})->values();
	}
		$paginate = 10;
		$page = $request->get('page');
	   if(is_null($page))
	   {
	      $page=1;
	    }
		$option=['path' => '/products/selectImages/'.$product_image_filter,'pageName'=>'page'];
		$offSet = ($page * $paginate) - $paginate;

		$itemsForCurrentPage = array_slice($products->toArray(), $offSet, $paginate, true);
		$products= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($products), $paginate, $page,$option);
		return view('products.index')
			->with('products', $products);
	}
	public function getselectImages($filter,Request $request)
	{
		
		$this->authorize('products.index');
		$products = \Cache::get('products', []);
		$filter=$filter;
	
		if(is_null($filter) || $filter=='all')
		{
			$products = collect($products);
		}
		elseif($filter==0)
		{
		$products = collect($products)->filter(function ($p)  {
			
			
			if ($p['images'][0]['src']=='http://oscar.momentum-sol.com/images/image.png') {
			 	return $p;
			}
		})->values();
	}
	elseif($filter==1)
		{
		$products = collect($products)->filter(function ($p)  {
			
			
			if ($p['images'][0]['src']!='http://oscar.momentum-sol.com/images/image.png') {
			 	return $p;
			}
		})->values();
	}
		$paginate = 10;
		$page = $request->get('page');
	   if(is_null($page))
	   {
	      $page=1;
	    }
		$option=['path' => '/products/selectImages/'.$filter,'pageName'=>'page'];
		$offSet = ($page * $paginate) - $paginate;

		$itemsForCurrentPage = array_slice($products->toArray(), $offSet, $paginate, true);
		$products= new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($products), $paginate, $page,$option);
		return view('products.index')
			->with('products', $products);
	}
}
