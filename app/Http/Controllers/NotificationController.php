<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
// use App\Repositories\ProductRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\NotificationRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Importer;
use App\Models\Device;
use Edujugon\PushNotification\PushNotification;
use App\Models\Notification;



class NotificationController extends AppBaseController
{
	/** @var  ProductRepository */
	// private $productRepository;
	private $notificationRepository;

	public function __construct(NotificationRepository $notificationRepo)
	{
		// $this->productRepository = $productRepo;
		$this->notificationRepository = $notificationRepo;
	}

	/**
	 * Display a listing of the Product.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{

	
        $this->notificationRepository->pushCriteria(new RequestCriteria($request));
        $notifications = $this->notificationRepository->paginate(10);
	
		return view('notifications.index')
			->with('notifications', $notifications);
	}

	/**
	 * Show the form for creating a new Product.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->authorize('notifications.create');

		return view('notifications.create');
	}

	

	public function store(Request $request)
	{
		$input = $request->all();
		if ($request->file('image')) {
			$file = \Storage::put('public/notification', $request->file('image'), 'public');
			$input['image'] =  url(\Storage::url($file))
           ;
		}
	
		$this->authorize('notifications.create');
		$notification = $this->notificationRepository->create($input);

		
			 
        $devices=\DB::table('devices')->where('os',$notification->os)->where('language',$notification->language)->pluck('device_token');
	 
		 
			 if($devices->ToArray() !=[])
			 {
	 
			   
				 foreach($devices->ToArray() as $device)	
				 {
					 
	 
	 #prep the bundle
	 if(!is_null($notification->image))
	 {
		 $msg = array
		 (
		 "body" => $notification->message,
		 "title" => $notification->title,
		 'sound' => 'notify.mp3',
		 'image'=>$notification->image
		 
		 );
	 }
	 else
	 {
		 $msg = array
		 (
			"body" => $notification->message,
	    "title" => $notification->title,
		 'sound' => 'notify.mp3',
		 
		 );
	 }
	 
	 $fields = array
	 (
	 'to' => $device,
	 'notification' => $msg,
	 'priority' => 'high',
	 );
	 $headers = array
	 (
	 'Authorization: key=' . 'AAAAfNRNLzw:APA91bEBA9-Wbm89gVPYRsZ7T9g-mNpM9ctT2TUDyZrgRcsaL99V4KuehTtOKLimInp1_G7uvJKOWQjjqsZF5N-fMhRI-r-oHN1nTMgTZA0v6qnVnV1RDoVbfKOym7cCJQFL-BHoCEj8',
	 'Content-Type: application/json'
	 );
	 #Send Reponse To FireBase Server    
	 $ch = curl_init(); 
	 curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
	 curl_setopt($ch, CURLOPT_POST, true);
	 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	 curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	 $result = curl_exec($ch);
	 curl_close($ch);
	 $cur_message = json_decode($result);
			}
		 }
	  
	



        Flash::success('Notifications saved successfully.');

        return redirect(route('notifications.index'));
		}
	

	/**
	 * Display the specified Product.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$this->authorize('notifications.create');
		$notification  = $this->notificationRepository->findWithoutFail($id);

		if (empty($notification)) {
			Flash::error('Notification not found');

			return redirect(route('notifications.index'));
		}

		return view('notifications.show')->with('notification', $notification );
	}

	/**
	 * Show the form for editing the specified Product.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$this->authorize('notifications.create');
		$notification  = $this->notificationRepository->findWithoutFail($id);

		if (empty($notification)) {
			Flash::error('Notification not found');

			return redirect(route('notifications.index'));
		}
		$devices=\DB::table('devices')->where('os',$notification->os)->where('language',$notification->language)->pluck('device_token');
	 
		 
		if($devices->ToArray() !=[])
		{

		  
			foreach($devices->ToArray() as $device)	
			{
				

#prep the bundle
if(!is_null($notification->image))
{
	$msg = array
	(
	"body" => $notification->message,
	"title" => $notification->title,
	'sound' => 'notify.mp3',
	'image'=>$notification->image
	
	);
}
else
{
	$msg = array
	(
	   "body" => $notification->message,
   "title" => $notification->title,
	'sound' => 'notify.mp3',
	
	);
}

$fields = array
(
'to' => $device,
'notification' => $msg,
'priority' => 'high',
);
$headers = array
(
'Authorization: key=' . 'AAAAfNRNLzw:APA91bEBA9-Wbm89gVPYRsZ7T9g-mNpM9ctT2TUDyZrgRcsaL99V4KuehTtOKLimInp1_G7uvJKOWQjjqsZF5N-fMhRI-r-oHN1nTMgTZA0v6qnVnV1RDoVbfKOym7cCJQFL-BHoCEj8',
'Content-Type: application/json'
);
#Send Reponse To FireBase Server    
$ch = curl_init(); 
curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
$result = curl_exec($ch);
curl_close($ch);
$cur_message = json_decode($result);
	   }
	}

		return view('notifications.edit')->with('notification', $notification );
	}

	/**
	 * Update the specified Product in storage.
	 *
	 * @param int $id
	 * @param UpdateProductRequest $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$this->authorize('notifications.create');
		$input = $request->all();
		$notification  = $this->notificationRepository->findWithoutFail($id);

		if (empty($notification)) {
			Flash::error('notification not found');

			return redirect(route('notifications.index'));
		}
		if ($request->file('image')) {
			$file = \Storage::put('public/notification', $request->file('image'), 'public');
			$input['image'] =  url(\Storage::url($file))
;
		}
		
		$notification = $this->notificationRepository->update($input, $id);

		Flash::success('notification updated successfully.');

		return redirect(route('notifications.index'));
	}

	/**
	 * Remove the specified Product from storage.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{

		$this->authorize('notifications.delete');
		$notification = $this->notificationRepository->findWithoutFail($id);

		if (empty($notification)) {
			Flash::error('Notifications not found');

			return redirect(route('notifications.index'));
		}
		$this->notificationRepository->delete($id);

		Flash::success('Notification deleted successfully.');

		return redirect(route('notifications.index'));
	}



	public function import(Request $request)
	{
		$arr = [];
		$products = \Cache::get('products', []);

		if ($request->hasFile('file')) {
			$rules = [
				'file' => ["required", ''],
			];
			$messages = [
				'mimes' => 'The :attribute field is must be a file of type: xlsx.',
			];

			$this->validate($request, $rules, $messages);
			$excel = Importer::make('Excel');
			$excel->load($request->file('file'));
			$collection = $excel->getCollection();
			$collection = $collection->forget($collection->shift());
			foreach ($collection as $record) {
				if ($record[0] || $record[1]) {
					$ids = [];
					$randomNumber = rand(1, 1000000000000);
					foreach ($products as $p) {
						array_push($ids, $p['id']);
					}
					if ($this->checkIfExistId($ids, $randomNumber)) {
						$product = [
							'id' => $randomNumber,
							'name' => '',
							'description' => '',
							'sale_price' => '',
							'on_sale' => false,
							'regular_price' => '',
							'reviews_allowed' => false,
							'in_stock' => true,
							'categories' => [],

						];

						$product['name'] = $record[0];
						$product['description'] = $record[2];
						$product['name_ar'] = $record[1];
						$product['description_ar'] = $record[3];
						$product['sale_price'] = $record[5];
						$product['on_sale'] = !empty($product['sale_price']);
						$product['regular_price'] = $record[4];
						$product['hot_price'] = ($record[6] == 'yes') ? true : false;

						if ($record[7]) {
							$categories = explode(',', $record[7]);
							$selectedCategories = array();
							foreach ($categories as $cat) {
								$cats = $this->categoryRepository->findByField('slug', $cat)->toArray();
								if (!empty($cats))
									array_push($selectedCategories, $cats[0]);
							}

							$product['categories'] = $selectedCategories;
						}
						$img[0]['src'] = url(\Storage::url("media/$record[8]"));
						$product['images'] = $img;
						array_push($products, $product);
					}
				}
			}
			\Cache::forever('products', $products);

			Flash::success('Product imported successfully.');
			return redirect(route('products.index'));
			//dd($products);
		}
		return view('products.import')->with('array', $arr);
	}
}
