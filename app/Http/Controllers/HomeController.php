<?php namespace App\Http\Controllers;

use FCM;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use App\Repositories\PageRepository;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Response;

class HomeController extends AppBaseController
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(PageRepository $pageRepo) {
		$this->middleware('auth', ['except' => ['readQrcode']]);
    $this->pageRepository = $pageRepo;
  }

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$pages = $this->pageRepository->pluck('title', 'id');
		$stats = [];
		$stats['users'] = \App\Models\User::count();
		$stats['ios'] = \App\Models\Device::where('os', 'ios')->count();
		$stats['android'] = \App\Models\Device::where('os', 'android')->count();
		$stats['devices'] = $stats['ios'] + $stats['android'];
		$stats['promotions'] = \App\Models\Promotion::count();
		return view('home')
						->with('stats', $stats)
						->with('pages', $pages);
	}

	public function upload(Request $request) {
		if (($request->has('files'))) {
			$files = $request->file('files');
			$urls = [];
			foreach ($files as $file) {
				$file = \Storage::put('public/page_items', $file, 'public');
				array_push($urls, url(\Storage::url($file)));
			}
		}
		return $this->sendResponse($urls, 'Success');
	}

	public function sendPN(Request $request) {
		$optionBuilder = new OptionsBuilder();
		$optionBuilder->setTimeToLive(60 * 20);
		$optionBuilder->setContentAvailable(true);
		$optionBuilder->setPriority(\LaravelFCM\Message\OptionsPriorities::high);

		$notificationBuilder = new PayloadNotificationBuilder($request->get('title', 'my title'));
		$notificationBuilder->setBody($request->get('message', 'Hello world'))->setSound('default');

		$dataBuilder = new PayloadDataBuilder();

		$type = $request->get('type', '');

		$dataBuilder->addData([
			'content-available' => '1',
			'content_available' => '1',
			'data' => [
				'content-available' => '1',
				'content_available' => '1',
			],
			'oscar_data' => [
				'type' => $type,
				'id' => $request->get('page_id', ''),
				'pdf_url' => 'https://www.yumpu.com/xx/embed/view/UG4iV7YwXqvuofr3',
				'promotion_type' => 'magazine',
				'name' => 'name'
			]
		]);

		$option = $optionBuilder->build();
		$notification = $notificationBuilder->build();
		$data = $dataBuilder->build();

		$tokens = [];
		$tokens[0] = "eoR_nDjd7z4:APA91bHUMCMAtykx_DWLdiulpqZUOntXOv0IWy4sm7A7fqm0NJxVNZjiUSeeNDuaQ2SulGY56hJNSa9rVqv3AjNm9ruH19RyCxtntaM87mvWEedemXPHKLpqM58EJHJohofZOWPViodR"; //Ghazaly
		$token = "d85M_bymjsw:APA91bH4zGQPTpKJOcJygoosKdk8aPiyJw7eD7Pxm8RHurms1jJYkjW6c8tUeJYPqIKUTnSDXDCMBwxtJBsdgtwT_euvby-vsAvZT9pdB5h7ZYCrz3HvIST8k4n-VNo9SB1l1fM7Eg_C"; //Fam
		$token = "dH7c3Hj1xQI:APA91bGE_OxdiW0LGj97UyKbxjNrg5TXRfIUR_VrRMrkMTr3ikX7YIDkiBpwj8nwSxQbnJZDcftWrxbH3JZmIZu6cvQw0B-UFwdZ8OphTuxUgePI5dxESCnk0h4HtOHX2PaMJXBVK67E"; //Tamer Android
		$downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

		$downstreamResponse->numberSuccess();
		$downstreamResponse->numberFailure();
		$downstreamResponse->numberModification();

		//return Array - you must remove all this tokens in your database
		$downstreamResponse->tokensToDelete();

		//return Array (key : oldToken, value : new token - you must change the token in your database )
		$downstreamResponse->tokensToModify();

		//return Array - you should try to resend the message to the tokens in the array
		$downstreamResponse->tokensToRetry();
		return redirect('home');
	}

	public function qrcode(Request $requst)
	{
		$url = $requst->get('url');
		if ($url) {
			$url = encrypt($url);
		}

		return view('qrcode')->with('url', $url);
	}

	public function readQrcode($code)
	{
		$encryptedValue = $code;
		try {
			$decrypted = decrypt($encryptedValue);
			return $this->sendResponse($decrypted, 'QrCode');
		} catch (DecryptException $e) {
			return $this->sendResponse(null, 'Error');
		}
	}
}
