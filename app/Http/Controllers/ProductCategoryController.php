<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Repositories\CategoryRepository;
// use App\Repositories\ProductCategoryRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Category;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Importer;
use Exporter;
use App\Models\Product;

class ProductCategoryController extends AppBaseController
{
	/** @var  ProductCategoryRepository */
	// private $productCategoryRepository;

	/** @var  CategoryRepository */
	private $categoryRepository;

	public function __construct(CategoryRepository $categoryRepo)
	{
		$this->middleware('auth');
		$this->categoryRepository = $categoryRepo;
	}

	/**
	 * Display a listing of the ProductCategory.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->authorize('categories.index');
		$this->categoryRepository->pushCriteria(new RequestCriteria($request));
		$categories = $this->categoryRepository->orderBy('id')->paginate(10);
		$categories_select = $this->categoryRepository->whereNull('parent')->get();
		
		return view('product_categories.index')
			->with('productCategories', $categories)->with('categories_select', $categories_select);
	}
	public function  selectCategory(Request $request)
	{
		
		$this->authorize('categories.index');
		$this->categoryRepository->pushCriteria(new RequestCriteria($request));
		if($request->get('categories_select')=="main")
		{
			$categories = $this->categoryRepository->whereNull('parent')->orderBy('id')->paginate(20);

		}
		else
		{
			$categories = $this->categoryRepository->where('id',$request->get('categories_select'))->orWhere('parent',$request->get('categories_select'))->orderBy('id')->paginate(20);

		}
		$categories_select = $this->categoryRepository->whereNull('parent')->get();		
		return view('product_categories.index')
		->with('productCategories', $categories)->with('categories_select', $categories_select);

	}

	/**
	 * Show the form for creating a new ProductCategory.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->authorize('categories.create');
		$categories = $this->categoryRepository->all()->pluck('display_name', 'id')->toArray();
		return view('product_categories.create')->with('categories', $categories);
	}

	/**
	 * Store a newly created ProductCategory in storage.
	 *
	 * @param CreateProductCategoryRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCategoryRequest $request)
	{
		$this->authorize('categories.create');
		$input = $request->all();

		
		$category = $this->categoryRepository->create($input);

		
		

		Flash::success('Product Category saved successfully.');

		return redirect(route('productCategories.index'));
	}

	/**
	 * Display the specified ProductCategory.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$this->authorize('categories.show');
		$category = $this->categoryRepository->findWithoutFail($id);

		if (empty($category)) {
			Flash::error('Category not found');

			return redirect(route('productCategories.index'));
		}

		return view('product_categories.show')->with('productCategory', $category);
	}

	/**
	 * Show the form for editing the specified ProductCategory.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$this->authorize('categories.edit');
		$category = $this->categoryRepository->findWithoutFail($id);

		if (empty($category)) {
			Flash::error('Category not found');

			return redirect(route('productCategories.index'));
		}
		$categories = $this->categoryRepository->all()->except($id)->pluck('display_name', 'id')->toArray();

		//$categories = $this->categoryRepository->all()->load('children');
	

		return view('product_categories.edit')->with('productCategory', $category)->with('categories', $categories);
	}

	/**
	 * Update the specified ProductCategory in storage.
	 *
	 * @param  int              $id
	 * @param UpdateProductCategoryRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCategoryRequest $request)
	{
		ini_set('memory_limit', '-1');
		$this->authorize('categories.edit');
		$category = $this->categoryRepository->findWithoutFail($id);
		if($request->has('signature'))
			{				

		$products_data=Product::all();
		if ($data = $products_data) {
			
			$products = array_values($data->ToArray());
			
		}
		
		$res = collect($products)->filter(function ($p) use ($category, $id) {
	
		
			
			$categories = collect($p['categories']);
	
				$subCategories = $category->children->pluck('id')->toArray();
				
			
			
				foreach ($subCategories as $subCat) {
			
					if ($categories->contains('id', $subCat)) return true;
					if($categories->contains('parent', $subCat))return true;
					
					
					
					
				}
			


return $categories->contains('id', $id);

		
			
		})->values();
	}
		if (empty($category)) {
			Flash::error('Product Category not found');

			return redirect(route('productCategories.index'));
		}
		elseif(count($category->children) >= 4 && $category->id !=89)
		{
		
		
			$category = $this->categoryRepository->update($request->except(['parent']), $id);
			if($request->has('signature'))
			{
				
				$jsonstring = array();
		
				$jsonstring['src'] =$category->image['src'];
				$jsonstring['title'] = $category->slug.'-signature';
				$jsonstring['alt'] = '';
				$sign = \App\Models\Category::where('id', '5637168576')->first();
			
				$new_cat= \App\Models\Category::where('slug', $category->slug.'-signature')->first();
				if(is_null($new_cat))
				{
					$new_cat=Category::create(['parent'=>$sign->id,'description'=>$category->description,'name'=>$category->name,'display'=>$category->display,'slug'=>$category->slug.'-signature','image'=>$jsonstring,'count'=>$category->count]);
					
	
				}	
				foreach($res as $pro)
					{
						
						$sign->update(array('count'=>$sign->count+1));	
						$categories=$pro['categories'];

						array_push($categories,$sign);
						array_push($categories,$new_cat);
						
					
					$product=Product::create(['id'=>$pro['id'].'-signature','name'=>$pro['name'],'description'=>$pro['description'],
					'name_ar'=> $pro['name_ar'],'description_ar'=>$pro['description_ar'],'sale_price'=> $pro['discountprice'],'on_sale'=>$pro['on_sale'],
					'regular_price'=> $pro['regular_price'],'hot_price'=>$pro['hot_price'],'discountFrom'=>$pro['discountFrom'],'discountto'=>$pro['discountto'],'discountprice'=>$pro['discountprice'],
					'barcode'=>$pro['barcode'],'PriceUnit'=>$pro['PriceUnit'],'in_stock' => $pro['in_stock'],'category_id'=>$sign->id,'images'=>json_encode($pro['images']),
					'01'=>$pro['01'],'02'=>$pro['02'],'04'=>$pro['04'],'categories'=>json_encode($categories)]);
					}			
			}
			return redirect(route('productCategories.index'));
		}
		else{
		
			$category = $this->categoryRepository->update($request->all(), $id);
			if($request->has('signature'))
			{	
					

					$jsonstring = array();
		
				$jsonstring['src'] =$category->image['src'];
				$jsonstring['title'] = $category->slug.'-signature';
				$jsonstring['alt'] = '';
				$sign = \App\Models\Category::where('id', '5637168576')->first();
$new_cat= \App\Models\Category::where('slug', $category->slug.'-signature')->first();
			if(is_null($new_cat))
			{
				$new_cat=Category::create(['parent'=>$sign->id,'description'=>$category->description,'name'=>$category->name,'display'=>$category->display,'slug'=>$category->slug.'-signature','image'=>$jsonstring,'count'=>$category->count]);

			}
			foreach($res as $pro)
					{
						
						$sign->update(array('count'=>$sign->count+1));	
						$categories=$pro['categories'];

						array_push($categories,$sign);
						array_push($categories,$new_cat);
						
					$product=Product::create(['id'=>$pro['id'].'-signature','name'=>$pro['name'],'description'=>$pro['description'],
					'name_ar'=> $pro['name_ar'],'description_ar'=>$pro['description_ar'],'sale_price'=> $pro['discountprice'],'on_sale'=>$pro['on_sale'],
					'regular_price'=> $pro['regular_price'],'hot_price'=>$pro['hot_price'],'discountFrom'=>$pro['discountFrom'],'discountto'=>$pro['discountto'],'discountprice'=>$pro['discountprice'],
					'barcode'=>$pro['barcode'],'PriceUnit'=>$pro['PriceUnit'],'in_stock' => $pro['in_stock'],'category_id'=>$sign->id,'images'=>json_encode($pro['images']),
					'01'=>$pro['01'],'02'=>$pro['02'],'04'=>$pro['04'],'categories'=>json_encode($categories)]);
					}	
			
			
			}
		}
		
		

		Flash::success('Product Category updated successfully.');

		return redirect(route('productCategories.index'));
	}

	/**
	 * Remove the specified ProductCategory from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->authorize('categories.delete');
		$category = $this->categoryRepository->findWithoutFail($id);
	
		$products = \Cache::get('products', []);
		$find=$this->searchForId($category->id, $products);
		
		if($find !== false)
		{
			Flash::error('Category Contain Products');

			return redirect(route('productCategories.index'));

		}
	
		elseif (empty($category)) {
			Flash::error('Category not found');

			return redirect(route('productCategories.index'));
		}

		$this->categoryRepository->delete($id);

		Flash::success('Product Category deleted successfully.');

		return redirect(route('productCategories.index'));
	}
	public function  searchForId($id, $array)
	{
	
	   foreach ($array as $key => $val) {
		  if($val['categories']!=[])
		  {
			if ($val['categories'][0]['id'] === $id) {
			
				return $key;
			}
		  }
		 
		  
	   }
	   return false;
	}
	public function syncAPI(Request $request)
	{
	
	
	// if(count(Category::all())==0)
	// {
			$ch = curl_init('http://41.33.238.100/RR_Oscar_Product/Integration_WB.asmx/GetCategory');

			// Request headers
			$headers = array();
			$headers[] = '';
	
			// Return the transfer as a string
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
			// $output contains the output string
			$output = curl_exec($ch);
	
			// Close curl resource to free up system resources
			curl_close($ch);
			$category_response= json_decode($output);
			
			foreach($category_response as $category)
			{
			
				
		
				$name=['en'=>$category->Cat1En,'ar'=>$category->Cat1Ar];
				$description=['en'=>null,'ar'=>null];
				$parent=null;
				if($category->Cat2En!='Mobileapp')
				{
				
					$parent=Category::where('slug',str_slug($category->Cat2En))->pluck('id');
					$parent=$parent->first();
				}
				$slug=str_slug($category->Cat1En);
				$id=$category->CatRecid;
				$fond=Category::find($category->CatRecid);

				$jsonstring = array();
		
				$jsonstring['src'] =url('/images/image.png');
				$jsonstring['title'] = $category->Cat1En;
				$jsonstring['alt'] = '';
			

				if(strlen( $category->Cat1En) != 0  && strlen($category->Cat1Ar)!=0)
				{
				if(is_null($fond))
				{
					$category=\DB::table('categories')->insert(['id'=>$id,'slug'=>$slug,'name'=>json_encode($name),'parent'=>$parent,'image'=>json_encode($jsonstring)]);

				}
				else{
		
					//$category=Category::where('id','=',$category->CatRecid)->update(['id'=>$id,'slug'=>$slug,'name'=>json_encode($name),'parent'=>$parent,'image'=>json_encode($jsonstring)]);

				}
			}
			
		}
		Flash::success('Product Category Imported successfully.');

		return redirect(route('productCategories.index'));
	// }
	// else
	// {
		// $ch = curl_init('http://41.33.238.100/RR_Oscar_Product/Integration_WB.asmx/GetCategoryupdate');

		// 	// Request headers
		// 	$headers = array();
		// 	$headers[] = '';
	
		// 	// Return the transfer as a string
		// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// 	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
		// 	// $output contains the output string
		// 	$output = curl_exec($ch);
	
		// 	// Close curl resource to free up system resources
		// 	curl_close($ch);
		// 	$update_category_response= json_decode($output);
			
		// 	if($update_category_response !=[])
		// 	{
		// 	foreach($update_category_response as $category)
		// 	{
		// 		if(strtolower($category->Action)=="update")
		// 		{
	

		// 			$name=['en'=>$category->Cat1En,'ar'=>$category->Cat1Ar];
		// 		$description=['en'=>null,'ar'=>null];
		// 		$parent=null;
		// 		if($category->Cat2En!='Mobileapp')
		// 		{
				
		// 			$parent=Category::where('slug',str_slug($category->Cat2En))->pluck('id');
		// 			$parent=$parent->first();
		// 		}
		// 		$slug=str_slug($category->Cat1En);
		// 		$id=$category->CatRecid;
		// 		$fond=Category::find($category->CatRecid);

		// 		$jsonstring = array();
		
		// 		$jsonstring['src'] =url('/images/image.png');
		// 		$jsonstring['title'] = $category->Cat1En;
		// 		$jsonstring['alt'] = '';
		// 		if(strlen( $category->Cat1En) != 0  && strlen($category->Cat1Ar)!=0)
		// 		{
		// 		$category=Category::where('id','=',$category->CatRecid)->update(['id'=>$id,'slug'=>$slug,'name'=>json_encode($name),'parent'=>$parent,'image'=>json_encode($jsonstring)]);
		// 		}
		// 		}
		// 		if(strtolower($category->Action)=="insert")
		// 		{
		// 			$name=['en'=>$category->Cat1En,'ar'=>$category->Cat1Ar];
		// 		$description=['en'=>null,'ar'=>null];
		// 		$parent=null;
		// 		if($category->Cat2En!='Mobileapp')
		// 		{
				
		// 			$parent=Category::where('slug',str_slug($category->Cat2En))->pluck('id');
		// 			$parent=$parent->first();
		// 		}
		// 		$slug=str_slug($category->Cat1En);
		// 		$id=$category->CatRecid;
		// 		$fond=Category::find($category->CatRecid);

		// 		$jsonstring = array();
		
		// 		$jsonstring['src'] =url('/images/image.png');
		// 		$jsonstring['title'] = $category->Cat1En;
		// 		$jsonstring['alt'] = '';

		// 		if(strlen( $category->Cat1En) != 0  && strlen($category->Cat1Ar)!=0)
		// 		{
				
		// 			$category=\DB::table('categories')->insert(['id'=>$id,'slug'=>$slug,'name'=>json_encode($name),'parent'=>$parent,'image'=>json_encode($jsonstring)]);


				
		// 		}
		// 	}
		// 	elseif(strtolower($category->Action)=="delete")
		// 	{
		// 		$category=Category::where('id','=',$category->CatRecid)->delete();
		// 	}
		// }
	// }

		// Flash::success('Product Category Imported successfully.');

		// return redirect(route('productCategories.index'));
// }		
			
	}
	public function import(Request $request)
	{
	
		
		$arr = [];
		

		if ($request->hasFile('file')) {
			$rules = [
				'file' => ["required", ''],
			];
			$messages = [
				'mimes' => 'The :attribute field is must be a file of type: xlsx.',
			];

			$this->validate($request, $rules, $messages);
			$excel = Importer::make('Excel');
			$excel->load($request->file('file'));
			$collection = $excel->getCollection();
			$collection = $collection->forget($collection->shift());

			foreach ($collection as $record) {
				$inputs=[];
				$img[0] = $record[6];
				array_push($inputs, ['name'=>['en'=> $record[0],'ar'=>$record[1]],'description'=>[$record[2],'ar'=>$record[3]],'slug'=>$record[4],'parent'=>$record[5]
				,'image'=>$img]);

				$cats = $this->categoryRepository->findByField('slug',$record[4])->toArray();
				if (!empty($cats))
				{
					Flash::error('Product Category Slug '.$record[4]. ' existed');

					return redirect(route('productCategories.index'));
				}
				
		else
		{
			$category = $this->categoryRepository->create($inputs[0]);


		}


				
					}
				
			
	

			Flash::success('Product imported successfully.');
			return redirect(route('productCategories.index'));
			//dd($products);
		}

		return view('product_categories.import')->with('array', $arr);
	}
}
  
 