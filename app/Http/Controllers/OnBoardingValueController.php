<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOnBoardingValueRequest;
use App\Http\Requests\UpdateOnBoardingValueRequest;
use App\Repositories\OnBoardingValueRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class OnBoardingValueController extends AppBaseController
{
    /** @var  OnBoardingValueRepository */
    private $onBoardingValueRepository;

    public function __construct(OnBoardingValueRepository $onBoardingValueRepo)
    {
			$this->middleware('auth');
        $this->onBoardingValueRepository = $onBoardingValueRepo;
    }

    /**
     * Display a listing of the OnBoardingValue.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->onBoardingValueRepository->pushCriteria(new RequestCriteria($request));
        $onBoardingValues = $this->onBoardingValueRepository->paginate(20);

        return view('on_boarding_values.index')
            ->with('onBoardingValues', $onBoardingValues);
    }

    /**
     * Show the form for creating a new OnBoardingValue.
     *
     * @return Response
     */
    public function create()
    {
        return view('on_boarding_values.create');
    }

    /**
     * Store a newly created OnBoardingValue in storage.
     *
     * @param CreateOnBoardingValueRequest $request
     *
     * @return Response
     */
    public function store(CreateOnBoardingValueRequest $request)
    {
        $input = $request->all();

        $onBoardingValue = $this->onBoardingValueRepository->create($input);

        Flash::success('On Boarding Value saved successfully.');

        return redirect(route('onBoardingValues.index'));
    }

    /**
     * Display the specified OnBoardingValue.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $onBoardingValue = $this->onBoardingValueRepository->findWithoutFail($id);

        if (empty($onBoardingValue)) {
            Flash::error('On Boarding Value not found');

            return redirect(route('onBoardingValues.index'));
        }

        return view('on_boarding_values.show')->with('onBoardingValue', $onBoardingValue);
    }

    /**
     * Show the form for editing the specified OnBoardingValue.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $onBoardingValue = $this->onBoardingValueRepository->findWithoutFail($id);

        if (empty($onBoardingValue)) {
            Flash::error('On Boarding Value not found');

            return redirect(route('onBoardingValues.index'));
        }

        return view('on_boarding_values.edit')->with('onBoardingValue', $onBoardingValue);
    }

    /**
     * Update the specified OnBoardingValue in storage.
     *
     * @param  int              $id
     * @param UpdateOnBoardingValueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOnBoardingValueRequest $request)
    {
        $onBoardingValue = $this->onBoardingValueRepository->findWithoutFail($id);

        if (empty($onBoardingValue)) {
            Flash::error('On Boarding Value not found');

            return redirect(route('onBoardingValues.index'));
        }

        $onBoardingValue = $this->onBoardingValueRepository->update($request->all(), $id);

        Flash::success('On Boarding Value updated successfully.');

        return redirect(route('onBoardingValues.index'));
    }

    /**
     * Remove the specified OnBoardingValue from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $onBoardingValue = $this->onBoardingValueRepository->findWithoutFail($id);

        if (empty($onBoardingValue)) {
            Flash::error('On Boarding Value not found');

            return redirect(route('onBoardingValues.index'));
        }

        $this->onBoardingValueRepository->delete($id);

        Flash::success('On Boarding Value deleted successfully.');

        return redirect(route('onBoardingValues.index'));
    }
}
