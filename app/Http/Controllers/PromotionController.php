<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreatePromotionRequest;
use App\Http\Requests\UpdatePromotionRequest;
use App\Models\Product;
use App\Repositories\CategoryRepository;
use App\Repositories\PromotionRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Importer;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class PromotionController extends AppBaseController
{
	/** @var  PromotionRepository */
	private $promotionRepository;

	public function __construct(PromotionRepository $promotionRepo, CategoryRepository $categoryRepo)
	{
		$this->middleware('auth');
		$this->promotionRepository = $promotionRepo;
		$this->categoryRepository = $categoryRepo;
	}

	public function import(Request $request)
	{
		$arr = [];
		if ($request->hasFile('file')) {
			$excel = Importer::make('Excel');
			$excel->load($request->file('file'));
			$collection = $excel->getCollection();
			foreach($collection as $record) {
				if($record[0]) {
					$promotion = $this->promotionRepository->create([
						'title' => $record[1],
						"categories" => [$record[6]],
						"type" => "magazine",
						"description" => "",
						"image" => $record[5],
						"price" => $record[3],
						"sale" => sprintf('%0.2f', $record[3] - $record[4]),
						"start_date" => $record[8],
						"end_date" => $record[9]
					]);
					$promotion->m_title = [
						'en' => $record[1],
						'ar' => $record[2]
					];
					$promotion->save();
				}
			}
		}
		
		return view('promotions.import')->with('array', $arr);
	}

	/**
	 * Display a listing of the Promotion.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$this->promotionRepository->pushCriteria(new RequestCriteria($request));
		$promotions = $this->promotionRepository->paginate(10);


		
		return view('promotions.index')
			->with('promotions', $promotions);
	}

	/**
	 * Show the form for creating a new Promotion.
	 *
	 * @return Response
	 */
	public function create()
	{
		$categories = $this->categoryRepository->pluck('title', 'id');
		return view('promotions.create')->with('categories', $categories);
	}

	/**
	 * Store a newly created Promotion in storage.
	 *
	 * @param CreatePromotionRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePromotionRequest $request)
	{
		$input = $request->all();

		$promotion = $this->promotionRepository->create($input);

		Flash::success('Promotion saved successfully.');

		return redirect(route('promotions.index'));
	}

	/**
	 * Display the specified Promotion.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$promotion = $this->promotionRepository->findWithoutFail($id);

		if (empty($promotion)) {
			Flash::error('Promotion not found');

			return redirect(route('promotions.index'));
		}

		return view('promotions.show')->with('promotion', $promotion);
	}

	/**
	 * Show the form for editing the specified Promotion.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$promotion = $this->promotionRepository->findWithoutFail($id);
		$categories = $this->categoryRepository->pluck('title', 'id');

		if (empty($promotion)) {
			Flash::error('Promotion not found');

			return redirect(route('promotions.index'));
		}

		return view('promotions.edit')->with('promotion', $promotion)->with('categories', $categories);
	}

	/**
	 * Update the specified Promotion in storage.
	 *
	 * @param  int              $id
	 * @param UpdatePromotionRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatePromotionRequest $request)
	{
		$promotion = $this->promotionRepository->findWithoutFail($id);

		if (empty($promotion)) {
			Flash::error('Promotion not found');

			return redirect(route('promotions.index'));
		}

		$input = $request->all();
		if(!$input['start_date']) unset($input['start_date']);
		if(!$input['end_date']) unset($input['end_date']);

		$promotion = $this->promotionRepository->update($input, $id);

		Flash::success('Promotion updated successfully.');

		return redirect(route('promotions.index'));
	}

	/**
	 * Remove the specified Promotion from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$promotion = $this->promotionRepository->findWithoutFail($id);

		if (empty($promotion)) {
			Flash::error('Promotion not found');

			return redirect(route('promotions.index'));
		}

		$this->promotionRepository->delete($id);

		Flash::success('Promotion deleted successfully.');

		return redirect(route('promotions.index'));
	}

	/**
	 * Remove the specified Promotion from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroyAll(Request $request)
	{
		$this->promotionRepository->pushCriteria(new RequestCriteria($request));
		$promotions = $this->promotionRepository->pluck('id');

		if(count($promotions)) {
			\App\Models\Promotion::destroy($promotions[0]);
			\App\Models\Promotion::destroy(explode(",", $promotions));
	
			Flash::success('Promotion deleted successfully.');
		}

		return redirect(route('promotions.index'));
	}
}
