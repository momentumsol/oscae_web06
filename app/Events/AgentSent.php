<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Customer;
use App\Models\Message;
class AgentSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Customer that sent the message
     *
     * @var Customer
     */
    public $customer;

    /**
     * Message details
     *
     * @var Message
     */
    public $message;
    public $id;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, Message $message,$id)
    {
      
        $this->customer = $customer;
        $this->message = $message;
        $this->id = $id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat.'.$this->id);

        
    }
}
