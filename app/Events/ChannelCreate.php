<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Customer;
use App\Models\Message;
use App\Models\Channel as ChannelNew;
class ChannelCreate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Customer that sent the message
     *
     * @var Customer
     */
    public $customer;
    public $channel;

    /**
     * Message details
     *
     * @var Message
     */



    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Customer $customer,ChannelNew $channel)
    {
        $this->customer = $customer;
        $this->channel = $channel;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('new_chat');
    }
}
