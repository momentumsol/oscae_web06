<?php

use App\Models\OnBoarding;
use App\Repositories\OnBoardingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OnBoardingRepositoryTest extends TestCase
{
    use MakeOnBoardingTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var OnBoardingRepository
     */
    protected $onBoardingRepo;

    public function setUp()
    {
        parent::setUp();
        $this->onBoardingRepo = App::make(OnBoardingRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateOnBoarding()
    {
        $onBoarding = $this->fakeOnBoardingData();
        $createdOnBoarding = $this->onBoardingRepo->create($onBoarding);
        $createdOnBoarding = $createdOnBoarding->toArray();
        $this->assertArrayHasKey('id', $createdOnBoarding);
        $this->assertNotNull($createdOnBoarding['id'], 'Created OnBoarding must have id specified');
        $this->assertNotNull(OnBoarding::find($createdOnBoarding['id']), 'OnBoarding with given id must be in DB');
        $this->assertModelData($onBoarding, $createdOnBoarding);
    }

    /**
     * @test read
     */
    public function testReadOnBoarding()
    {
        $onBoarding = $this->makeOnBoarding();
        $dbOnBoarding = $this->onBoardingRepo->find($onBoarding->id);
        $dbOnBoarding = $dbOnBoarding->toArray();
        $this->assertModelData($onBoarding->toArray(), $dbOnBoarding);
    }

    /**
     * @test update
     */
    public function testUpdateOnBoarding()
    {
        $onBoarding = $this->makeOnBoarding();
        $fakeOnBoarding = $this->fakeOnBoardingData();
        $updatedOnBoarding = $this->onBoardingRepo->update($fakeOnBoarding, $onBoarding->id);
        $this->assertModelData($fakeOnBoarding, $updatedOnBoarding->toArray());
        $dbOnBoarding = $this->onBoardingRepo->find($onBoarding->id);
        $this->assertModelData($fakeOnBoarding, $dbOnBoarding->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteOnBoarding()
    {
        $onBoarding = $this->makeOnBoarding();
        $resp = $this->onBoardingRepo->delete($onBoarding->id);
        $this->assertTrue($resp);
        $this->assertNull(OnBoarding::find($onBoarding->id), 'OnBoarding should not exist in DB');
    }
}
