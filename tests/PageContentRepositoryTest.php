<?php

use App\Models\PageContent;
use App\Repositories\PageContentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PageContentRepositoryTest extends TestCase
{
    use MakePageContentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PageContentRepository
     */
    protected $pageContentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->pageContentRepo = App::make(PageContentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePageContent()
    {
        $pageContent = $this->fakePageContentData();
        $createdPageContent = $this->pageContentRepo->create($pageContent);
        $createdPageContent = $createdPageContent->toArray();
        $this->assertArrayHasKey('id', $createdPageContent);
        $this->assertNotNull($createdPageContent['id'], 'Created PageContent must have id specified');
        $this->assertNotNull(PageContent::find($createdPageContent['id']), 'PageContent with given id must be in DB');
        $this->assertModelData($pageContent, $createdPageContent);
    }

    /**
     * @test read
     */
    public function testReadPageContent()
    {
        $pageContent = $this->makePageContent();
        $dbPageContent = $this->pageContentRepo->find($pageContent->id);
        $dbPageContent = $dbPageContent->toArray();
        $this->assertModelData($pageContent->toArray(), $dbPageContent);
    }

    /**
     * @test update
     */
    public function testUpdatePageContent()
    {
        $pageContent = $this->makePageContent();
        $fakePageContent = $this->fakePageContentData();
        $updatedPageContent = $this->pageContentRepo->update($fakePageContent, $pageContent->id);
        $this->assertModelData($fakePageContent, $updatedPageContent->toArray());
        $dbPageContent = $this->pageContentRepo->find($pageContent->id);
        $this->assertModelData($fakePageContent, $dbPageContent->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePageContent()
    {
        $pageContent = $this->makePageContent();
        $resp = $this->pageContentRepo->delete($pageContent->id);
        $this->assertTrue($resp);
        $this->assertNull(PageContent::find($pageContent->id), 'PageContent should not exist in DB');
    }
}
