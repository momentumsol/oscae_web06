<?php

use Faker\Factory as Faker;
use App\Models\PageType;
use App\Repositories\PageTypeRepository;

trait MakePageTypeTrait
{
    /**
     * Create fake instance of PageType and save it in database
     *
     * @param array $pageTypeFields
     * @return PageType
     */
    public function makePageType($pageTypeFields = [])
    {
        /** @var PageTypeRepository $pageTypeRepo */
        $pageTypeRepo = App::make(PageTypeRepository::class);
        $theme = $this->fakePageTypeData($pageTypeFields);
        return $pageTypeRepo->create($theme);
    }

    /**
     * Get fake instance of PageType
     *
     * @param array $pageTypeFields
     * @return PageType
     */
    public function fakePageType($pageTypeFields = [])
    {
        return new PageType($this->fakePageTypeData($pageTypeFields));
    }

    /**
     * Get fake data of PageType
     *
     * @param array $postFields
     * @return array
     */
    public function fakePageTypeData($pageTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $pageTypeFields);
    }
}
