<?php

use Faker\Factory as Faker;
use App\Models\PageContent;
use App\Repositories\PageContentRepository;

trait MakePageContentTrait
{
    /**
     * Create fake instance of PageContent and save it in database
     *
     * @param array $pageContentFields
     * @return PageContent
     */
    public function makePageContent($pageContentFields = [])
    {
        /** @var PageContentRepository $pageContentRepo */
        $pageContentRepo = App::make(PageContentRepository::class);
        $theme = $this->fakePageContentData($pageContentFields);
        return $pageContentRepo->create($theme);
    }

    /**
     * Get fake instance of PageContent
     *
     * @param array $pageContentFields
     * @return PageContent
     */
    public function fakePageContent($pageContentFields = [])
    {
        return new PageContent($this->fakePageContentData($pageContentFields));
    }

    /**
     * Get fake data of PageContent
     *
     * @param array $postFields
     * @return array
     */
    public function fakePageContentData($pageContentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order' => $fake->randomDigitNotNull,
            'type' => $fake->word,
            'value' => $fake->text,
            'page_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $pageContentFields);
    }
}
