<?php

use Faker\Factory as Faker;
use App\Models\Kitchen;
use App\Repositories\KitchenRepository;

trait MakeKitchenTrait
{
    /**
     * Create fake instance of Kitchen and save it in database
     *
     * @param array $kitchenFields
     * @return Kitchen
     */
    public function makeKitchen($kitchenFields = [])
    {
        /** @var KitchenRepository $kitchenRepo */
        $kitchenRepo = App::make(KitchenRepository::class);
        $theme = $this->fakeKitchenData($kitchenFields);
        return $kitchenRepo->create($theme);
    }

    /**
     * Get fake instance of Kitchen
     *
     * @param array $kitchenFields
     * @return Kitchen
     */
    public function fakeKitchen($kitchenFields = [])
    {
        return new Kitchen($this->fakeKitchenData($kitchenFields));
    }

    /**
     * Get fake data of Kitchen
     *
     * @param array $postFields
     * @return array
     */
    public function fakeKitchenData($kitchenFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->text,
            'slug' => $fake->text,
            'contect' => $fake->text,
            'better_featured_image' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $kitchenFields);
    }
}
