<?php

use Faker\Factory as Faker;
use App\Models\PageItem;
use App\Repositories\PageItemRepository;

trait MakePageItemTrait
{
    /**
     * Create fake instance of PageItem and save it in database
     *
     * @param array $pageItemFields
     * @return PageItem
     */
    public function makePageItem($pageItemFields = [])
    {
        /** @var PageItemRepository $pageItemRepo */
        $pageItemRepo = App::make(PageItemRepository::class);
        $theme = $this->fakePageItemData($pageItemFields);
        return $pageItemRepo->create($theme);
    }

    /**
     * Get fake instance of PageItem
     *
     * @param array $pageItemFields
     * @return PageItem
     */
    public function fakePageItem($pageItemFields = [])
    {
        return new PageItem($this->fakePageItemData($pageItemFields));
    }

    /**
     * Get fake data of PageItem
     *
     * @param array $postFields
     * @return array
     */
    public function fakePageItemData($pageItemFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->text,
            'image' => $fake->word,
            'location' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $pageItemFields);
    }
}
