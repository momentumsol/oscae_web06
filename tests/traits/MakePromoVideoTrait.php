<?php

use Faker\Factory as Faker;
use App\Models\PromoVideo;
use App\Repositories\PromoVideoRepository;

trait MakePromoVideoTrait
{
    /**
     * Create fake instance of PromoVideo and save it in database
     *
     * @param array $promoVideoFields
     * @return PromoVideo
     */
    public function makePromoVideo($promoVideoFields = [])
    {
        /** @var PromoVideoRepository $promoVideoRepo */
        $promoVideoRepo = App::make(PromoVideoRepository::class);
        $theme = $this->fakePromoVideoData($promoVideoFields);
        return $promoVideoRepo->create($theme);
    }

    /**
     * Get fake instance of PromoVideo
     *
     * @param array $promoVideoFields
     * @return PromoVideo
     */
    public function fakePromoVideo($promoVideoFields = [])
    {
        return new PromoVideo($this->fakePromoVideoData($promoVideoFields));
    }

    /**
     * Get fake data of PromoVideo
     *
     * @param array $postFields
     * @return array
     */
    public function fakePromoVideoData($promoVideoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'url' => $fake->word,
            'is_active' => $fake->word,
            'views' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $promoVideoFields);
    }
}
