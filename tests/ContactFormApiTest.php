<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactFormApiTest extends TestCase
{
    use MakeContactFormTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateContactForm()
    {
        $contactForm = $this->fakeContactFormData();
        $this->json('POST', '/api/v1/contactForms', $contactForm);

        $this->assertApiResponse($contactForm);
    }

    /**
     * @test
     */
    public function testReadContactForm()
    {
        $contactForm = $this->makeContactForm();
        $this->json('GET', '/api/v1/contactForms/'.$contactForm->id);

        $this->assertApiResponse($contactForm->toArray());
    }

    /**
     * @test
     */
    public function testUpdateContactForm()
    {
        $contactForm = $this->makeContactForm();
        $editedContactForm = $this->fakeContactFormData();

        $this->json('PUT', '/api/v1/contactForms/'.$contactForm->id, $editedContactForm);

        $this->assertApiResponse($editedContactForm);
    }

    /**
     * @test
     */
    public function testDeleteContactForm()
    {
        $contactForm = $this->makeContactForm();
        $this->json('DELETE', '/api/v1/contactForms/'.$contactForm->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/contactForms/'.$contactForm->id);

        $this->assertResponseStatus(404);
    }
}
