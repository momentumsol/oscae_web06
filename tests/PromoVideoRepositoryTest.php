<?php

use App\Models\PromoVideo;
use App\Repositories\PromoVideoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PromoVideoRepositoryTest extends TestCase
{
    use MakePromoVideoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PromoVideoRepository
     */
    protected $promoVideoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->promoVideoRepo = App::make(PromoVideoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePromoVideo()
    {
        $promoVideo = $this->fakePromoVideoData();
        $createdPromoVideo = $this->promoVideoRepo->create($promoVideo);
        $createdPromoVideo = $createdPromoVideo->toArray();
        $this->assertArrayHasKey('id', $createdPromoVideo);
        $this->assertNotNull($createdPromoVideo['id'], 'Created PromoVideo must have id specified');
        $this->assertNotNull(PromoVideo::find($createdPromoVideo['id']), 'PromoVideo with given id must be in DB');
        $this->assertModelData($promoVideo, $createdPromoVideo);
    }

    /**
     * @test read
     */
    public function testReadPromoVideo()
    {
        $promoVideo = $this->makePromoVideo();
        $dbPromoVideo = $this->promoVideoRepo->find($promoVideo->id);
        $dbPromoVideo = $dbPromoVideo->toArray();
        $this->assertModelData($promoVideo->toArray(), $dbPromoVideo);
    }

    /**
     * @test update
     */
    public function testUpdatePromoVideo()
    {
        $promoVideo = $this->makePromoVideo();
        $fakePromoVideo = $this->fakePromoVideoData();
        $updatedPromoVideo = $this->promoVideoRepo->update($fakePromoVideo, $promoVideo->id);
        $this->assertModelData($fakePromoVideo, $updatedPromoVideo->toArray());
        $dbPromoVideo = $this->promoVideoRepo->find($promoVideo->id);
        $this->assertModelData($fakePromoVideo, $dbPromoVideo->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePromoVideo()
    {
        $promoVideo = $this->makePromoVideo();
        $resp = $this->promoVideoRepo->delete($promoVideo->id);
        $this->assertTrue($resp);
        $this->assertNull(PromoVideo::find($promoVideo->id), 'PromoVideo should not exist in DB');
    }
}
