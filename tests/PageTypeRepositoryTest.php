<?php

use App\Models\PageType;
use App\Repositories\PageTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PageTypeRepositoryTest extends TestCase
{
    use MakePageTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PageTypeRepository
     */
    protected $pageTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->pageTypeRepo = App::make(PageTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePageType()
    {
        $pageType = $this->fakePageTypeData();
        $createdPageType = $this->pageTypeRepo->create($pageType);
        $createdPageType = $createdPageType->toArray();
        $this->assertArrayHasKey('id', $createdPageType);
        $this->assertNotNull($createdPageType['id'], 'Created PageType must have id specified');
        $this->assertNotNull(PageType::find($createdPageType['id']), 'PageType with given id must be in DB');
        $this->assertModelData($pageType, $createdPageType);
    }

    /**
     * @test read
     */
    public function testReadPageType()
    {
        $pageType = $this->makePageType();
        $dbPageType = $this->pageTypeRepo->find($pageType->id);
        $dbPageType = $dbPageType->toArray();
        $this->assertModelData($pageType->toArray(), $dbPageType);
    }

    /**
     * @test update
     */
    public function testUpdatePageType()
    {
        $pageType = $this->makePageType();
        $fakePageType = $this->fakePageTypeData();
        $updatedPageType = $this->pageTypeRepo->update($fakePageType, $pageType->id);
        $this->assertModelData($fakePageType, $updatedPageType->toArray());
        $dbPageType = $this->pageTypeRepo->find($pageType->id);
        $this->assertModelData($fakePageType, $dbPageType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePageType()
    {
        $pageType = $this->makePageType();
        $resp = $this->pageTypeRepo->delete($pageType->id);
        $this->assertTrue($resp);
        $this->assertNull(PageType::find($pageType->id), 'PageType should not exist in DB');
    }
}
