<?php

use App\Models\ContactForm;
use App\Repositories\ContactFormRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactFormRepositoryTest extends TestCase
{
    use MakeContactFormTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ContactFormRepository
     */
    protected $contactFormRepo;

    public function setUp()
    {
        parent::setUp();
        $this->contactFormRepo = App::make(ContactFormRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateContactForm()
    {
        $contactForm = $this->fakeContactFormData();
        $createdContactForm = $this->contactFormRepo->create($contactForm);
        $createdContactForm = $createdContactForm->toArray();
        $this->assertArrayHasKey('id', $createdContactForm);
        $this->assertNotNull($createdContactForm['id'], 'Created ContactForm must have id specified');
        $this->assertNotNull(ContactForm::find($createdContactForm['id']), 'ContactForm with given id must be in DB');
        $this->assertModelData($contactForm, $createdContactForm);
    }

    /**
     * @test read
     */
    public function testReadContactForm()
    {
        $contactForm = $this->makeContactForm();
        $dbContactForm = $this->contactFormRepo->find($contactForm->id);
        $dbContactForm = $dbContactForm->toArray();
        $this->assertModelData($contactForm->toArray(), $dbContactForm);
    }

    /**
     * @test update
     */
    public function testUpdateContactForm()
    {
        $contactForm = $this->makeContactForm();
        $fakeContactForm = $this->fakeContactFormData();
        $updatedContactForm = $this->contactFormRepo->update($fakeContactForm, $contactForm->id);
        $this->assertModelData($fakeContactForm, $updatedContactForm->toArray());
        $dbContactForm = $this->contactFormRepo->find($contactForm->id);
        $this->assertModelData($fakeContactForm, $dbContactForm->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteContactForm()
    {
        $contactForm = $this->makeContactForm();
        $resp = $this->contactFormRepo->delete($contactForm->id);
        $this->assertTrue($resp);
        $this->assertNull(ContactForm::find($contactForm->id), 'ContactForm should not exist in DB');
    }
}
