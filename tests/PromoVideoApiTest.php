<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PromoVideoApiTest extends TestCase
{
    use MakePromoVideoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePromoVideo()
    {
        $promoVideo = $this->fakePromoVideoData();
        $this->json('POST', '/api/v1/promoVideos', $promoVideo);

        $this->assertApiResponse($promoVideo);
    }

    /**
     * @test
     */
    public function testReadPromoVideo()
    {
        $promoVideo = $this->makePromoVideo();
        $this->json('GET', '/api/v1/promoVideos/'.$promoVideo->id);

        $this->assertApiResponse($promoVideo->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePromoVideo()
    {
        $promoVideo = $this->makePromoVideo();
        $editedPromoVideo = $this->fakePromoVideoData();

        $this->json('PUT', '/api/v1/promoVideos/'.$promoVideo->id, $editedPromoVideo);

        $this->assertApiResponse($editedPromoVideo);
    }

    /**
     * @test
     */
    public function testDeletePromoVideo()
    {
        $promoVideo = $this->makePromoVideo();
        $this->json('DELETE', '/api/v1/promoVideos/'.$promoVideo->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/promoVideos/'.$promoVideo->id);

        $this->assertResponseStatus(404);
    }
}
