<!-- resources/views/chat.blade.php -->

@extends('layouts.app_chat')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="container" id="app">
    <div class="row"  >
        <div class="col-lg-8" >
        
            <div class="panel panel-default">
                <div class="panel-heading">Chats</div>

                <div class="panel-body">
                    <chat-messages :messages="messages"></chat-messages>
                </div>
                <div class="panel-footer">
                    <chat-form
                        v-on:messagesent="addMessage"
                        :user="{{ Auth::guard('customers')->user() }}"
                    ></chat-form>
                </div>
            </div>
            <div>
            </div>
            

        </div>
    </div>
</div>

<script>
   window.localStorage.setItem("token", '{!! Auth::guard('customers')->user()->token !!}');
   window.localStorage.setItem("id", '{!! Auth::guard('customers')->user()->id !!}');

</script>

<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>

@endsection