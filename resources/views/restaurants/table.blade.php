<table class="table table-responsive" id="restaurants-table">
	<thead>
		<tr>
			<th>Title</th>
			<th>Image</th>
			<th>Menu</th>
			<th colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($restaurants as $restaurant)
		<tr>
			<td>{!! $restaurant->title->en !!}</td>
			<td><img src="{!! $restaurant->image !!}" width="50" alt=""></td>
			<td>
				<a href="{!! $restaurant->menu !!}" target="blank">Link</a>
			</td>
			<td>
				{!! Form::open(['route' => ['restaurants.destroy', $restaurant->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('restaurants.show', [$restaurant->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('restaurants.edit', [$restaurant->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>					{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs',
					'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>