<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $restaurant->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $restaurant->gettitle() !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}

    <p>
		<img src="{!! $restaurant->image !!}" height="100" alt="">
	</p>

</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $restaurant->description !!}</p>
</div>

<!-- Emails Field -->
<div class="form-group">
    {!! Form::label('emails', 'Emails:') !!}
    <p>{!! $restaurant->emails !!}</p>
</div>

<!-- Menu Field -->
<div class="form-group">
    {!! Form::label('menu', 'Menu:') !!}
    <p>{!! $restaurant->menu !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $restaurant->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $restaurant->updated_at !!}</p>
</div>

