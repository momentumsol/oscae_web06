{{-- @component('mail::message') --}}

@if (isset($data['data']['support']))
    @if (!is_null($data['data']['order']))

    <p> Order Number : {{ $data['data']['order'] }}</p>
    <br>
    <p> Message  : {{ $data['data']['order-message'] }}</p>
    @elseif(!is_null($data['data']['driver']))

    <p>Driver name : {{ $data['data']['driver'] }}</p>
    <br>
    <p>Message  : {{ $data['data']['driver-message'] }}
    @else
    <p>Message  : {{ $data['data']['other-message'] }}</p>

    @endif
@else
<div>
    <h1>Contact us email form</h1>
    <p><span>Name:</span><span>{{ $data['data']['fname'] ?? Auth::guard('customerForWeb')->user()->name }}  {{ $data['data']['lname'] ??' '  }} </span></p>
    <p>Email :{{ $data['data']['email']  ??  Auth::guard('customerForWeb')->user()->email}}</p>
    <p>Message :{{ $data['data']['comment'] ?? ' ' }}</p>
</div>


@endif



{{-- 
Thanks,<br>
Oscar --}}
{{-- @endcomponent --}}
