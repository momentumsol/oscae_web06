@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Product Categories</h1>
        <h1 class="pull-right">
        <a class="btn btn-warning pull-left" style="margin-top: -10px;margin-bottom: 5px"
        href="{!! route('productCategories.syncAPI') !!}">Import From API</a>
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
               href="{!! route('productCategories.create') !!}">Add New</a>
               <a class="btn btn-warning pull-left" style="margin-top: -10px;margin-bottom: 5px"
            href="{!! url('productCategories/import') !!}">Import</a>
          
        </h1>

        
    </section>
    <div class="content">
        <div class="clearfix"></div>


        @include('flash::message')
        <div class="form-group col-sm-12">
        {!! Form::open(['route' => 'productCategories.selectCategory', 'files' => true]) !!}
		{!! Form::label('categories', 'Categories:') !!}
        <select class="form-control" name="categories_select">
        <option value="main">Main Category</option>
    @foreach($categories_select as $item)
      <option value="{{$item->id}}">{{$item->display_name}}</option>
    @endforeach
  </select>
		
        <div class="form-group col-sm-12">
		{!! Form::submit('Select', ['class' => 'btn btn-primary']) !!}
	
	
	</div>
    </div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('product_categories.table')
            </div>
        </div>
        <div class="text-center">
            {{$productCategories->links()}}
        </div>
    </div>
  
@endsection
  