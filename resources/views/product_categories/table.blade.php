<table class="table table-hover table-striped table-responsive" id="productCategories-table">
	<thead>
		<tr>
			<td>Image</td>
			<td>Title</td>
			<td>Slug</td>
			<th colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($productCategories as $productCategory)
		<tr>
			<td>@if(isset($productCategory->image)) <img src="{!! $productCategory['image']['src'] !!}" alt="" height="50"> @endif</td>
			<td>{!! $productCategory->name->en !!}</td>
			<td>{!! $productCategory->slug !!}</td>
			<td>
				{!! Form::open(['route' => ['productCategories.destroy', $productCategory['id']], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('productCategories.show', [$productCategory['id']]) !!}" class='btn btn-default btn-xs'><i
							class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('productCategories.edit', [$productCategory['id']]) !!}" class='btn btn-default btn-xs'><i
							class="glyphicon glyphicon-edit"></i></a>
					{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger
					btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>