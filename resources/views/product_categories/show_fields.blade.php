<div class="form-group">
	{!! Form::label('id', 'Id:') !!}
	<p>{!! $productCategory['id'] !!}</p>
</div>

<div class="form-group">
	{!! Form::label('slug', 'Slug:') !!}
	<p>{!! $productCategory['slug'] !!}</p>
</div>

<div class="form-group">
	{!! Form::label('name', 'Name:') !!}
	<p>{!! $productCategory->getname() !!}</p>
</div>

<div class="form-group">
	{!! Form::label('image', 'Image:') !!}
	<p>
		<img src="{!! $productCategory['image']['src'] !!}" height="100" alt="">
	</p>
</div>

{{--  <!-- Created At Field -->
<div class="form-group">
	{!! Form::label('created_at', 'Created At:') !!}
	<p>{!! $productCategory['created_at'] !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
	{!! Form::label('updated_at', 'Updated At:') !!}
	<p>{!! $productCategory['updated_at'] !!}</p>
</div>  --}}