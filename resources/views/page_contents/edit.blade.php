@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Page Content
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($pageContent, ['route' => ['pageContents.update', $pageContent->id], 'method' => 'patch']) !!}

                        @include('page_contents.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection