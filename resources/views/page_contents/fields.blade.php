<!-- Order Field -->
<div class="form-group col-sm-6">
	{!! Form::label('order', 'Order:') !!}
	{!! Form::number('order', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
	{!! Form::label('type', 'Type:') !!}
	{!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-12 col-lg-12">
	{!! Form::label('value', 'Value:') !!}
	{!! Form::textarea('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Page Id Field -->
<div class="form-group col-sm-6">
	{!! Form::label('page_id', 'Page Id:') !!}
	{!! Form::select('page_id', $pages, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('pageContents.index') !!}" class="btn btn-default">Cancel</a>
</div>
