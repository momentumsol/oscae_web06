<table class="table table-responsive" id="promotions-table">
	<thead>
		<tr>
			<th>Title</th>
			<th>Type</th>
			<th>Description</th>
			<th>Image</th>
			<th>URL</th>
			<th>Price</th>
			<th>Sale</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($promotions as $promotion)
		<tr>
			<td>{!! $promotion->title !!}</td>
			<td>{!! $promotion->type !!}</td>
			<td>{!! $promotion->description !!}</td>
			<td><img src="{!! $promotion->image !!}" width="50" alt=""></td>
			<td>
				@if($promotion->url)
					<a target="blank" href="{!! $promotion->url !!}">Link</a>
				@else
					NO URL
				@endif
			</td>
			<td>{!! $promotion->price !!}</td>
			<td>{!! $promotion->sale !!}</td>
			<td>{!! $promotion->start_date !!}</td>
			<td>{!! $promotion->end_date !!}</td>
			<td>
				{!! Form::open(['route' => ['promotions.destroy', $promotion->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('promotions.show', [$promotion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('promotions.edit', [$promotion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>					{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs',
					'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>