@extends('layouts.app') 
@section('content')
<section class="content-header">
	<h1 class="pull-left">Promotions</h1>
	<h1 class="pull-right">
		{!! Form::open(['route' => ['promotions.destroy_all'], 'method' => 'delete']) !!}
			<div class='btn-group'>
				{!! Form::hidden('search', 'magazine') !!}
				{!! Form::button('Delete Magazine', ['type' => 'submit', 'class' => 'btn btn-danger',
				'onclick' => "return confirm('Are you sure?')"]) !!}
				<a class="btn btn-primary pull-right" href="{!! route('promotions.create') !!}">Add New</a>
			</div>
		{!! Form::close() !!}
	</h1>
</section>
<div class="content">
	<div class="clearfix"></div>
	@include('flash::message')
	<div class="clearfix"></div>
	<div class="box box-primary">
		<div class="box-body">
	@include('promotions.table')
		</div>
	</div>
	<div class="text-center">
		@include('adminlte-templates::common.paginate', ['records' => $promotions])
	</div>
</div>
@endsection