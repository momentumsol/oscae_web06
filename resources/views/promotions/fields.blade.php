<!-- Title Field -->
<div class="form-group col-sm-6">
	{!! Form::label('title', 'Title:') !!} {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<!-- Categories Field -->
<div class="form-group col-sm-6">
	{!! Form::label('categories', 'Categories:') !!} {!! Form::select('categories[]', $categories, null, ['class' => 'form-control',
	'multiple' => true]) !!}
</div>
<!-- Type Field -->
<div class="form-group col-sm-6">
	{!! Form::label('type', 'Type:') !!} {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>
<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
	{!! Form::label('description', 'Description:') !!} {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
<!-- Image Field -->
<div class="form-group col-sm-6">
	{!! Form::label('image', 'Image:') !!} {!! Form::file('image') !!}
</div>
<div class="clearfix"></div>
<!-- Url Field -->
<div class="form-group col-sm-6">
	{!! Form::label('url', 'Url:') !!} {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>
<!-- Price Field -->
<div class="form-group col-sm-6">
	{!! Form::label('price', 'Price:') !!} {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>
<!-- Sale Field -->
<div class="form-group col-sm-6">
	{!! Form::label('sale', 'Sale:') !!} {!! Form::number('sale', null, ['class' => 'form-control']) !!}
</div>
<!-- Start Date Field -->
<div class="form-group col-sm-6">
	{!! Form::label('start_date', 'Start Date:') !!} {!! Form::date('start_date', null, ['class' => 'form-control']) !!}
</div>
<!-- End Date Field -->
<div class="form-group col-sm-6">
	{!! Form::label('end_date', 'End Date:') !!} {!! Form::date('end_date', null, ['class' => 'form-control']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('promotions.index') !!}" class="btn btn-default">Cancel</a>
</div>