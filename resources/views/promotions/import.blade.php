@extends('layouts.app') 
@section('content')
<section class="content-header">
	<h1>
		Import
	</h1>
</section>
<div class="content">
	@include('adminlte-templates::common.errors')
	<div class="box box-primary">
		<div class="box-body">
			<div class="row">
				{!! Form::open(['files' => true]) !!}
				<div class="form-group col-sm-6">
					{!! Form::label('file', 'File:') !!} {!! Form::file('file') !!}
				</div>
				<div class="clearfix"></div>
				<!-- Submit Field -->
				<div class="form-group col-sm-12">
					{!! Form::submit('Import', ['class' => 'btn btn-primary']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection