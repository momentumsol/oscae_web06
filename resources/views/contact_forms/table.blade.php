<table class="table table-responsive" id="contactForms-table">
	<thead>
		<tr>
			<th>Type</th>
		<th>Values</th>
		<th>Language</th>
			<th colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
	@foreach($contactForms as $contactForm)
		<tr>
			<td>{!! $contactForm->type !!}</td>
			<td class="json_data">{!! $contactForm->values !!}</td>
			<td>{!! $contactForm->language !!}</td>
			<td>
				{!! Form::open(['route' => ['contactForms.destroy', $contactForm->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('contactForms.show', [$contactForm->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('contactForms.edit', [$contactForm->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
					{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
@section('css')
	<link rel="stylesheet" href="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.css" />
@endsection
@section('scripts')
<script src="https://rawgit.com/abodelot/jquery.json-viewer/master/json-viewer/jquery.json-viewer.js"></script>
<script>
	var options = {
		collapsed: true,
		withQuotes: true
	};
	$('.json_data').each(function(i, e) {
		try {
			var input = eval('(' + $(e).html() + ')');
			$(e).jsonViewer(input, options);
		} catch (err) {
			var tmp = $(e).html();
			var i = 0;
			if((i = tmp.split('.').length) > 1) {
				if(['jpg', 'png'].indexOf(tmp.split('.')[i-1].toLowerCase()) != -1) {
					$(e).html('<img src="'+$(e).html()+'" width=50 />');
				}
			}
			//console.error(err);
			//console.log($(e).html());
		}
	})
</script>
@endsection