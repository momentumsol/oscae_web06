<!-- Name Field -->
<div class="form-group col-sm-6">
	{!! Form::label('name', 'Name:') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Guard Name Field -->
<div class="form-group col-sm-6">
	{!! Form::label('guard_name', 'Guard Name:') !!}
	{!! Form::text('guard_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Permissions Field -->
<div class="form-group col-sm-12">
	{!! Form::label('permissions', 'Permissions:') !!}
	{!! Form::select('permissions[]', $permissions, null, ['id' => 'show', 'class' => 'select2 form-control', 'multiple'=> true]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('roles.index') !!}" class="btn btn-default">Cancel</a>
</div>
