<!-- Title Field -->
<div class="form-group col-sm-6">
	{!! Form::label('title', 'Title:') !!} {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<!-- Categories Field -->
<div class="form-group col-sm-6">
	{!! Form::label('categories', 'Categories:') !!} {!! Form::select('categories[]', $categories, null, ['class' => 'form-control',
	'multiple' => true]) !!}
</div>
<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
	{!! Form::label('description', 'Description:') !!} {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
<!-- Image Field -->
<div class="form-group col-sm-6">
	{!! Form::label('image', 'Image:') !!} {!! Form::file('image') !!}
</div>
<div class="clearfix"></div>
<!-- Location Field -->
<div class="form-group col-sm-12 col-lg-12">
	{!! Form::label('location', 'Location:') !!} {!! Form::textarea('location', null, ['class' => 'form-control']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('pageItems.index') !!}" class="btn btn-default">Cancel</a>
</div>