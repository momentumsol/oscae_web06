<table class="table table-responsive" id="pageItems-table">
	<thead>
		<tr>
			<th>Image</th>
			<th>Title</th>
			<th>Description</th>
			<th>Location</th>
			<th colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($pageItems as $pageItem)
		<tr>
			<td><img src="{!! $pageItem->image !!}" width=100 alt=""></td>
			<td>{!! $pageItem->title !!}</td>
			<td>{!! $pageItem->description !!}</td>
			<td>{!! $pageItem->location !!}</td>
			<td>
				{!! Form::open(['route' => ['pageItems.destroy', $pageItem->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('pageItems.show', [$pageItem->id]) !!}" class='btn btn-default btn-xs'>
						<i class="glyphicon glyphicon-eye-open"></i>
					</a>
					<a href="{!! route('pageItems.edit', [$pageItem->id]) !!}" class='btn btn-default btn-xs'>
						<i class="glyphicon glyphicon-edit"></i>
					</a> {!! Form::button('
					<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return
					confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>