@extends('layouts.app') 
@section('content')
<section class="content-header">
	<h1>
		Page Item
	</h1>
</section>
<div class="content">
	@include('adminlte-templates::common.errors')
	<div class="box box-primary">
		<div class="box-body">
			<div class="row">
				{!! Form::model($pageItem, ['route' => ['pageItems.update', $pageItem->id], 'method' => 'patch', 'files' => true]) !!}
	@include('page_items.fields')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection