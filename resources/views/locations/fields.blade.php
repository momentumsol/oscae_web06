<!-- Name Field -->
<div class="form-group col-sm-6">
	{!! Form::label('name', 'Name:') !!}
	{!! Form::text('name', isset($product)? $product['name'] : null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
	{!! Form::label('image', 'Image:') !!}
	{!! Form::file('image', ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
	{!! Form::label('description', 'Description:') !!}
	<textarea name="description" class="form-control">
		@if(isset($product))
			{!! $product['description'] !!}
		@endif
	</textarea>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('products.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
<script>
	tinymce.init({ selector:'textarea' });
</script>
@stop