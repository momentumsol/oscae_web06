@extends('layouts.app') 
@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<h2>Generate QrCode</h2>
		{!! Form::open(['method' => 'get']) !!}
		<div class="form-group">
			{!! Form::text('url', null, ['class' => 'form-control']) !!}
		</div>
		{!! Form::submit('Generate', ['class' => 'btn btn-primary']) !!}
		
		{!! Form::close() !!}
		@if(isset($url) && $url)
		<p class="text-center">
			{!! QrCode::size(250)->generate($url); !!}
		</p>
		@endif
	</div>
</div>
@endsection