@extends('layouts.app')

@section('content')
<style>
	.box {
		font-size: 1.25rem; /* 20 */
		background-color: #c8dadf;
		position: relative;
		padding: 100px 20px;
	}
	.box.has-advanced-upload {
		outline: 2px dashed #92b0b3;
		outline-offset: -10px;

		-webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
		transition: outline-offset .15s ease-in-out, background-color .15s linear;
	}
	.box.is-dragover {
		outline-offset: -20px;
		outline-color: #c8dadf;
		background-color: #fff;
	}
	.box__dragndrop,
	.box__icon {
		display: none;
	}
	.box.has-advanced-upload .box__dragndrop {
		display: inline;
	}
	.box.has-advanced-upload .box__icon {
		width: 100%;
		height: 80px;
		fill: #92b0b3;
		display: block;
		margin-bottom: 40px;
	}

	.box.is-uploading .box__input,
	.box.is-success .box__input,
	.box.is-error .box__input {
		visibility: hidden;
	}

	.box__uploading,
	.box__success,
	.box__error {
		display: none;
	}
	.box.is-uploading .box__uploading,
	.box.is-success .box__success,
	.box.is-error .box__error {
		display: block;
		position: absolute;
		top: 50%;
		right: 0;
		left: 0;
		text-align: center;
		-webkit-transform: translateY( -50% );
		transform: translateY( -50% );
	}
	.box__uploading {
		font-style: italic;
	}
	.box__success {
		-webkit-animation: appear-from-inside .25s ease-in-out;
		animation: appear-from-inside .25s ease-in-out;
	}
	@-webkit-keyframes appear-from-inside {
		from	{ -webkit-transform: translateY( -50% ) scale( 0 ); }
		75%		{ -webkit-transform: translateY( -50% ) scale( 1.1 ); }
		to		{ -webkit-transform: translateY( -50% ) scale( 1 ); }
	}
	@keyframes appear-from-inside {
		from	{ transform: translateY( -50% ) scale( 0 ); }
		75%		{ transform: translateY( -50% ) scale( 1.1 ); }
		to		{ transform: translateY( -50% ) scale( 1 ); }
	}

	.box__restart {
		font-weight: 700;
	}
	.box__restart:focus,
	.box__restart:hover {
		color: #39bfd3;
	}

	.js .box__file {
		width: 0.1px;
		height: 0.1px;
		opacity: 0;
		overflow: hidden;
		position: absolute;
		z-index: -1;
	}
	.js .box__file + label {
		max-width: 80%;
		text-overflow: ellipsis;
		white-space: nowrap;
		cursor: pointer;
		display: inline-block;
		overflow: hidden;
	}
	.js .box__file + label:hover strong,
	.box__file:focus + label strong,
	.box__file.has-focus + label strong {
		color: #39bfd3;
	}
	.js .box__file:focus + label,
	.js .box__file.has-focus + label {
		outline: 1px dotted #000;
		outline: -webkit-focus-ring-color auto 5px;
	}
	.js .box__file + label * {
		/* pointer-events: none; */ /* in case of FastClick lib use */
	}

	.no-js .box__file + label {
		display: none;
	}

	.no-js .box__button {
		display: block;
	}
	.box__button {
		font-weight: 700;
		color: #e5edf1;
		background-color: #39bfd3;
		display: block;
		padding: 8px 16px;
		margin: 40px auto 0;
	}
	.box__button:hover,
	.box__button:focus
	{
		background-color: #0f3c4b;
	}
	.image-row {
		background: #ddd;
		display: flex;
		align-items: center;
		margin-bottom: 5px;
		padding: 10px;
	}

	.image-row img {
		width: 100px;
		height: auto;
	}

	.image-row span {
		padding: 20px;
	}
</style>
<section class="content-header">
	<h1>
		Dashboard
	</h1>
</section>
<section class="content">
	<div class="row">
		@if(isset($stats['devices']))
		<div class="col-lg-4 col-xs-6">
				<div class="small-box bg-green">
					<div class="inner">
						<h3>{{ $stats['devices'] }}</h3>
						<p>Registered Device</p>
					</div>
					<div class="icon">
						<i class="icon ion-ios-phone-portrait"></i>
					</div>
					<a href="{{url('devices')}}" class="small-box-footer">More info<i class="fa fa-arrow-circle-right"></i></a>
				</div>
		</div>
		@endif
		@if(isset($stats['ios']))
		<div class="col-lg-4 col-xs-6">
			<div class="small-box bg-green">
				<div class="inner">
					<h3>{{ $stats['ios'] }}</h3>
					<p>iOS Device</p>
				</div>
				<div class="icon">
					<i class="icon ion-logo-apple"></i>
				</div>
				<a href="{{url('devices?search=ios')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		@endif
		@if(isset($stats['android']))
		<div class="col-lg-4 col-xs-6">
			<div class="small-box bg-green">
				<div class="inner">
					<h3>{{ $stats['android'] }}</h3>
					<p>Android Device</p>
				</div>
				<div class="icon">
					<i class="icon ion-logo-android"></i>
				</div>
				<a href="{{url('devices?search=android')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		@endif
	</div>
	  <div class="row">
		<div class="col-lg-6">
			<form class="box" method="post" action="{!! url('/upload') !!}" enctype="multipart/form-data">
				@csrf
				<div class="box__input">
					<svg class="box__icon" xmlns="https://www.w3.org/2000/svg" width="50" height="43" viewBox="0 0 50 43"><path d="M48.4 26.5c-.9 0-1.7.7-1.7 1.7v11.6h-43.3v-11.6c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v13.2c0 .9.7 1.7 1.7 1.7h46.7c.9 0 1.7-.7 1.7-1.7v-13.2c0-1-.7-1.7-1.7-1.7zm-24.5 6.1c.3.3.8.5 1.2.5.4 0 .9-.2 1.2-.5l10-11.6c.7-.7.7-1.7 0-2.4s-1.7-.7-2.4 0l-7.1 8.3v-25.3c0-.9-.7-1.7-1.7-1.7s-1.7.7-1.7 1.7v25.3l-7.1-8.3c-.7-.7-1.7-.7-2.4 0s-.7 1.7 0 2.4l10 11.6z"/></svg>
					<div class="form-group">
						<input class="box__file" type="file" name="files[]" id="file" data-multiple-caption="{count} files selected" multiple />
						<label for="file"><strong>Choose a file</strong><span class="box__dragndrop"> or drag it here</span>.</label>
					</div>
					<button class="box__button" type="submit">Upload</button>
				</div>
				<div class="box__uploading">Uploading&hellip;</div>
				<div class="box__success">Done!</div>
				<div class="box__error">Error! <span></span>.</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12" id="uploaded-files">

		</div>
	</div>
	<div class="row hidden">
		<div class="col-lg-6">
			<div class="box box-info">
				<div class="box-header ui-sortable-handle" style="cursor: move;">
					<i class="fa fa-bell"></i>
					<h3 class="box-title">Quick Push Notification</h3>
					<!-- tools box -->
					<div class="pull-right box-tools">
						<!-- button with a dropdown -->
						{{--  <button type="button" class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>  --}}
					</div>
					<!-- /. tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					{!! Form::open(['url' => 'sendPN', 'method' => 'post']) !!}
						<div class="form-group">
							<label>
								<input type="radio" name="device" value="test" class="flat-green" checked> Sandbox
							</label>
							<label>
								<input type="radio" name="device" value="all" class="flat-green"> All
							</label>
							<label>
								<input type="radio" name="device" value="ios" class="flat-green"> iOS
							</label>
							<label>
								<input type="radio" name="device" value="android" class="flat-green"> Android
							</label>
						</div>
						<div class="form-group">
							<input name="title" class="form-control" placeholder="Title here ..." />
						</div>
						<div class="form-group">
							<textarea name="message" class="form-control" rows="3" placeholder="Message here ..."></textarea>
						</div>
						<div class="form-group">
							<label>
								<input type="radio" name="type" value="normal" class="flat-green" checked> Text
							</label>
							<label>
								<input type="radio" name="type" value="promotions" class="flat-green"> Promotions
							</label>
							{{--  <label>
								<input type="radio" name="type" value="page" class="flat-green"> Page
							</label>  --}}
							<label>
								<input type="radio" name="type" value="pdf" class="flat-green"> Magazine
							</label>
						</div>
						<div class="form-group" id="page_select">
							<label>Page</label>
							{!! Form::select('id', $pages, null, ['class' => 'form-control']) !!}
						</div>
						<button type="submit" class="btn btn-info pull-right">SEND</button>
					{!! Form::close() !!}
				</div>
				<!-- /.box-body -->
			</div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
	<script>
			'use strict';

			;( function( $, window, document, undefined )
			{
				// feature detection for drag&drop upload

				var isAdvancedUpload = function()
					{
						var div = document.createElement( 'div' );
						return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
					}();


				// applying the effect for every form

				$( '.box' ).each( function()
				{
					var $form		 = $( this ),
						$input		 = $form.find( 'input[type="file"]' ),
						$label		 = $form.find( 'label' ),
						$errorMsg	 = $form.find( '.box__error span' ),
						$restart	 = $form.find( '.box__restart' ),
						droppedFiles = false,
						showFiles	 = function( files )
						{
							$label.text( files.length > 1 ? ( $input.attr( 'data-multiple-caption' ) || '' ).replace( '{count}', files.length ) : files[ 0 ].name );
						};

					// letting the server side to know we are going to make an Ajax request
					$form.append( '<input type="hidden" name="ajax" value="1" />' );

					// automatically submit the form on file select
					$input.on( 'change', function( e )
					{
						showFiles( e.target.files );


					});


					// drag&drop files if the feature is available
					if( isAdvancedUpload )
					{
						$form
						.addClass( 'has-advanced-upload' ) // letting the CSS part to know drag&drop is supported by the browser
						.on( 'drag dragstart dragend dragover dragenter dragleave drop', function( e )
						{
							// preventing the unwanted behaviours
							e.preventDefault();
							e.stopPropagation();
						})
						.on( 'dragover dragenter', function() //
						{
							$form.addClass( 'is-dragover' );
						})
						.on( 'dragleave dragend drop', function()
						{
							$form.removeClass( 'is-dragover' );
						})
						.on( 'drop', function( e )
						{
							droppedFiles = e.originalEvent.dataTransfer.files; // the files that were dropped
							showFiles( droppedFiles );


						});
					}


					// if the form was submitted

					$form.on( 'submit', function( e )
					{
						// preventing the duplicate submissions if the current one is in progress
						if( $form.hasClass( 'is-uploading' ) ) return false;

						$form.addClass( 'is-uploading' ).removeClass( 'is-error' );

						if( isAdvancedUpload ) // ajax file upload for modern browsers
						{
							e.preventDefault();

							// gathering the form data
							var ajaxData = new FormData( $form.get( 0 ) );
							if( droppedFiles )
							{
								$.each( droppedFiles, function( i, file )
								{
									ajaxData.append( $input.attr( 'name' ), file );
								});
							}

							// ajax request
							$.ajax(
							{
								url: 			$form.attr( 'action' ),
								type:			$form.attr( 'method' ),
								data: 			ajaxData,
								dataType:		'json',
								cache:			false,
								contentType:	false,
								processData:	false,
								complete: function()
								{
									$form.removeClass( 'is-uploading' );
								},
								success: function( data )
								{
									$form.addClass( data.success == true ? 'is-success' : 'is-error' );
									data.data.forEach(url => {
										var row = $('<div class="image-row"></div>');
										var img = $('<img height=50>');
										var span = $('<span>' + url + '</span>');
										img.attr('src', url);
										img.appendTo(row);
										span.appendTo(row);
										row.appendTo('#uploaded-files');
									});
									if( !data.success ) $errorMsg.text( data.error );
								},
								error: function()
								{
									alert( 'Error. Please, contact the webmaster!' );
								}
							});
						}
						else // fallback Ajax solution upload for older browsers
						{
							var iframeName	= 'uploadiframe' + new Date().getTime(),
								$iframe		= $( '<iframe name="' + iframeName + '" style="display: none;"></iframe>' );

							$( 'body' ).append( $iframe );
							$form.attr( 'target', iframeName );

							$iframe.one( 'load', function()
							{
								var data = $.parseJSON( $iframe.contents().find( 'body' ).text() );
								$form.removeClass( 'is-uploading' ).addClass( data.success == true ? 'is-success' : 'is-error' ).removeAttr( 'target' );
								if( !data.success ) $errorMsg.text( data.error );
								$iframe.remove();
							});
						}
					});


					// restart the form if has a state of error/success

					$restart.on( 'click', function( e )
					{
						e.preventDefault();
						$form.removeClass( 'is-error is-success' );
						$input.trigger( 'click' );
					});

					// Firefox focus bug fix for file input
					$input
					.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
					.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
				});

			})( jQuery, window, document );


		$('input[type=radio][name=type]').change(function() {
			$('#page_select').hide();
			console.log(this.value);
			if(this.value == 'page')
			$('#page_select').show();
		});
		$('input[type=radio][name=type]').change();
	</script>
@endsection
