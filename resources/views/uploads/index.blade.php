@extends('layouts.app')

@section('content')
<section class="content-header">
	<h1 class="pull-left">Images</h1>
</section>
<div class="content">
	<div class="clearfix"></div>

	@include('flash::message')

	<div class="clearfix"></div>
	<div class="box box-primary">
		<div class="box-body">
			<table class="table table-responsive" id="products-table">
				<thead>
					<tr>
						<th>photo</th>
						<th>name</th>
					</tr>
				</thead>
				<tbody>
					@foreach($files as $file)
					<tr>
						<td>
							<img src="{!! url(Storage::url($file)) !!}" alt="{!! $file !!}" height="100">
						</td>
						<td>{!! $file !!}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="text-center">

	</div>
</div>
@endsection