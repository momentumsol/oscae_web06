@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            On Boarding
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($onBoarding, ['route' => ['onBoardings.update', $onBoarding->id], 'method' => 'patch']) !!}

                        @include('on_boardings.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection