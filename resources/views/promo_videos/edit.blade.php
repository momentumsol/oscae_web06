@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Promo Video
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($promoVideo, ['route' => ['promoVideos.update', $promoVideo->id], 'method' => 'patch']) !!}

                        @include('promo_videos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection