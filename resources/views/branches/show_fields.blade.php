<!-- Id Field -->
<div class="form-group">
	{!! Form::label('id', 'Id:') !!}
	<p>{!! $branch->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
	{!! Form::label('title', 'Title:') !!}
	<p>{!! $branch->gettitle() !!}</p>
</div>

<!-- Image Field -->
<div class="form-group row">
	<div class="col-md-2">
		{!! Form::label('image', 'Image:') !!}
		<img src="{!! $branch->image !!}" class="img-responsive" alt="">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-2">
		{!! Form::label('image', 'Store Id:') !!}
		<img src="{!! $branch->store_id !!}" class="img-responsive" alt="">
	</div>
</div>
<!-- Location Field -->
<div class="form-group">
	{!! Form::label('location', 'Location:') !!}
	<p>
		<a target="blank" href="{!! $branch->location !!}">Link</a>
	</p>
</div>
<!-- Latitudes Field -->
<div class="form-group">
	{!! Form::label('latitudes', 'Latitudes:') !!}
	<p>{!! $branch->latitudes !!}</p>
</div>
<!-- Longitudes Field -->
<div class="form-group">
	{!! Form::label('longitudes', 'Longitudes:') !!}
	<p>{!! $branch->longitudes !!}</p>
</div>



<!-- Created At Field -->
<div class="form-group">
	{!! Form::label('created_at', 'Created At:') !!}
	<p>{!! $branch->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
	{!! Form::label('updated_at', 'Updated At:') !!}
	<p>{!! $branch->updated_at !!}</p>
</div>