<table class="table table-responsive" id="branches-table">
	<thead>
		<tr>
			<th>Title</th>
			<th>Image</th>
			<th>Store id</th>
			<th>Location</th>
			<th colspan="3">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($branches as $branch)
		<tr>
			<td>{!! $branch->title->en !!}</td>
			<td><img src="{!! $branch->image !!}" height="50" alt=""></td>
			<td>{!! $branch->store_id !!}</td>
			<td><a target="blank" href="{!! $branch->location !!}">{!! $branch->location !!}</a></td>
			<td>
				{!! Form::open(['route' => ['branches.destroy', $branch->id], 'method' => 'delete']) !!}
				<div class='btn-group'>
					<a href="{!! route('branches.show', [$branch->id]) !!}" class='btn btn-default btn-xs'><i
							class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('branches.edit', [$branch->id]) !!}" class='btn btn-default btn-xs'><i
							class="glyphicon glyphicon-edit"></i></a>
					{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger
					btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
				</div>
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>