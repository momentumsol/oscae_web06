@extends('website.layouts.app')

<style>
.restaurant{
    margin: 30 auto;
}
.restaurant .image-group{
}
.restaurant h1{
    color: #EA0706;
    text-align: center;
    margin: 10px

}

/* .circl1 {
    border-radius: 50%;
} */
.circl1 img {
    width: 100%;
    border-radius: 100%;
    height: 30vh;
    
   
}

/* .circl2 {
    border-radius: 50%;
} */
@media only screen and (min-width: 600px) {
.circl2 img {
    width: 100%;
    border-radius: 100%;
    height: 30vh;
}
.circl1 img {
    width: 100%;
    border-radius: 100%;
    height: 30vh;
    
   
}

}
.nameOfRestaurant {
    font-size: 25px;
    color: #000;
}

.retaurant a:hover {
    color: #000;
}
@media only screen and (max-width: 767px) {
 .circl1 img {
    width: 100%;
    border-radius: 100%;
    height: 10vh;
    
   
}

/* .circl2 {
    border-radius: 50%;
} */
.circl2 img {
    width: 100%;
    border-radius: 100%;
    height: 10vh;
}
}
</style>
@section('content')

    <div class="breadcumb-wrapper breadcumb-layout1 bg-fluid pt-200 pb-200" data-bg-src="{{ asset('assets/img/breadcumb/hero-bg-4-3.jpg') }}">
        <div class="container">
            <div class="breadcumb-content text-center">
                <h1 class="breadcumb-title">Restaurants</h1>
                {{-- <ul class="breadcumb-menu-style1 mx-auto">
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Restaurants</li>
                </ul> --}}
                <img src="{{ asset('assets/img/icons/sec-icon-1.png') }}" alt="Section Shape">
            </div>
        </div>
    </div>
    <section class="vs-blog-wrapper space-md-bottom mt-5" id="blog">
        <div class="container">
            <div class="row vs-carousel wow fadeIn" data-wow-delay="0.3s" data-slide-show="3">
            @foreach($resaurants as $res)
            <?php   $image=str_replace("public",'storage',$res->image);
                    $menu=str_replace("public",'storage',$res->menu);
            ?>


                <div class="col-lg-4">
                    <div class="vs-blog blog-grid">
                        <div class="blog-img image-scale-hover">
                            <a href="http://oscar.momentum-sol.com/{!!$menu !!}" download="http://oscar.momentum-sol.com/{!!$menu !!}"><img src="http://oscar.momentum-sol.com/{!!$image !!}" alt="Blog Image" class="w-100"></a>
                        </div>
                        <div class="blog-content">
                            {{-- <h4 class="blog-title fw-semibold"><a href="blog-details.html">From its medieval origins</a></h4> --}}
                            {{-- <div class="blog-meta">
                                <a href="blog-details.html">January 04, 2021</a>
                                <a href="blog-details.html">5 Views</a>
                            </div> --}}
                        </div>
                    </div>
                </div>
               
                @endforeach
               
                    
                    

            </div>
            {{-- <h1>Opening Soon!</h1> --}}

           
        </div>
    </div>
@endsection
@push('scripts')
	<script type="text/javascript">


	</script>
@endpush
