@extends('website.layouts.app')
@push('top_script')
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
@endpush
<style>
/* 
	.contact .col-md-3{
        background: #C5171C;
        color: #fff;
		padding: 4%;
		border-top-left-radius: 0.5rem;
		border-bottom-left-radius: 0.5rem;
	}
	.contact-info{
		margin-top:10%;
	}
	.contact-info img{
		margin-bottom: 15%;
	}
	.contact-info h2{
		margin-bottom: 10%;
	}
	.contact .col-md-9{
		background: #fff;
		padding: 3%;
		border-top-right-radius: 0.5rem;
		border-bottom-right-radius: 0.5rem;
	}
	.contact-form label{
		font-weight:600;
	}
	.contact-form button{
		background: #25274d;
		color: #fff;
		font-weight: 600;
		/* width: 25%; */
	}
	.contact-form button:focus{
		box-shadow:none;
	} */
</style>

@section('content')

    @if(\Session::has('success'))
         <p class="alert {{ \Session::get('alert-class', 'alert-info') }}">{{ \Session::get('success') }}</p>
    @endif

    <?php $lang= app()->getLocale();?>
    @if ($lang == 'en')
        @include('website.contactus_en')
    @else    
        @include('website.contactus_ar')
    @endif
    
 
@endsection
@push('scripts')
<script>
   

//    $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });

//     $("#submit").click(function(e){

//         e.preventDefault();
//     var contact = $("input[name=contact]").val();
//     var fname = $("input[name=fname]").val();
//     var lname = $("input[name=lname]").val();
//     var email = $("#e_mail").val();
//     var comment = $("#comment").val();
       


//     $.ajax({
//         type:'POST',
//         url: '/{{app()->getLocale()}}/sendemail',

//         data:{
               
//                 fname:fname,
//                 lname:lname,
//                 email:email,
//                 comment:comment,
//                 contact:contact
               
//             },
//             beforeSend: function(){
//                 // Show image container
//                 $(".preloader").show();
//             },
//         success:function(data){
//             var checkout=$("input[name=checkout]").val();
//             $('.alert-danger').empty();
//             if(data.errors){
//                 $('.alert-danger').empty();
                
//             jQuery.each(data.errors, function(key, value){
//                     jQuery('.alert-danger').show();
//                     jQuery('.alert-danger').append('<p>'+value+'</p>');
//                 });
                
//             }
          
           
//         },
//         complete:function(data){
//             // Hide image container
//             $(".preloader").hide();
//         }
//         ,error: function (data) {

//             $('.alert').html(JSON.parse(data.responseText).message);
//             console.log(JSON.parse(data.responseText).message)
//             $('.alert').css('display','block');
//             setTimeout(function() { 
//                     $('.alert').fadeOut('fast'); 
//             }, 2000);
//         }	
//     });





// });

</script>
@endpush