<style>
    p,h1,h4{
        text-align: right !important
    }
    </style>    
    
    <!--==============================
        Breadcumb
    ============================== -->
    <div class="breadcumb-wrapper breadcumb-layout1 bg-fluid pt-200 pb-200" data-bg-src="{{asset('assets/img/shape/footer-4-bg.jpg')}}">
        <div class="container">
            <div class="breadcumb-content text-center">
                <h1 class="breadcumb-title" style="text-align: center !important">اتصل بنا</h1>
                
                {{-- <ul class="breadcumb-menu-style1 mx-auto">
                    <li><a href="index-5.html">Home</a></li>
                    <li class="active">Contact</li>
                </ul> --}}
            </div>
        </div>
    </div>

     <!--==============================
      Contact Form Area
    ==============================-->
    <section class="vs-contact-wrapper vs-contact-layout1 space-top space-md-bottom" style="direction: rtl">
        <div class="container">
            <div class="row text-center text-lg-start" style="direction: rtl">
                <div class="col-lg-6 ">
                    <div class="section-title mb-25">
                        <h2 class="sec-title1" style="text-align: right; font-size:50px">معلومات عن العنوان</h2>
                        {{-- <h3 class="sec-title2">Information</h3> --}}
                        <p class=" fs-md mt-4 pt-1" style="text-align: right">معلومات عن العنوان </p>
                        <p>نود أن نرحب بكم في عالم أوسكار منذ عام ١٩٨٢ ونحن نعمل بجد لتحقيق والحفاظ على سمعتنا كوجهة تسوق مفضلة لجميع أولئك الذين يسعون إلى مجموعة متنوعة من المنتجات عالية الجودة وبأفضل الأسعار. ندعوكم للانضمام إلينا في فروعنا الموجودة في:</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <h4>التجمع الخامس
                            </h4>
                            <p class="mb-0 fw-semibold">اوسكار جراند ستور
                            </p>
                            <p class="mb-0 fw-semibold">٤٢٥شارع ٩٠
                            </p>
                            <p class="mb-0 fw-semibold">التجمع الخامس ، القاهرة </p>
                            <p class="mb-0 fw-semibold">ساعات العمل: ٨ صباحا - ١٢ منتصف الليل </p>
                            <p class="mb-0 fw-semibold"><a href="https://www.google.com/maps/place/Oscar+Grand+Stores,+425+N+90th+Street+-+Service+Ln,+First+New+Cairo,+Cairo+Governorate/data=!4m2!3m1!1s0x145822f524fabd0d:0x73efaf4b5775e78b?utm_source=mstt_1&entry=gps">احصل على الاتجاهات
                            </a>  </p>

                        </div>
                        <div class="col-sm-6 mb-20">
                            <h4>مصر الجديدة
                            </h4>
                            <p class="mb-0 fw-semibold">اوسكار ستور
                            </p>
                            <p class="mb-0 fw-semibold">١٠٥ شارع عمر بن الخطاب
                            </p>
                            <p class="mb-0 fw-semibold">مصر الجديدة ، القاهرة
                            </p>
                            <p class="mb-0 fw-semibold">ساعات العمل: ٨ صباحا - ١٢ منتصف الليل
                            </p>
                            <p class="mb-0 fw-semibold"><a href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x14583e027fb38807:0xbff1f05b786d2e2e?utm_source=mstt_1&entry=gps&lucs=swa">احصل على الاتجاهات
                            </a>  </p>

                        </div>
                        <div class="col-sm-6">
                            <h4>المعادى </h4>
                            <p class="mb-0 fw-semibold">اوسكار ستور
                            </p>
                            <p class="mb-0 fw-semibold">ريحانة بلازا شارع زهراء المعادي.
                            </p>
                            <p class="mb-0 fw-semibold">المعادى، القاهرة
                            </p>
                            <p class="mb-0 fw-semibold">ساعات العمل: ٨ صباحا - ١٢ منتصف الليل
                            </p>
                            <p class="mb-0 fw-semibold"><a href="https://www.google.com/maps/place/Oscar+Grand+Stores/@29.9607097,31.2952341,18z/data=!4m5!3m4!1s0x145839a86f55a773:0xf6088000b2c7ac65!8m2!3d29.9611396!4d31.2969722">احصل على الاتجاهات
                            </a>  </p>

                        </div>
                        <div class="col-sm-6 mb-20">
                            <h4>الزمالك </h4>
                            <p class="mb-0 fw-semibold">توقيع أوسكار
                            </p>
                            <p class="mb-0 fw-semibold">صن مول ، شارع ٢٦ يوليو
                            </p>
                            <p class="mb-0 fw-semibold">الزمالك ، القاهرة
                            </p>
                            <p class="mb-0 fw-semibold">ساعات العمل: ٨ صباحا - ١٢ منتصف الليل
                            </p>
                            <p class="mb-0 fw-semibold"><a href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x14584172eaef8a8d:0xd8097bb652715e1d?utm_source=mstt_1&entry=gps&lucs=swa">احصل على الاتجاهات
                            </a>  </p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mt-30 mt-lg-0">
                    <div class="section-title mb-25">
                        <h2 class="sec-title1" style="text-align: right; font-size:50px">ابقى على تواصل
                        </h2>
                        <p class="fs-md mt-4 pt-1" style="text-align: right; ">ابلغنا
                    </p>
                    <p>كيف يمكن أن نساعد؟ هناك عدة طرق يمكنك من خلالها الاتصال بأوسكار. إذا كنت ترغب في التحدث إلى أحد ممثلي خدمة العملاء مباشرة ، فيرجى الاتصال بنا على ١٦٩٩١ من داخل مصر.</p>
                        <p class="fs-md mt-4 pt-1">كيف يمكن أن نساعد؟ هناك عدة طرق يمكنك من خلالها الاتصال بأوسكار. إذا كنت ترغب في التحدث إلى أحد ممثلي خدمة العملاء مباشرة ، فيرجى الاتصال بنا على ١٦٩٩١ من داخل مصر.</p>
                        <p>يمكنك أيضًا الاتصال عن طريق ملء النموذج أدناه. يستجيب فريق الخدمة المخصص لدينا بشكل عام لجميع الاستفسارات في غضون ١-٣ أيام عمل.	 </p>
                    </div>
                    <form action="{{ url(app()->getLocale().'/sendemail') }}" method="POST" >
                        @csrf
                        <input type="text" name="contact" value="1" hidden>

                        <div class="row g-4">
                            <div class="col-6 form-group mb-0">
                                <input type="text" name="fname" class="form-control" placeholder="الاسم الاول">
                            </div>
                            <div class="col-6 form-group mb-0">
                                <input type="text" name="lname" class="form-control" placeholder="الاسم الاخير ">
                            </div>
                            <div class="col-lg-12 form-group mb-0">
                                <input id="e_mail" type="email" name="email" class="form-control" placeholder="بريدك الالكترونى">
                            </div>
                            {{-- <div class="col-lg-6 form-group mb-0">
                                <input type="text" name="subject" class="form-control" placeholder="Your Subject">
                            </div> --}}
                            <div class="col-12 form-group mb-0">
                                <textarea id="comment" class="form-control" name="comment" placeholder="رسالتك"></textarea>
                            </div>
                            <div class="col-12 form-group mb-0">
                                <button id="submit" type="submit" class="vs-btn ">ارسل رسالة</button>
                            </div>
                        </div>
                    </form>
                    <p class="form-messages mt-20 mb-0"></p>
                </div>
              
            </div>
        </div>
    </section>