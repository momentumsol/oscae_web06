@extends('website.layouts.app')
<style>
	/* .promotion-content h3, h2 {
		color: #333;
	}

	.empty-space {
		height: 52.5px;
		width: 100%;
	} */
	.slick-track > [class*=col]{
		height: 460px;
	}
	.product-title{
    white-space: nowrap !important;
    overflow: hidden !important;
    text-overflow: ellipsis !important;
    display: inline-block !important;
    width: 100%;
}

@media (max-width: 400px) { 

.promotion-content .col-lg-4{
	height: 180px !important;
}
.product-content{
	min-height: 110px
}
}

</style>
@section('content')

<div class="promotion-content">
	<div class="container">

		@if( count($offers) > 0)
			<section class="vs-blog-wrapper pt-5 space-md-bottom" id="blog">
				<div class="container">
					<div class="section-title text-center wow fadeIn" data-wow-delay="0.3s">
						<h1 class="sec-title1">{{ __('lang.promotions_members') }}</h1>
						<p><a href="{{ url(app()->getLocale().'/all_promotions') }}" style="text-decoration: underline">{{ __('lang.show_all') }}</a></p>
						{{-- <h2 class="sec-title2">from oscar</h2> --}}
						{{-- <img style="margin: 0 auto" src="{{ asset('assets/img/icons/sec-icon-1.png') }}" alt="Section Shape"> --}}
					</div>
					<div class="row vs-carousel wow fadeIn" data-wow-delay="0.3s" data-slide-show="3">
						@foreach ($offers as $meal)
							<div class="col-lg-4">
								<div class="vs-blog blog-grid vs-product-box1 thumb_swap">
									<div class="blog-img image-scale-hover">
										<a href="{{ url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id) }}"><img src="{{$meal->images['0']->src}}" alt="Blog Image" class="w-100"></a>
									</div>
									<div class="blog-content text-center product-content">
										<div class="actions-btn">
											<a href="{{ $meal->PriceUnit == "kg" ? url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id) : ''}} " title="{{ $meal->id }}" class="{{  $meal->in_stock != 1 ? 'd-none' : '' }} {{ $meal->PriceUnit != "kg" ? 'cart' : ''}} "><i class="fal fa-cart-plus"></i></a>
											@if(Auth::guard('customerForWeb')->user())
											<a href="#" title="{{ $meal->id }}" class="Unlike" style="display: {{ in_array($meal->id,$pros) ? 'block' : 'none' }}"><i style="color:#C53330;" class="fal fa-heart"></i></a>
											<a href="#" title="{{ $meal->id }}" class="Wishlist" style="display: {{ in_array($meal->id,$pros) ? 'none' : 'block' }}"><i class="fal fa-heart"></i></a>
											@endif
										</div>
										<div >
											<h4 class=" product-title h5 mb-0"><a href="{{ url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id) }}">{{ app()->getLocale() =='en' ? $meal->name : $meal->name_ar }}</a></h4>
											<span class="price font-theme"><strong><span style="text-decoration: {{   $meal->on_sale == '1' ?  'line-through' : ' ' }} ">{{ $meal->regular_price}}</span>   {{$meal->on_sale == '1' ? ' '.$meal->discountprice :'' }} </strong>  {{ __('lang.EGP') }}</span>
										
										</div>
										
									</div>
								</div> 
							</div>	
						@endforeach
					</div>
				</div>
			</section>
		@else
			<div class="row justify-content-center mt-5">
				<div class="col-lg-6 col-md-6 col-sm-6 col-6 text-center">
					<div class="empty-space"></div>
					<h2>No promotions available yet</h2>
					<h3>Please check back later</h3>
					<div class="empty-space"></div>
				</div>
			</div>
		@endif
		
	</div>
</div>

@endsection