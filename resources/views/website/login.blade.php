@extends('website.layouts.app')

@push('style')
<style>
    .login{
    border-top: 1px solid #C5C5C5;
    border-bottom: 1px solid #C5C5C5;
    background-color: #FAFAFA;
    overflow: hidden;
    padding: 10px;
    color: #535353
}
.form{

    border: 1px solid #C53330;
    margin:  0 auto;
    padding: 20px 20px 30px
}

/* @media only screen and (min-width: 800px) {
    .form{
    width: 40%;
    }
}
@media only screen and (min-width: 1200px) {
    .form{
    width: 20%;
    }
} */
/* .form-control{
    width: 80%;
    margin:  0 auto;
    margin-top: 20px;
    margin-bottom: 20px;

}
button{
    border: 1px solid   #414742 !important;
    color:   #414742 !important;
    margin:  0 auto;
}
.forget_pss{
    color: #aaaeb3;
    text-decoration: underline
}
.create_new_account{
    text-align: center;
    margin-bottom: 40px;
    margin-top: 25px
}
.create_new_account a{
    color:   #414742;
    text-decoration: underline

}
.text-center{
    font-size: 12px;
    padding: 20px
}
.text-center a{
    color: #aaaeb3;
    text-decoration: underline
} */



</style>

@endpush

@section('content')


<div>
    <div class="row  justify-content-center my-5" style="margin: 0">
        <div class="col-lg-4 col-md-6 col-sm-10 " style="padding: 0">
            @include('website.partials.errors')
    
            <div class="form">
                <form action="{{route('web.login', app()->getLocale())}}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input class="form-control" name="email" type="text" placeholder="{{ __('lang.email')}}" style="width: 90%;margin-bottom: 8px">
                        <input class="form-control" name="password" type="password" placeholder="{{ __('lang.password')}}" style="width: 90%">
                        <a  href="{{route('web.forgot_password', app()->getLocale())}}" class="forget_pss float-right mt-2" style="font-size: 14px">{{ __('lang.forget_password')}}</a>
                    </div>
                    <div class="row" style="justify-content: center;margin:0" >
                        <button type="submit" class="btn qut-btn vs-btn shadow-none " style="width: 85%"> {{ __('lang.login')}}</button>
                    </div>
                </form>
                <hr>
        
                <div class="create_new_account">
                    <span>{{ __('lang.oscarnew') }}</span> <a style="color: blue;text-decoration:underline" href="{{ route('register',app()->getLocale()) }}" > {{ __('lang.newaccount') }}</a>
                </div>
                <p class="mt-2" style="line-height: 1.5;">{{ __('lang.login-policy')}} <a style="color: blue;text-decoration:underline" href="{{ url (app()->getLocale().'/return_policy') }}">{{ __('lang.Refund & Returns Policy')}}</a> {{ __('lang.and') }} <a style="color: blue;text-decoration:underline" href="{{ url (app()->getLocale().'/privacy_policy') }}">{{ __('lang.Privacy Policy')}}</a>.</p>
            </div>
        </div>
    </div>
</div>


@endsection


@push('script')
<script src="{{ asset('/front/js/dish.js') }}"></script>


<script>
    $( document ).ready(function() {
        $('.carousel-item').first().addClass( "active" );
});

</script>
@endpush
