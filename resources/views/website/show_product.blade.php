@extends('website.layouts.app')

<style>
	
</style>
@section('content')
<?php 
$lang= app()->getLocale();
$category=\App\Models\Category::find($catId);
?>


    <!--==============================
    Breadcumb
============================== -->
<div class="breadcumb-wrapper breadcumb-layout1 bg-fluid pt-200 pb-200" style="  background:linear-gradient(to bottom, rgba(245, 246, 252, 0.52), rgba(219, 212, 217, 0.73)),url('{{ $category->image['src'] }}')">
	<div class="container">
		<div class="breadcumb-content text-center">
			<h1 class="breadcumb-title">{{$category->name->$lang }}</h1>
			{{-- <ul class="breadcumb-menu-style1 mx-auto">
				<li><a href="{{ url(app()->getLocale().'/homeWeb/all_products/'. $category->id ) }}">{{$category->name->$lang }}</a></li>
				<li class="active">{{$lang == 'en' ? $product->name : $product->name_ar }}</li>
			</ul> --}}
		</div>
	</div>
	
</div>

 <!--==============================
    Shop Details testww
    ==============================-->
    <section class="vs-shop-wrapper shop-details space-top space-md-bottom">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-6 col-xl-5 mb-30 mb-md-0">
                    <div class="product-big-img" >
					@php
									
										$product_image=str_replace('http://34.105.27.34/oscar/public', 'https://cms.oscarstoresapp.com', $product->images[0]->src);
										
									@endphp
					<img class="" src="{{ $product_image  }}"   style="background: white;"  onerror="this.src='https://oscarstoresapp.com/images/not_found_image.jpeg'">

                    </div>

                </div>
                <div class="col-md-6 col-lg-4" style=" display: flex;align-items: center;">
                    <div class="product-content">
                        <h3 class="product-title mb-1">{{$lang == 'en' ? $product->name : $product->name_ar }}</h3>
						@if($product->PriceUnit=='kg')
						<span class="price font-theme"><strong><span style="text-decoration: {{   $product->on_sale == '1' ?  'line-through' : ' ' }} ">{{ $product->regular_price}} {{ __('lang.EGP') }}/ {{ __('lang.kg') }}</span>   {{$product->on_sale == '1' ? ' '.$product->discountprice.__('lang.EGP') :'' }}/ {{ __('lang.kg') }} </strong> </span>


						@else
						<span class="price font-theme"><strong><span style="text-decoration: {{   $product->on_sale == '1' ?  'line-through' : ' ' }} ">{{ $product->regular_price}} {{ __('lang.EGP') }}</span>   {{$product->on_sale == '1' ? ' '.$product->discountprice.__('lang.EGP') :'' }} </strong> </span>

						@endif
                        <div class="mt-2">

                        </div>
                        <p class="fs-xs my-4">{{ $product->description }}</p>
                        <div class="mt-2 link-inherit fs-xs" style="height: 40px">
                            <p>
								<strong class="text-title me-3 font-theme fl-left">{{ __('lang.Availability') }} :</strong>
								<span  class="fl-right" style="line-height: 1">{!! $product->in_stock == 1 ? '<i class="far fa-check-square me-2" style="cursor: auto !important;"></i>'.__('lang.In Stock') : '<i class="icon-check-empty" style="cursor: auto !important;"></i>'.__('lang.Out Stock')  !!}</span></p>
                        </div>
                        <div class="actions mb-4 pb-2" >
                            <div class="quantity style2" >
                                <input type="number" class="qty-input" id="qty'"value="1" min="1" max="99">
                                <button class="quantity-minus qut-btn remove"><i class="far fa-chevron-down"></i></button>
                                <button class="quantity-plus qut-btn add"><i class="far fa-chevron-up"></i></button>

                            </div>
                            @if($product->PriceUnit=='kg' && $product->in_stock == 1)
							<div>
								<select name="weight" id="weights">
                                    <option value="250" selected>250{{ __('lang.g') }}</option>
                                    <option value="500">500{{ __('lang.g') }}</option>
                                    <option value="750">750{{ __('lang.g') }}</option>
                                    <option value="1000">1.00{{ __('lang.kg') }}</option>
                                    <option value="1250">1.25{{ __('lang.kg') }}</option>
                                    <option value="1500">1.50{{ __('lang.kg') }}</option>
                                    <option value="1750">1.75{{ __('lang.kg') }}</option>
                                    <option value="2000">2.00{{ __('lang.kg') }}</option>
                                    <option value="2250">2.25{{ __('lang.kg') }}</option>
                                    <option value="2500">2.50{{ __('lang.kg') }}</option>
								</select>
							</div>
							@endif
                           
                        </div>
					
                        @if(Auth::guard('customerForWeb')->user())
						@if( $product->in_stock==1)
						<a style="cursor: pointer;" unit={{ $product->PriceUnit }}  title="{{ $product->id }} "  class="cart mb-2 vs-btn shadow-none {{  $product->in_stock != 1 ? 'd-none' : '' }}">{{ __('lang.Add To Cart') }}</a>
@else
<a style="cursor: pointer;"  class="mb-2 vs-btn shadow-none ">{{ __('lang.out_of_stock') }}</a>

@endif
                        <a  unit={{ $product->PriceUnit }}  title="{{ $product->id }} " style="cursor: pointer;display: {{ in_array($product->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'none' : 'block' }}"  class="Wishlist  vs-btn shadow-none {{  $product->in_stock != 1 ? 'd-none' : '' }}">{{ __('lang.Add To WishList') }}</a>
                        <a  unit={{ $product->PriceUnit }}  title="{{ $product->id }} " style="cursor: pointer;display: {{ in_array($product->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'block' : 'none' }}"  class=" Unlike  vs-btn shadow-none {{  $product->in_stock != 1 ? 'd-none' : '' }}">{{ __('lang.Remove From WishList') }}</a>
                        @endif
                        <div class="product_meta mt-2">
                          
                            <span class="posted_in">{{ __('lang.Category') }}: <a style="cursor: pointer;"  href="{{ url(app()->getLocale().'/homeWeb/all_products/'. $category->id ) }}" rel="tag">{{$category->name->$lang }}</a> </span>
                        </div>
                    </div>
                </div>

            </div>
            {{-- <ul class="nav product-tab-style1 mb-30 justify-content-center mb-4" id="productTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link " id="description-tab" data-bs-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="false">description</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="reviews-tab" data-bs-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="true">reviews</a>
                </li>
            </ul>
            <div class="tab-content mb-30" id="productTabContent">
                <div class="tab-pane fade" id="description" role="tabpanel" aria-labelledby="description-tab">
                    <p class="fs-md">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, </p>
                    <div class="row mt-30">
                        <div class="col-md-6 mb-30">
                            <img src="assets/img/shop/shop-desc-1.jpg" class="w-100" alt="Shop Image">
                        </div>
                        <div class="col-md-6 mb-30">
                            <img src="assets/img/shop/shop-desc-2.jpg" class="w-100" alt="Shop Image">
                        </div>
                    </div>
                    <div class="product-inner-list mb-4">
                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                            <li>Fusce vitae orci id leo pulvinar euismod et placerat diam.</li>
                            <li>Etiam pharetra mauris at fringilla laoreet.</li>
                            <li>Vivamus eu tellus pretium, fringilla justo nec, volutpat sapien.</li>
                        </ul>
                    </div>
                    <div class="product-inner-list ">
                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                            <li>Fusce vitae orci id leo pulvinar euismod et placerat diam.</li>
                            <li>Etiam pharetra mauris at fringilla laoreet.</li>
                            <li>Vivamus eu tellus pretium, fringilla justo nec, volutpat sapien.</li>
                        </ul>
                    </div>
                </div>
                <div class="tab-pane fade show active" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                    <div class="vs-comment-area list-style-none vs-comments-layout1 pt-3 ">
                        <ul class="comment-list">
                            <li class="review vs-comment">
                                <div class="vs-post-comment">
                                    <div class="author-img">
                                        <img src="assets/img/blog/comment-author-1.jpg" alt="Comment Author">
                                    </div>
                                    <div class="comment-content">


                                        <h4 class="name h5">Mark Jack</h4>
                                        <span class="commented-on">22 April, 2020</span>
                                        <p class="text">Progressively procrastinate mission-critical action items before team building ROI.
                                            Interactively provide access to cross functional quality vectors for client-centric catalysts for change.
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="review vs-comment">
                                <div class="vs-post-comment">
                                    <div class="author-img">
                                        <img src="assets/img/blog/comment-author-2.jpg" alt="Comment Author">
                                    </div>
                                    <div class="comment-content">

                                        <h4 class="name h5">John Deo</h4>
                                        <span class="commented-on">26 April, 2020</span>
                                        <p class="text">Competently provide access to fully researched methods of empowerment without sticky models. Credibly morph front-end niche markets whereas 2.0 users. Enthusiastically seize team.</p>
                                    </div>
                                </div>
                            </li>
                            <li class="review vs-comment">
                                <div class="vs-post-comment">
                                    <div class="author-img">
                                        <img src="assets/img/blog/comment-author-1.jpg" alt="Comment Author">
                                    </div>
                                    <div class="comment-content">

                                        <h4 class="name h5">Tara sing</h4>
                                        <span class="commented-on">26 April, 2020</span>
                                        <p class="text">Competently provide access to fully researched methods of empowerment without sticky models. Credibly morph front-end niche markets whereas 2.0 users. Enthusiastically seize team.</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- Comment Form -->

                </div>
            </div> --}}
        </div>
    </section>


@endsection

@push('scripts')
	<script type="text/javascript">
	let id=''
	function addToCartAjax(id,weight,qty)
{
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/{{app()->getLocale()}}/homeWeb/addToCart',
	data:{ product_id:id,weight:weight,quantity:parseInt(qty) },

	success:function(data)
	{
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);

	}	
});
}
function addToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/{{app()->getLocale()}}/homeWeb/add/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}
function removeToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/{{app()->getLocale()}}/homeWeb/remove/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}	
$(".cart").click(function(e){
	

	e.preventDefault();
	var unit=$(this).attr('unit');

	 id = $(this).attr("title");
	id=id.replace(/\s+/g, '')
	$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		}

	});
	$.ajax({

	type:'POST',
	
	url:url+'/{{app()->getLocale()}}/homeWeb/add-to-cart',

	data:{ id:id },
	success:function(data){
		let weight=250;
			if(unit == 'kg'){
				 weight=document.getElementById('weights').value
				data['weight']=weight;
				data['quantity']=parseInt(document.getElementsByClassName('qty-input')[0].value);
			}else{
				data['weight']=1;
				data['quantity']=parseInt(document.getElementsByClassName('qty-input')[0].value);
			}
			var res=jQuery.inArray(id,localStorage);
			var key='product_'+data.id;
			var storage=localStorage['cart'];
        let existed_qty=0
	
		
			if(storage.includes(key)){
		let storage_keys=JSON.parse(storage)
		for(var i in storage_keys){
		if (typeof(storage_keys[i][key]) !== "undefined") {
			 existed_qty=storage_keys[i][key]['quantity']
			
	
		}
		}
			}
		
	// console.log(existed_qty, parseInt(document.getElementsByClassName('qty-input')[0].value),'l',id+'_qty')
			if(storage.includes(key)  ){
				if(existed_qty==parseInt(document.getElementsByClassName('qty-input')[0].value))
				{

				
				$('#alert-cart').show();
				var in_Cart=	@json( __('lang.already in cart'));
				var lang=@json(app()->getLocale());
				var name=data.name;
				
				if(lang =='ar'){
					name=data.name_ar;
				}
				$('#alert-cart').html(name+' '+in_Cart);

				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
			}
			else
			{
				
		var qty=parseInt(document.getElementsByClassName('qty-input')[0].value);;
		var storage=JSON.parse(localStorage.getItem("cart"));

		for (let i = 0; i< storage.length; i++) {
			var pro=storage[i];
			var key=Object.keys(pro);
			if (typeof(storage[i][key]) !== "undefined") {
				storage.splice(i, 1); 
				var cart =[];
				console.log(storage)
				localStorage.setItem('cart', JSON.stringify(cart));
				localStorage.setItem('cart', JSON.stringify(storage));
				localStorage.removeItem(key);
				addToCart(data)

			}

			
		}
	

				var add_Cart=	@json( __('lang.updated to cart succssfully'));

$('#alert-cart').show();
$('#alert-cart').html(add_Cart);
setTimeout(function() { 
		$('#alert-cart').fadeOut('fast'); 
}, 2000);
;
			}
			}else{
				var add_Cart=	@json( __('lang.added to cart succssfully'));
				qty=parseInt(document.getElementsByClassName('qty-input')[0].value);
				addToCart(data);
				$('#alert-cart').show();
				$('#alert-cart').html(add_Cart);
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
				$('.total-count').html('');
				var total=JSON.parse(localStorage.getItem("cart"));
				$('.total-count').html(JSON.parse(total.length));
				
			}
		
		addToCartAjax(id,weight,qty)	
		
		}
		
	});
	
});

// let url='';


		$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		}

		});
		// if(! $(this).hasClass('quick')){

	$(".Wishlist").click(function(e){
			e.preventDefault();
			
			
	
			var id = $(this).attr("title");
			var wishlistLink =this;
			$.ajax({
			   type:'POST',
			   url:url+'/{{app()->getLocale()}}/homeWeb/wishList',
				
			   data:{ id:id },
	
			   success:function(data){

				
					$('#alert-wishlist').show();
					$('#alert-wishlist').html(@json(__('lang.added to wishlist succssfully')));
					addToWhishList(id)
					setTimeout(function() { 
							$('#alert-wishlist').fadeOut('fast'); 
						}, 2000);
	
				}	
			});
			// if(! $(this).hasClass('quick')){

				$(this).parent().find('.Wishlist').css('display','none');
				$(this).parent().find('.Unlike').css('display','block');

				

			// }else{
				// $(this).removeClass('Wishlist');
				// $(this).addClass('Wishlist');
			// }

			

			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)+Number(1));

		});

		$(".Unlike").click(function(e){
			e.preventDefault();



			
			$('#alert-wishlist').show();
			$('#alert-wishlist').html(@json(__('lang.removed from wishlist succssfully')));
			
			setTimeout(function() { 
                    $('#alert-wishlist').fadeOut('fast'); 
                }, 2000);
	
			var id = $(this).attr("title");
	
			$.ajax({
			   type:'POST',
			   url:url+'/{{app()->getLocale()}}/homeWeb/unLike',
	
			   data:{ id:id },

			   success:function(data){
				removeToWhishList(id)
			
			}
	
			});
			$(this).parent().find('.Wishlist').css('display','block');
			$(this).parent().find('.Unlike').css('display','none');
			// $(this).parent().find('.Unlike i').css('color','#858585');

			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)-Number(1));

			// $(this).removeClass('Unlike');
			// $(this).addClass('Wishlist');
		});

		var products= [];
		var local_storage=[];
		
		if( !JSON.parse(localStorage.getItem("cart"))){
			var cart =[];
			localStorage.setItem('cart', JSON.stringify(cart));
		}
	



// $('.add').each(function () {
//     $(this).on('click', function (e) {
//       e.preventDefault();
//       var $qty = $(this).siblings(".qty-input");
	  
//       var currentVal = parseInt($qty.val());
	
//       if (!isNaN(currentVal)) {
//         $qty.val(currentVal + 1);
//       }
	  
//     })
//   });
  
//   $('.remove').each(function () {
//     $(this).on('click', function (e) {
//       e.preventDefault();
//       var $qty = $(this).siblings(".qty-input");
//       var currentVal = parseInt($qty.val());
//       if (!isNaN(currentVal) && currentVal > 1) {
//         $qty.val(currentVal - 1);
//       }
//     });
//   })
  

function addToCart(product) {
	if (localStorage) {
		var cart;
		if (!localStorage['cart']) cart = [];
		else cart = JSON.parse(localStorage['cart']);            
		if (!(cart instanceof Array)) cart = [];
		var key ='product_'+product.id;
		var obj = {};
		obj[key] = product;
		cart.push(obj);
		localStorage.setItem('cart', JSON.stringify(cart));
	} 
}

	</script>
@endpush
