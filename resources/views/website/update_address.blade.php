@extends('website.layouts.app')

@push('style')

   <link rel="stylesheet" href="{{ asset('/css/customer-profile.css') }}">
    <style>
        .form{

            border: 1px solid   #414742;
            /* border-top: #09158C; */
            margin:  0 auto;
            padding: 0px 0px 30px;
            }

            @media only screen and (min-width: 800px) {
            .form{
            width: 40%;
            }
            }
            .modal-header{
              background: #09158C;
              color: #fff;
              border-radius: inherit;
            }
           

    </style>
@endpush

@section('content')
    <div class="form my-5">
      {{-- <div class="modal-header">
        <h5 class="modal-title">{{__('lang.edit_address')}}</h5>
        
      </div> --}}
        <div class="mt-5">

          
            @include('website.partials.errors')

            <form class="w-100 text-center" action="{{ url(app()->getLocale().'/address/'.$address->id) }}" method="post">
              @method('put')
              @csrf
              <div class="row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                      <label >{{__('lang.phone')}} </label>
                  </div>
                  
                  <div class="col-8" style="padding-left: 30px"><input   name="phone" type="text" class="form-control" value="{{ $address->phone }}"></div> 
              </div>
                  
              <div class=" row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                      <label >{{__('lang.name')}} </label>
                  </div>
                  <div class="col-8" style="padding-left: 30px"><input   name="name" type="text" class="form-control" value="{{ $address->name  ?? old('name')}}"></div> 
              </div>
              <div class=" row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                      <label >{{__('lang.address')}} </label>
                  </div>
                  <div class="col-8"style="padding-left: 30px"><input    name="address" type="text" class="form-control" value="{{ $address->address  ?? old('address')}}"></div> 
              </div>
              <div class="row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                    <label >{{ __('lang.building_number')}}  </label>
                  </div> 
                    <div class="col-8" style="padding-left: 30px"><input   name="building_number" type="text" class="form-control" value="{{ $address->building_number  ?? old('address')}}"></div> 
                </div>

                <div class="row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                    <label >{{ __('lang.floor_number')}}  </label>
                  </div> 
                    <div class="col-8" style="padding-left: 30px"><input   name="floor_number" type="text" class="form-control" value="{{ $address->floor_number  ?? old('address')}}"></div> 
                </div>

               

                <div class="row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                    <label >{{ __('lang.apartment_number')}}  </label>
                  </div> 
                    <div class="col-8" style="padding-left: 30px"><input   name="apartment_number" type="text" class="form-control" value="{{ $address->building_number  ?? old('address')}}"></div> 
                </div>
              <div class=" row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                      <label >{{__('lang.area')}} </label>
                  </div>
                  <div class="col-8"style="padding-left: 30px"><input   name="area" type="text" class="form-control" value="{{ $address->area ?? old('area')}} "></div> 
              </div>
              <div class=" row justify-content-center my-3">
                  <div class="col-1" style="margin:7px">
                      <label >{{__('lang.city')}} </label>
                  </div>
                  <div class="col-8 "style="padding-left: 30px"><input   name="city" type="text" class="form-control" value="{{ $address->city ?? old('city')}} "></div> 
              </div> 
              <input type="text" value=""  class="form-control "    name="coordinates" id="coordinates"   hidden>
              <div id="map"  style="width:100%;height:200px "></div>
              <button type="submit" class="btn mt-5 w-50" > {{__('lang.update')}}</button>
          
              <!-- Grd row -->
          </form>
        </div>
    </div>
   
@endsection



@push('scripts')
<script>

let map, infoWindow;
let coordinates = '{{ $coordinates }}';
coordinates=(JSON.parse(coordinates.replace(/&quot;/g,'"')));
coordinate=String(coordinates);
let res = coordinate.split(",");
console.log(res[0],res[1],'res')
$( document ).ready(function() {
  let map, infoWindow;
navigator.geolocation.getCurrentPosition(
   function (position) {
      initMap(parseFloat(res[0]),parseFloat(res[1]))
   },
   function errorCallback(error) {
    initMap(26.6194394,21.8566593)
      console.log(error)
   }
);

function initMap(lat, lng) {



var myLatLng = {
  lat,
   lng
};
coordinates=lat+','+lng

$('#coordinates').attr('value',coordinates);
var map = new google.maps.Map(document.getElementById('map'), {
   zoom: 15,
     center: {
            lat: lat,
            lng: lng
        }
});
const locationButton = document.createElement("button");
  locationButton.textContent = "Get Current Location";
  locationButton.type = "button";
  locationButton.classList.add("custom-map-control-button");
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
var marker = new google.maps.Marker({
   position: myLatLng,
   map: map,
   draggable:true,

anchorPoint: new google.maps.Point(0, -29)
});

locationButton.addEventListener("click", () => {
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
   function (position) {
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };

         
          map.setCenter(pos);
          marker.setPosition(pos);

          coordinates= position.coords.latitude+','+position.coords.longitude

           $('#coordinates').attr('value',coordinates);
        
        },
        () => {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }
  });

google.maps.event.addListener(marker, 'dragend', function() 
{
  $('#coordinates').attr('value','');
    geocodePosition(marker.getPosition());
    coordinates=marker.getPosition().lat()+','+marker.getPosition().lng()
    console.log(coordinates);
    $('#coordinates').attr('value',coordinates);

  //   document.getElementById('lat').value=marker.getPosition().lat();
  //  document.getElementById('lng').value=marker.getPosition().lng();

})
}



function geocodePosition(pos) 
{
   geocoder = new google.maps.Geocoder();
   geocoder.geocode
    ({
        latLng: pos
    }, 
        function(results, status) 
        {
            if (status == google.maps.GeocoderStatus.OK) 
            {
                $("#mapSearchInput").val(results[0].formatted_address);
                $("#mapErrorMsg").hide(100);
            } 
            else 
            {
                $("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
            }
        }
    );
}
     
});
 


</script>

@endpush
