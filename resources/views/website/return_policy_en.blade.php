
<div class="container  " >
        <div class="contact my-5">
         
            <div class="row">
                <!-- <h2>OSCAR Grand Stores</h2> -->
                <div class="col-md-12">
                    <h3 style="color: black  !important;">RETURN POLICY</h3>

                    <p>Last updated July 1st, 2021</p>

                    <p>&nbsp;</p>

                    <p>Thank you for your purchase. We hope you are happy with your purchase. However, if you are not completely satisfied with your purchase for any reason, you may return it to us for a refund, store credit, or an exchange. Please see our return policy below:&nbsp;</p>

                    <p>&nbsp;</p>

                    <ul>
                        <li>Original receipt must be presented for all Returns &amp; Refunds&nbsp;</li>
                        <li>Refunds must be within 14 days of purchase date&nbsp;</li>
                        <li>Returns of Fresh &amp; Frozen items must be within 24 hours of purchase given that the goods are in the same condition and of the same weight at purchase. Oscar is not responsible for the damage of items caused by weather or transport conditions once the goods are in the customer&rsquo;s possession.&nbsp;</li>
                        <li>The recoverable amount is refunded in the same method of original payment&nbsp;</li>
                        <li>Refunds are for the full amount paid as specified on the receipt&nbsp;</li>
                        <li>Exchanges of goods can be made with the same value of the original&nbsp; receipt&nbsp;</li>
                    </ul>

                    <p>&nbsp;</p>

                    <p>If you have any questions regarding our Returns &amp; Refunds policy please contact us on: <a style="color: #19408C;text-decoration:underline" href="mailto:info@oscargrandstores.com">info@oscargrandstores.com</a> or call us on 16991. You can also chat with our agents through WhatsApp by messaging this number: 01050403443</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>
                    </p>
                </div>
                <!-- <div class="col-md-3">
                    <img src="{{ asset('images/about1.gif') }}" alt="">
                </div> -->
            </div>
        </div>
    </div>

 
 
