<div class="container  " >
    <div class="contact my-5">
     
        <div class="row">
            <!-- <h2>OSCAR Grand Stores</h2> -->
            <div class="col-md-12">
                <h3 style="color: black  !important;">PRIVACY NOTICE</h3>
                <p>&nbsp;</p>
                {{-- <p>&nbsp;</p> --}}

                <p>Last updated July 1st, 2021</p>

                <p>&nbsp;</p>

                <p>Thank you for choosing to be part of our community at Oscar Grand Stores (&quot;Company&quot;, &quot;we&quot;, &quot;us&quot;, &quot;our&quot;). We are committed to protecting your personal information and your right to privacy. If you have any questions or concerns about this privacy notice, or our practices with regards to your personal information, please contact us at <a href="mailto:info@oscargrandstores.com"> info@oscargrandstores.com</a>.</p>

                <p>&nbsp;</p>

                <p>When you visit our website <a style="color:blue;text-decoration:underline" href="http://www.oscargrandstores.com/">http://www.oscargrandstores.com</a> (the &quot;Website&quot;), use our mobile application, as the case may be (the &quot;App&quot;) and more generally, use any of our services (the &quot;Services&quot;, which include the Website and App), we appreciate that you are trusting us with your personal information. We take your privacy very seriously. In this privacy notice, we seek to explain to you in the clearest way possible what information we collect, how we use it and what rights you have in relation to it. We hope you take some time to read through it carefully, as it is important. If there are any terms in this privacy notice that you do not agree with, please discontinue use of our Services immediately.</p>

                <p>&nbsp;</p>

                <p>This privacy notice applies to all information collected through our Services (which, as described above, includes our Website and App), as well as, any related services, sales, marketing or events.</p>

                <p>&nbsp;</p>

                <p>Please read this privacy notice carefully as it will help you understand what we do with the information that we collect.</p>

                <p>&nbsp;</p>

                <p>TABLE OF CONTENTS</p>

                <p>&nbsp;</p>

                <p><a style="text-decoration: underline" href="#link_1" >1. WHAT INFORMATION DO WE COLLECT?</a></p>

                <p><a style="text-decoration: underline" href="#link_2">2. HOW DO WE USE YOUR INFORMATION?</a></p>

                <p><a style="text-decoration: underline" href="#link_3">3. WILL YOUR INFORMATION BE SHARED WITH ANYONE?</a></p>

                <p><a style="text-decoration: underline" href="#link_4">4. WHO WILL YOUR INFORMATION BE SHARED WITH?</a></p>

                <p><a style="text-decoration: underline" href="#link_5">5. DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</a></p>

                <p><a style="text-decoration: underline" href="#link_6">6. HOW DO WE HANDLE YOUR SOCIAL LOGINS?</a></p>

                <p><a style="text-decoration: underline" href="#link_7">7. HOW LONG DO WE KEEP YOUR INFORMATION?</a></p>

                <p><a style="text-decoration: underline" href="#link_8">8. HOW DO WE KEEP YOUR INFORMATION SAFE?</a></p>

                <p><a style="text-decoration: underline" href="#link_9">9. DO WE MAKE UPDATES TO THIS NOTICE?</a></p>

                <p><a style="text-decoration: underline" href="#link_10">10. HOW CAN YOU CONTACT US ABOUT THIS NOTICE?</a></p>

                <p>&nbsp;</p>

                <p id="link_1">1. WHAT INFORMATION DO WE COLLECT?</p>

                <p>Personal information you disclose to us</p>

                <p>We collect personal information that you voluntarily provide to us when you register on the Services, express an interest in obtaining information about us or our products and Services, when you participate in activities on the Services or otherwise when you contact us.</p>

                <p>&nbsp;</p>

                <p>The personal information that we collect depends on the context of your interactions with us and the Services, the choices you make and the products and features you use. The personal information we collect may include the following:</p>

                <p>&nbsp;</p>

                <p>Personal Information Provided by You. We collect names; phone numbers; email addresses; mailing addresses; billing addresses; and other similar information.</p>

                <p>&nbsp;</p>

                <p>Payment Data. We may collect data necessary to process your payment if you make purchases, such as your payment instrument number (such as a credit card number), and the security code associated with your payment instrument.</p>

                <p>&nbsp;</p>

                <p>Social Media Login Data. We may provide you with the option to register with us using your existing social media account details, like your Facebook, Twitter or other social media account. If you choose to register in this way, we will collect the information described in the section called &quot;<a style="color:blue ;text-decoration:underline" href="sociallogins">HOW DO WE HANDLE YOUR SOCIAL LOGINS?</a>&quot; below.</p>

                <p>&nbsp;</p>

                <p>All personal information that you provide to us must be true, complete and accurate, and you must notify us of any changes to such personal information.</p>

                <p>&nbsp;</p>

                <p>Information automatically collected</p>

                <p>We automatically collect certain information when you visit, use or navigate the Services. This information does not reveal your specific identity (like your name or contact information) but may include device and usage information, such as your IP address, browser and device characteristics, operating system, language preferences, referring URLs, device name, country, location, information about how and when you use our Services and other technical information. This information is primarily needed to maintain the security and operation of our Services, and for our internal analytics and reporting purposes.</p>

                <p>&nbsp;</p>

                <p>Like many businesses, we also collect information through cookies and similar technologies.</p>

                <p>&nbsp;</p>

                <p>The information we collect includes:</p>

                <ul>
                    <li><em>Log and Usage Data.</em> Log and usage data is service-related, diagnostic, usage and performance information our servers automatically collect when you access or use our Services and which we record in log files. Depending on how you interact with us, this log data may include your IP address, device information, browser type and settings and information about your activity in the Services (such as the date/time stamps associated with your usage, pages and files viewed, searches and other actions you take such as which features you use), device event information (such as system activity, error reports (sometimes called &#39;crash dumps&#39;) and hardware settings).</li>
                </ul>

                <ul>
                    <li><em>Location Data.</em> We collect location data such as information about your device&#39;s location, which can be either precise or imprecise. How much information we collect depends on the type and settings of the device you use to access the Services. For example, we may use GPS and other technologies to collect geolocation data that tells us your current location (based on your IP address). You can opt out of allowing us to collect this information either by refusing access to the information or by disabling your Location setting on your device. Note however, if you choose to opt out, you may not be able to use certain aspects of the Services.</li>
                </ul>

                <p>&nbsp;</p>

                <p>Information collected through our App</p>

                <p>If you use our App, we also collect the following information:</p>

                <ul>
                    <li><em>Geolocation Information.</em> We may request access or permission to and track location-based information from your mobile device, either continuously or while you are using our App, to provide certain location-based services. If you wish to change our access or permissions, you may do so in your device&#39;s settings.</li>
                </ul>

                <ul>
                    <li><em>Push Notifications. </em>We may request to send you push notifications regarding your account or certain features of the App. If you wish to opt-out from receiving these types of communications, you may turn them off in your device&#39;s settings.</li>
                </ul>

                <p>This information is primarily needed to maintain the security and operation of our App, for troubleshooting and for our internal analytics and reporting purposes.</p>

                <p>&nbsp;</p>

                <p id="link_2">2. HOW DO WE USE YOUR INFORMATION?</p>

                <p>We use personal information collected via our Services for a variety of business purposes described below. We process your personal information for these purposes in reliance on our legitimate business interests, in order to enter into or perform a contract with you, with your consent, and/or for compliance with our legal obligations. We indicate the specific processing grounds we rely on next to each purpose listed below.</p>

                <p>&nbsp;</p>

                <p>We use the information we collect or receive:</p>

                <ul>
                    <li>To facilitate account creation and logon process. If you choose to link your account with us to a third-party account (such as your Google or Facebook account), we use the information you allowed us to collect from those third parties to facilitate account creation and logon process for the performance of the contract. See the section below headed &quot;<a href="sociallogins">HOW DO WE HANDLE YOUR SOCIAL LOGINS?</a>&quot; for further information.</li>
                </ul>

                <ul>
                    <li>To post testimonials. We post testimonials on our Services that may contain personal information. Prior to posting a testimonial, we will obtain your consent to use your name and the content of the testimonial. If you wish to update, or delete your testimonial, please contact us at <a href="mailto:info@oscargrandstores.com"> info@oscargrandstores.com</a> and be sure to include your name, testimonial location, and contact information.</li>
                </ul>

                <ul>
                    <li>Request feedback. We may use your information to request feedback and to contact you about your use of our Services.</li>
                </ul>

                <ul>
                    <li>To enable user-to-user communications. We may use your information in order to enable user-to-user communications with each user&#39;s consent.</li>
                </ul>

                <ul>
                    <li>To manage user accounts. We may use your information for the purposes of managing our account and keeping it in working order.</li>
                </ul>

                <ul>
                    <li>Fulfill and manage your orders. We may use your information to fulfill and manage your orders, payments, returns, and exchanges made through the Services.</li>
                </ul>

                <ul>
                    <li>Administer prize draws and competitions. We may use your information to administer prize draws and competitions when you elect to participate in our competitions.</li>
                </ul>

                <ul>
                    <li>To deliver and facilitate delivery of services to the user. We may use your information to provide you with the requested service.</li>
                </ul>

                <ul>
                    <li>To respond to user inquiries/offer support to users. We may use your information to respond to your inquiries and solve any potential issues you might have with the use of our Services.</li>
                </ul>

                <ul>
                    <li>To send you marketing and promotional communications. We and/or our third-party marketing partners may use the personal information you send to us for our marketing purposes, if this is in accordance with your marketing preferences. For example, when expressing an interest in obtaining information about us or our Services, subscribing to marketing or otherwise contacting us, we will collect personal information from you. You can opt-out of our marketing emails at any time (see the &quot;<a style="text-decoration:underline" href="privacyrights">WHAT ARE YOUR PRIVACY RIGHTS?</a>&quot; below).</li>
                </ul>

                <ul>
                    <li>Deliver targeted advertising to you. We may use your information to develop and display personalized content and advertising (and work with third parties who do so) tailored to your interests and/or location and to measure its effectiveness.</li>
                </ul>

                <p>&nbsp;</p>

                <p id="link_3">3. WILL YOUR INFORMATION BE SHARED WITH ANYONE?</p>

                <p>We may process or share your data that we hold based on the following legal basis:</p>

                <ul>
                    <li>Consent: We may process your data if you have given us specific consent to use your personal information for a specific purpose.</li>
                </ul>

                <ul>
                    <li>Legitimate Interests: We may process your data when it is reasonably necessary to achieve our legitimate business interests.</li>
                </ul>

                <ul>
                    <li>Performance of a Contract: Where we have entered into a contract with you, we may process your personal information to fulfill the terms of our contract.</li>
                </ul>

                <ul>
                    <li>Legal Obligations: We may disclose your information where we are legally required to do so in order to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal process, such as in response to a court order or a subpoena (including in response to public authorities to meet national security or law enforcement requirements).</li>
                </ul>

                <ul>
                    <li>Vital Interests: We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.</li>
                </ul>

                <p>More specifically, we may need to process your data or share your personal information in the following situations:</p>

                <ul>
                    <li>Business Transfers. We may share or transfer your information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business to another company.</li>
                </ul>

                <ul>
                    <li>Vendors, Consultants and Other Third-Party Service Providers. We may share your data with third-party vendors, service providers, contractors or agents who perform services for us or on our behalf and require access to such information to do that work. Examples include: payment processing, data analysis, email delivery, hosting services, customer service and marketing efforts. We may allow selected third parties to use tracking technology on the Services, which will enable them to collect data on our behalf about how you interact with our Services over time. This information may be used to, among other things, analyze and track data, determine the popularity of certain content, pages or features, and better understand online activity. Unless described in this notice, we do not share, sell, rent or trade any of your information with third parties for their promotional purposes.</li>
                </ul>

                <ul>
                    <li>Third-Party Advertisers. We may use third-party advertising companies to serve ads when you visit or use the Services. These companies may use information about your visits to our Website(s) and other websites that are contained in web cookies and other tracking technologies in order to provide advertisements about goods and services of interest to you.</li>
                </ul>

                <ul>
                    <li>Offer Wall. Our App may display a third-party hosted &quot;offer wall.&quot; Such an offer wall allows third-party advertisers to offer virtual currency, gifts, or other items to users in return for the acceptance and completion of an advertisement offer. Such an offer wall may appear in our App and be displayed to you based on certain data, such as your geographic area or demographic information. When you click on an offer wall, you will be brought to an external website belonging to other persons and will leave our App. A unique identifier, such as your user ID, will be shared with the offer wall provider in order to prevent fraud and properly credit your account with the relevant reward. Please note that we do not control third-party websites and have no responsibility in relation to the content of such websites. The inclusion of a link towards a third-party website does not imply an endorsement by us of such website. Accordingly, we do not make any warranty regarding such third-party websites and we will not be liable for any loss or damage caused by the use of such websites. In addition, when you access any third-party website, please understand that your rights while accessing and using those websites will be governed by the privacy notice and terms of service relating to the use of those websites.</li>
                </ul>

                <p>&nbsp;</p>

                <p id="link_4">4. WHO WILL YOUR INFORMATION BE SHARED WITH? &nbsp;&nbsp;&nbsp;&nbsp;</p>

                <p>We only share and disclose your information with the following categories of third parties. If we have processed your data based on your consent and you wish to revoke your consent, please contact us using the contact details provided in the section below titled &quot;<a href="contact">HOW CAN YOU CONTACT US ABOUT THIS NOTICE?</a>&quot;.</p>

                <ul>
                    <li>Data Analytics Services</li>
                </ul>

                <ul>
                    <li>Communication &amp; Collaboration Tools</li>
                </ul>

                <ul>
                    <li>Payment Processors</li>
                </ul>

                <ul>
                    <li>Retargeting Platforms</li>
                </ul>

                <ul>
                    <li>User Account Registration &amp; Authentication Services</li>
                </ul>

                <p>&nbsp;</p>

                <p id="link_5">5. DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</p>

                <p>We may use cookies and similar tracking technologies (like web beacons and pixels) to access or store information. Specific information about how we use such technologies and how you can refuse certain cookies is set out in our Cookie Notice.</p>

                <p>&nbsp;</p>

                <p id="link_6">6. HOW DO WE HANDLE YOUR SOCIAL LOGINS? &nbsp;&nbsp;&nbsp;&nbsp;</p>

                <p>Our Services offers you the ability to register and login using your third-party social media account details (like your Facebook or Twitter logins). Where you choose to do this, we will receive certain profile information about you from your social media provider. The profile information we receive may vary depending on the social media provider concerned, but will often include your name, email address, friends list, profile picture as well as other information you choose to make public on such social media platform.</p>

                <p>&nbsp;</p>

                <p>We will use the information we receive only for the purposes that are described in this privacy notice or that are otherwise made clear to you on the relevant Services. Please note that we do not control, and are not responsible for, other uses of your personal information by your third-party social media provider. We recommend that you review their privacy notice to understand how they collect, use and share your personal information, and how you can set your privacy preferences on their sites and apps.</p>

                <p>&nbsp;</p>

                <p id="link_7">7. HOW LONG DO WE KEEP YOUR INFORMATION?</p>

                <p>We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy notice, unless a longer retention period is required or permitted by law (such as tax, accounting or other legal requirements). No purpose in this notice will require us keeping your personal information for longer than the period of time in which users have an account with us.</p>

                <p>&nbsp;</p>

                <p>When we have no ongoing legitimate business need to process your personal information, we will either delete or anonymize such information, or, if this is not possible (for example, because your personal information has been stored in backup archives), then we will securely store your personal information and isolate it from any further processing until deletion is possible.</p>

                <p>&nbsp;</p>

                <p id="link_8">8. HOW DO WE KEEP YOUR INFORMATION SAFE?</p>

                <p>We have implemented appropriate technical and organizational security measures designed to protect the security of any personal information we process. However, despite our safeguards and efforts to secure your information, no electronic transmission over the Internet or information storage technology can be guaranteed to be 100% secure, so we cannot promise or guarantee that hackers, cybercriminals, or other unauthorized third parties will not be able to defeat our security, and improperly collect, access, steal, or modify your information. Although we will do our best to protect your personal information, transmission of personal information to and from our Services is at your own risk. You should only access the Services within a secure environment.</p>

                <p>&nbsp;</p>

                <p>Account Information</p>

                <p>If you would at any time like to review or change the information in your account or terminate your account, you can:</p>

                <ul>
                    <li>Log in to your account settings and update your user account.</li>
                </ul>

                <p>Upon your request to terminate your account, we will deactivate or delete your account and information from our active databases. However, we may retain some information in our files to prevent fraud, troubleshoot problems, assist with any investigations, enforce our Terms of Use and/or comply with applicable legal requirements.</p>

                <p>&nbsp;</p>

                <p>Cookies and similar technologies: Most Web browsers are set to accept cookies by default. If you prefer, you can usually choose to set your browser to remove cookies and to reject cookies. If you choose to remove cookies or reject cookies, this could affect certain features or services of our Services. To opt-out of interest-based advertising by advertisers on our Services visit <a href="http://www.aboutads.info/choices/">http://www.aboutads.info/choices/</a>.</p>

                <p>&nbsp;</p>

                <p>Opting out of email marketing: You can unsubscribe from our marketing email list at any time by clicking on the unsubscribe link in the emails that we send or by contacting us using the details provided below. You will then be removed from the marketing email list &mdash; however, we may still communicate with you, for example to send you service-related emails that are necessary for the administration and use of your account, to respond to service requests, or for other non-marketing purposes. To otherwise opt-out, you may:</p>

                <ul>
                    <li>Access your account settings and update your preferences.</li>
                </ul>

                <p>&nbsp;</p>

                <p id="link_9">9. DO WE MAKE UPDATES TO THIS NOTICE? &nbsp;&nbsp;&nbsp;&nbsp;</p>

                <p>We may update this privacy notice from time to time. The updated version will be indicated by an updated &quot;Revised&quot; date and the updated version will be effective as soon as it is accessible. If we make material changes to this privacy notice, we may notify you either by prominently posting a notice of such changes or by directly sending you a notification. We encourage you to review this privacy notice frequently to be informed of how we are protecting your information.</p>

                <p>&nbsp;</p>

                <p id="link_10">10. HOW CAN YOU CONTACT US ABOUT THIS NOTICE? &nbsp;&nbsp;&nbsp;&nbsp;</p>

                <p>If you have questions or comments about this notice, you may email us at <a href="mailto:info@oscargrandstores.com"> info@oscargrandstores.com</a></p>

                <p>&nbsp;</p>'
                </p>
            </div>
            <!-- <div class="col-md-3">
                <img src="{{ asset('images/about1.gif') }}" alt="">
            </div> -->
        </div>
    </div>
</div>