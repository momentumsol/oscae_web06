@foreach ($main_categories as $i=>$category)
									@php
										$i+=1;
										$icon=str_replace('http://34.105.27.34/oscar/public', 'https://cms.oscarstoresapp.com', $category->icon_image);
										
									@endphp
									
									<li class=" menu-item-has-children">
										<a rel="preload" href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $category->id ) }}"> <img loading="lazy" style="margin: 0 8px 0 0 !important; display:inline !important; width:20px;" src="{{ asset($icon )}}"  alt="" > {{  $category->name->$lang }} </a>
										
										@foreach ($category->children as $sub1_child)

											@if(isset($sub1_child->children) )
												<ul class="mega-menu-wrap sub-menu">
													<li class="menu-item-has-children">
														<div class="box-nav-popup" data-bg-src="assets/img/bg/nav-bg-1.png">
															<div class="row" id="removeRow" style="margin: 0">
																<ul style="    margin: 0 0 15px;">
																	<li style="text-decoration: underline;padding:0;border:0" ><a rel="preload" href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $category->id ) }}">{{ __('lang.Show-all') }}</a> </li>

																</ul>

																@foreach ($category->children as $sub1_child)
																	@if(count($sub1_child->children) > 1)
																		<ul class="col-md-12 col-lg-4  ">
																			
																			<div class="widget widget_nav_menu">
																				<h3 class="widget-title" style="align-items: center;   "><a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $sub1_child->id ) }}" style=" display: inline;">  {{$sub1_child->name->$lang }} </a> <i class="fa fa-plus  show_sub" style="padding: 0 2px 0;font-size: 10px !important;" ></i> </h3>
																				<div class="vs-box-nav" style="display: none">
																					<ul>
																						<li ><a rel="preload" href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $sub1_child->id ) }}">{{ __('lang.Show-all') }}</a> </li>
																						@foreach ($sub1_child->children as $sub2_child)
																							
																							@if(count($sub2_child->children) > 1)
																							<li >
																								<a rel="preload" href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $sub2_child->id ) }}">{{$sub2_child->name->$lang  }} </a></li>
																							</li>
																							 @else
																							<li ><a  rel="preload" href="{{ url( app()->getLocale().'/homeWeb/all_products/'.$sub2_child->id) }}">{{$sub2_child->name->$lang }}</a></li>
																							@endif 
																						@endforeach
																					</ul>
																				</div>
																			</div>
																		</ul>
																	@else
																		<ul class="col-md-12 col-lg-4 ">
																			<div class="widget widget_nav_menu">
																				<h3 class="widget-title" style="align-items: center;   "><a href="{{ url( app()->getLocale().'/homeWeb/all_products/'. $sub1_child->id ) }}" style=" display: inline;">  {{$sub1_child->name->$lang }} </a>
																				</h3>
																				<div class="vs-box-nav" style="display: none">
																					
																				</div>
																				
																			</div>
																		</ul>
																	@endif 
																@endforeach	
																
															</div>
															
														</div>
													</li>
												</ul>
											
											@endif
										@endforeach
									</li>
									@endforeach