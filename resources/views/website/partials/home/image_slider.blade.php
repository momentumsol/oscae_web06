<section class="vs-hero-wrapper hero-layout4  ">
		<div class="vs-hero-carousel" data-height="650" data-container="1800" data-slidertype="fullwidth">

			@foreach ($sliders as $slider)
			@php
				$link=explode('/',$slider->link);
				$link=array_pop($link);
				$image=str_replace("http://34.105.27.34/oscar/public","https://cms.oscarstoresapp.com",$slider->image);
			@endphp
			
					<div class="ls-slide" data-ls="duration: 8000; transition2d: 106, 5, 111;">
						<a class="ls-link" href="{{ $slider->link}}"></a>

						<img class="ls-bg" onclick="sliderLink(this)" src="{{ $image ?? '' }}" alt="..." >
						

					</div>
			@endforeach

		</div>
	</section>