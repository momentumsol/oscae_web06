<script>
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
})
if($(window).width() <= 767){
	$('.mega-hover').css('height','100px')
}else if($(window).width() <= 500){
	$('.mega-hover').css('height','70px')

}
else{
	$('.mega-hover').css('height','300px')

}
</script>

	<script type="text/javascript">
function addToCartAjax(id,weight,qty)
{
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/{{app()->getLocale()}}/homeWeb/addToCart',
	data:{ product_id:id,weight:weight,quantity:parseInt(qty)  },

	success:function(data){
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);
	}	
});
}
function addToWhishList(id)
{


	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/{{app()->getLocale()}}/homeWeb/add/wishlist',
	data:{ product_id:id ,_token: '{{csrf_token()}}' },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}
function removeToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/{{app()->getLocale()}}/homeWeb/remove/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}	
	

		$.ajaxSetup({

		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		}

		});

		$(".Wishlist").click(function(e){
		
			e.preventDefault();
			
			$('#alert-wishlist').show();
			$('#alert-wishlist').html(@json(__('lang.added to wishlist succssfully')));
			
			setTimeout(function() { 
                    $('#alert-wishlist').fadeOut('fast'); 
                }, 2000);
			var id = $(this).attr("title");
			
			$.ajax({
			   type:'POST',
			   url: url+'/{{app()->getLocale()}}/homeWeb/wishList',
	
			   data:{ id:id },
	
			   success:function(data){
				 addToWhishList(id)
				
	
				}	
			});
			
		
				$(this).parent().find('.Wishlist').css('display','none');
			
			$(this).parent().find('.Unlike').css('display','block');

			

			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)+Number(1));
	
		});

		$(".Unlike").click(function(e){
			e.preventDefault();
			e.preventDefault();
			$('#alert-wishlist').show();
			$('#alert-wishlist').html(@json(__('lang.removed from wishlist succssfully')));
			
			setTimeout(function() { 
                    $('#alert-wishlist').fadeOut('fast'); 
                }, 2000);
	
	
			var id = $(this).attr("title");
	
			$.ajax({
			   type:'POST',
			   url:url+'/{app()->getLocale()}}/homeWeb/unLike',
	
			   data:{ id:id },

			   success:function(data){
				//    console.log(data);
				$('li#'+data).remove();
				removeToWhishList(id)
			}
	
			});
			
			$(this).parent().find('.Wishlist').css('display','block');
			$(this).parent().find('.Unlike').css('display','none');
			
			var count=$('.wishlist-total-count').html();
			$('.wishlist-total-count').html(Number(count)-Number(1));
		});
		if( !JSON.parse(localStorage.getItem("cart"))){
			var cart =[];
			localStorage.setItem('cart', JSON.stringify(cart));
		}
		var products= [];
		var local_storage=[];



		$(".cart").click(function(e){
	

			e.preventDefault();
			var unit=$(this).attr('unit');

			var id = $(this).attr("title");
			$.ajaxSetup({

				headers: {

					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

				}

			});
			$.ajax({

			type:'POST',
			
			url:'/{{app()->getLocale()}}/homeWeb/add-to-cart',

			data:{ id:id },
			success:function(data){
				let weight=250;
					if(unit == 'kg'){
						 weight=document.getElementById('weights').value
						data['weight']=weight;
								data['quantity']=1;
					}else{
						data['weight']=1;
						data['quantity']=1;
					}
					var res=jQuery.inArray(id,localStorage);
					var key='product_'+data.id;
					var storage=localStorage['cart'];
					let qty =1;
					if(storage.includes(key)){

						let storage_keys=JSON.parse(storage)
						for(var i in storage_keys){
							let item=storage_keys[i]
							let key_check=Object.keys(storage_keys[i])[0]
		if (Object.keys(storage_keys[i])[0]== 'product_'+id) {
			 qty=storage_keys[i][key_check]['quantity'];
			qty=qty+1
			item[Object.keys(item)[0]]['quantity']=qty
		
			localStorage.setItem('cart', JSON.stringify(storage_keys));

			// storage.splice(i, 1); 
	
		}
		}
						
						var in_Cart=	@json( __('lang.updated to cart succssfully'));
				var lang=@json(app()->getLocale());
				var name=data.name;
				
				if(lang =='ar'){
					name=data.name_ar;
				}
				$('#alert-cart').html(name+' '+in_Cart);
						setTimeout(function() { 
								$('#alert-cart').fadeOut('fast'); 
						}, 2000);
					}else{
						addToCart(data);
						$('#alert-cart').show();
						$('#alert-cart').html('added to cart succssfully');
						setTimeout(function() { 
								$('#alert-cart').fadeOut('fast'); 
						}, 2000);
						$('.total-count').html('');
						var total=JSON.parse(localStorage.getItem("cart"));
						$('.total-count').html(JSON.parse(total.length));
					}
					console.log(id,weight,qty,'l')
					addToCartAjax(id,weight,qty)

				}
			

			});
			
		});



		function addToCart(product) {
			if (localStorage) {
				var cart;
				if (!localStorage['cart']) cart = [];
				else cart = JSON.parse(localStorage['cart']);            
				if (!(cart instanceof Array)) cart = [];
				var key ='product_'+product.id;
				var obj = {};
				obj[key] = product;
				cart.push(obj);
				localStorage.setItem('cart', JSON.stringify(cart));
			} 
		}


		function quickcart(elem,event){

		event.preventDefault();
		var id=elem.title;
		var qty= $(elem).parent().parent().find('.quantity').find('input').val();
		var storage=JSON.parse(localStorage.getItem("cart"));

		for (let i = 0; i< storage.length; i++) {
			var pro=storage[i];
			var key=Object.keys(pro);
			if(key== 'product_'+id){
				storage.splice(i, 1); 
				var cart =[];
				localStorage.setItem('cart', JSON.stringify(cart));
				localStorage.setItem('cart', JSON.stringify(storage));
				localStorage.removeItem(key);
			}
		}

		
		$.ajax({
		type:'POST',

		url:url+'/{{app()->getLocale()}}/homeWeb/add-to-cart',

		data:{ id:id },
		success:function(data){
			
			data['quantity']=qty;
			var res=jQuery.inArray(id,localStorage);
			var key='product_'+data.id;
			var storage=localStorage['cart'];
			if(storage.includes(key)){
				// console.log(JSON.parse(storage));

				
				$('#alert-cart').show();
				$('#alert-cart').html(data.name+' already in cart ');
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
			}else{
				
				addToCart(data);
				$('#alert-cart').show();
				$('#alert-cart').html('added to cart succssfully');
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
				$('.total-count').html('');
				var total=JSON.parse(localStorage.getItem("cart"));
				$('.total-count').html(total.length);

			}
		}
	});
	var modal=$(elem).closest(".modal").modal("hide");
	}





				
</script>