<section class="vs-blog-wrapper " id="blog">
			<div class="container">
				<div class="section-title text-center" style="margin-bottom: 0">
					<h1 class="sec-title1">{{ __('lang.Ready Meals') }}</h1>
					<h2 class="sec-title2">{{ __('lang.from Oscar') }}</h2>
					<img loading="lazy"style="margin: 0 auto" src="{{ asset('assets/img/icons/sec-icon-1.png') }}" alt="Section Shape">
				</div>
				<div class="row vs-carousel wow fadeIn" data-slide-show="3">
					@foreach ($readyMeals as $meal)

					@php
				
				$meal_image=str_replace("http://34.105.27.34/oscar/public","https://cms.oscarstoresapp.com",$meal->images['0']->src);
			@endphp
						<div class="col-lg-4">
							<div class="vs-blog blog-grid vs-product-box1 thumb_swap">
								<div class="blog-img image-scale-hover">
									<a href="{{ url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id) }}"><img src="{{meal_image}}" alt="Blog Image" class="w-100"></a>
								</div>
								<div class="blog-content text-center product-content">
									<div class="actions-btn">
										@if(Auth::guard('customerForWeb')->user())
									
                                       @if($meal->in_stock == 1)
									   <a  title="{{ $meal->id }}" class="cart {{ $meal->PriceUnit != "kg" ? 'cart' : ''}} "><i class="fal fa-cart-plus"></i></a>
									   @endif
										<a href="#" title="{{ $meal->id }}" class="Unlike" style="display: {{ in_array($meal->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'block' : 'none' }}"><i style="color:#C53330;" class="fal fa-heart"></i></a>
										<a href="#" title="{{ $meal->id }}" class="Wishlist" style="display: {{ in_array($meal->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'none' : 'block' }}"><i class="fal fa-heart"></i></a>
									
										@endif
									</div>
									@if($meal->in_stock != 1)
										<h4 class="product-title h5 mb-0">{{ __('lang.out_of_stock') }}</h4>
										@endif
									<div class="cat-content" >
										<h4 class="cat-title"><a href="{{ url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id) }}">{{ app()->getLocale() =='en' ? $meal->name : $meal->name_ar }}</a></h4>
									</div>
								
								</div>
							</div>
						</div>	
					@endforeach
				</div>
			</div>
		</section>