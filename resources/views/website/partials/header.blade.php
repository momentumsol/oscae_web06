<header class="header-wrapper">
        <div class="header6-inner" style="padding:15px 0">
            <div class="container position-relative">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto">
                        <div class="header-logo">
							
                            <a href="{{ url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id) }}"><img width="170" src="{{ asset($logo) }}" alt="Oscar"></a>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="header-address d-none d-md-block">
                            <p class="font-theme fs-xs fw-bold tmb-1">
								{{-- <a href="email:info@oscarstores.com" class="text-reset"><i class="far fa-envelope-open-text"></i> info@oscarstores.com, </a> --}}
								<a href="tel:16991" class="text-reset" ><i class="far fa-phone"></i> <span style="font-size: 30px">16991</span></a></p>
                        </div>
                    </div>
					<div class="col-auto">
                        <button type="button" class="vs-menu-toggle d-inline-block d-lg-none" id="ToggleBTN"><i class="far fa-bars"></i></button>
                        <div class="head-top-links text-body2 d-none d-lg-block">
							<span class="icon-btn bg3" style="margin:0"><a   style="display: block !important" href="{{Auth::guard('customerForWeb')->user() ? url(app()->getLocale().'/profile') : route('login',$lang)  }}"><i class="fal fa-user" style="color: var(--body-color2)" ></i></a></span>
                            <ul>
								@if(!Auth::guard('customerForWeb')->check() )
								
									<li><a href="{{ route('login',$lang) }}" >{{ __('lang.login')}}</a></li>
									<li><a href="{{ route('register',$lang) }}">{{ __('lang.register')}}</a></li>
								@endif
                            </ul>
							<div class="modal fade" id="modalLRFormmobile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="    z-index: 999999;" aria-hidden="true">
								<div class="modal-dialog cascading-modal" role="document">
									<!--Content-->
									<div class="modal-content">
					
										<!--Modal cascading tabs-->
										<div class="modal-c-tabs">
					
											<!-- Nav tabs -->
											<ul class="nav nav-tabs md-tabs tabs-2  darken-3" role="tablist">
												<li class="nav-item" >
													<a class="nav-link active" id="active"  data-toggle="tab" href="#panel7mobile"
													role="tab"><i class="fas fa-user mr-1"></i>
													{{ __('lang.login')}}</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#panel8mobile" role="tab"><i
														class="fas fa-user-plus mr-1"></i>
													{{ __('lang.register')}}</a>
												</li>
											</ul>
					
											<!-- Tab panels -->
											<div class="tab-content">
					
												<!--Panel 7-->
												<div class="tab-pane fade in show active" id="panel7mobile" role="tabpanel">
					
													<!--Body-->
													<div class="modal-body mb-1">
														<form action="{{route('web.login', app()->getLocale())}}" method="post" class="needs-validation form-search" novalidate>
															{{ csrf_field() }}
															<div class="md-form form-sm ">
																<i class="fas fa-envelope prefix"></i>
																<input type="email" id="validationCustom032" name="email" placeholder="Your email"
																class="form-control form-control-sm validate" required>
					
																{{-- <label for="validationCustom032"></label> --}}
															</div>
					
															<div class="md-form form-sm mb-4">
																<i class="fas fa-lock prefix"></i>
																<input type="password" id="modalLRInput11" name="password" placeholder="Your password"
																class="form-control form-control-sm validate" required>
					
																{{-- <label data-error="wrong" data-success="right"
																for="modalLRInput11"></label> --}}
															</div>
															<div class="text-center form-sm mt-2">
																<button class="btn btn-info" type="submit">{{ __('lang.login')}} <i
																	class="fas fa-sign-in ml-1"></i></button>
																</div>
														</form>
													</div>
													<!--Footer-->
													<div class="modal-footer">
														<div class="options text-center text-md-right mt-1">
															<p> <a href="{{route('web.forgot_password', app()->getLocale())}}" class="blue-text">{{ __('lang.forget_password')}}?</a></p>
														</div>
														<button type="button" style="padding: 10px"
														class="btn btn-outline-info waves-effect ml-auto"
														data-dismiss="modal">{{ __('lang.close')}}</button>
													</div>
					
												</div>
													<!--/.Panel 7-->
					
													<!--Panel 8-->
												<div class="tab-pane fade" id="panel8mobile" role="tabpanel">
					
													<!--Body-->
													<div class="modal-body">
														<form action="{{route('web.register', app()->getLocale())}}" method="post" class="needs-validation" novalidate>
															{{ csrf_field() }}
															<div class="md-form form-sm mb-1">
																<i class="fas fa-user prefix"></i>
																<input type="text" name="name"
																class="form-control form-control-sm validate" placeholder="Your Name" value="{{ old('name') }}">
																{{-- <label data-error="wrong" data-success="right"></label> --}}
															</div>
															<div class="md-form form-sm mb-1">
																<i class="fas fa-mobile prefix"></i>
																<input type="number" name="phone" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
																type = "number"
																maxlength = "12"
																class="form-control form-control-sm validate" placeholder="Your Phone" value="{{ old('phone') }}">
																{{-- <label data-error="wrong" data-success="right"></label> --}}
															</div>
					
															<div class="md-form form-sm mb-1">
																<i class="fas fa-envelope prefix"></i>
																<input type="email" id="email" name="email"
																class="form-control form-control-sm validate" placeholder="Your email" required value="{{ old('email') }}">
																{{-- <label data-error="wrong" data-success="right"
																for="modalLRInput12"></label> --}}
															</div>
					
															<div class="md-form form-sm mb-1">
																<i class="fas fa-lock prefix"></i>
																<input type="password" name="password"
																class="form-control form-control-sm validate" placeholder="Your password">
																{{-- <label data-error="wrong" data-success="right"
																for="modalLRInput13"></label> --}}
															</div>
					
															<div class="md-form form-sm mb-1">
																<i class="fas fa-lock prefix"></i>
																<input type="password"  name="password_confirmation"
																class="form-control form-control-sm validate" placeholder="Repeat password">
																{{-- <label data-error="wrong" data-success="right"
																for="modalLRInput14"></label> --}}
															</div>
					
															<div class="text-center form-sm mt-2">
																<button class="btn btn-info" type="submit">Sign up <i
																	class="fas fa-sign-in ml-1"></i></button>
																</div>
															</form>
					
													</div>
													<!--Footer-->
													<div class="modal-footer" style="padding-right:1rem !important;padding-left:1rem !important">
					
														<button type="button"
														class="btn btn-outline-info waves-effect ml-auto"
														data-dismiss="modal">Close</button>
													</div>
												</div>
													<!--/.Panel 8-->
											</div>
					
										</div>
									</div>
									<!--/.Content-->
								</div>
							</div>
							@if(Auth::guard('customerForWeb')->check() )
                            	<a href="{{ url(app()->getLocale().'/get_wishlist') }}" class="icon-btn has-badge bg3 ms-3"><i style="font-size: 21px !important" class="fal fa-heart"></i><span id="wishlist-count"  class="badge wishlist-total-count">{{ count(auth()->guard('customerForWeb')->user()->wishlist->unique('barcode')->values() )}}</span></a>
							@endif
							@if(Auth::guard('customerForWeb')->check() )
							<a href="{{ route('cart', app()->getLocale()) }}" class="icon-btn has-badge bg2 ms-2 "><i class="fal fa-shopping-cart"></i><span id="cart-count2" class="badge total-count cart-count2">{{count(auth()->guard('customerForWeb')->user()->cart->where($stockId,1)->unique('barcode')->values() )}}</span></a>

					<a style="display: block !important;float: right;margin-left: 15px !important;" class="icon-btn has-badge bg2 ms-2 " href="{{ url(app()->getLocale().'/all_promotions') }}"><img  src="{{ asset('images/promotions.svg') }}" alt=""></a>					
							@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </header>