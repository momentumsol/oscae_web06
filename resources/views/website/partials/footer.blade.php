<footer style="text-align:{{ $lang == 'ar' ? 'right' : 'left' }}" class="footer-wrapper footer-layout1 pt-5" data-bg-src="{{asset('assets/img/shape/footer.webp')}}">
        <div class="container">
			
			<div class="widget-area">
                <div class="row align-items-start justify-content-between">
                    <div class="col-sm-6 col-lg-4 col-xl-4">
                        <div class="widget  ">
                            <div class="vs-widget-about">
                                <div class="widget-about-logo mb-30">
                                    <a href="{{ url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id) }}" id="home_link">
                                        <img loading="lazy" src="{{asset($logo)}}" alt="Oscar" width="170">
                                    </a>
                                </div>
                                <p class="widget-about-text mb-20">{{ __('lang.welcome')}}</p>
                                <div class="social-btn">
                                    <ul>
                                        <li><a href="https://facebook.com/oscargrandstores/"><i class="fab fa-facebook-f"></i></a></li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 col-xl-2">
                        <div class="widget widget_nav_menu  footer-widget  ">
                            <h3 class="widget_title">{{ __('lang.Useful Links')}}</h3>
                            <div class="menu-all-pages-container">
                                <ul class="menu">
                                    <li><a href="{{ url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id) }}">{{ __('lang.Home')}}</a></li>
                                    <li > <a href="{{ url(app()->getLocale().'/categoties') }}"  data-content="Link Hover" aria-hidden="true">{{ __('lang.Categories')}}</a></li>
									<li><a href="{{ url (app()->getLocale().'/aboutus') }}"  data-content="Link Hover" aria-hidden="true">{{ __('lang.about_us')}}</a></li>
									<li ><a href="{{ url(app()->getLocale().'/contact') }}">{{ __('lang.contact_us')}}</a></li>
                                    <li><a href="{{ url (app()->getLocale().'/return_policy') }}" >{{ __('lang.Refund & Returns Policy')}}</a></li>
                                    <li><a href="{{ url (app()->getLocale().'/privacy_policy') }}" >{{ __('lang.Privacy Policy')}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 col-xl-2">
                        <div class="widget  footer-widget  ">
                            <h3 class="widget_title">{{ __('lang.Download App')}}</h3>
                            <div class="footer-social-links">
                                <ul>
                                    <li><a href="https://play.google.com/store/apps/details?id=com.msolapps.oscar"><img src="{{ asset('android.png') }}" alt=""></a></li>
                                    <li><a href="https://apps.apple.com/eg/app/oscar-grand-stores/id1474983542"><img style="width: 152px !important;height: 47px;" src="{{ asset('apple.png') }}" alt=""></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4 col-xl-3">
                        <div class="widget footer-widget  ">
                            <h3 class="widget_title">{{ __('lang.contact_us')}}</h3>
                            <div class="vs-widget-about">
                                <p class="mb-1 link-inherit"><i class="fas fa-paper-plane me-3"></i><a href="mailto:info@oscargrandstores.com"> info@oscargrandstores.com</a></p>
                                <p class="mb-0 link-inherit"><i class="fal fa-fax me-3"></i><a href="tel:16991">{{ __('lang.Hotline') }} {{ __('lang.16991') }}</a></p>
								<p class="mb-0 link-inherit"><i class="fab fa-whatsapp me-3"></i><a href="https://wa.me/+201050403443">{{ __("lang.whatsapp")}}</a></p>
                                <p class="fs-md">{{ __("lang.open")}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright text-center pb-35">
                <p class="mb-0 link-inherit font-theme">&copy; {{ __('lang.2021') }} <a href="{{ url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id) }}">Oscar </a>{{ __('lang.By')}}  <a href="https://momentum-sol.com/"> Momentum Solutions. </a>{{ __('lang.All Rights Reserved')}} .</p>
            </div>
        </div>
    </footer>
				 <!--==============================
        All Js File
    ============================== -->
    <!-- Jquery -->
    <script src="{{asset('assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <!-- Slick Slider -->
    <script src="{{asset('assets/js/slick.min.js')}}"></script>
    <!-- Layerslider -->
    <script src="{{asset('assets/js/layerslider.utils.js')}}"></script>
    <script src="{{asset('assets/js/layerslider.transitions.js')}}"></script>
    <script src="{{asset('assets/js/layerslider.kreaturamedia.jquery.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- Date Picker -->
    <script src="{{asset('assets/js/jquery.datetimepicker.min.js')}}"></script>
    <!-- Filter -->
    <script src="{{asset('assets/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('assets/js/isotope.pkgd.min.js')}}"></script>
    <!-- Magnific Popup -->
    <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
    <!-- Jquery UI -->
    <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
    <!-- WOW JS (Animation JS) -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <!-- Custom Carousel -->
    <script src="{{asset('assets/js/vscustom-carousel.min.js')}}"></script>
    <!-- Mobile Menu -->
    <script src="{{asset('assets/js/vsmenu.min.js')}}"></script>
    <!-- Form Js -->
    <script src="{{asset('assets/js/ajax-mail.js')}}"></script>
    <!-- Main Js File -->
    <script src="{{asset('assets/js/main.js')}}"></script>


    