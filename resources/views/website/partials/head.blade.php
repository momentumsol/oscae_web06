<head>

	<!-- Meta Tag -->
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta http-equiv=“Content-Security-Policy” content=“default-src ‘self’ gap://ready file://* *; style-src ‘self’ ‘unsafe-inline’; script-src ‘self’ ‘unsafe-inline’ ‘unsafe-eval’”/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="facebook-domain-verification" content="fjz6bon4kqlzpspfnlbwdgg4rzwr3c" />
	<!-- Title Tag  --->
	<title>Oscar</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="https://oscarstoresapp.com/images/favicon.png">




	<!-- Web Font -->
	<link  href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"rel="stylesheet">
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtBC-vEWQokUP6yPiNXQOD-LPk2ru10W4&callback=initMap&libraries=&v=weekly"defer></script>


	<!-- Favicons - Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets/img/favicons/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('assets/img/favicons/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/img/favicons/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/favicons/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/img/favicons/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets/img/favicons/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets/img/favicons/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/img/favicons/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="170x170" href="{{asset('assets/img/favicons/apple-icon-170x170.png')}}">
	    <link rel="shortcut icon" sizes="96x96" href="{{asset('assets/img/favicons/favicon-96x96.png')}}"  type="image/x-icon">
		<link  rel="icon" type="image/x-icon" sizes="96x96" href="{{asset('assets/img/favicons/favicon-96x96.png')}}">

		<link rel="apple-touch-icon-precompose" sizes="60x60" href="{{asset('assets/img/favicons/apple-icon-60x60.png')}}">

		
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('assets/img/favicons/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/img/favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('assets/img/favicons/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/img/favicons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('assets/img/favicons/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('assets/img/favicons/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">

    <!--==============================
	    All CSS File
	============================== -->
    <!-- Bootstrap -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/css/bootstrap.min.css"
       >
    <!-- Fontawesome Icon -->
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome.min.css')}}">
    <!-- Flaticon -->
    <link rel="stylesheet" href="{{asset('assets/css/flaticon.min.css')}}">
    <!-- Layerslider -->
    <link rel="stylesheet" href="{{asset('assets/css/layerslider.min.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset('assets/css/jquery.datetimepicker.min.css')}}">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.min.css')}}">
    <!-- Slick Slider -->
    <link rel="stylesheet" href="{{asset('assets/css/slick.min.css')}}">
    <!-- Animation CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- Theme Color CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/theme-color1.css')}}">


	<link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet'>

	<!-- StyleSheet -->

	
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css">




<style>
	.fa, .fab, .fad, .fal, .far, .fas{
		line-height: inherit;
		font-size: 20px;
		cursor: pointer;
		margin: 0 3px
	}
	.icon-btn.bg3,.icon-btn.bg2{
		background-color: #ccc;
		cursor: pointer;

	}
	.box-nav-popup ul {
		display: block !important
	}

.mega-menu-wrap{
	max-width:1000px;
}
.actions-btn{
	display: flex !important
}

.head-top-links {
	/* display: flex !important; */
    align-items: center !important
}

.head-top-links .icon-btn{
	width: 40px;
	height: 40px;
	line-height: 42px
}

.fa-shopping-cart{
margin-top: -2px
}
.vs-menu-wrapper i,.header6-inner i{
	font-size: 24px !important
}


.menu-item-has-children a i{
	margin-right: 8 !important
}


.vs-product-box1:hover .actions-btn{
	opacity: 1;
    visibility: visible;
    margin-top: 0;
    width: 100px;
    display: flex !important;
    align-items: center;
    justify-content: center;
}
.actions-btn a {
	font-size: 25px !important;
	width: 40px !important;
	height: 40px !important;
}
.actions-btn i{
	font-size: 25px !important;
	/* width: 40px; */
	line-height: 1.8 !important

}
.actions-btn a:hover i{
/* padding: 10px !important */
} 
.product-img img{
	max-height: 375px;

}
.department .content-image{
	width: 100% !important;
}
.bg-theme{
	height: 60px;
}
.vs-mobile-menu{
	max-height: 100% !important;
}
.vs-menu-area{
	height: 100%;
	overflow: auto
}
.menu-item-has-children img{
	display: inline !important;
}
.vs-mobile-menu ul li a{
	border:none;
	padding: 5px 0 !important;
}
.box-nav-popup {
	margin: 5px
}
.main-menu a{
	border: 0
}

body,h1,h2,h3,h4,h5,h6,p,span,button,a{
	font-family: 'Oswald' !important
}

@media (max-width: 768px) { 
    .featured-bar{
		z-index: 0;
	}

}
.vs-mobile-menu ul .vs-item-has-children > a:after{
	content:'+';
	box-shadow:none;
}
.vs-mobile-menu ul .vs-item-has-children.vs-active > a:after{
		content:'-';

}
.close-search{
	display: inline;
    /* padding: 8px 0 0 10px; */
    color: #fff;
    /* line-height: 1.5; */
    font-size: 30px;
}
.head-top-links > ul > li::after{
	content: ''
}

.palceholder-search{
	margin: 0 0 0 15px;
}
.featured-bar{
	z-index: 40;
}
.main-menu ul.sub-menu li ul{
	margin: 0
}
.box-nav-popup {
    padding: 20px 0px 0px 15px;

}
.main-menu ul.sub-menu li.menu-item-has-children>a:after{
	font-family: 'Font Awesome 5 Pro';
}
.main-menu ul.sub-menu li.menu-item-has-children>a::before{
	font-family: 'Font Awesome 5 Pro';
}
.mega-menu-wrap {
	/* width: 700px !important; */
}
.main-menu ul.sub-menu li.menu-item-has-children>a:after {
	margin-left: 60px
}

.product-title a{
	font-size: 16px !important
}
.vs-mobile-menu ul li a:before{
	content: ''
}
.widget_title:before{
	left: 50%;
	width: 0;
}
.head-top-links  .fa-user{
	line-height: 1.5 !important;
}
.ms-2 {
        margin: 0 .5rem!important;
    }
</style>
<style>
	.fa-user:hover{
		color:#fff !important
	}
	.head-top-links>ul>li{
		margin:  0 7px !important
	}
	/* #cart-count2{
		top: -9px;
    right: -8px
	} */
	.fa-heart{
		font-size: 21px !important;
	}

</style>
<?php $lang= app()->getLocale();
			if($lang == 'ar'){
				echo'<link rel="stylesheet" href="'.asset('css/ar.css').'">';

			}else{
				echo'<link rel="stylesheet" href="'.asset('css/en.css').'">';

			}
	?>
<style>
	.widget-title{
		display: flex;
		/* width: fit-content; */
	}
	.widget-title a{
    white-space: nowrap !important;
    overflow: hidden !important;
    text-overflow: ellipsis !important;
    display: inline-block !important;
    width: 100%;
}
.vs-mobile-enu{
	max-height: auto !important;
}
/* .sub-menu{
	height: 100%;
} */
/* div.sticky{
  position: sticky;
  top: 0 !important;
} */
.product-area img:not([draggable]), embed, object, video{
	background-size: contain !important;
}
.row{
	margin:0
}
.sub-menu{
	z-index: 99 !important;
}




@media (max-width: 768px) { 

	.product-img{
		height: 50%;
	}
}
.widget_title{
	margin-bottom: 0 !important
}
.section-title{
	margin:20px 0 !important ;
	padding-top:20px !important ;
}
@media (max-width: 10000px){
	.featured-bar {
		margin-top:-60px
	}

}
.fa-shopping-cart{
		height: 1.9 !important;
		margin-top: 0;
	}

@media (max-width: 400px) { 
	.product-img{
		height: 35%;
	}
	.product-area .col-sm-6 {
		height: max-content;
	}
	.fa-shopping-cart{
		height: 1.9 !important;
	}
	.header-search{
		width: 33px !important; 
    height: 33px !important;
	top: -2px !important;
    right: -2px !important;
	}
	.product-area img:not([draggable]), embed, object, video {
    	max-height: 130px !important;

	}
	.bg-theme{
	display: flex;
	align-items: center
	}
	.header-search.style2{
		min-width: fit-content !important;
	}
	.bg-theme  form  .icon-btn{
		width: 35px !important;
		height: inherit !important;
		line-height: 0;
		top: 0px;
		right: 0px;
	}
	#icons-header{
		display: flex !important
	}

}

@media (max-width: 550px) { 
.product-area img:not([draggable]), embed, object, video {
		height: 130px !important;
    	max-height: 130px !important;
		-webkit-max-height: 130px !important;
	}
	.product-area img{
		height: 130px !important;
		max-height: 130px !important;
		-webkit-max-height: 130px !important;

	}
}

@media (max-width: 400px) { 
.product-area img:not([draggable]), embed, object, video {
		height: 100px !important;
    	max-height: 100px !important;
		-webkit-max-height: 100px !important;

	}
	.product-area img{
		height: 100px !important;
		max-height: 100px !important;
		-webkit-max-height: 100px !important;

	}
}


.fa-search{
	line-height: 0 !important
}
</style>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '743778210245842'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=743778210245842&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
@stack('style')
@stack('top_script')
</head>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-CL6MBY29MF"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-CL6MBY29MF');
</script>