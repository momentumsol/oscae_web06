@extends('website.layouts.app')

@push('style')

   <link rel="stylesheet" href="{{ asset('/css/customer-profile.css') }}">
    <style>
        .nav-link{
            background-color: #fff !important
        }

        #addresses th,#addresses td{
            padding: 8px
        }
        .button-5 {
            color: #C5171C !important;
            padding: 8px 8px 5px
        }
        h5 .button-5:hover {
            color: #fff !important
        }
        .pay_method img{
          width: 100px;
          margin-right: 20px;
          cursor: pointer;
      }
      .pay_method img:active{
        box-shadow: -1px 1px 14px -4px rgba(0,0,0,0.75);
      }
      [type=radio] { 
        position: absolute;
        opacity: 0;
        width: 0;
        height: 0;
        }

        /* IMAGE STYLES */
        [type=radio] + img {
        cursor: pointer;
        }

        /* CHECKED STYLES */
        [type=radio]:checked + img {
        outline: 2px solid #f00;
        }
        /* Styles FOR MESSEGES */
        .container2 {
            border: 2px solid #dedede;
            background-color: #f1f1f1;
            border-radius: 5px;
            padding: 10px;
            margin: 10px 0;
        }

        .darker {
            border-color: #ccc;
            background-color: #ddd;
        }

        .container2::after {
            content: "";
            clear: both;
            display: table;
        }

        .container2 img {
            float: left;
            max-width: 60px;
            width: 100%;
            margin-right: 20px;
            border-radius: 50%;
        }

        .container2 img.right {
            float: right;
            margin-left: 20px;
            margin-right:0;
        }

        .time-right {
            float: right;
            color: #aaa;
        }

        .time-left {
            float: left;
            color: #999;
        }

        .btn-send {
            border-radius: 5px;
            width: 100% !important;
            padding: 5px 10px;
        }
       
        .aside .nav-tabs .nav-link{
            padding: 0 !important
        }
        .aside .nav-tabs .nav-link:focus, .nav-tabs .nav-link:hover{
            background-color: transparent !important;
            border: 0 !important
        }
        input{
            max-width: 400px;
        }
        form ::placeholder {
            color: #a39f9f !important
        }

        .table-area {
            position: relative;
            z-index: 0;
            margin-top: 50px;
        }
        
        table.responsive-table {
            display: table;
            /* required for table-layout to be used (not normally necessary; included for completeness) */
            table-layout: fixed;
            /* this keeps your columns with fixed with exactly the right width */
            width: 100%;
            /* table must have width set for fixed layout to work as expected */
            height: 100%;
        }
        
        /* table.responsive-table thead {
            position: fixed;
            top: 50px;
            left: 0;
            right: 0;
            width: 100%;
            height: 50px;
            line-height: 3em;
            background: #eee;
            table-layout: fixed;
            display: table;
        } */
        
        table.responsive-table th {
            background: #eee;
        }
        
        table.responsive-table td {
            line-height: 2em;
        }
        
        table.responsive-table tr>td,
        table.responsive-table th {
            text-align: left;
        }
        
        @media screen and (max-width: 600px) {
            .table-area table {
                border: 0;
            }
            .table-area table caption {
                font-size: 1.3em;
            }
            .table-area table thead {
                border: none;
                clip: rect(0 0 0 0);
                height: 1px;
                margin: -1px;
                overflow: hidden;
                padding: 0;
                position: absolute;
                width: 1px;
            }
            .table-area table tr {
                border-bottom: 3px solid #ddd;
                display: block;
                margin-bottom: .625em;
            }
            .table-area table td {
                border-bottom: 1px solid #ddd;
                display: block;
                font-size: .8em;
                text-align: right;
            }
            .table-area table td::before {
                /*
    * aria-label has no advantage, it won't be read inside a table
    content: attr(aria-label);
    */
                content: attr(data-label);
                float: left;
                font-weight: bold;
                text-transform: uppercase;
            }
            .table-area table td:last-child {
                border-bottom: 0;
            }
            .table-areatbody tr td {
                text-align: center !important
            }
            .table-area body {
                padding-top: 0px;
            }
            .table-area {
                margin: 0;
            }
        }
        

        

        /* input[type="radio"]~label::before {
        top: 1px;
        left: 0;
        width: 16px;
        height: 16px;
        border: 1px solid var(--border-color);
        background: var(--white-color);
    } */



    /* input[type="radio"]+label:after {
        width: 0;
    }
    
    input[type="radio"]+label:before {
        right: -19px !important;
        left: auto !important;
    } */
    </style>
<?php $lang= app()->getLocale();
if($lang == 'ar'){
    echo'<style> input[type="radio"]+label:after {
        width: 0;
    }
    
    input[type="radio"]+label:before {
        right: -19px !important;
        left: auto !important;
    }</style>';

    echo'<style> table.responsive-table tr>td,
        table.responsive-table th {
            text-align: right;
        }</style>';

}else{
    echo'<style> input[type="radio"]+label:after {
        width: 0;
    }
    
    input[type="radio"]+label:before {
        left: 0px !important;
        top: 1px !important;
    }</style>';

}
?>


@endpush

@section('content')

{{-- <div class="my-account">
    <div class="container">
        <h4>{{ __('lang.my_account')}}</h4>
    </div>
</div> --}}

{{-- @if(Session::has('update'))
<h1>{{ Session::get('update') }}</h1>
@endif --}}

<div class="main mt-5">
    <div class="container">

        <div class="row profile-content" style="justify-content: space-around">

          

        
            <div class="col-lg-8 col-md-6 col-sm-12 my-5">
                <div class="tab-content" id="myTabContent">
                  
                
           
                 
                <div class="tab-pane " id="favorite" role="tabpanel" aria-labelledby="favorites-tab" style="display: block;">
                            <h5>{{ __('lang.wishlist') }}</h5>
                            <h5>{{ __('lang.cart_message') }}</h5>
                            <hr>
                        <div >
                            <div class="row text-center">
                                @if (isset($pros) && count($pros) > 0)

                                <table class="table-bordered table-hover" style="direction:{{ app()->getLocale() == 'ar' ?'rtl' : 'ltr' }};text-align:{{ app()->getLocale() == 'ar' ?'right' : 'left' }}">
                                    <thead>
                                        <tr >
                                            <th>{{ __('lang.name') }}</th>
                                            <th>{{ __('lang.image') }}</th>
                                            {{-- <th>Category</th> --}}
                                            <th>{{ __('lang.description') }}</th>
                                            <th>{{ __('lang.Price') }}</th>
                                            <th colspan="2">{{ __('lang.action')}}</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pros  as $pro)
                                        {{-- @dump($pro) --}}
                                        <tr>
                                            <td>{{$lang=='en' ? $pro->name : $pro->name_ar }}</td>
                                            @php
									
                                    $product_image=str_replace('http://34.105.27.34/oscar/public', 'https://cms.oscarstoresapp.com', $pro->images[0]->src);
                                    
                                @endphp
                                            <td><img width="40" src="{{ $product_image }}" alt=""></td>
                                         
                                            <td>{{ $lang=='en' ?  $pro->description: $pro->description_ar }}</td>
                                            <td>{{ $pro->regular_price }} {{ __('lang.EGP') }}</td>
                                            <td>
                                                <a class="  quickcart" onclick="quickcart(this,event)" title="{{ $pro->id }}" class="btn"  ><i class=" fa fa-cart-plus" aria-hidden="true"></i></a>
                                            </td>
                                            <td>
                                                <a class=" warning Unlike" style="color: rgb(189, 55, 55)" title="{{ $pro->id }}"><i class=" fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>  
                                @endif
                            </div>
                        </div>
                    </div>
                
              
             
                   
                </div>
            </div>

        </div>
    </div>
</div>




@endsection




@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('/js/customer-profile.js') }}"></script>

<script >





function reOrder(id){
    $.get(url+'/{{app()->getLocale()}}/homeWeb/getProducts/'+id, function(data, status){
       
        localStorage.removeItem('cart');
        var cart;
                if (!localStorage['cart']) cart = [];
                else cart = JSON.parse(localStorage['cart']);            
                if (!(cart instanceof Array)) cart = [];
                
                for(var i=0;i<data.length;i++)
            {
                var obj = {};
            var key =data[i].id;
            obj[key] = data[i];
             cart.push(obj);
            // console.log(cart)
                }
            

               localStorage.setItem('cart', JSON.stringify(cart));
               
               window.location.replace("https://oscarstores.com/en/cart");
               

  });
  
  
}
function addToCartAjax(id,weight,qty)
{
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/{{app()->getLocale()}}/homeWeb/addToCart',
	data:{ product_id:id,weight:weight,quantity:parseInt(qty)  },

	success:function(data){
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);
	}	
});
}
function addToCart(product) {
            if (localStorage) {
                var cart;
                if (!localStorage['cart']) cart = [];
                else cart = JSON.parse(localStorage['cart']);            
                if (!(cart instanceof Array)) cart = [];
                var key ='product_'+product.id;
                var obj = {};
                obj[key] = product;
                cart.push(obj);
                localStorage.setItem('cart', JSON.stringify(cart));
            } 
        }

function quickcart(elem,event){

event.preventDefault();
var id=elem.title;
var qty= $(elem).parent().parent().find('.quantity').find('input').val();
var storage=JSON.parse(localStorage.getItem("cart"));



$.ajax({
type:'POST',

url:url+'/{{app()->getLocale()}}/homeWeb/add-to-cart',

data:{ id:id },
success:function(data){
    let weight=250
    data['qty']=qty;

    if(data.PriceUnit=='kg')
	{
	
		data['weight']=250
	}
	else
	{
		data['weight']=1
	}
    var res=jQuery.inArray(id,localStorage);
    var key='product_'+data.id;
    var storage=localStorage['cart'];
    let check=false;
    for(var i=0;i<JSON.parse(storage).length;i++)
    {
        let item=JSON.parse(storage)[i]
        if(Object.keys(item)[0]==key)
        {
            check=true
        }
        
        
    }
   
        // console.log(JSON.parse(storage));

     
 
        
        addToCart(data);
        $('#alert-cart').show();
        $('#alert-cart').html(@json( __('lang.add_to_cart')));
        setTimeout(function() { 
                $('#alert-cart').fadeOut('fast'); 
        }, 10000);
        $('.total-count').html('');
        var total=JSON.parse(localStorage.getItem("cart"));
        $('.total-count').html(total.length);

    
    addToCartAjax(id,weight,1)
//$(elem).parent().parent().find('.Unlike').click();

}
});
var modal=$(elem).closest(".modal").modal("hide");
}

 function removeDiv(elem){
            $(elem).parent('div').remove();
            $('#file').val('');
        }


function showImage(file){


for (var i = 0; i < file.files['length']; i++) {
console.log(file.files[i],'i');
var img='<div class="upload-image float-left" id="upload-image" style="display:inline-block">'+
        "<img id='img_src' width=50 src="+"'" +window.URL.createObjectURL(file.files[i])+"'"+">"+
        '<a onClick="removeDiv(this)" href="javascript:void(0);" class="btn  btn-danger btn-sm" style="padding:3px"><i class="far fa-trash-alt"></i></a>'+
    '</div>';

$(".upload-wrap").append(img);

$('.upload-image').css('display','block');
};
};

$.ajaxSetup({

headers: {

    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

}

});

$(".delete").click(function(e){
    var select = $(this);
    var id =select.attr('id');
    e.preventDefault();
    
    '/{{app()->getLocale()}}/address/'

    $.ajax({
    type:'DELETE',
    url:url+'/{{app()->getLocale()}}/address/'+id,
  

    success:function(data){
           
        select.closest('tr').remove();

        
        
        }   
    });


});

function removeToWhishList(id)
{

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/{{app()->getLocale()}}/homeWeb/remove/wishlist',
	data:{ product_id:id  },

	success:function(data){

		$('.wishlist-total-count').html('')
		$('.wishlist-total-count').html(data.count);
		
	}	
});
}	

$(".Unlike").click(function(e){
            e.preventDefault();
            
    
            var id = $(this).attr("title");
            $.ajax({
               type:'POST',
               url:url+'/{{app()->getLocale()}}/homeWeb/unLike',
    
               data:{ id:id },

               success:function(data){
                removeToWhishList(id)
                //    console.log(data);

            }
    
            });
            
            $(this).closest('tr').remove();
            var count=$('.wishlist-total-count').html();
            $('.wishlist-total-count').html(Number(count)-Number(1));
        });
    
var hash = document.location.hash;
if (hash =='#favorite') {

    $('#favorite-tab').tab('show');
}




</script>
@endpush
