    <!--==============================
        Breadcumb
    ============================== -->
    <div class="breadcumb-wrapper breadcumb-layout1 bg-fluid pt-200 pb-200" data-bg-src="{{asset('assets/img/shape/footer-4-bg.jpg')}}">
        <div class="container">
            <div class="breadcumb-content text-center">
                <h1 class="breadcumb-title">Contact Us</h1>
                {{-- <ul class="breadcumb-menu-style1 mx-auto">
                    <li><a href="index-5.html">Home</a></li>
                    <li class="active">Contact</li>
                </ul> --}}
            </div>
        </div>
    </div>

     <!--==============================
      Contact Form Area
    ==============================-->
    <section class="vs-contact-wrapper vs-contact-layout1 space-top space-md-bottom">
        <div class="container">
            <div class="row text-center text-lg-start">
                <div class="col-lg-6 ">
                    <div class="section-title mb-25">
                        <h2 class="sec-title1">Address</h2>
                        <h3 class="sec-title2">Information</h3>
                        <img src="{{ asset('assets/img/icons/sec-icon-1.png') }}" alt="Section Image">
                        <p class=" fs-md mt-4 pt-1">We would like to welcome you to the world of OSCAR. Since 1982 we have been working hard to achieve and maintain our reputation as the favored shopping destination for all those who seek a wide variety of high quality products at optimum prices. We invite you to join us in our stores located at: </p>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <h4>5th Settlement</h4>
                            <p class="mb-0 fw-semibold">Oscar Grand Store </p>
                            <p class="mb-0 fw-semibold">425 St. 90 </p>
                            <p class="mb-0 fw-semibold">5th Settlement, Cairo </p>
                            <p class="mb-0 fw-semibold">Opening hours:  8am- 12am </p>
                            <p class="mb-0 fw-semibold"><a href="https://www.google.com/maps/place/Oscar+Grand+Stores,+425+N+90th+Street+-+Service+Ln,+First+New+Cairo,+Cairo+Governorate/data=!4m2!3m1!1s0x145822f524fabd0d:0x73efaf4b5775e78b?utm_source=mstt_1&entry=gps">Get Directions</a>  </p>

                        </div>
                        <div class="col-sm-6 mb-20">
                            <h4>Heliopolis</h4>
                            <p class="mb-0 fw-semibold">Oscar Store  </p>
                            <p class="mb-0 fw-semibold">105 Omar Ibn El Khattab St. </p>
                            <p class="mb-0 fw-semibold">Heliopolis, Cairo </p>
                            <p class="mb-0 fw-semibold">Opening hours:  8am- 12am </p>
                            <p class="mb-0 fw-semibold"><a href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x14583e027fb38807:0xbff1f05b786d2e2e?utm_source=mstt_1&entry=gps&lucs=swa">Get Directions</a>  </p>

                        </div>
                        <div class="col-sm-6">
                            <h4>Maadi </h4>
                            <p class="mb-0 fw-semibold">Oscar Store</p>
                            <p class="mb-0 fw-semibold">Rayhanah Plaza, Zahraa El Maadi st.</p>
                            <p class="mb-0 fw-semibold">Maadi, Cairo </p>
                            <p class="mb-0 fw-semibold">Opening hours:  8am- 12am </p>
                            <p class="mb-0 fw-semibold"><a href="https://www.google.com/maps/place/Oscar+Grand+Stores/@29.9607097,31.2952341,18z/data=!4m5!3m4!1s0x145839a86f55a773:0xf6088000b2c7ac65!8m2!3d29.9611396!4d31.2969722">Get Directions</a>  </p>

                        </div>
                        <div class="col-sm-6 mb-20">
                            <h4>Zamalek </h4>
                            <p class="mb-0 fw-semibold">Oscar Signature  </p>
                            <p class="mb-0 fw-semibold">Sun Mall, 26th July St. </p>
                            <p class="mb-0 fw-semibold">Zamalek, Cairo </p>
                            <p class="mb-0 fw-semibold">Opening hours:  8am- 12am </p>
                            <p class="mb-0 fw-semibold"><a href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x14584172eaef8a8d:0xd8097bb652715e1d?utm_source=mstt_1&entry=gps&lucs=swa">Get Directions</a>  </p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mt-30 mt-lg-0">
                    <div class="section-title mb-25">
                        <h2 class="sec-title1">Get In Touch</h2>
                        <h3 class="sec-title2">Inform Us</h3>
                        <img src="{{ asset('assets/img/icons/sec-icon-1.png') }}" alt="Section Image">
                        <p class="fs-md mt-4 pt-1">How can we help? There are several ways you can contact Oscar. If you would like to speak to a customer services representative directly please call us on 16991 from inside Egypt.</p>
                        <p>You can also get in touch by filling in the below form. Our dedicated service team generally responds to all enquiries within 1-3 business days.  </p>
                    </div>
                    <form action="{{ url(app()->getLocale().'/sendemail') }}" method="POST" >
                       @csrf

                        <input type="text" name="contact" value="1" hidden>

                        <div class="row g-4">
                            <div class="col-6 form-group mb-0">
                                <input  name="fname" class="form-control" placeholder="First Name">
                            </div>
                            <div class="col-6 form-group mb-0">
                                <input  name="lname" class="form-control" placeholder="Last Name">
                            </div>
                            <div class="col-lg-12 form-group mb-0">
                                <input type="email" name="email" class="form-control" placeholder="Your Email">
                            </div>
                            {{-- <div class="col-lg-6 form-group mb-0">
                                <input type="text" name="subject" class="form-control" placeholder="Your Subject">
                            </div> --}}
                            <div class="col-12 form-group mb-0">
                                <textarea class="form-control" name="comment" placeholder="Your Message"></textarea>
                            </div>
                            <div class="col-12 form-group mb-0">
                                <button  type="submit" class="vs-btn ">Send Message</button>
                            </div>
                        </div>
                    </form>
                    <p class="form-messages mt-20 mb-0"></p>
                </div>
              
            </div>
        </div>
    </section>