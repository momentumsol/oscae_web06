@extends('website.layouts.app')

@push('style')
	<style>
        #map {
            height: 300px;
            width: 100%;
        }

     
        
        /* input[type="radio"] {
                visibility: visible;
    opacity: 1;
    display: inline-block;
    vertical-align: middle;
    width: 5px;
    height: 5px;
    display: block;
        }
        input[type="radio"] ~ label::before ,input[type="radio"] ~ label::after{
            display: none;
        } */
    </style>

@php
    if ($lang= app()->getLocale() == 'ar') {
        echo ' <style>
        input[type="radio"]~label:after {
            width: 10px !important;
            height: 10px;
            top: 4px;
            right: -16px;
          }
    </style>';
    }

@endphp
   
@endpush
@push('top_script')
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
@endpush
@section('content')
    <div class="alert alert-success order_confirmation" style="display:none">
        <p>{{ __('lang.Thank you for your purchase') }}</p>
    </div>
        <!--==============================
    Breadcumb
    ============================== -->
    <div class="breadcumb-wrapper breadcumb-layout1 bg-fluid pt-200 pb-200" data-bg-src="{{asset('assets/img/shape/footer-4-bg.jpg')}}">
        <div class="container">
            <div class="breadcumb-content text-center">
                <h1 class="breadcumb-title">{{ __('lang.Check out') }}</h1>
                <ul class="breadcumb-menu-style1 mx-auto mt-2">
                    <li><a href="{{ url( app()->getLocale().'/homeWeb/branch/'.$branch->store_id) }}">{{ __('lang.Home') }}</a></li>
                    <li class="active">{{ __('lang.Check out') }}</li>
                </ul>
            </div>
        </div>
    </div>
    <!--==============================
    Checkout Arae
    ==============================-->
    <div class="vs-checkout-wrapper space-top space-md-bottom">
        <div class="container">
            <form id="form" action="{{ route('homeWeb' , app()->getLocale()) }}"  method="post"  class="woocommerce-checkout mt-40 form" novalidate>
                @csrf
                <div class="row">

                    <div class="col-lg-6">

                        <h2 class="h4" >{{ __('lang.Billing Details') }}</h2>
                        <div class="row gx-2" >
                        <h5><a href='{{ url(app()->getLocale().'/address') }}'"  class="btn button-6 float-right"><i class="fa fa-plus" aria-hidden="true"></i> </a> {{ __('lang.add_new_address_or')}} </h5> 

                            <div class="col-12 form-group" id="billing">
                                <label>{{ __('lang.select_your_address')}} *</label>
                                <select class="form-select" name="address" id="address">
                                    <option  value=""></option>

                                    @foreach ($addresses as $address)
                                        <option value="{{ $address->id }}">{{ $address->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 form-group " id="name">
                                <label>{{ __('lang.username')}} *</label>
                                <input type="text" name="userName" class="form-control" placeholder="" value="{{ $customer->name }}" required>
                            </div>

                            <div class="col-md-6 form-group "  id="phone">
                                <label>{{ __('lang.email')}}*</label>
                                <input  type="email" name="Email" placeholder="" value="{{ $customer->email }}"   class="form-control" required>
                            </div>
                            <div class="col-md-6 form-group " id="name">
                                <label>{{ __('lang.name')}} *</label>
                                <input type="text" name="address_name" class="form-control" placeholder="" value="" required>
                            </div>

                            <div class="col-md-6 form-group "  id="phone">
                                <label>{{ __('lang.phone')}}*</label>
                                <input  type="text" name="number"  value="" max="11"  class="form-control" required>
                            </div>
                            
                           
                            <div class="col-6 form-group" id="building_number">
                                <label>{{ __('lang.building_number')}} *</label>
                                <input  type="text" name="building_number"class="form-control" value="" required>
                            </div>


                            <div class="col-6 form-group"  id="floor_no">
                                <label>{{ __('lang.floor_number')}} *</label>
                                <input type="text" class="form-control"  type="text" name="floor_no" required>
                            </div>
                            <div class="col-12 form-group" id="apartment_no">
                                <label>{{ __('lang.apartment_number')}} *</label>
                                <input type="text" class="form-control"  name="apartment_no" required>
                            </div>
                            <div class="col-12 form-group" id="area">
                                <label>{{ __('lang.area')}} *</label>
                                <input type="text" class="form-control" name="area"   value="" required>
                            </div>
                            <div class="col-12 form-group"  id="city">
                                <label>{{ __('lang.city')}} *</label>
                                <input  type="text" class="input_city form-control" name="city"   value="" required>
                            </div>
                            <div class="col-12 form-group" id="coordinatesDiv">
                                <input class="form-control" type="text" name="coordinates"  id="coordinates" value=""  hidden>
                            </div>

                        </div>
                    </div>


                    <div class="col-lg-6">
                        <p id="ship-to-different-address">
                            <input id="ship-to-different-address-checkbox" type="checkbox" name="ship_to_different_address" value="1">
                            <label for="ship-to-different-address-checkbox">
                                {{ __('lang.Ship to a different address?') }}
                                <span class="checkmark"></span>
                            </label>
                        </p>
                        <div class="shipping_address">
                            <div class="row gx-2">

                                <div class="col-12 form-group">
                                    <label>{{ __('lang.address')}} *</label>
                                    <input type="text" name="Newaddress" class="form-control" placeholder="" value="" required>

                                </div>
                                <div class="col-md-6 form-group " id="name">
                                    <label>{{ __('lang.username')}} *</label>
                                    <input type="text" name="userName" class="form-control" placeholder="" value="{{ $customer->name }}" required>
                                </div>
    
                                <div class="col-md-6 form-group "  id="phone">
                                    <label>{{ __('lang.email')}}*</label>
                                    <input  type="email" name="Email" placeholder="" value="{{ $customer->email }}"   class="form-control" required>
                                </div>
                                <div class="col-md-6 form-group " id="name">
                                    <label>{{ __('lang.name')}} *</label>
                                    <input type="text" name="address_name" class="form-control" placeholder="" value="" required>
                                </div>
    
                                <div class="col-md-6 form-group "  id="phone">
                                    <label>{{ __('lang.phone')}}*</label>
                                    <input  type="text" name="Newnumber"  value="" max="11"  class="form-control" required>
                                </div>
                                
                               
                                <div class="col-6 form-group" id="building_number">
                                    <label>{{ __('lang.building_number')}} *</label>
                                    <input  type="text" name="Newbuilding_number"class="form-control" value="" required>
                                </div>
    
    
                                <div class="col-6 form-group"  id="floor_no">
                                    <label>{{ __('lang.floor_number')}} *</label>
                                    <input type="text" class="form-control"  type="text" name="Newfloor_no" required>
                                </div>
                                <div class="col-12 form-group" id="apartment_no">
                                    <label>{{ __('lang.apartment_number')}} *</label>
                                    <input type="text" class="form-control"  name="Newapartment_no" required>
                                </div>
                                <div class="col-12 form-group" id="area">
                                    <label>{{ __('lang.area')}} *</label>
                                    <input type="text" class="form-control" name="Newarea"   value="" required>
                                </div>
                                <div class="col-12 form-group"  id="city">
                                    <label>{{ __('lang.city')}} *</label>
                                    <input type="text" class="input_city form-control" name="Newcity"   value="" required>
                                </div>
                                <div class="col-12 form-group" id="coordinatesDiv">
                                    <input class="form-control" type="text" name="coordinates"  id="coordinates" value=""  hidden>
                                </div>
    
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <label>{{ __('lang.notes')}}</label>
                            <textarea cols="20" rows="5" class="form-control" name="notes" placeholder="{{ __('lang.Notes about your order, special notes for delivery.') }}"></textarea>
                        </div>
                    </div>
                </div>
                <div id="map" ></div>
                <input type="text" hidden value="" name="mylocation" id="mylocation">
                <div class="row" style="justify-content: center">
                    <button style="margin: 0 auto;display:none" class="col-xs-2 text-center  vs-btn shadow-none"  id="location-btn">{{ __('lang.submit-location') }}</button>

                </div>
                <div class="pt-10 pt-lg-5 mb-30 zone my-5" style="display:none">
                    <input type="text" name="delivery_date" value="" id="delivery_date"hidden >
                    <input type="text" name="flag_checked" value="" id="flag_checked" hidden>
                    <input type="text" name="cost_flag" value="" id="cost_flag" hidden>
                    <div class="woocommerce-checkout-payment d-fees" style="display: none">
                        <h5 id="h-fees" style="display: none">{{ __('lang.Delivery Fees') }}</h5>
                        <ul class="wc_payment_methods payment_methods methods" id="new_flags" >
                            
                        </ul>
                    </div>
                </div>
                <div class="pt-10 pt-lg-5 mb-30  my-5 payment"  style="display: none">
                    <div class="woocommerce-checkout-payment">
                        <h5 id="payment_note">{{ __('lang.Payment') }}</h5> <label for="payment_note"> *{{ __('lang.Payment upon delivery') }}</label>
                        <ul class="wc_payment_methods payment_methods "  >
                            <li class="wc_payment_method payment_method_bacs">
                                <input id="credit" type="radio" class="input-radio" name="pay" value="credit" >
                                <label for="credit">{{ __('lang.Card') }}</label>
                               
                            </li>
                            <li class="wc_payment_method payment_method_bacs">
                                <input id="pay_cash" type="radio" class="input-radio" name="pay"  value="cash" checked="checked">
                                <label for="pay_cash">{{ __('lang.Cash') }}</label>
                                
                            </li>
                        </ul>
                    </div>
                </div>




                <div class="border ps-2 py-2 border-light"  id="payment">
                    <div class="row  justify-content-lg-end">
                        <div class="col-md-8 col-lg-6 col-xl-4">
                            <table class="checkout-ordertable mb-0">
                                <tbody>
                                    {{-- <tr class="cart-subtotal">
                                        <th>Cart Subtotal</th>
                                        <td>
                                            <span class="amount"><bdi><span>$</span>47</bdi>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="woocommerce-shipping-totals shipping">
                                        <th>Shipping and Handling</th>
                                        <td data-title="Shipping">
                                            <ul class="woocommerce-shipping-methods list-unstyled">
                                                <li>
                                                    <input type="checkbox" name="shipping_method" class="shipping_method" checked="checked">
                                                    <label>Flat rate</label>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr> --}}
                                    <tr class="order-total">
                                    <th>{{ __('lang.delivery_fee')}}</th>
                                        <td><strong><span class="amount" id="delivery_fee"></span></strong>{{ __('lang.EGP')}}</td>
                                        <th>{{ __('lang.total')}}</th>
                                        <td><strong><span class="amount" id="total_price"></span></strong>{{ __('lang.EGP')}}</td>
                                       
                                    </tr>
                                </tbody>                    

                               
                            </table>
                            <div class="form-row place-order mt-5">
                                <button id="submit" type="submit" class="vs-btn button-5">{{ __('lang.Place order') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    // $('#submit').click(function(){
    //     $('#form').submit();
    // });
		// let url='';

        var $loading = $('.preloader').hide();
        $(document)
        .ajaxStart(function () {
            $loading.show();
        })
        .ajaxStop(function () {
            $loading.hide();
        });

        let map, infoWindow;







        var total_price=0;
        var local_storage=localStorage['cart'];

      
        $( document ).ready(function() {
            var totalPrice=localStorage.getItem('total_price');
            var cost=$('#cost_flag').val();


            $("#total_price").html(parseFloat(totalPrice).toFixed(2));
            total_price=totalPrice;
       
        });

      

      
        $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

        });

      


     

        $('#ship-to-different-address-checkbox').click(function(){
            
            if($('#billing').css('display').toLowerCase() == 'block'){
                $('.methods').empty();
                $('.schedule').empty();
                $('#location-btn').show();
                $('#h-fees').hide();
               


                document.getElementById('billing').style.display="none"
                document.getElementById('city').style.display="none"
                document.getElementById('name').style.display="none"
                document.getElementById('area').style.display="none"
                document.getElementById('phone').style.display="none"
                document.getElementById('building_number').style.display="none"
                document.getElementById('floor_no').style.display="none"
                document.getElementById('apartment_no').style.display="none"

                $("#address option:first").attr('selected','selected');

               
                initialize(26.6194394,21.8566593);

            }else{
                document.getElementById('billing').style.display="block"
               
            }
            $('.payment').show()

        });

        function replace(){
            $('#delivery_date').val($('#date').val());
        }
      



        document.getElementById('map').style.display="none"
        document.getElementById('city').style.display="none"
        document.getElementById('name').style.display="none"
        document.getElementById('area').style.display="none"
        document.getElementById('phone').style.display="none"
        document.getElementById('building_number').style.display="none"
        document.getElementById('floor_no').style.display="none"
        document.getElementById('apartment_no').style.display="none"

        $("#address").change(function(){
            $('#h-fees').hide();
            $('#location-btn').show();
            
            var selected = $(this).children("option:selected").val();
            var id = { id : selected }
            $('#new_flags').empty();
            $('.schedule').empty();
            $('.alert ').hide();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
   
        var getData = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                type: 'POST',
                url:url +'/{{app()->getLocale()}}/address/getaddress',
                data: id,
                dataType : 'json',
            
            
                success: function(resultData) { 
                    let Data=resultData.data['address']
                    let flags=resultData.data['flags']
                    $('.payment').show()
                    $('input[name="address_name"]').val(Data.name)
                    $('input[name="city"]').val(Data.city)
                    $('input[name="number"]').val(Data.phone)
                    $('input[name="area"]').val(Data.area)
                    $('input[name="coordinates"]').val(Data.coordinates)
                    $('input[name="building_number"]').val(Data.building_number)
                    $('input[name="floor_no"]').val(Data.floor_number)
                    $('input[name="apartment_no"]').val(Data.apartment_number)
                    document.getElementById('city').style.display="block"
                    document.getElementById('name').style.display="block"
                    document.getElementById('area').style.display="block"
                    document.getElementById('phone').style.display="block"
                    document.getElementById('building_number').style.display="block"
                    document.getElementById('floor_no').style.display="block"
                    document.getElementById('apartment_no').style.display="block"
                    document.getElementById('map').style.display="block"


                            
                            $('.zone').css('display','block');

                      

                     
                    coordinate=String(Data.coordinates);
                    let res = coordinate.split(",");
                    initialize(parseFloat(res[0]),parseFloat(res[1]));
                    console.log(Data.coordinates)

                }

                
            });
        });
        $('.flaglabel').click(function(){
        $(this).parent().find('#flag').click();
      });

        function flaaags() {
            var elem=$('input[name="flag"]:checked').val()
            var cost =0;
            if ( elem!= 'Scheduled') {
                $('.schedule').css('display','none');
                document.getElementById('flag_checked').value=elem;
                 cost=$('input[name="flag"]:checked').parent().find('input[name="cost"]').val();
                 console.log(cost,'cost');
                $('#delivery_date').val('');
                $('#cost_flag').val(cost);
                document.getElementById("delivery_fee").innerHTML =cost;
                $('#delivery_fee').val(cost);



            }else if (elem == 'Scheduled') {
                $('.schedule').css('display','block');
                document.getElementById('flag_checked').value=elem;
                 cost=$('input[name="flag"]:checked').parent().find('input[name="cost"]').val();
                $('#cost_flag').val(cost);
            }
            var totalPrice=localStorage.getItem('total_price');
          
            totalPrice=totalPrice.replace("جنيه","");
            totalPrice=totalPrice.replace("EGP","");
            let cost_i=document.getElementById('cost')
            var totalWithDelivery=parseFloat(totalPrice)+ parseFloat(cost);
            total_price=totalWithDelivery;
            $("#total_price").html(Number(totalWithDelivery.toString().match(/^\d+(?:\.\d{0,2})?/)) );

        }


        $('#location-btn').click(function(e){
            e.preventDefault();

            var coordinates = $('input[name="coordinates"]').val();
            if(coordinates == ''){
                alert('click on get current location');
            }
            $('#mylocation').val(coordinates);
            $('.schedule').empty();
            $('.d-fees').hide();
            $('.schedule').hide();
            $('#h-fees').show();
            $('#new_flags').empty();
            $.ajax({
                    type:'POST',
                    url: url+'/{{app()->getLocale()}}/flags',

                    data:{
                        coordinates:coordinates
                    },
                    success:function(resultData){
                        

                        if(! resultData.errors){
                            $('.d-fees').show();
                            $('.schedule').remove();

                            $('.woocommerce-checkout-payment').show();
                            $('#h-fees').show();
                            let flags=resultData.data.flags;
                            $('#new_flags').empty()
                            // console.log(resultData.data,'llllllllllll')
                        for (let i = 0; i < flags.length; i++) {
                           
                                if(i==0)
                                {
                                    var flag= ' <li class="wc_payment_method payment_method_bacs">'+
                                                '<input id="'+i+'" type="radio" class="input-radio" name="flag" value="'+flags[i].flag+'" onchange="flaaags()"  checked>'+
                                                '<label class="flaglabel" for='+i+'>'+flags[i].flag+'  <span>'+flags[i].cost+' {{ __("lang.EGP") }}</span></label>'+
                                                '<input type="text" id="cost" name="cost" value="'+flags[i].cost+'" "  hidden>'+
                                           ' </li>';
                                }
                                else
                                {
                                    var flag= ' <li class="wc_payment_method payment_method_bacs">'+
                                                '<input id="'+i+'" type="radio" class="input-radio" name="flag" value="'+flags[i].flag+'" onchange="flaaags()"  >'+
                                                '<label class="flaglabel" for='+i+'>'+flags[i].flag+'  <span>'+flags[i].cost+' {{ __("lang.EGP") }}</span></label>'+
                                                '<input type="text"  name="cost" value="'+flags[i].cost+'" "  hidden>'+
                                           ' </li>';
                                }
                            
                            $('#new_flags').append(flag);
                           
                            }
                            $('.zone').css('display','block');
                            var schedulle ='<div class="schedule" style="display:none">'+
                                            '<input class="form-control" id="date" value="" onchange="replace()" type="datetime-local" required style="width:40%;height:auto" selected>'+
                                        '</div>';
                            $('.zone').append(schedulle);  

                            flaaags()
                        }else{

                            $('.alert').html((resultData.errors['message']));
                            $('.alert').css('display','block');
                            setTimeout(function() { 
                                    $('.alert').fadeOut('fast'); 
                            }, 2000);
                            
                        }
                   

                    } ,error: function (data) {

                   
                    }	

            });
        });


        $(".button-5").click(function(e){
            
                e.preventDefault();
                var local_storage=localStorage['cart'];
                var name = $("input[name=userName]").val();
                var email = $("input[name=Email]").val();
                var number = $("input[name=number]").val();
                var city = $("input[name=city]").val();
                var area = $("input[name=area]").val();
                var building_number = $("input[name=building_number]").val();
                var floor_no = $("input[name=floor_no]").val();
                var apartment_no = $("input[name=apartment_no]").val();
                var address = $("input[name=address_name]").val();
                var coordinates = $("input[name=mylocation]").val();
                // var coordinates =   document.getElementById('coordinates').value;
                var delivery_date=$("input[name=delivery_date]").val();
                var zone=$("input[name=zone]").val();
                var checkout=$("input[name=checkout]").val();
                var pay = $( 'input[name=pay]:checked').val();

                if($('#billing').css('display').toLowerCase() != 'block'){
                    var city = $("input[name=Newcity]").val();
                    var area = $("input[name=Newarea]").val();
                    var building_number = $("input[name=Newbuilding_number]").val();
                    var floor_no = $("input[name=Newfloor_no]").val();
                    var apartment_no = $("input[name=Newapartment_no]").val();
                    var number = $("input[name=Newnumber]").val();
                    var address = $("input[name=Newaddress]").val();

                }


                // var flag=document.getElementById('flag_checked').value($('input[name="flag"]:checked').val());
                 var cost=document.getElementById('cost_flag').value;

                $.ajax({
                    type:'POST',
                    url: url+'/{{app()->getLocale()}}/homeWeb',

                    data:{
                            local_storage: local_storage,
                            total_price: total_price,
                            name:name,
                            email:email,
                            number:number,
                            city:city,
                            area:area,
                            address:address,
                            pay:pay,
                            coordinates:coordinates,
                            delivery_date:delivery_date,
                            building_number:building_number,
                            floor_no:floor_no,
                            apartment_no:apartment_no,
                            zone:zone,
                            checkout:checkout,
                            // flag:flag,
                            cost:cost
                        },

                    success:function(data){
                        var checkout=$("input[name=checkout]").val();
                        $('.alert-danger').empty();
                        if(data.errors){
                            $('.alert-danger').empty();
                            
                        jQuery.each(data.errors, function(key, value){
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<p>'+value+'</p>');
                            });
                            
                        }

                        if (data.redirect) {
                            var cart =[];
                            var total_price='';
		                    localStorage.setItem('cart', JSON.stringify(cart));
		                    localStorage.setItem('total_price', JSON.stringify(total_price));

                            $(window).scrollTop(0);

                            $('.order_confirmation').show();
                             setTimeout(function() { 
                            window.location.href =url +data.redirect;
						}, 2000);
                        }  
                       
                    }
                    ,error: function (data) {

                        $('.alert').html(JSON.parse(data.responseText).message);
                        $('.alert').css('display','block');
                        setTimeout(function() { 
								$('.alert').fadeOut('fast'); 
						}, 2000);
                    }	
                });
        });
    




        function initialize(lat,lng) {
        document.getElementById('map').style.display="block"

            map = new google.maps.Map(document.getElementById("map"), {
            center: { lat: lat, lng: lng },
            zoom: 18,
        });
                infoWindow = new google.maps.InfoWindow();
                const locationButton = document.createElement("button");
                // var curent='<?php echo('')?>;'
                locationButton.textContent = @json( __('lang.Get current location') );;
                locationButton.type = "button";
                locationButton.classList.add("custom-map-control-button");
                map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
                    // Try HTML5 geolocation.
                    if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        (position) => {
                        var pos = {
                            lat: lat,
                            lng:lng ,
                        };
                        // //   infoWindow.setPosition(pos);
                        infoWindow.open(map);
                        map.setCenter(pos);
                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,});
                            locationButton.addEventListener("click", () => {
                    // Try HTML5 geolocation.
                    if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        function (position) {
                        const pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude,
                        };

                    
                    map.setCenter(pos);
                    marker.setPosition(pos);

                    coordinates= position.coords.latitude+','+position.coords.longitude

                    document.getElementById('coordinates').value=coordinates;
                    
                    },
                    () => {
                    handleLocationError(true, infoWindow, map.getCenter());
                    }
                );
                } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
                }
            });
                        var coordinates=(marker.getPosition().lat()).toString()+','+(marker.getPosition().lng()).toString();
                        document.getElementById('coordinates').value=coordinates

                        map.addListener("click", (mapsMouseEvent) => {

                    // Close the current InfoWindow.
                        marker.setMap(null);
                        // Create a new InfoWindow.
                        marker = new google.maps.Marker({
                        position: mapsMouseEvent.latLng,
                        map: map,});

                    
                        coordinates=(marker.getPosition().lat()).toString()+','+(marker.getPosition().lng()).toString();
                    document.getElementById('coordinates').value=coordinates
                    

                    });
                    },
                    () => {
                    handleLocationError(true, infoWindow, map.getCenter());
                    }
                );
                } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
                }


            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
//   var marker = new google.maps.Marker({
//                             position: pos,
//                             map: map});
//   marker.setPosition(pos);
  console.log(pos)

//   coordinates= '26.6194394,21.8566593'

//  document.getElementById('coordinates').value=coordinates;

  infoWindow.open(map);
}
</script>

@endpush
