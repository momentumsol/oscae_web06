@extends('website.layouts.app')

@push('style')
<style>
.register{
    border-top: 1px solid #C5C5C5;
    border-bottom: 1px solid #C5C5C5;
    background-color: #FAFAFA;
    overflow: hidden;
    padding: 10px;
    color: #535353
}
.form .form-control{
    margin:  0 auto;
    margin-top: 20px;
    margin-bottom: 20px;
    margin-bottom: 8px

}
.form{
    border: 1px solid #C53330;
margin:  0 auto;
padding: 20px 20px 30px
}
/* 
@media only screen and (min-width: 800px) {
    .form{
    width: 40%;

}}
@media only screen and (min-width: 1200px) {
    .form{
    width: 20%;

}}



button{
    border: 1px solid   #414742 !important;
    color:   #414742 !important;
    margin:  0 auto;
}
.forget_pss{
    color: #aaaeb3;
    text-decoration: underline
}
.create_new_account{
    text-align: center;
    margin-bottom: 40px;
    margin-top: 25px
}
.create_new_account a{
    color:   #414742;
    text-decoration: underline

}
.text-center{
    font-size: 12px;
    padding: 20px
}
.text-center a{
    color: #aaaeb3;
    text-decoration: underline
} */
</style>

@endpush


@section('content')


{{-- <div class="register">
    <div class="container">
        <h4>register</h4>
    </div>
</div> --}}

<div>
    <div class="row my-5 justify-content-center" style="margin: 0">
        <div class="col-lg-4 col-md-6 col-sm-10"  style="padding: 0">
            @include('website.partials.errors')
            <div class="form">
                <form action="{{route('web.register', app()->getLocale())}}" method="post">
                    @csrf
                    <div class="form-group">
                        <input class="form-control" name="name" type="text" placeholder="{{ __('lang.name')}}" value="{{ old('name') }}">
                        <input class="form-control" name="email" type="text" placeholder="{{ __('lang.email')}}"  value="{{ old('email') }}">
                        <input class="form-control"name="phone" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "12" placeholder="{{ __('lang.phone')}}" value="{{ old('phone') }}">
                        {{-- <div class="col-12 col-sm-12"> --}}
                            {{-- <select  name="city" class="selectpicker mt-3 form-control"   data-live-search="true"   style="width:50%;paddind:2px">
                                @foreach ($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                            </select> --}}
                        {{-- </div> --}}
                        <input class="form-control" name="password" type="password" placeholder="{{ __('lang.password')}}">
                        <input class="form-control" name="password_confirmation" type="password" placeholder="{{ __('lang.confirm_password')}}">
                    </div>
                    <div class="row mt-5" style="justify-content: center">
                        <button type="submit" class="btn qut-btn vs-btn shadow-none"  style="width: 85%"> {{ __('lang.register')}}</button>
                    </div>
                </form>
    
    
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script src="{{ asset('/front/js/dish.js') }}"></script>


<script>
    $( document ).ready(function() {
        $('.carousel-item').first().addClass( "active" );
});

</script>
@endpush
