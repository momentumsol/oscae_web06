<div class="container  " style="direction: rtl" >
    <div class="contact my-5">
     
        <div class="row">
            <!-- <h2>OSCAR Grand Stores</h2> -->
            <div class="col-md-12">
                <h3 style="color: black  !important;">إشعار الخصوصية</h3>
                <p>&nbsp;</p>
                {{-- <p>&nbsp;</p> --}}

                <p>تم التحديث الأخير في الأول من يوليو ٢٠٢١</p>

                <p>&nbsp;</p>

                <p>شكرًا لاختيارك أن تكون جزءًا من مجتمعنا في أوسكار جراند ستورز (الشركة" ، "نحن" ، "نحن" ، "لدينا"). نحن ملتزمون بحماية معلوماتك الشخصية وحقك في الخصوصية. إذا كانت لديك أي أسئلة أو مخاوف بشأن إشعار الخصوصية هذا أو ممارساتنا فيما يتعلق بمعلوماتك الشخصية ، فيرجى التواصل معنا على info@oscargrandstores.com</a>.</p>

                <p>&nbsp;</p>

                <p>عندما تزور موقعنا على الويب <a style="color:blue;text-decoration:underline" href="http://www.oscargrandstores.com/">http://www.oscargrandstores.com</a> استخدم تطبيق الهاتف المحمول الخاص بنا ، حسب الحالة (التطبيق) وبشكل عام ، استخدم أيًا من خدماتنا (الخدمات)، بما في ذلك موقع (الويب والتطبيق) ، نحن نقدر أنك تثق بنا فيما يتعلق بمعلوماتك الشخصية. نحن نأخذ خصوصيتك على محمل الجد. و في إشعار الخصوصية هذا، نسعى إلى أن نشرح لك بأوضح طريقة ممكنة ما هي المعلومات التي نجمعها وكيف نستخدمها وما هي الحقوق التي لديك فيما يتعلق بها. نأمل أن تأخذ بعض الوقت لقراءتها بعناية، لأنها مهمة.                </p>
                <p>إذا كانت هناك أي شروط في إشعار الخصوصية هذه لا توافق عليها ، فيرجى التوقف عن استخدام خدماتنا على الفور.</p>
                <p>&nbsp;</p>

                <p>ينطبق إشعار الخصوصية هذا على جميع المعلومات التي تم جمعها من خلال خدماتنا (والتي ، كما هو موضح أعلاه ، تشمل موقعنا على الويب وتطبيقنا) ، بالإضافة إلى أي خدمات أو مبيعات أو تسويق أو أحداث ذات صلة.</p>

                <p>&nbsp;</p>

                <p>يرجى قراءة إشعار الخصوصية هذا بعناية لأنه سيساعدك على فهم ما نفعله بالمعلومات التي نجمعها.</p>

                <p>&nbsp;</p>

                <p>جدول المحتويات:</p>

                <p>&nbsp;</p>

                <p><a style="text-decoration: underline" href="#link_1" >1. ما هي المعلومات التي نجمعها؟</a></p>

                <p><a style="text-decoration: underline" href="#link_2">2.  كيف نستخدم معلوماتك؟</a></p>

                <p><a style="text-decoration: underline" href="#link_3">3.  هل سيتم تبادل المعلومات الخاصة بك مع أي شخص؟</a></p>

                <p><a style="text-decoration: underline" href="#link_4">4. مع من سيتم تبادل معلوماتك؟</a></p>

                <p><a style="text-decoration: underline" href="#link_5">5. هل نستخدم ملفات تعريف الارتباط وتقنيات التتبع الأخرى؟</a></p>

                <p><a style="text-decoration: underline" href="#link_6">6. كيف نتعامل مع تسجيلات الدخول الاجتماعية الخاصة بك؟</a></p>

                <p><a style="text-decoration: underline" href="#link_7">7. ما هي مدة احتفاظنا بمعلوماتك؟</a></p>

                <p><a style="text-decoration: underline" href="#link_8">8.  كيف نحافظ على أمان معلوماتك؟</a></p>

                <p><a style="text-decoration: underline" href="#link_9">9. هل نجري تحديثات على هذا الإشعار؟</a></p>

                <p><a style="text-decoration: underline" href="#link_10">10. كيف يمكنك الاتصال بنا بخصوص هذا الإشعار؟</a></p>

                <p>&nbsp;</p>

                <p id="link_1">1. ما هي المعلومات التي نجمعها؟</p>

                <p>المعلومات الشخصية التي تكشفها لنا</p>

                <p>نقوم بجمع المعلومات الشخصية التي تقدمها لنا متطوعا عندما تقوم بالتسجيل في الخدمات ، أو تعبر عن اهتمامك بالحصول على معلومات عنا أو عن منتجاتنا وخدماتنا ، عندما تشارك في أنشطة على الخدمات أو بطريقة أخرى عندما تتصل بنا.</p>

                <p>&nbsp;</p>

                <p>تعتمد المعلومات الشخصية التي نجمعها على سياق تفاعلاتك معنا والخدمات ، والاختيارات التي تقوم بها والمنتجات والميزات التي تستخدمها. قد تتضمن المعلومات الشخصية التي نجمعها ما يلي:</p>

                <p>&nbsp;</p>

                <p>المعلومات الشخصية التي قدمتها. نقوم بجمع الأسماء، أرقام الهواتف، عناوين البريد الإلكتروني، عناوين بريدية عناوين الفواتير وغيرها من المعلومات المماثلة.</p>

                <p>&nbsp;</p>

                <p>بيانات الدفع. قد نقوم بجمع البيانات اللازمة لمعالجة الدفع الخاص بك إذا قمت بإجراء عمليات شراء ، مثل رقم وسيلة الدفع الخاصة بك (مثل رقم بطاقة الائتمان) ، ورمز الأمان المرتبط بأداة الدفع الخاصة بك.</p>

                <p>&nbsp;</p>

                <p>بيانات تسجيل الدخول إلى وسائل التواصل الاجتماعي، قد نوفر لك خيار التسجيل معنا باستخدام تفاصيل حساب الوسائط الاجتماعية الحالي الخاص بك ، مثل فيسبوك أو تويتر أو أي حساب آخر على وسائل التواصل الاجتماعي، إذا اخترت التسجيل بهذه الطريقة، فسنجمع المعلومات الموضحة في القسم المسمى "كيف نتعامل مع تسجيلاتك الاجتماعية؟" أقل.</p>

                <p>&nbsp;</p>

                <p>يجب أن تكون جميع المعلومات الشخصية التي تقدمها لنا صحيحة وكاملة ودقيقة، ويجب عليك إخطارنا بأي تغييرات تطرأ على هذه المعلومات الشخصية.</p>

                <p>&nbsp;</p>

                <p>يتم جمع المعلومات تلقائيًا</p>

                <p>نقوم تلقائيًا بجمع معلومات معينة عند زيارة الخدمات أو استخدامها أو التنقل فيها. لا تكشف هذه المعلومات عن هويتك المحددة (مثل اسمك أو معلومات الاتصال) ولكنها قد تتضمن معلومات الجهاز والاستخدام ، مثل عنوان IP وخصائص المتصفح والجهاز ونظام التشغيل وتفضيلات اللغة وعناوين URL المرجعية واسم الجهاز والبلد والموقع، معلومات حول كيفية ووقت استخدامك لخدماتنا والمعلومات الفنية الأخرى، هذه المعلومات ضرورية في المقام الأول للحفاظ على أمان وتشغيل خدماتنا ، ولأغراض التحليلات الداخلية وإعداد التقارير.
                     </p>

                <p>&nbsp;</p>

                <p>مثل العديد من الشركات، نقوم أيضًا بجمع المعلومات من خلال ملفات تعريف الارتباط والتقنيات المماثلة.</p>

                <p>&nbsp;</p>

                <p>تتضمن المعلومات التي نجمعها ما يلي:</p>

                <ul>
                    <li><em>بيانات السجل والاستخدام:</em> بيانات السجل والاستخدام هي معلومات متعلقة بالخدمة والتشخيص والاستخدام والأداء تجمعها خدماتنا تلقائيًا عند الوصول إلى خدماتنا أو استخدامها والتي نسجلها في ملفات السجل. بناءً على كيفية تفاعلك معنا ، قد تتضمن بيانات السجل هذه عنوان IP الخاص بك ومعلومات الجهاز ونوع المتصفح والإعدادات ومعلومات حول نشاطك في الخدمات (مثل طوابع التاريخ / الوقت المرتبطة باستخدامك والصفحات والملفات التي تم عرضها ، عمليات البحث والإجراءات الأخرى التي تتخذها مثل الميزات التي تستخدمها) ومعلومات أحداث الجهاز (مثل نشاط النظام وتقارير الأخطاء (تسمى أحيانًا "تفريغ الأعطال") وإعدادات الأجهزة).</li>
                </ul>

                <ul>
                    <li><em> الموقع:</em>  نقوم بجمع بيانات الموقع مثل المعلومات حول موقع جهازك، والتي يمكن أن تكون دقيقة أو غير دقيقة، يعتمد مقدار المعلومات التي نجمعها على نوع وإعدادات الجهاز الذي تستخدمه للوصول إلى الخدمات، على سبيل المثال، قد نستخدم GPS وتقنيات أخرى لجمع بيانات تحديد الموقع الجغرافي التي تخبرنا عن موقعك الحالي (بناءً على عنوان IP الخاص بك)، يمكنك إلغاء الاشتراك في السماح لنا بجمع هذه المعلومات إما عن طريق رفض الوصول إلى المعلومات أو عن طريق تعطيل إعداد الموقع الخاص بك على جهازك. ومع ذلك ، لاحظ أنه إذا اخترت إلغاء الاشتراك ، فقد لا تتمكن من استخدام جوانب معينة من الخدمات.</li>
                </ul>

                <p>&nbsp;</p>

                <p>المعلومات التي تم جمعها من خلال تطبيقنا
                </p>

                <p>إذا كنت تستخدم تطبيقنا ، فإننا نجمع أيضًا المعلومات التالية:
                </p>

                <ul>
                    <li><em>معلومات تحديد الموقع الجغرافي:</em>  قد نطلب الوصول أو الإذن إلى وتتبع المعلومات المستندة إلى الموقع من جهازك المحمول، إما بشكل مستمر أو أثناء استخدامك لتطبيقنا، لتقديم خدمات معينة قائمة على الموقع، إذا كنت ترغب في تغيير وصولنا أو أذوناتنا، فيمكنك القيام بذلك في إعدادات جهازك.</li>
                </ul>

                <ul>
                    <li><em>دفع الإخطارات: </em>قد نطلب إرسال إشعارات فورية إليك بخصوص حسابك أو بعض ميزات التطبيق. إذا كنت ترغب في إلغاء الاشتراك من تلقي هذه الأنواع من الاتصالات ، فيمكنك إيقاف تشغيلها في إعدادات جهازك.</li>
                </ul>

                <p>هذه المعلومات ضرورية في المقام الأول للحفاظ على أمان وتشغيل تطبيقنا ، لاستكشاف الأخطاء وإصلاحها ولأغراض التحليلات الداخلية وإعداد التقارير.</p>

                <p>&nbsp;</p>

                <p id="link_2">2. كيف نستخدم معلوماتك؟</p>

                <p>كيف نستخدم معلوماتك؟</p>

                <p>&nbsp;</p>

                <p>نستخدم المعلومات التي نجمعها أو نتلقاها:</p>

                <ul>
                    <li>لتسهيل إنشاء الحساب وعملية تسجيل الدخول، إذا اخترت ربط حسابك معنا بحساب طرف ثالث (مثل حساب جوجل أو فيسبوك الخاص بك)، فإننا نستخدم المعلومات التي سمحت لنا بجمعها من تلك الأطراف الثالثة لتسهيل إنشاء الحساب وعملية تسجيل الدخول لأداء عقد، راجع القسم أدناه بعنوان "كيف نتعامل مع تسجيلات الدخول الاجتماعية الخاصة بك؟" لمزيد من المعلومات.</li>
                </ul>

                <ul>
                    <li>لنشر الشهادات، ننشر شهادات على خدماتنا التي قد تحتوي على معلومات شخصية، قبل نشر الشهادة، سنحصل على موافقتك على استخدام اسمك ومحتوى الشهادة، إذا كنت ترغب في تحديث شهادتك أو حذفها، فيرجى الاتصال بنا على info@oscargrandstores.com وتأكد من تضمين اسمك وموقع الشهادة ومعلومات الاتصال.</li>
                </ul>

                <ul>
                    <li>طلب ملاحظات: قد نستخدم معلوماتك لطلب التعليقات وللاتصال بك بشأن استخدامك لخدماتنا.
                        -لتمكين الاتصالات بين المستخدم: قد نستخدم معلوماتك من أجل تمكين الاتصالات بين المستخدم والمستخدم بموافقة كل مستخدم.</li>
                </ul>

                <ul>
                    <li>لإدارة حسابات المستخدمين: قد نستخدم معلوماتك لأغراض إدارة حسابنا وإبقائه في حالة جيدة.</li>
                </ul>

                <ul>
                    <li>تلبية وإدارة طلباتك: قد نستخدم معلوماتك للوفاء وإدارة الطلبات والمدفوعات والمرتجعات وعمليات التبادل التي تتم من خلال الخدمات.</li>
                </ul>

                <ul>
                    <li>إدارة سحوبات الجوائز والمسابقات: قد نستخدم معلوماتك لإدارة سحوبات الجوائز والمسابقات عندما تختار المشاركة في مسابقاتنا.</li>
                </ul>

                <ul>
                    <li>لتقديم وتسهيل تقديم الخدمات للمستخدم: قد نستخدم معلوماتك لتزويدك بالخدمة المطلوبة.</li>
                </ul>

                <ul>
                    <li>للرد على استفسارات المستخدم / تقديم الدعم للمستخدمين: قد نستخدم معلوماتك للرد على استفساراتك وحل أي مشاكل محتملة قد تواجهك مع استخدام خدماتنا.</li>
                </ul>

                <ul>
                    <li>لنرسل لك اتصالات تسويقية وترويجية: يجوز لنا و / أو شركائنا في التسويق من الجهات الخارجية استخدام المعلومات الشخصية التي ترسلها إلينا لأغراض التسويق الخاصة بنا ، إذا كان ذلك يتوافق مع تفضيلاتك التسويقية. على سبيل المثال ، عند التعبير عن اهتمامنا بالحصول على معلومات عنا أو عن خدماتنا ، أو الاشتراك في التسويق أو الاتصال بنا بأي طريقة أخرى ، سنجمع معلومات شخصية منك. يمكنك إلغاء الاشتراك في رسائل البريد الإلكتروني التسويقية في أي وقت (انظر "ما هي حقوق الخصوصية الخاصة بك؟" أدناه).</li>
                </ul>

                <ul>
                    <li>تسليم الإعلانات المستهدفة لك: قد نستخدم معلوماتك لتطوير وعرض محتوى وإعلانات مخصصة (والعمل مع أطراف ثالثة تقوم بذلك) مصممة خصيصًا لتناسب اهتماماتك و / أو موقعك ولقياس مدى فعاليتها.</li>
                </ul>

                

                <p>&nbsp;</p>

                <p id="link_3">3.هل سيتم تبادل المعلومات الخاصة بك مع أي شخص؟</p>

                <p>قد نعالج أو نشارك بياناتك التي نحتفظ بها بناءً على الأساس القانوني التالي:</p>

                <ul>
                    <li>الموافقة: قد نعالج بياناتك إذا منحتنا موافقة محددة على استخدام معلوماتك الشخصية لغرض معين.</li>
                </ul>

                <ul>
                    <li>المصالح المشروعة: قد نعالج بياناتك عندما تكون ضرورية بشكل معقول لتحقيق مصالحنا التجارية المشروعة.</li>
                </ul>

                <ul>
                    <li>أداء العقد: عندما أبرمنا عقدًا معك ، يجوز لنا معالجة معلوماتك الشخصية للوفاء بشروط عقدنا.</li>
                </ul>

                <ul>
                    <li>الالتزامات القانونية: قد نكشف عن معلوماتك حيثما طُلب منا قانونًا القيام بذلك من أجل الامتثال للقانون المعمول به ، أو الطلبات الحكومية ، أو الإجراءات القضائية ، أو أمر المحكمة ، أو الإجراءات القانونية ، مثل الرد على أمر محكمة أو أمر استدعاء ( بما في ذلك استجابة للسلطات العامة لتلبية متطلبات الأمن القومي أو إنفاذ القانون).</li>
                </ul>

                <ul>
                    <li>الاهتمامات الحيوية: قد نكشف عن معلوماتك عندما نعتقد أنه من الضروري التحقيق أو منع أو اتخاذ إجراء بشأن الانتهاكات المحتملة لسياساتنا أو الاحتيال المشتبه به أو المواقف التي تنطوي على تهديدات محتملة لسلامة أي شخص وأنشطة غير قانونية أو كدليل في التقاضي الذي نشارك فيه.</li>
                </ul>

                <p>وبشكل أكثر تحديدًا ، قد نحتاج إلى معالجة بياناتك أو مشاركة معلوماتك الشخصية في المواقف التالية:</p>

                <ul>
                    <li>تحويلات الأعمال: يجوز لنا مشاركة أو نقل معلوماتك فيما يتعلق أو أثناء المفاوضات بشأن أي اندماج أو بيع أصول الشركة أو تمويل أو الاستحواذ على كل أو جزء من أعمالنا إلى شركة أخرى.</li>
                </ul>

                <ul>
                    <li>الباعة والمستشارون ومقدمو الخدمات الآخرون: قد نشارك بياناتك مع البائعين الخارجيين أو مقدمي الخدمات أو المقاولين أو الوكلاء الذين يؤدون خدمات لنا أو نيابة عنا ويطلبون الوصول إلى هذه المعلومات للقيام بهذا العمل. تشمل الأمثلة: معالجة الدفع وتحليل البيانات وتسليم البريد الإلكتروني وخدمات الاستضافة وخدمة العملاء وجهود التسويق. قد نسمح لأطراف ثالثة محددة باستخدام تقنية التتبع على الخدمات ، والتي ستمكنهم من جمع البيانات نيابة عنا حول كيفية تفاعلك مع خدماتنا بمرور الوقت. يمكن استخدام هذه المعلومات ، من بين أشياء أخرى ، لتحليل البيانات وتتبعها ، وتحديد مدى شعبية محتوى أو صفحات أو ميزات معينة ، وفهم النشاط عبر الإنترنت بشكل أفضل. ما لم يرد وصف ذلك في هذا الإشعار ، نحن لا نشارك أو نبيع أو نؤجر أو نتاجر بأي من معلوماتك مع أطراف ثالثة لأغراض ترويجية.</li>
                </ul>

                <ul>
                    <li>معلنو الطرف الثالث: يجوز لنا الاستعانة بشركات إعلان تابعة لجهات خارجية لعرض الإعلانات عند زيارة الخدمات أو استخدامها. قد تستخدم هذه الشركات معلومات حول زياراتك إلى موقعنا (مواقعنا) على الويب ومواقع الويب الأخرى المضمنة في ملفات تعريف ارتباط الويب وتقنيات التتبع الأخرى من أجل تقديم إعلانات حول السلع والخدمات التي تهمك.</li>
                </ul>

                <ul>
                    <li>عرض الحائط: قد يعرض تطبيقنا "جدار عرض" مستضاف من طرف ثالث. يسمح جدار العرض هذا للمعلنين الخارجيين بتقديم عملة افتراضية أو هدايا أو عناصر أخرى للمستخدمين مقابل قبول عرض إعلاني وإتمامه. قد يظهر جدار العرض هذا في تطبيقنا ويتم عرضه لك بناءً على بيانات معينة ، مثل منطقتك الجغرافية أو المعلومات الديموغرافية. عند النقر فوق أحد جدران العرض ، سيتم نقلك إلى موقع ويب خارجي تابع لأشخاص آخرين وستغادر تطبيقنا. ستتم مشاركة معرّف فريد ، مثل معرف المستخدم الخاص بك ، مع مزود جدار العرض من أجل منع الاحتيال وائتمان حسابك بشكل صحيح بالمكافأة ذات الصلة. يرجى ملاحظة أننا لا نتحكم في مواقع الأطراف الثالثة ولا نتحمل أي مسؤولية فيما يتعلق بمحتوى هذه المواقع. إن تضمين ارتباط إلى موقع ويب تابع لجهة خارجية لا يعني موافقة منا على هذا الموقع. وبناءً على ذلك ، فإننا لا نقدم أي ضمان فيما يتعلق بمواقع الطرف الثالث ولن نتحمل أي مسؤولية عن أي خسارة أو ضرر ناتج عن استخدام هذه المواقع. بالإضافة إلى ذلك ، عند الوصول إلى أي موقع ويب تابع لجهة خارجية ، يرجى تفهم أن حقوقك أثناء الوصول إلى تلك المواقع واستخدامها ستخضع لإشعار الخصوصية وشروط الخدمة المتعلقة باستخدام تلك المواقع.
                    </li>
                </ul>

                <p>&nbsp;</p>

                <p id="link_4">4. مع من سيتم تبادل معلوماتك؟&nbsp;&nbsp;&nbsp;&nbsp;</p>

                <p>نحن نشارك معلوماتك ونكشف عنها فقط مع الفئات التالية من الأطراف الثالثة. إذا قمنا بمعالجة بياناتك بناءً على موافقتك وترغب في إلغاء موافقتك، فيرجى الاتصال بنا باستخدام تفاصيل الاتصال الواردة في القسم أدناه بعنوان "كيف يمكنك الاتصال بنا بخصوص هذا الإشعار؟</a>&quot;.</p>

                <ul>
                    <li>خدمات تحليلات البيانات</li>
                </ul>

                <ul>
                    <li>أدوات الاتصال والتعاون </li>
                </ul>

                <ul>
                    <li>معالجات الدفع
                    </li>
                </ul>

                <ul>
                    <li>منصات إعادة الاستهداف
                    </li>
                </ul>

                <ul>
                    <li>تسجيل حساب المستخدم و خدمات المصادقة
                    </li>
                </ul>

                <p>&nbsp;</p>

                <p id="link_5">5. هل نستخدم ملفات تعريف الارتباط وتقنيات التتبع الأخرى؟</p>

                <p>قد نستخدم ملفات تعريف الارتباط وتقنيات التتبع المماثلة (مثل إشارات الويب ووحدات البكسل) للوصول إلى المعلومات أو تخزينها. معلومات محددة حول كيفية استخدامنا لهذه التقنيات وكيف يمكنك رفض بعض ملفات تعريف الارتباط مذكورة في إشعار ملفات تعريف الارتباط الخاص بنا.</p>

                <p>&nbsp;</p>

                <p id="link_6">6. كيف نتعامل مع تسجيلات الدخول الاجتماعية الخاصة بك؟ &nbsp;&nbsp;&nbsp;&nbsp;</p>

                <p>توفر لك خدماتنا القدرة على التسجيل وتسجيل الدخول باستخدام تفاصيل حساب وسائل التواصل الاجتماعي الخاصة بطرف ثالث (مثل عمليات تسجيل الدخول على فيسبوك أوتويتر) عندما تختار القيام بذلك ، سوف نتلقى معلومات ملف تعريف معينة عنك من مزود الوسائط الاجتماعية الخاص بك،قد تختلف معلومات الملف الشخصي التي نتلقاها اعتمادًا على مزود وسائل التواصل الاجتماعي المعني، ولكنها غالبًا ما تتضمن اسمك وعنوان بريدك الإلكتروني وقائمة أصدقائك وصورة ملفك الشخصي بالإضافة إلى المعلومات الأخرى التي تختار نشرها على منصة التواصل الاجتماعي هذه.</p>

                <p>&nbsp;</p>

                <p>سنستخدم المعلومات التي نتلقاها فقط للأغراض الموضحة في إشعار الخصوصية هذا أو التي تم توضيحها لك بطريقة أخرى في الخدمات ذات الصلة،  يرجى ملاحظة أننا لا نتحكم، ولسنا مسؤولين عن، الاستخدامات الأخرى لمعلوماتك الشخصية بواسطة موفر الوسائط الاجتماعية التابع لجهة خارجية، نوصي بمراجعة إشعار الخصوصية الخاص بهم لفهم كيفية جمعهم لمعلوماتك الشخصية واستخدامها ومشاركتها، وكيف يمكنك تعيين تفضيلات الخصوصية الخاصة بك على مواقعهم وتطبيقاتهم.</p>

                <p>&nbsp;</p>

                <p id="link_7">7.  ما هي مدة احتفاظنا بمعلوماتك؟</p>

                <p>سنحتفظ فقط بمعلوماتك الشخصية طالما كانت ضرورية للأغراض المنصوص عليها في إشعار الخصوصية هذا ، ما لم يكن هناك حاجة إلى فترة احتفاظ أطول أو مسموح بها بموجب القانون (مثل الضرائب أو المحاسبة أو المتطلبات القانونية الأخرى)  لن يتطلب منا أي غرض في هذا الإشعار الاحتفاظ بمعلوماتك الشخصية لفترة أطول من الفترة الزمنية التي يكون لدى المستخدمين فيها حساب معنا.</p>

                <p>&nbsp;</p>

                <p>عندما لا يكون لدينا عمل شرعي مستمر لمعالجة معلوماتك الشخصية ، فسنقوم إما بحذف هذه المعلومات أو إخفاء مصدرها، أو إذا لم يكن ذلك ممكنًا (على سبيل المثال ، لأنه تم تخزين معلوماتك الشخصية في أرشيفات النسخ الاحتياطي)، فسنقوم بذلك بأمان قم بتخزين معلوماتك الشخصية وعزلها عن أي معالجة أخرى حتى يصبح الحذف ممكنًا.</p>

                <p>&nbsp;</p>

                <p id="link_8">8.  كيف نحافظ على أمان معلوماتك؟</p>

                <p>لقد قمنا بتنفيذ إجراءات أمنية تقنية وتنظيمية مناسبة مصممة لحماية أمن أي معلومات شخصية نقوم بمعالجتها. ومع ذلك، على الرغم من الضمانات والجهود التي نبذلها لتأمين معلوماتك ، لا يمكن ضمان أن يكون النقل الإلكتروني عبر الإنترنت أو تقنية تخزين المعلومات آمنًا بنسبة ١٠٠%، لذلك لا يمكننا أن نعد أو نضمن أن المتسللين أو مجرمي الإنترنت أو غيرهم من الأطراف الثالثة غير المصرح لهم لن يكونوا قادرًا على هزيمة أمننا وجمع معلوماتك أو الوصول إليها أو سرقتها أو تعديلها بشكل غير صحيح، على الرغم من أننا سنبذل قصارى جهدنا لحماية معلوماتك الشخصية، فإن نقل المعلومات الشخصية من وإلى خدماتنا يكون على مسؤوليتك الخاصة. يجب عليك الوصول إلى الخدمات فقط في بيئة آمنة.</p>

                <p>&nbsp;</p>

                <p>معلومات الحساب
                </p>

                <p>إذا كنت ترغب في أي وقت في مراجعة أو تغيير المعلومات الموجودة في حسابك أو إنهاء حسابك، فيمكنك:</p>

                <ul>
                    <li>قم بتسجيل الدخول إلى إعدادات حسابك وقم بتحديث حساب المستخدم الخاص بك.                        </li>
                </ul>

                <p>بناءً على طلبك لإنهاء حسابك، سنقوم بإلغاء تنشيط أو حذف حسابك ومعلوماتك من قواعد البيانات النشطة لدينا، ومع ذلك، قد نحتفظ ببعض المعلومات في ملفاتنا لمنع الاحتيال واستكشاف المشكلات وإصلاحها والمساعدة في أي تحقيقات وفرض شروط الاستخدام  أو الامتثال للمتطلبات القانونية المعمول بها.</p>

                <p>&nbsp;</p>

                <p>ملفات تعريف الارتباط والتقنيات المشابهة: يتم تعيين معظم متصفحات الويب على قبول ملفات تعريف الارتباط افتراضيًا، إذا كنت تفضل ذلك، يمكنك عادةً اختيار ضبط متصفحك لإزالة ملفات تعريف الارتباط ورفض ملفات تعريف الارتباط، إذا اخترت إزالة ملفات تعريف الارتباط أو رفض ملفات تعريف الارتباط، فقد يؤثر ذلك على ميزات أو خدمات معينة لخدماتنا، لإلغاء الاشتراك في الإعلانات القائمة على الاهتمامات من قبل المعلنين على خدماتنا، قم بزيارة  <a href="http://www.aboutads.info/choices/">http://www.aboutads.info/choices/</a>.</p>

                <p>&nbsp;</p>

                <p>إلغاء الاشتراك في التسويق عبر البريد الإلكتروني: يمكنك إلغاء الاشتراك في قائمة البريد الإلكتروني التسويقي الخاصة بنا في أي وقت عن طريق النقر فوق رابط إلغاء الاشتراك في رسائل البريد الإلكتروني التي نرسلها أو عن طريق الاتصال بنا باستخدام التفاصيل الواردة أدناه، ستتم إزالتك بعد ذلك من قائمة البريد الإلكتروني التسويقي - ومع ذلك، قد لا نزال نتواصل معك، على سبيل المثال لإرسال رسائل البريد الإلكتروني المتعلقة بالخدمة والضرورية لإدارة حسابك واستخدامه، أو للرد على طلبات الخدمة ، أو لأغراض أخرى أغراض غير تسويقية، لإلغاء الاشتراك بطريقة أخرى، يمكنك:</p>

                <ul>
                    <li>الوصول إلى إعدادات حسابك وتحديث تفضيلاتك.
                    </li>
                </ul>

                <p>&nbsp;</p>

                <p id="link_9">9. هل نجري تحديثات على هذا الإشعار؟ &nbsp;&nbsp;&nbsp;&nbsp;</p>

                <p>قد نقوم بتحديث إشعار الخصوصية هذا من وقت لآخر، ستتم الإشارة إلى الإصدار المحدّث من خلال تاريخ محدّث "منقّح" وستصبح النسخة المحدّثة سارية حالما يمكن الوصول إليها، إذا أجرينا تغييرات جوهرية على إشعار الخصوصية هذا، فقد نخطرك إما عن طريق نشر إشعار واضح بهذه التغييرات أو بإرسال إشعار إليك مباشرةً، نحن نشجعك على مراجعة إشعار الخصوصية هذا بشكل متكرر لتكون على علم بكيفية حماية معلوماتك.</p>

                <p>&nbsp;</p>

                <p id="link_10">10. كيف يمكنك الاتصال بنا بخصوص هذا الإشعار؟ &nbsp;&nbsp;&nbsp;&nbsp;</p>

                <p>إذا كانت لديك أسئلة أو تعليقات حول هذا الإشعار، فيمكنك مراسلتنا عبر البريد الإلكتروني على  <a href="mailto:info@oscargrandstores.com"> info@oscargrandstores.com</a></p>

                <p>&nbsp;</p>'
                </p>
            </div>
            <!-- <div class="col-md-3">
                <img src="{{ asset('images/about1.gif') }}" alt="">
            </div> -->
        </div>
    </div>
</div>