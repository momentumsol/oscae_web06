@extends('website.layouts.app')

<style>
	.product-area  .image{
		z-index: -1;
		position: absolute;
		left: 0;
		top: -100px;
	}
	.content .content-overlay {
  background: rgba(0,0,0,0.7);
  position: absolute;
  height: 99%;
  width: 92%;
  left: 15px;
  top: 0;
  bottom: 0;
  right: 0;
  opacity: 0;
  -webkit-transition: all 0.4s ease-in-out 0s;
  -moz-transition: all 0.4s ease-in-out 0s;
  transition: all 0.4s ease-in-out 0s;
}

.content:hover .content-overlay{
  opacity: 1;
}

.content-image{
  width: 100%;
}

.content-details {
  position: absolute;
  text-align: center;
  padding-left: 1em;
  padding-right: 1em;
  width: 100%;
  top: 50%;
  left: 50%;
  opacity: 0;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  -webkit-transition: all 0.3s ease-in-out 0s;
  -moz-transition: all 0.3s ease-in-out 0s;
  transition: all 0.3s ease-in-out 0s;
}

.content:hover .content-details{
  top: 50%;
  left: 50%;
  opacity: 1;
}

.content-details h3{
  color: #fff;
  font-weight: 500;
  letter-spacing: 0.15em;
  margin-bottom: 0.5em;
  text-transform: uppercase;
}

.content-details p{
  color: #fff;
  font-size: 0.8em;
}

.fadeIn-bottom{
  top: 80%;
}



.fadeIn-right{
  left: 80%;
}

.single-product .product-img .product-action .Wishlist:hover {
	color:#C5171C !important;
}

.showcolorheart:hover{
	color: #C5171C
}
.ti-heart:hover{
color: red !important
}


</style>
@section('content')
<?php $lang= app()->getLocale();?>

<!-- Start Product Area -->
<div class="product-area section">
	<div class="container">
		
		<div class="row">
			<div class="col-12">
				<div class="product-info">
					
					<div class="tab-content" id="myTabContent">
						<!-- Start Single Tab -->
						<div class="tab-pane fade show active" id="man" role="tabpanel">
							<div class="tab-single">
								@isset($products)
								<div class="row justify-content-center" >
									@foreach ($products as $product)
										<div class="col-xl-3 col-lg-4 col-md-4 col-12">
											<div class="single-product">
												
												<div class="product-img">
													<a  href="{{ url(app()->getLocale().'/show_product/'.$product->id) }}">
<img  style="background: white;" src="{{ $product->images[0]->src }}"  onerror="this.src='https://oscarstores.com/images/not_found_image.jpeg'">
													</a>
													<div class="button-head">
														<div class="product-action">
															<a data-toggle="modal" data-target="#exampleModal{{ $product->id }}" title="Quick View" href="#"><i class=" ti-eye"></i><span>Quick Shop</span></a>
																@isset($pros)
																	{{-- @if(in_array($product->id,$pros)) --}}
																	<a  class="Unlike" title="{{ $product->id }}" href="#" style="display: {{ in_array($product->id,$pros) ? 'block' : 'none' }};margin-right: 0;margin-left: 15px;">
																		<i class=" ti-heart liked"></i>
																		<span>Remove From Wishlist</span>
																	<a>
																	{{-- @else --}}
																	<a  class="Wishlist" title="{{ $product->id }}" href="#" style="display: {{ in_array($product->id,$pros) ? 'none' : 'block' }}">
																		<i class=" ti-heart "></i>
																		<span>Add to Wishlist</span>
																	</a>
																	{{-- @endif --}}
																@endisset
														</div>
														<div class="product-action-2">
															<a class="cart" unit={{ $product->PriceUnit }}  title="{{ $product->id }} " href="#">{{ __('lang.add_to_cart')}}</a>
														</div>
													</div>
												</div>
												<div class="product-content">
													<span >{{ $product->name }}</span>
													<div class="product-price" >
														<span>{{ __('lang.EGP') }} {{ $product->regular_price }}</span>
													</div>
													
												</div>
											</div>
										</div>
										@endforeach
										
                                </div>
								<div class="row" style="justify-content:" >
						{{ $products->links() }}
						</div>
                                
                                @endisset
                                
                                @isset($res)
                                <div class="row"> 
									@foreach ($res as $product)
										<div class="col-xl-3 col-lg-4 col-md-4 col-12">
											<div class="single-product">
												
												<div class="product-img">
													<a href="#"  data-toggle="modal" data-target="#exampleModal{{ $product->id }}" title="Quick View">
<img  style="background: white;" src="{{ $product->images[0]->src }}"  onerror="this.src='https://oscarstores.com/images/not_found_image.jpeg'">
													</a>
													<div class="button-head">
														<div class="product-action">
															<a data-toggle="modal" data-target="#exampleModal{{ $product->id }}" title="Quick View" href="#"><i class=" ti-eye"></i><span>Quick Shop</span></a>
																@isset($pros)
																	{{-- @if(in_array($product->id,$pros)) --}}
																	<a  class="Unlike" title="{{ $product->id }}" href="#" style="display: {{ in_array($product->id,$pros) ? 'block' : 'none' }};margin-right: 0;margin-left: 15px;">
																		<i class=" ti-heart liked"></i>
																		<span>Remove From Wishlist</span>
																	<a>
																	{{-- @else --}}
																	<a  class="Wishlist" title="{{ $product->id }}" href="#" style="display: {{ in_array($product->id,$pros) ? 'none' : 'block' }}">
																		<i class=" ti-heart "></i>
																		<span>Add to Wishlist</span>
																	</a>
																	{{-- @endif --}}
																@endisset
														</div>
														<div class="product-action-2">
															<a class="cart" unit={{ $product->PriceUnit }}  title="{{ $product->id }} " href="#">{{ __('lang.add_to_cart')}}</a>
														</div>
													</div>
												</div>
												<div class="product-content">
													<span >{{ $product->name }}</span>
													<div class="product-price" >
														<span>{{ __('lang.EGP') }} {{ $product->regular_price }}</span>
													</div>
													
												</div>
											</div>
										</div>
										@endforeach
										
								</div>
								
                                @endisset 
                                @if (count($products)<1)
                                    <h1 style="text-align: center"> No Match Product</h1>
                                @endif
							</div>
							
						</div>
						<div class="row" style="justify-content:" >
							{{-- {{ $products->links() }} --}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Product Area -->
@if (isset($products))
@foreach ($products as $product)
<div class="modal fade" id="exampleModal{{$product->id}}" tabindex="-1" role="dialog" style="margin-top: 50px; overflow: unset;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close"
						aria-hidden="true"></span></button>
			</div>
			<div class="modal-body" style="overflow: initial;">
				<div class="row no-gutters">
					<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
						<!-- Product Slider -->
						<div class="product-gallery">
							<div class="quickview-slider-active">
								<div class="single-slider">
<img  style="background: white;" src="{{ $product->images[0]->src }}"  onerror="this.src='https://oscarstores.com/images/not_found_image.jpeg'">
								</div>
								
							</div>
						</div>
						<!-- End Product slider -->
					</div>
					<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="background-color: #ccc">
						<div class="quickview-content">
							<h2 style="margin-top: 100px;font-size: large;">{{ $product->name  }}</h2>
								<h6><span style="font-weight:bold;font-size:small">{{ __('lang.category')}} :</span>{{ $product->categories_all->name->$lang }}</h6>
								<div class="product-price" style="font-size: small">
									<span style="font-weight:bold;font-size:small">{{ __('lang.unite_price')}} : </span>
									<span class="old">{{   $product->regular_price}} {{ __('lang.EGP') }}</span>

								</div>
								@if (isset($product->discription  ))
								<div class="quickview-peragraph">
									<p>{{$product->discription }}</p>
								</div>
								@endif
								@if($product->PriceUnit=='kg')
								<div>
								<select name="weight" id="{{$product->id}}_weights">
								<option value="250" selected>250g</option>
								<option value="500">500g</option>
								<option value="750">750g</option>
								<option value="1000">1.00kg</option>
								<option value="1250">1.25kg</option>
								<option value="1500">1.50kg</option>
								<option value="1750">1.75kg</option>
								<option value="2000">2.00kg</option>
								<option value="2250">2.25kg</option>
								<option value="2500">2.50kg</option>
								</select>
								</div>
								@endif
							
								<div class="quantity my-3">
									<!-- Input Order -->
									<div class="input-group">
										<div class="button minus"  id="{{ $product->PriceUnit }}">
											<button type="button" class="btn remove btn-number"  disabled="disabled"
												data-type="minus" data-field="quant[1]" style="height:33px">
												<i class="ti-minus"></i>
											</button>
										</div>
										<input type="text" name="quant[1]" class="input-number qty" data-min="1"
											data-max="9000" value="{{ $product->PriceUnit =='Quantity'?'1' : '1' }}" style="border:0">
										<div class="button plus" id="{{ $product->PriceUnit }}">
											<button id="{{  $product->id }}" type="button" class="btn add  btn-number"   data-type="plus"
												data-field="quant[1]" style="height:33px" data={{ $product }}>
												<i class="ti-plus"></i>
											</button>
										</div>
									</div>
									<!--/ End Input Order -->
								</div>
								<div class="add-to-cart mt-2" style="display: flex;justify-content: space-between;align-items: center;">
									<a class="quickcart" onclick="quickcart(this,event)" title="{{ $product->id }}" href="#" class="btn" style="background-color: #333;color:#fff;padding: 10px 42px;" >{{ __('lang.add_to_cart')}}</a>
									@if(auth()->guard('customerForWeb')->user())
										<a  class="Wishlist" title="{{ $product->id }}" href="#" class="btn min"  style="display: {{ in_array($product->id,$pros) ? 'none' : 'block' }};background-color: #333;color:#fff;padding:0 10px"><i class="ti-heart" style="font-weight: bold;color: #fff;margin:auto; line-height: 43px"></i></a>
										<a  class="Unlike" title="{{ $product->id }}" href="#" class="btn min"  style="display: {{ in_array($product->id,$pros) ? 'block' : 'none' }};background-color: #333;color:#fff;padding:0 10px"><i class="ti-heart" style="font-weight: bold;color: #fff;margin:auto; line-height: 43px"></i></a>
									@endif
								
								</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal end -->
@endforeach
@endif
@endsection

@push('scripts')
	<script type="text/javascript">

	
	

let url='';


$.ajaxSetup({

headers: {

	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

}

});

$(".Wishlist").click(function(e){
	e.preventDefault();
	
	$('#alert-wishlist').show();
	$('#alert-wishlist').html('added to wishlist succssfully');
	
	setTimeout(function() { 
			$('#alert-wishlist').fadeOut('fast'); 
		}, 2000);

	var id = $(this).attr("title");

	$.ajax({
	   type:'POST',
	   url:url+'/oscar06/public/{{app()->getLocale()}}/homeWeb/wishList',

	   data:{ id:id },

	   success:function(data){
			// var newProduct = '<li id="'+data.id+'">' +
			// 						'<a href="" class="remove" title="Remove this item">' +
			// 							'<i class="fa fa-remove"></i>' +
			// 						'</a>' +
			// 						'<a class="cart-img" href="#"><img src="'+data.images[0]['src']+'" ></a>' +
			// 						'<h4><a href="#">'+data.name+'</a></h4>' +
			// 						'<p class="quantity"> <span class="amount">'+data.regular_price+'</span></p>' +
			// 				'</li>' ;

			// 		$(".shopping-list").append(newProduct);

		}	
	});
	
	if(! $(this).hasClass('quick')){

$(this).parent().find('.Wishlist').css('display','none');
$(this).parent().find('.Unlike').css('display','block');

}else{
// $(this).removeClass('Wishlist');
// $(this).addClass('Wishlist');
}



var count=$('.wishlist-total-count').html();
$('.wishlist-total-count').html(Number(count)+Number(1));

});

$(".Unlike").click(function(e){
	e.preventDefault();
	e.preventDefault();
			$('#alert-wishlist').show();
			$('#alert-wishlist').html('removed from wishlist succssfully');
			
			setTimeout(function() { 
                    $('#alert-wishlist').fadeOut('fast'); 
                }, 2000);
	

	var id = $(this).attr("title");

	$.ajax({
	   type:'POST',
	   url: url+'/oscar06/public/{{app()->getLocale()}}/homeWeb/unLike',

	   data:{ id:id },

	   success:function(data){
		//    console.log(data);
		$('li#'+data).remove();
	}

	});
	
	$(this).parent().find('.Wishlist').css('display','block');
	$(this).parent().find('.Unlike').css('display','none');
	
	var count=$('.wishlist-total-count').html();
	$('.wishlist-total-count').html(Number(count)-Number(1));
});

var products= [];
var local_storage=[];

if( !JSON.parse(localStorage.getItem("cart"))){
	var cart =[];
	localStorage.setItem('cart', JSON.stringify(cart));
}
$(".cart").click(function(e){
	


	e.preventDefault();
	var unit=$(this).attr('unit');

	var id = $(this).attr("title");

	$.ajax({
	type:'POST',
	
	url:url+'/oscar06/public/{{app()->getLocale()}}/homeWeb/add-to-cart',

	data:{ id:id },
	success:function(data){
		if(unit == 'kg'){
						let weight_id=id+'_weights'
					let weight=document.getElementById(weight_id).value
					data['weight']=weight;
						data['qty']=1;
					}else{
					data['weight']=1;
					data['qty']=1;
					}
			var res=jQuery.inArray(id,localStorage);
			var key='product_'+data.id;
			var storage=localStorage['cart'];
			if(storage.includes(key)){
				$('#alert-cart').show();
				$('#alert-cart').html(data.name+' already in cart ');
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
			}else{
				addToCart(data);
				$('#alert-cart').show();
				$('#alert-cart').html('added to cart succssfully');
				setTimeout(function() { 
						$('#alert-cart').fadeOut('fast'); 
				}, 2000);
				$('.total-count').html('');
				var total=JSON.parse(localStorage.getItem("cart"));
				$('.total-count').html(JSON.parse(total.length));
			}
		}	
	});
	
});



function addToCart(product) {
	if (localStorage) {
		var cart;
		if (!localStorage['cart']) cart = [];
		else cart = JSON.parse(localStorage['cart']);            
		if (!(cart instanceof Array)) cart = [];
		var key ='product_'+product.id;
		var obj = {};
		obj[key] = product;
		cart.push(obj);
		localStorage.setItem('cart', JSON.stringify(cart));
	} 
}

$(".add").click(function(){


var product_id=$(this).attr('id');
var localStorage_key='product_'+product_id;
var qty=$(this).parent().parent().find("input").val();
var total=0;
var total_price=parseInt($(this).parent().parent().parent().parent().find("#total_price").html());

// total=qty*total_price+total_price;
// total_price.html(total);

// Get the existing data
// Add new data to localStorage Array


});

function quickcart(elem,event){
	event.preventDefault();
	var id=elem.title;
		var qty= $(elem).parent().parent().find('.quantity').find('input').val();
		var storage=JSON.parse(localStorage.getItem("cart"));

		for (let i = 0; i< storage.length; i++) {
			var pro=storage[i];
			var key=Object.keys(pro);
			if(key== 'product_'+id){
				storage.splice(i, 1); 
				var cart =[];
				localStorage.setItem('cart', JSON.stringify(cart));
				localStorage.setItem('cart', JSON.stringify(storage));
				localStorage.removeItem(key);
			}
		}

$.ajax({
type:'POST',

url: url+'/oscar06/public/{{app()->getLocale()}}/homeWeb/add-to-cart',

data:{ id:id },
success:function(data){
	data['qty']=qty;

	if(data.PriceUnit=='kg')
	{
		let weights_id=id+'_weights'
		data['weight']=document.getElementById(weights_id).value
	}
	else
	{
		data['weight']=1
	}
	var res=jQuery.inArray(id,localStorage);
	var key='product_'+data.id;
	var storage=localStorage['cart'];
	if(storage.includes(key)){
		$('#alert-cart').show();
		$('#alert-cart').html(data.name+' already in cart ');
		setTimeout(function() { 
				$('#alert-cart').fadeOut('fast'); 
		}, 2000);
	}else{
		
		addToCart(data);
		$('#alert-cart').show();
		$('#alert-cart').html('added to cart succssfully');
		setTimeout(function() { 
				$('#alert-cart').fadeOut('fast'); 
		}, 2000);
		$('.total-count').html('');
		var total=JSON.parse(localStorage.getItem("cart"));
		$('.total-count').html(total.length);

	}
}
});
// $(elem).parent().parent().find('.quantity').find('input').val(1)
var modal=$(elem).closest(".modal").modal("hide");

}

	
    </script>
    
@endpush
