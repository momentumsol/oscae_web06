@extends('website.layouts.app')

@push('style')
	<style>
		td{
		text-align:center !important
	}
	</style>
@endpush

@section('content')

		
	 <!--==============================
    Cart Area
    ==============================-->
    <div class="vs-cart-wrapper  space-top space-md-bottom">
        <div class="container">
            {{-- <div class="woocommerce-notices-wrapper">
                <div class="woocommerce-message">Shipping costs updated.</div>
            </div> --}}
            {{-- <form action="#" class="woocommerce-cart-form"> --}}
                <table class="cart_table table shopping-summery">
                    <thead>
                        <tr>
							<th>{{ __('lang.product')}}</th>
                            <th>{{ __('lang.name')}}</th>
                            <th class="text-center">{{ __('lang.unite_price')}}</th>
                            <th class="text-center">{{ __('lang.quantity')}}</th>
                            <th class="text-center">{{ __('lang.total')}}</th> 
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- <tr class="cart_item">
                            <td data-title="Product">
                                <a class="cart-productimage" href="#"><img width="91" height="91" src="assets/img/cart/cat-img-1.png" alt="Image"></a>
                            </td>
                            <td data-title="Name">
                                <a class="cart-productname" href="#">Parmesan Vegetable</a>
                            </td>
                            <td data-title="Price">
                                <span class="amount"><bdi><span>$</span>18</bdi></span>
                            </td>
                            <td data-title="Quantity">
                                <div class="quantity">
                                    <button class="quantity-minus qut-btn"><i class="far fa-minus"></i></button>
                                    <input type="number" class="qty-input" value="1" min="1" max="99">
                                    <button class="quantity-plus qut-btn"><i class="far fa-plus"></i></button>
                                </div>
                            </td>
                            <td data-title="Total">
                                <span class="amount"><bdi><span>$</span>18</bdi></span>
                            </td>
                            <td data-title="Remove">
                                <a href="#" class="remove"><i class="fal fa-trash-alt"></i></a>
                            </td>
                        </tr> --}}
						<tfoot>
							<tr>
								<td colspan="6" class="" style="text-align: right !important;">
									{{-- <div class="vs-cart-coupon">
										<input type="text" class="form-control" placeholder="Coupon Code...">
										<button type="submit" class="vs-btn rounded-1 shadow-none">Apply Coupon</button>
									</div> --}}
									{{-- <button type="submit" class="vs-btn style2 rounded-1 shadow-none">Update cart</button> --}}
									<ul style="list-style:none">
										<li style="font-weight: bolder;">{{ __('lang.cart_sub_total')}} <span style"" id="total_price">  </span> </li>
@if (Auth::guard('customerForWeb')->user())
<li><a id="setprice" style="z-index:99999;width:100%;" href="{{ url(app()->getLocale().'/checkout') }}" class="vs-btn rounded-1 shadow-none">{{ __('lang.check_out')}} </a></li>
@else
<li><a href="{{ route('login',app()->getLocale()) }}" class="vs-btn rounded-1 shadow-none"> {{ __("lang.You should Login first") }}</a></li>
@endif

									</ul>
								</td>
							</tr>

						</tfoot>
                    </tbody>
                </table>

            {{-- </form> --}}
            {{-- <div class="row justify-content-end">
                <div class="col-md-8 col-lg-7 col-xl-6">
                    <h2 class="h4 summary-title">Cart Totals</h2>
                    <table class="cart_totals">
                        <tbody>
                            <tr>
                                <td>Cart Subtotal</td>
                                <td data-title="Cart Subtotal">
                                    <span class="amount"><bdi><span>$</span>47</bdi></span>
                                </td>
                            </tr>
                            <tr class="shipping">
                                <th>Shipping and Handling</th>
                                <td data-title="Shipping and Handling">
                                    <ul class="woocommerce-shipping-methods list-unstyled">
                                        <li>
                                            <input type="radio" id="free_shipping" name="shipping_method" class="shipping_method">
                                            <label for="free_shipping">Free shipping</label>
                                        </li>
                                        <li>
                                            <input type="radio" id="flat_rate" name="shipping_method" class="shipping_method" checked="checked">
                                            <label for="flat_rate">Flat rate</label>
                                        </li>
                                    </ul>
                                    <p class="woocommerce-shipping-destination">
                                        Shipping options will be updated during checkout.
                                    </p>
                                    <form action="#" method="post">
                                        <a href="#" class="shipping-calculator-button">Select from your saved address</a>
                                        <div class="shipping-calculator-form">
                                            <p class="form-row">
                                                <select class="form-select">
                                                    <option value="AR">Argentina</option>
                                                    <option value="AM">Armenia</option>
                                                    <option value="BD" selected="selected">Bangladesh</option>
                                                </select>
                                            </p>
                                            <p>
                                                <select class="form-select">
                                                    <option value="">Select an option…</option>
                                                    <option value="BD-05">Bagerhat</option>
                                                    <option value="BD-01">Bandarban</option>
                                                    <option value="BD-02">Barguna</option>
                                                    <option value="BD-06">Barishal</option>
                                                </select>
                                            </p>
                                            <p class="form-row">
                                                <input type="text" class="form-control" placeholder="Town / City">
                                            </p>
                                            <p class="form-row">
                                                <input type="text" class="form-control" placeholder="Postcode / ZIP">
                                            </p>
                                            <p>
                                                <button class="vs-btn shadow-none rounded-1">Update</button>
                                            </p>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr class="order-total">
                                <td>Order Total</td>
                                <td data-title="Total">
                                    <strong><span class="amount"><bdi><span>$</span>47</bdi></span></strong>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="wc-proceed-to-checkout mb-30">
                        <a href="#" class="vs-btn rounded-1 shadow-none">Proceed to checkout</a>
                    </div>
                </div>
            </div> --}}
        </div>
		{{-- <div class="row">
            <div class="col-12">
                <!-- Total Amount -->
                <div class="total-amount">
                    <div class="row">
                        
                        <div class="offset-lg-8 offset-md-5 col-lg-4 col-md-7 col-12">
                            <div class="right">
                                <ul>
                                    <li>{{ __('lang.cart_sub_total')}}<span id="total_price"> </span></li>
                                    
                                </ul>
                                <div class="button5" >

                                    @if (Auth::guard('customerForWeb')->user())
                                        <a id="setprice" href="{{ url(app()->getLocale().'/checkout') }}" class="btn">
                                        {{ __('lang.check_out')}}
                                        </a>
                                    @else
                                    <div >
                                        <button class="mb-5" id="big" type="button" role="button" data-toggle="modal" data-target="#modalLRForm" >
                                            You should Login first
                                        </button>
                                      

                                    </div>
                                    @endif
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ End Total Amount -->
            </div>
        </div> --}}
    </div>

@endsection

@push('scripts')

<script type="text/javascript">


// Closure
(function() {
  /**
   * Decimal adjustment of a number.
   *
   * @param {String}  type  The type of adjustment.
   * @param {Number}  value The number.
   * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
   * @returns {Number} The adjusted value.
   */
  function decimalAdjust(type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }
})();

function addToCartAjax(id,weight,qty)
{
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/{{app()->getLocale()}}/homeWeb/addToCart',
	data:{ product_id:id,weight:weight,quantity:parseInt(qty) },

	success:function(data){
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);

	}	
});
}
var total_price=0.00;
var storage=JSON.parse(localStorage.getItem("cart"));
var cartArray = <?php echo json_encode($proArray); ?>;
//  console.log(cartArray,cartArray.length,'cartArray')
if(cartArray.length==0)
{
	$("#setprice").remove();

}
var lang=<?php echo json_encode(app()->getLocale()); ?>;

for (var i = 0; i < cartArray.length; i++){

	var pro=cartArray[i];

	var key=Object.keys(pro)[0];
	var cartProducts = [];
	var id=pro.id;
	cartProducts.push(id);
	var name=pro.name;

	if(lang=='ar')
	{
		name=pro.name_ar;
	}
	var unit=pro.PriceUnit;
	if(unit=='kg')
	{
		name=name+' ('+pro.weight+'g)'
	}

	if(pro.images[0]['src']){
	
		var src=pro.images[0]['src'];
		src=src.replace("http://34.105.27.34/oscar/public", "https://cms.oscarstoresapp.com");
		console.log(pro.images[0]['src'],src)
	}else{
		var src ='';
	}
	var price=pro.regular_price;
	var original_price=pro.regular_price;

	var qty=pro.quantity;
	if(!qty){
		qty=1;
	}
	if(parseInt(pro.discountprice) !==0)
	{
		price=pro.discountprice
		original_price=pro.discountprice
	}
	if(unit=='kg')
	{
		price=price*pro.weight/1000
	}
	var weight=pro.weight
	
	total_price = Number(price*Number(qty).toFixed(2)) + Number(total_price.toFixed(2));
	
 	var table_row='<tr id="'+id+'">' +
					'<td class="image" data-title="{{ __('lang.image')}}"><img width="100" src="'+src+'" alt="#"></td>' +
					'<td class="product-des" data-title="{{ __('lang.name')}}">' +
						'<p class="product-name"><a href="#">'+name+'</a></p>' +
					'</td>'+
					'<td class="price" data-title="{{ __('lang.unite_price')}}"> <span class="regular_price"> '+Number(original_price)+' {{ __('lang.EGP') }}</span></td>'+
					'<td class="qty" data-title="{{ __('lang.quantity')}}">'+
						'<div class="input-group" style="display: flex;  justify-content: center;">'+

						'<input type="hidden"  value="'+weight+'" id="'+id+'_weight">'+
											'<button type="button" class="remove btn  btn-number quantity-minus qut-btn"  data-type="minus" data-field="quant[2]" style="padding:5px">'+
												'<i class="far fa-minus" style="line-height: 0;font-size: inherit;"></i>'+
											'</button>'+

											
											'<input type="text" unit="'+unit+'" class="qty" value="'+qty+'" style="width: 20%;border: 0;text-align: center;" readonly>'+
											'<button type="button"  class="add btn quantity-plus qut-btn  btn-number" data-type="plus" data-field="quant[2]" style="padding:5px">'+
												'<i class="far fa-plus" style="line-height: 0;font-size: inherit;"></i>'+
											'</button>'+
						'</div>'+
						'<!--/ End Input Order -->'+
					'</td>'+
					
					'<td class="product_total_pice" data-title="{{ __('lang.cart_sub_total')}}">'+Number(price*Number(qty)).toFixed(2)+'</td>'+
					'<td class="action" data-title="{{ __('lang.remove')}}"><a href="#"><i class="fal fa-trash-alt"></i></a></td>'+
				'</tr>';

				
	
		$('tbody').append(table_row);	

								

}
	$("#total_price").html(Number(total_price.toString().match(/^\d+(?:\.\d{0,2})?/)) +" {{ __('lang.EGP') }}");
	$(".add").click(function(){
		var qty=$(this).prev("input").val();

		var proUnit=$(this).prev("input").attr('unit');
		
			parseInt(qty);
			qty++;
		
		 $(this).prev("input").val(qty);

		$('.remove').prop("disabled", false);
		var product_id=$(this).closest('tr').attr('id');
		var localStorage_key='product_'+product_id;
		var regular_price=$(this).closest('tr').find('.regular_price').text();
		var product_total_pice =$(this).closest('tr').find('.product_total_pice');
		regular_price=regular_price.replace("EGP","");
		let weight =250
		regular_price=parseFloat(regular_price)
		// product_total_pice=regular_price * qty;

		if(proUnit=='kg')
		{
			let weight_id=product_id+'_weight'

			let weight_total=parseInt(document.getElementById(weight_id).value)
			weight=weight_total
			product_total_pice=(regular_price*weight_total/1000) * qty;
			regular_price = regular_price*weight_total/1000
		

		
		}
		else

		{
			product_total_pice = regular_price * qty;
		}
	
		$(this).closest('tr').find('.product_total_pice').html(Math.ceil10(product_total_pice.toFixed(1),-1));
		total_price += parseFloat(regular_price);
		$("#total_price").html(total_price.toFixed(2));
	
		// Get the existing data
		// var existing =JSON.parse(localStorage.getItem(localStorage_key));
      
		 for (var i=0;i<storage.length;i++)
		 {
			let item=storage[i]
			// console.log(Object.keys(item)[0],localStorage_key)
			// console.log('product_'+Object.keys(item)[0],localStorage_key)
        if(Object.keys(item)[0]==localStorage_key)
        {
			// alert('a')
			item[Object.keys(item)[0]]['quantity']=qty
			localStorage.setItem('cart', JSON.stringify(storage));
			
         
        }
		 }
	
		 addToCartAjax(product_id,weight,1)	

	});
	$(".remove").click(function(){
	

		var product_id=$(this).closest('tr').attr('id');
		var localStorage_key='product_'+product_id;
		var qty=$(this).next("input").val();

		var proUnit=$(this).next("input").attr('unit');
		var price=$(this).closest('tr').find('.product_total_pice').html();
		let weight=250;
		var unit_price=$(this).closest('tr').find('.regular_price').text();
		unit_price=unit_price.replace("EGP","");
		unit_price=unit_price.replace("جنيه","");

		unit_price=parseFloat(unit_price)
			if ( qty == 1) {

			$(this).prop("disabled", true);
			}
	
		else if ( qty >= 1) {

			qty--;
			$(this).next("input").val(qty);
			if(proUnit =='kg')
			{
				let weight_id_remove=product_id+'_weight'
				let weight_total_remove=parseInt(document.getElementById(weight_id_remove).value)
				weight=weight_total_remove
				let total_unite_price=parseFloat(unit_price)*(weight_total_remove/1000)
				$(this).closest('tr').find('.product_total_pice').html((price - total_unite_price).toFixed(1));
				total_price -= total_unite_price;
			  $("#total_price").html(total_price.toFixed(1) +" {{ __('lang.EGP') }}");
		

			}
			else
			{
				$(this).closest('tr').find('.product_total_pice').html((price - unit_price).toFixed(1));
				total_price =total_price - unit_price;
				$("#total_price").html(total_price.toFixed(1) +" {{ __('lang.EGP') }}");
			}
		
			// Get the existing data
			for (var i=0;i<storage.length;i++)
		 {
			let item=storage[i]
        if(Object.keys(item)[0]==localStorage_key)
        {
			item[Object.keys(item)[0]]['quantity']=qty

			localStorage.setItem('cart', JSON.stringify(storage));
			
         
        }
		 }

		}
		else {
			$(this).prop("disabled", true);
		}	

		addToCartAjax(product_id,weight,-1)	
	});

	$('.action').click(function (e) {
	
		e.preventDefault();
storage.length
		x=$(this).closest('tr').find('.product_total_pice').html();
		total_price =parseFloat(total_price) - parseFloat(x) ;
		console.log(storage.length,'total_price')
		if(parseFloat(total_price) <= 0)
		{


			$("#total_price").html(0.00+" {{ __('lang.EGP') }}")
		}
		else if(storage.length==1)
		{

			$("#total_price").html(0.00+" {{ __('lang.EGP') }}")
		}
		else
		{
			$("#total_price").html( total_price.toFixed(1) +" {{ __('lang.EGP') }}");
		}
		$(this).closest('tr').remove();
		var product_id=$(this).closest('tr').attr('id');
		$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	type:'POST',
	url:url+'/{{app()->getLocale()}}/homeWeb/removeFromCart',
	data:{ product_id:product_id },
	success:function(data)
	{
		$('.cart-count2').html('')
		$('.cart-count2').html(data.count);

	}	


});
		for (let i = 0; i< storage.length; i++) {
			var pro=storage[i];
			var key=Object.keys(pro)[0];
			if(key == 'product_'+product_id){
				storage.splice(i, 1); 
				// delete storage[key]
			}
		}

		var cart =[];
		localStorage.setItem('cart', JSON.stringify(cart));
		localStorage.setItem('cart', JSON.stringify(storage));
		localStorage.removeItem('product_'+product_id);

		var total=JSON.parse(localStorage.getItem("cart"));
		$('.total-count').html(JSON.parse(total.length));
	});


	$('#setprice').click(function(){
			var total= $("#total_price").html();
			localStorage.setItem("total_price", total);

	});

</script>

@endpush
