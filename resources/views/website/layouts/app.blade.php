<!DOCTYPE html>
<html lang="en">

@include('website.partials.head')


<body class="js">
    @php
        $logo = '';
        if ($branch->store_id == '02') {
            $logo = 'branches/Oscar final logo-1.png';
        } elseif ($branch->store_id == '01' || $branch->store_id == '05') {
            $logo = 'branches/Oscar stores logo copy.png';
        } else {
            $logo = 'branches/Oscar signature.png';
        }
    @endphp







    @php
        $lang = app()->getLocale();
        $arr = [];
        if (
            auth()
                ->guard('customerForWeb')
                ->user()
        ) {
            $store_id = $branch->store_id;
            $stockId = 'in_stock_';
            switch ($store_id) {
                case 01:
                    $stockId .= 1;
                    break;
                case 02:
                    $stockId .= 2;
                    break;
                case 04:
                    $stockId .= 4;
                    break;
                case 05:
                    $stockId .= 5;
                    break;
                case 06:
                    $stockId .= 6;
                    break;
                case 07:
                    $stockId .= 7;
            }
            $auth = count(
                auth()
                    ->guard('customerForWeb')
                    ->user()
                    ->cart->where($stockId, 1)
                    ->unique('barcode')
                    ->values()
            );
        } else {
            $auth = 0;
        }
    @endphp

    <!--==============================
        Header Top
    ==============================-->

    <div class="header-top-wrapper header-top-v3">
        <div class="container">
            <div class="row justify-content-center justify-content-md-between align-items-center">
                <div class="">
                    <div class="head-top-links">
                        <ul class="w-100">
                            <li class="fl-left">

                                <!-- Dropdown -->
                                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fas fa-globe"></i>
                                    {{ strtoupper($lang) }}

                                </a>

                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink1">
                                    <li>

                                        <div>
                                            @foreach (config('app.available_locales') as $locale)
                                                <?php
                                                if ($locale == 'ar') {
                                                    $local_lang = 'ع';
                                                } else {
                                                    $local_lang = 'EN';
                                                }
                                                ?>
                                                @if (is_null(\Illuminate\Support\Facades\Route::currentRouteName()))
                                                    @if (!is_null(\Request::segment(3)) && \Request::segment(3) == 'branch')
                                                        <?php $id = \Request::segment(4); ?>
                                                        <a class="nav-link"href="{{ url($locale . '/homeWeb/branch/' . $id) }}"
                                                            @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>
                                                            {{ strtoupper($local_lang) }}
                                                        </a>
                                                    @elseif(!is_null(\Request::segment(3)) && \Request::segment(3) == 'all_products')
                                                        <?php $id = \Request::segment(4); ?>
                                                        <a class="nav-link"
                                                            href="{{ url($locale . '/homeWeb/all_products/' . $id) }}"
                                                            @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($local_lang) }}</a>
                                                    @else
                                                        <a class="nav-link" href="{{ route('homeWeb', $locale) }}"
                                                            @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($local_lang) }}</a>
                                                    @endif
                                                @elseif(\Illuminate\Support\Facades\Route::currentRouteName() == 'homeWeb.show')
                                                    <a class="nav-link" <?php $slug = \Request::segment(3);
                                                    $url = 'homeWeb/' . $slug; ?>
                                                        href="{{ url($locale . '/homeWeb/' . $slug) }}"
                                                        @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($local_lang) }}</a>
                                                @elseif(\Illuminate\Support\Facades\Route::currentRouteName() == 'homeWeb.index')
                                                    <a class="nav-link" <?php $slug = \Request::segment(4);
                                                    ?>
                                                        href="{{ url($locale . '/homeWeb/branch/' . $slug) }}"
                                                        @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($local_lang) }}</a>
                                                @elseif(!is_null(\Request::segment(3)) && \Request::segment(3) == 'search_product')
                                                    <?php $search = \Request::query('search'); ?>
                                                    <a class="nav-link"
                                                        href="{{ url($locale . '/homeWeb/search_product?search=' . $search) }}"
                                                        @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($local_lang) }}</a>
                                                @elseif(!is_null(\Request::segment(2)) && \Request::segment(2) == 'address' && \Request::segment(4) == 'edit')
                                                    <?php $id = \Request::segment(3); ?>
                                                    <a class="nav-link"
                                                        href="{{ url($locale . '/address/' . $id . '/edit') }}"
                                                        @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($local_lang) }}</a>
                                                @else
                                                    <a class="nav-link"
                                                        href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), $locale) }}"
                                                        @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>{{ strtoupper($local_lang) }}</a>
                                                @endif
                                            @endforeach
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="fl-right">
                                <!-- Dropdown -->
                                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink2"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fas fa-map-marker-alt mr-2"></i>{{ $branch['title']->$lang }}
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">

                                    @for ($i = 0; $i < count($branches); $i++)
                                        @if ($branches[$i]->store_id != $branch['store_id'])
                                            <li><a id="{{ $branches[$i]->store_id }}" onclick="set_branch(this)"
                                                    href="{{ url(app()->getLocale() . '/homeWeb/branch/' . $branches[$i]->store_id) }}">{{ $branches[$i]->title->$lang }}</a>
                                            </li>
                                        @endif
                                    @endfor
                                </ul>
                            </li>


                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!--========================
    Sticky Header
 ========================-->
    <div class="sticky-header-wrap sticky-header py-2 py-lg-1">
        <div class="container position-relative">
            <div class="row align-items-center">
                <div class="col-5 col-md-3">
                    <div class="logo">
                        <a href="{{ url(app()->getLocale() . '/homeWeb/branch/' . $branch->store_id) }}"><img
                                src="{{ asset($logo) }}" alt="Oscar" width="100"></a>
                    </div>
                </div>
                <div class="col-7 col-md-9 text-end position-static">
                    <nav class="main-menu menu-sticky1 d-none d-lg-block link-inherit">
                        <ul>
                            <li class="menu-item-has-children">
                                <a
                                    href="{{ url(app()->getLocale() . '/homeWeb/branch/' . $branch->store_id) }}">{{ __('lang.home') }}</a>
                            </li>
                            <li> <a href="{{ url(app()->getLocale() . '/categoties') }}" data-content="Link Hover"
                                    aria-hidden="true">{{ __('lang.Categories') }}</a></li>



                            <li class="{{ Request::segment(1) == 'promotions' ? 'active' : ' ' }}">
                                <a
                                    href="{{ url(app()->getLocale() . '/promotions') }}">{{ __('lang.promotions') }}</a>
                            </li>
                        </ul>
                    </nav>
                    <button class="vs-menu-toggle d-inline-block d-lg-none "><i class="far fa-bars"></i></button>
                </div>
            </div>
        </div>
    </div>
    <!--==============================
    Mobile Menu
  ============================== -->
    <div class="vs-menu-wrapper">
        <div class="vs-menu-area">
            <button class="vs-menu-toggle "><i class="fal fa-times"></i></button>
            <div class="mobile-logo">
                <a href="{{ url(app()->getLocale() . '/homeWeb/branch/' . $branch->store_id) }}"><img
                        src="{{ asset($logo) }}" width="120px" alt="Oscar"></a>
            </div>
            <div class="vs-mobile-menu link-inherit">


            </div>
            <!-- Menu Will Append With Javascript -->
        </div>
    </div>
    <!--==============================
        Header Area
    ==============================-->
    @include('website.partials.header')
    <div class="bg-theme  sticky-header ">
        <div class="container ">
            <div class="position-relative">
                <h1 class="close-search" style="display: none;">x</h1>
                <div class="text-body2 d-lg-none d-md-block fl-left" id="icons-header"
                    style="margin: 2.5% 0;display: flex ;align-items: center;">
                    <span class="icon-btn bg3"><a style="display: block !important"
                            href="{{ Auth::guard('customerForWeb')->user() ? url(app()->getLocale() . '/profile') : route('login', $lang) }}"><i
                                class="fal fa-user"></i></a></span>
                    <ul style="display: none">
                        @if (Auth::guard('customerForWeb')->check())
                            <li><a id="account"
                                    href="{{ url(app()->getLocale() . '/profile') }}">{{ __('lang.my_account') }}</a>
                            </li>
                        @else
                            <li><a href="{{ route('login', $lang) }}">Login</a></li>
                            <li><a href="{{ route('register', $lang) }}">Register</a></li>
                        @endif

                    </ul>
                    <div class="modal fade" id="modalLRFormmobile" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel" style="    z-index: 999999;" aria-hidden="true">
                        <div class="modal-dialog cascading-modal" role="document">
                            <!--Content-->
                            <div class="modal-content">

                                <!--Modal cascading tabs-->
                                <div class="modal-c-tabs">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs md-tabs tabs-2  darken-3" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="active" data-toggle="tab"
                                                href="#panel7mobile" role="tab"><i class="fas fa-user mr-1"></i>
                                                {{ __('lang.login') }}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#panel8mobile"
                                                role="tab"><i class="fas fa-user-plus mr-1"></i>
                                                {{ __('lang.register') }}</a>
                                        </li>

                                    </ul>

                                    <!-- Tab panels -->
                                    <div class="tab-content">


                                        <!--Panel 7-->
                                        <div class="tab-pane fade in show active" id="panel7mobile" role="tabpanel">

                                            <!--Body-->
                                            <div class="modal-body mb-1">
                                                <form action="{{ route('web.login', app()->getLocale()) }}"
                                                    method="post" class="needs-validation" novalidate>
                                                    {{ csrf_field() }}
                                                    <div class="md-form form-sm ">
                                                        <i class="fas fa-envelope prefix"></i>
                                                        <input type="email" id="validationCustom032" name="email"
                                                            placeholder="Your email"
                                                            class="form-control form-control-sm validate" required>

                                                        {{-- <label for="validationCustom032"></label> --}}
                                                    </div>

                                                    <div class="md-form form-sm mb-4">
                                                        <i class="fas fa-lock prefix"></i>
                                                        <input type="password" id="modalLRInput11" name="password"
                                                            placeholder="Your password"
                                                            class="form-control form-control-sm validate" required>

                                                        {{-- <label data-error="wrong" data-success="right"
														for="modalLRInput11"></label> --}}
                                                    </div>
                                                    <div class="text-center form-sm mt-2">
                                                        <button class="btn btn-info"
                                                            type="submit">{{ __('lang.login') }} <i
                                                                class="fas fa-sign-in ml-1"></i></button>
                                                    </div>
                                                </form>
                                            </div>
                                            <!--Footer-->
                                            <div class="modal-footer">
                                                <div class="options text-center text-md-right mt-1">
                                                    <p> <a href="{{ route('web.forgot_password', app()->getLocale()) }}"
                                                            class="blue-text">{{ __('lang.forget_password') }}?</a>
                                                    </p>
                                                </div>
                                                <button type="button" style="padding: 10px"
                                                    class="btn btn-outline-info waves-effect ml-auto"
                                                    data-dismiss="modal">{{ __('lang.close') }}</button>
                                            </div>

                                        </div>
                                        <!--/.Panel 7-->

                                        <!--Panel 8-->
                                        <div class="tab-pane fade" id="panel8mobile" role="tabpanel">

                                            <!--Body-->
                                            <div class="modal-body">
                                                <form action="{{ route('web.register', app()->getLocale()) }}"
                                                    method="post" class="needs-validation" novalidate>
                                                    {{ csrf_field() }}
                                                    <div class="md-form form-sm mb-1">
                                                        <i class="fas fa-user prefix"></i>
                                                        <input type="text" name="name"
                                                            class="form-control form-control-sm validate"
                                                            placeholder="Your Name" value="{{ old('name') }}">

                                                    </div>
                                                    <div class="md-form form-sm mb-1">
                                                        <i class="fas fa-mobile prefix"></i>
                                                        <input type="number" name="phone"
                                                            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                            type="number" maxlength="12"
                                                            class="form-control form-control-sm validate"
                                                            placeholder="Your Phone" value="{{ old('phone') }}">

                                                    </div>

                                                    <div class="md-form form-sm mb-1">
                                                        <i class="fas fa-envelope prefix"></i>
                                                        <input type="email" id="email" name="email"
                                                            class="form-control form-control-sm validate"
                                                            placeholder="Your email" required
                                                            value="{{ old('email') }}">

                                                    </div>

                                                    <div class="md-form form-sm mb-1">
                                                        <i class="fas fa-lock prefix"></i>
                                                        <input type="password" name="password"
                                                            class="form-control form-control-sm validate"
                                                            placeholder="Your password">

                                                    </div>

                                                    <div class="md-form form-sm mb-1">
                                                        <i class="fas fa-lock prefix"></i>
                                                        <input type="password" name="password_confirmation"
                                                            class="form-control form-control-sm validate"
                                                            placeholder="Repeat password">
                                                        {{-- <label data-error="wrong" data-success="right"
														for="modalLRInput14"></label> --}}
                                                    </div>

                                                    <div class="text-center form-sm mt-2">
                                                        <button class="btn btn-info" type="submit">Sign up <i
                                                                class="fas fa-sign-in ml-1"></i></button>
                                                    </div>
                                                </form>

                                            </div>
                                            <!--Footer-->
                                            <div class="modal-footer"
                                                style="padding-right:1rem !important;padding-left:1rem !important">

                                                <button type="button"
                                                    class="btn btn-outline-info waves-effect ml-auto"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!--/.Panel 8-->
                                    </div>

                                </div>
                            </div>
                            <!--/.Content-->
                        </div>
                    </div>
                    @if (Auth::guard('customerForWeb')->check())
                        <a href="{{ url(app()->getLocale() . '/get_wishlist') }}"
                            class="icon-btn has-badge bg3 mx-2"><i style="font-size: 21px !important"
                                class="fal fa-heart"></i><span id="wishlist-count"
                                class="badge wishlist-total-count">{{ count(auth()->guard('customerForWeb')->user()->wishlist->unique('barcode')->values()) }}</span></a>
                        <a href="{{ route('cart', app()->getLocale()) }}" class="icon-btn has-badge bg2 ms-2 "><i
                                class="fal fa-shopping-cart" style="color: var(--body-color2)"></i><span
                                id="cart-count2"
                                class="badge total-count cart-count2">{{ count(auth()->guard('customerForWeb')->user()->cart->where($stockId, 1)->unique('barcode')->values()) }}</span></a>
                    @endif
                    <a style="color: #fff;margin-left:20px;line-height:2;padding:3px"
                        class="icon-btn has-badge bg2 ms-2 "
                        href="{{ url(app()->getLocale() . '/all_promotions') }}"><img
                            src="{{ asset('images/promotions.svg') }}" alt=""></a>
                </div>
                <div class="col-lg-auto fl-right" id="search-header">
                    <form action="{{ route('search', app()->getLocale()) }}" method="GET"
                        class="header-search style2">
                        <input type="text" name="search" placeholder="{{ __('lang.search_here') }}"
                            class="form-control palceholder-search pleft-5" style="">
                        <button type="submit" class="icon-btn "><i class="far fa-search"></i></button>

                    </form>
                    <button class="icon-btn search-btn-mobile-view"
                        style="display: none ;background:#ccc;width: 30px; height:30px"><i class="far fa-search"
                            style="margin: 0 auto"></i></button>
                </div>
                <div class="col-auto fl-left">
                    <nav class="main-menu menu-style3 mobile-menu-active menu-style2 text-white">
                        <ul>

                            <li class="">
                                <a href="#"><i class="fas fa-bars mr-2"> </i> {{ __('lang.Categories') }}</a>
                                <ul class="sub-menu" style="z-index: 99 !important">

                                    @if (isset($main_categories))
                                        <!-- <div class="category_sections"></div> -->
                                        @include('website.partials.categories')
                                    @endif
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>

            </div>
        </div>
    </div>

    @if (session('reset'))
        <div id="alert-reset" class="alert alert-success" style="position: fixed ;z-index:9999">

            <p>Check Your Email.</p>

        </div>
    @endif
    @if (session('email'))
        <div id="alert-reset" class="alert alert-success" style="position: fixed ;z-index:9999">

            <p> {{ Session::get('email') }}</p>

        </div>
    @endif


    <div id="alert-cart" class="alert alert-success" style="position: fixed ;display:none;z-index:9999"></div>
    <div id="alert-wishlist" class="alert alert-danger" style="position: fixed ;display:none;z-index:9999"></div>
    @yield('content')



    <!--==============================
   Footer Areas

 ==============================-->
    @include('website.partials.footer')



    <!-- Jquery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-migrate-3.0.0.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!-- Popper JS -->
    <!-- <script src="{{ asset('js/popper.min.js') }}"></script> -->
    <!-- Bootstrap JS -->
    <!-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->





    <script>
        $('#ToggleBTN').click(function() {
            // $('.vs-mobile-menu ul .menu-item-has-children').click();
            // $('.vs-mean-expand').first().hide();
            // $('.vs-mobile-menu ul li').next('.vs-mean-expand').click()
            // $('.vs-submenu').show();
            // $('.vs-mean-expand').first().click();


        })
        let = url = '';


        $('.show_sub').click(function() {
            if ($(this).hasClass('fa-plus')) {
                $(this).removeClass('fa-plus');
                $(this).addClass('fa-minus');
                $(this).parent().next().css('display', 'block');

            } else {
                $(this).removeClass('fa-minus');
                $(this).addClass('fa-plus');
                $(this).parent().next().css('display', 'none');
            }
        })



        function set_branch() {

            var cart = [];
            localStorage.setItem('cart', JSON.stringify(cart));
        }

        $(document).ready(function() {

            $('body').on('hidden.bs.modal', function() {

                $('.input-number').val(1);
            });
            var storage = JSON.parse(localStorage.getItem("cart"));
            var cartArray = [];
            var cartArray = <?php echo json_encode($auth); ?>;





            if (storage == null) {
                var cart = [];
                localStorage.setItem('cart', JSON.stringify(cart));
            }
            if (storage == null) {

                $('.cart-count2').css('background', 'transparent');
                $('.cart-count2').hide();
                // $('#cart-count1').css('background', 'transparent');
                // $('#cart-count1').hide();

            } else {
                $('.cart-count2').html(cartArray.length);
                // $('#cart-count1').html(storage.length);


            }

            // $('#cartandwishlist').hide()

            if ($(window).width() < 442) {
                $('#logout').hide();
            }
            if ($(window).width() < 402) {
                $('#account').hide();
            }
            // if($(window).width() <768){

            // 	$('#cartandwishlist').show()
            // }
            getLocation();
            // var x = document.getElementById("demo");
            function getLocation() {

                if (navigator.geolocation) {

                    navigator.geolocation.getCurrentPosition(showPosition);


                }
                
                // else {
                // 	x.innerHTML = "Geolocation is not supported by this browser.";
                // }

            }

            function showPosition(position) {
                setCookie(position.coords.latitude, position.coords.longitude);

            }

            function setCookie(lat, long, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = 'lat' + "=" + lat + ";" + expires + ";path=/";
                document.cookie = 'long' + "=" + long + ";" + expires + ";path=/";
            }


        });
        $(document).ready(function() {

            setTimeout(function() {
                $('#error').fadeOut('fast');
            }, 6000);
            setTimeout(function() {
                $('#alert-reset').fadeOut('fast');
            }, 6000);


        });
    </script>
    <script>
        if (screen.width <= 991) {
            // $('#removeRow').removeClass('row');
            $('#removeRow ul').each(function() {
                $(this).css('width', '100%')
            })
        }
        if (screen.width <= 445) {

            $('.bg-theme ').addClass('sticky');
            $('.sticky-header').removeClass('sticky-header');
            $('.sticky').css({
                'position': "sticky",
                'top': '0',
                'z-index': '99999'
            });

            $('.vs-hero-wrapper ').css('margin-top:50px')

            $('.header-search').hide();
            $('.search-btn-mobile-view').show();
            $('#search-header').css({
                'margin': '2.5% 0'
            })
        } else {
            $('.header-search').show();
            $('.search-btn-mobile-view').hide();
        }

        $('.search-btn-mobile-view').click(function() {
            $(this).hide();
            $('.close-search').show();
            $('.header-search').show();
            $('#search-header').css('margin', '0');
            $('.position-relative .text-body2').hide()
            $('#search-header').css({
                'margin': '0',
                'width': '70%'
            })
            $('#icons-header').attr("style", "display: none !important");

        });
        if (screen.width <= 445) {
            $('.close-search').click(function(e) {
                $('.close-search').hide();


                $('.position-relative .text-body2').show()

                $('.header-search').hide();
                $('.search-btn-mobile-view').show()
                $('#search-header').css({
                    'margin': '2.5% 0',
                    'width': 'auto'
                })
                $('#icons-header').attr("style", "display: flex !important")
                $('.text-body2').css('margin-top', '10px')

            });
        }


        if (screen.width <= 350) {
            $('.text-body2 .icon-btn').css({
                'width': '30px',
                'height': '30px',
            });
            $('.text-body2 .icon-btn i').css({
                'line-height': '1.5',
            });
            $('.text-body2').css('margin', '3% 0');
            $('.bg-theme').css('height', '47px');
            $('.header-search').css('height', '30px');
            $('.header-search button').css({
                'width': '30px',
                'height': '30px',
            });

            $('.fa-search').css({
                'line-height': '1.5',
            });

        }
        if (screen.width <= 400) {}
    </script>




    @stack('scripts')
</body>



</html>
