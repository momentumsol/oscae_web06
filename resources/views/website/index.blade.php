@extends('website.layouts.app')
<style>

	.ti-heart:hover{
color: red !important
}

.slick-slide{
	height: auto !important;
}
.ad-box-area .gx-10{
	margin: 20px !important}

</style>
@section('content')
@isset($error)
	<h1 class="modal_error">{{ $error }}</h1>
@endisset


<meta name="csrf-token" content="{{ csrf_token() }}">

<?php $lang= app()->getLocale();?>
@include('website.partials.home.image_slider')

	@include('website.partials.home.branches')
	

	<div class="ad-box-area">
        <div class="container">
            <div class="row gx-10 my-5" style="margin-right: 0px !important;margin-left: 0 !important;">
				
                <div class=" col-4 mb-10">
                    <div class="vs-ad-box3 mega-hover">
						@if ($banners[0]['link'])
                        	<a   href="{{$banners[0]['link']}}" class="overlay"><span class="sr-only">hole div linkup</span></a>	
						@endif
						@php
				
				$banner_image=str_replace("http://34.105.27.34/oscar/public","https://cms.oscarstoresapp.com",$banners[0]['image']);
				$banner_image_mob=str_replace("http://34.105.27.34/oscar/public","https://cms.oscarstoresapp.com",$bannersMob[0]['image']);

			@endphp
					
                        <img loading="lazy"src="{{ $banner_image }}"  class="w-100 d-none d-md-block d-lg-block d-xl-block  h-100">
                        <img loading="lazy"src="{{ $banner_image_mob }}"  class="w-100 d-block  d-sm-block d-md-none h-100" >
                         <div class="ad-content">
                            {{-- <h2 class="h4">We Offer Premium & 100% Organic Food</h2>
                            <a href="shop.html" class="ad-btn">Shop Now</a> --}}
                        </div> 
                    </div>
                </div>
                <div class="col-4 mb-10">
                    <div class="vs-ad-box4 mega-hover">
						@if ($banners[1]['link'])
						
                        <a href="{{$banners[1]['link']}}" class="overlay"><span class="sr-only">hole div linkup  </span></a>
						@endif
						@php
				
				$banner_image_1=str_replace("http://34.105.27.34/oscar/public","https://cms.oscarstoresapp.com",$banners[1]['image']);
				$banner_image_mob_1=str_replace("http://34.105.27.34/oscar/public","https://cms.oscarstoresapp.com",$bannersMob[1]['image']);

			@endphp
						<img loading="lazy"src="{{ $banner_image_1 }}" class=" w-100 d-none d-md-block d-lg-block d-xl-block   h-100">
                        <img loading="lazy"src="{{ $banner_image_mob_1 }}"  class="w-100 d-block  d-sm-block d-md-none h-100">
                        <div class="ad-content">
                            {{-- <h2 class="h4">We Offer Premium & 100% Organic Food</h2>
                            <a href="shop.html" class="ad-btn">Shop Now</a> --}}
                        </div> 
                    </div>
                </div>
                <div class="col-4 mb-10">
                    <div class="vs-ad-box5 mega-hover">
						@if ($banners[2]['link'])

                        <a href="{{$banners[2]['link']}}" class="overlay"><span class="sr-only">hole div linkup</span></a>
						@endif
						@php
				
				$banner_image_2=str_replace("http://34.105.27.34/oscar/public","https://cms.oscarstoresapp.com",$banners[2]['image']);
				$banner_image_mob_2=str_replace("http://34.105.27.34/oscar/public","https://cms.oscarstoresapp.com",$bannersMob[2]['image']);

			@endphp
						<img loading="lazy"src="{{ $banner_image_2 }}" class="w-100 d-none d-md-block d-lg-block d-xl-block   h-100">
                        <img loading="lazy"src="{{ $banner_image_mob_2 }}"class="w-100 d-block  d-sm-block d-md-none h-100">
                        <div class="ad-content">
                          
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>

			<section class="shop-categories-wrapper  " data-bg-src="{{ asset('shop-cat-bg.webp') }}">
				<div class="container">
					<div class="section-title text-center  wow fadeIn" >
						<h2 class="sec-title1">{{ __('lang.Shop By Categories') }}</h2>
						<img loading="lazy" style="  margin: 0 auto;" src="{{ asset('assets/img/icons/sec-icon-1.png') }}" alt="Section Icon">
					</div>
					<div class="row vs-carousel  wow fadeIn" data-wow-delay="0.3s" data-slide-show="4">

						@foreach ($main_categories as $category)
							<div class="col-xl-3">
								<div class="vs-cat-card mb-30">
									<div class="cat-img">
										<a href="{{ url(app()->getLocale().'/homeWeb/all_products/'. $category->id ) }}">
											<img loading="lazy" style="background-size: cover;background-repeat: round;height:150px" src="https://cms.oscarstoresapp.com/categoryImages/{{$category->id}}.webp"  alt="Cat Image"></a>
									</div>
									<div class="cat-content">
										<h3 class="h5 cat-title"><a href="{{ url(app()->getLocale().'/homeWeb/all_products/'. $category->id ) }}">{{ $category->name->$lang }}</a></h3>
									</div>
								</div>
							</div>
						@endforeach
					
					
					</div>
				</div>
			</section>



		<section class="vs-blog-wrapper " id="blog">
			<div class="container">
				<div class="section-title text-center" style="margin-bottom: 0">
					<h1 class="sec-title1">{{ __('lang.Ready Meals') }}</h1>
					<h2 class="sec-title2">{{ __('lang.from Oscar') }}</h2>
					<img loading="lazy" style="margin: 0 auto" src="{{ asset('assets/img/icons/sec-icon-1.png') }}" alt="Section Shape">
				</div>
				<div class="row vs-carousel wow fadeIn" data-slide-show="3">
					@foreach ($readyMeals as $meal)
					@php
				
				$meal_image=str_replace("http://34.105.27.34/oscar/public","https://cms.oscarstoresapp.com",$meal->images['0']->src);
			@endphp
						<div class="col-lg-4">
							<div class="vs-blog blog-grid vs-product-box1 thumb_swap">
								<div class="blog-img image-scale-hover">
									<a href="{{ url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id) }}"><img src="{{$meal_image}}" alt="Blog Image" class="w-100"></a>
								</div>
								<div class="blog-content text-center product-content">
									<div class="actions-btn">
										@if(Auth::guard('customerForWeb')->user())
									
                                       @if($meal->in_stock == 1)
									   <a  title="{{ $meal->id }}" class="cart {{ $meal->PriceUnit != "kg" ? 'cart' : ''}} "><i class="fal fa-cart-plus"></i></a>
									   @endif
										<a href="#" title="{{ $meal->id }}" class="Unlike" style="display: {{ in_array($meal->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'block' : 'none' }}"><i style="color:#C53330;" class="fal fa-heart"></i></a>
										<a href="#" title="{{ $meal->id }}" class="Wishlist" style="display: {{ in_array($meal->id,auth('customerForWeb')->user()->wishlist->pluck('id')->toArray()) ? 'none' : 'block' }}"><i class="fal fa-heart"></i></a>
									
										@endif
									</div>
									@if($meal->in_stock != 1)
										<h4 class="product-title h5 mb-0">{{ __('lang.out_of_stock') }}</h4>
										@endif
									<div class="cat-content" >
										<h4 class="cat-title"><a href="{{ url(app()->getLocale().'/show_product/'.$meal->id.'/'.$meal->category_id) }}">{{ app()->getLocale() =='en' ? $meal->name : $meal->name_ar }}</a></h4>
									</div>
								
								</div>
							</div>
						</div>	
					@endforeach
				</div>
			</div>
		</section>


	@endsection


@push('scripts')

@include('website.partials.home.scripts')
@endpush
