<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Momentum CMS</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
		integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<!-- Ionicons -->
	<link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
	<!-- Theme style -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/AdminLTE.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/skins/_all-skins.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
	<!-- Ionicons -->
	<link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//waxolunist.github.io/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css">
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

	<link rel="stylesheet" href="{!! asset('css/app.css'); !!}">
	@yield('css')
	<!-- resources/views/layouts/app.blade.php -->

<style>
  .navbar-right{
        display: block!important;
    }
    .navbar-default{
        background-color:#fff!important;
        border:0px!important;
    }
    .navbar-right li{
        text-transform: capitalize!important;
        padding:0px !important;
    }


	.navbar-left{
        display: block!important;
	}
	.navbar-left li{
    font-size: 20px;
    text-transform: capitalize;
    }
    .navbar-left a{
    padding: 15px;
    line-height: 1.35;
    text-transform: capitalize;
    }

	
	.navbar{
        margin-bottom:0px !important;
        padding:10px !important;
    }
	.round { border-radius: 100%; }
  .chat {
    list-style: none;
    margin: 0;
    padding: 0;
  }

  .chat li {
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
  }

  .chat li .chat-body p {
    margin: 0;
    color: #777777;
  }

  .panel-body {
    overflow-y: scroll;
    height: 350px;
  }

  ::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
  }

  ::-webkit-scrollbar {
    width: 12px;
    background-color: #F5F5F5;
  }

  ::-webkit-scrollbar-thumb {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
	
  }
 






</style>
</head>

<body class="skin-red sidebar-mini"  >
	@if (Auth::guard('customers')->check())
	<nav class="navbar navbar-static-top">
		<div style="width:100%">
			
				<!-- Left Side Of Navbar -->
				
				<!-- Right Side Of Navbar -->
				<div class=" float-left">
					<img style="width:70px;" src="/images/oscar.png">
					<!-- Authentication Links -->

				</div>
                <div class=" float-right">
                    
                        <img  class="round" width="60" height="60" avatar="{{Auth::guard('customers')->user()->name}}">
                        {{Auth::guard('customers')->user()->name}}
                    
					<a style="line-height:1.35 !important;padding: 8px; margin-top: 12px; margin-left: 5px; color: #ffff;background-color: rgb(197,23,28);"  href="{!! url('/log_out_chat/'.Auth::guard('customers')->user()->id) !!}">logout</a>
				</div>
		</div>
	</nav>
	<div id="page-content-wrapper">
		<div class="container-fluid">
			<div class="row" >
				<div class="col-lg-12">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
	@else
    <nav class="navbar navbar-static-top">
		<div style="width:100%;padding:0px 12px 0px 3px;">
			
				<!-- Left Side Of Navbar -->
				
				<!-- Right Side Of Navbar -->
				<div class=" float-left">
					<img style="width:70px;" src="/images/oscar.png">
					<!-- Authentication Links -->

				</div>
                <div class=" float-right">    
                    <a href="{!! url('/login') !!}">Login</a>
                    <a href="{!! url('/register') !!}">Register</a>
				</div>
		</div>
	</nav>
	
	<div id="page-content-wrapper">
		<div class="container-fluid" style="padding:0 15px 0 5px"> 
			<div class="row">
				<div class="col-lg-12">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
	@endif
	<!-- jQuery 3.1.1 -->
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/js/adminlte.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	<script src="https://cdn.tiny.cloud/1/n1cjyxxgymwd2wum5970f6g9h6st14jhhoji8cn251kt1zud/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script>/*
     * LetterAvatar
     * 
     * Artur Heinze
     * Create Letter avatar based on Initials
     * based on https://gist.github.com/leecrossley/6027780
     */
    (function(w, d){


        function LetterAvatar (name, size) {

            name  = name || '';
            size  = size || 60;

            var colours = [
                    "#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50", 
                    "#f1c40f", "#e67e22", "#e74c3c", "#ecf0f1", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#bdc3c7", "#7f8c8d"
                ],

                nameSplit = String(name).toUpperCase().split(' '),
                initials, charIndex, colourIndex, canvas, context, dataURI;


            if (nameSplit.length == 1) {
                initials = nameSplit[0] ? nameSplit[0].charAt(0):'?';
            } else {
                initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
            }

            if (w.devicePixelRatio) {
                size = (size * w.devicePixelRatio);
            }
                
            charIndex     = (initials == '?' ? 72 : initials.charCodeAt(0)) - 64;
            colourIndex   = charIndex % 20;
            canvas        = d.createElement('canvas');
            canvas.width  = size;
            canvas.height = size;
            context       = canvas.getContext("2d");
             
            context.fillStyle = colours[colourIndex - 1];
            context.fillRect (0, 0, canvas.width, canvas.height);
            context.font = Math.round(canvas.width/2)+"px Arial";
            context.textAlign = "center";
            context.fillStyle = "#FFF";
            context.fillText(initials, size / 2, size / 1.5);

            dataURI = canvas.toDataURL();
            canvas  = null;

            return dataURI;
        }

        LetterAvatar.transform = function() {

            Array.prototype.forEach.call(d.querySelectorAll('img[avatar]'), function(img, name) {
                name = img.getAttribute('avatar');
                img.src = LetterAvatar(name, img.getAttribute('width'));
                img.removeAttribute('avatar');
                img.setAttribute('alt', name);
            });
        };


        // AMD support
        if (typeof define === 'function' && define.amd) {
            
            define(function () { return LetterAvatar; });
        
        // CommonJS and Node.js module support.
        } else if (typeof exports !== 'undefined') {
            
            // Support Node.js specific `module.exports` (which can be a function)
            if (typeof module != 'undefined' && module.exports) {
                exports = module.exports = LetterAvatar;
            }

            // But always support CommonJS module 1.1.1 spec (`exports` cannot be a function)
            exports.LetterAvatar = LetterAvatar;

        } else {
            
            window.LetterAvatar = LetterAvatar;

            d.addEventListener('DOMContentLoaded', function(event) {
                LetterAvatar.transform();
            });
        }

    })(window, document);</script>
	@yield('scripts')

</body>

</html>