<li class="{{ Request::is('home') ? 'active' : '' }}">
	<a href="{!! route('home') !!}"><i class="fa fa-tachometer-alt"></i><span>Dashboard</span></a>
</li>
<li
	class="treeview {{ Request::is('users*') || Request::is('roles*') || Request::is('permissions*') || Request::is('devices*') ? 'active' : '' }}">
	<a href="#">
		<i class="fa fa-users"></i>
		<span>Users</span>
		<span class="pull-right-container">
			<i class="fa fa-angle-left pull-right"></i>
		</span>
	</a>
	<ul class="treeview-menu">
		<li class="{{ Request::is('users*') ? 'active' : '' }}">
			<a href="{!! route('users.index') !!}">
				<i class="fa fa-users"></i>
				<span>All Users</span>
			</a>
		</li>
		<li class="{{ Request::is('roles*') ? 'active' : '' }}">
			<a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>Roles</span></a>
		</li>
		<li class="{{ Request::is('permissions*') ? 'active' : '' }}">
			<a href="{!! route('permissions.index') !!}"><i class="fa fa-edit"></i><span>Permissions</span></a>
		</li>
		{{--  <li class="{{ Request::is('devices*') ? 'active' : '' }}">
		<a href="{!! route('devices.index') !!}"><i class="fa fa-mobile"></i><span>Devices</span></a>
</li> --}}
</ul>
</li>
{{--  <li class="treeview {{ Request::is('pages*') || Request::is('pageTypes*') || Request::is('pageContents*') || Request::is('pageItems*') ? 'active' : '' }}">
<a href="#">
	<i class="fa fa-copy"></i>
	<span>Pages</span>
	<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
	</span>
</a>
<ul class="treeview-menu">
	<li class="{{ Request::is('pages*') ? 'active' : '' }}">
		<a href="{!! route('pages.index') !!}"><i class="fa fa-edit"></i><span>All Pages</span></a>
	</li>
	<li class="{{ Request::is('pageContents*') ? 'active' : '' }}">
		<a href="{!! route('pageContents.index') !!}"><i class="fa fa-edit"></i><span>Page Contents</span></a>
	</li>
	<li class="{{ Request::is('pageItems*') ? 'active' : '' }}">
		<a href="{!! route('pageItems.index') !!}"><i class="fa fa-edit"></i><span>Items</span></a>
	</li>
	<li class="{{ Request::is('pageTypes*') ? 'active' : '' }}">
		<a href="{!! route('pageTypes.index') !!}"><i class="fa fa-edit"></i><span>Types</span></a>
	</li>
</ul>
</li> --}}
{{--  <li class="treeview {{ Request::is('categories*') || Request::is('promotions*') || Request::is('import') ? 'active' : '' }}">
<a href="#">
	<i class="fa fa-tag"></i>
	<span>Products</span>
	<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
	</span>
</a>
<ul class="treeview-menu">
	<li class="{{ Request::is('import') ? 'active' : '' }}">
		<a href="{!! url('import') !!}"><i class="fa fa-edit"></i><span>Import Promotions</span></a>
	</li>
	<li class="{{ Request::is('promotions*') ? 'active' : '' }}">
		<a href="{!! route('promotions.index') !!}"><i class="fa fa-edit"></i><span>All Promotions</span></a>
	</li>
	<li class="{{ Request::is('categories*') ? 'active' : '' }}">
		<a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
	</li>
</ul>
</li> --}}
<li class="{{ Request::is('restaurants*') ? 'active' : '' }}">
	<a href="{!! route('restaurants.index') !!}"><i class="fa fa-utensils"></i><span>Restaurants</span></a>
</li>
{{--  <li class="treeview {{ Request::is('onBoardings*') || Request::is('onBoardingValues*') ? 'active' : '' }}">
<a href="#">
	<i class="fa fa-edit"></i>
	<span>On Boardings</span>
	<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
	</span>
</a>
<ul class="treeview-menu">
	<li class="{{ Request::is('onBoardings*') ? 'active' : '' }}">
		<a href="{!! route('onBoardings.index') !!}"><i class="fa fa-edit"></i><span>On Boardings</span></a>
	</li>
	<li class="{{ Request::is('onBoardingValues*') ? 'active' : '' }}">
		<a href="{!! route('onBoardingValues.index') !!}"><i class="fa fa-edit"></i><span>On Boarding Values</span></a>
	</li>
</ul>
</li> --}}
{{--  <li class="{{ Request::is('qrCode*') ? 'active' : '' }}">
<a href="{!! route('qrCode.index') !!}"><i class="fa fa-qrcode"></i><span>QRCode</span></a>
</li>
<li class="{{ Request::is('contactForms*') ? 'active' : '' }}">
	<a href="{!! route('contactForms.index') !!}"><i class="fa fa-envelope"></i><span>Contact Forms</span></a>
</li> --}}

{{--  <li class="{{ Request::is('media*') ? 'active' : '' }}">
<a href="{!! route('media.index') !!}"><i class="fa fa-edit"></i><span>Media</span></a>
</li> --}}

<li class="{{ Request::is('productCategories*') ? 'active' : '' }}">
	<a href="{!! route('productCategories.index') !!}"><i class="fa fa-th"></i><span>Product Categories</span></a>
</li>

<li class="{{ Request::is('products*') ? 'active' : '' }}">
	<a href="{!! route('products.index') !!}"><i class="fa fa-shopping-cart"></i><span>Products</span></a>
</li>
<li class="{{ Request::is('branches*') ? 'active' : '' }}">
	<a href="{!! route('branches.index') !!}"><i class="fa fa-map-marker-alt"></i><span>Branches</span></a>
</li><li class="{{ Request::is('kitchens*') ? 'active' : '' }}">
    <a href="{!! route('kitchens.index') !!}"><i class="fa fa-concierge-bell"></i><span>Kitchens</span></a>
</li>
<li class="{{ Request::is('promoVideos*') ? 'active' : '' }}">
    <a href="{!! route('promoVideos.index') !!}"><i class="fa fa-film"></i><span>Promo Videos</span></a>
</li>
<li class="{{ Request::is('uploads*') ? 'active' : '' }}">
    <a href="{!! route('uploads.index') !!}"><i class="fa fa-image"></i><span>Images</span></a>
</li>
<li class="{{ Request::is('notifications*') ? 'active' : '' }}">
	<a href="{!! route('notifications.index') !!}"><i class="fa fa-th"></i><span>notification</span></a>
</li>
<li class="{{ Request::is('imageSlider*') ? 'active' : '' }}">
	<a href="{!! route('imageSlider.index') !!}"><i class="fa fa-th"></i><span>Image Slider</span></a>
</li>
<li class="{{ Request::is('orders*') ? 'active' : '' }}">
	<a href="{!! route('orders.index') !!}"><i class="fa fa-th"></i><span>Orders</span></a>
</li>
<li class="{{ Request::is('customerService*') ? 'active' : '' }}">
	<a href="{!! route('customerService.index') !!}"><i class="fa fa-th"></i><span>Customer Service</span></a>
</li>


