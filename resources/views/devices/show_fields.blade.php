<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $device->id !!}</p>
</div>

<!-- Os Field -->
<div class="form-group">
    {!! Form::label('os', 'Os:') !!}
    <p>{!! $device->os !!}</p>
</div>

<!-- Device Token Field -->
<div class="form-group">
    {!! Form::label('device_token', 'Device Token:') !!}
    <p>{!! $device->device_token !!}</p>
</div>

<!-- Language Field -->
<div class="form-group">
    {!! Form::label('language', 'Language:') !!}
    <p>{!! $device->language !!}</p>
</div>

<!-- Topics Field -->
<div class="form-group">
    {!! Form::label('topics', 'Topics:') !!}
    <p>{!! $device->topics !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $device->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $device->updated_at !!}</p>
</div>

