<!-- Os Field -->
<div class="form-group col-sm-6">
    {!! Form::label('os', 'Os:') !!}
    {!! Form::text('os', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Token Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device_token', 'Device Token:') !!}
    {!! Form::text('device_token', null, ['class' => 'form-control']) !!}
</div>

<!-- Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language', 'Language:') !!}
    {!! Form::text('language', null, ['class' => 'form-control']) !!}
</div>

<!-- Topics Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('topics', 'Topics:') !!}
    {!! Form::textarea('topics', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('devices.index') !!}" class="btn btn-default">Cancel</a>
</div>
