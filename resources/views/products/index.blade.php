@extends('layouts.app')

@section('content')

<section class="content-header">
	<h1 class="pull-left">Products</h1>
  <h1 class="pull-right">

		<a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px "
			href="{!! route('products.create') !!}">Add New</a>
	<a class="btn btn-warning pull-left" style="margin-top: -10px;margin-bottom: 5px"
			href="{!! url('products/import') !!}">Import</a>
        <!-- <a class="btn btn-warning pull-left" style="margin-top: -10px;margin-bottom: 5px"
        href="{!! route('products.syncAPI') !!}">Import From API</a> -->
			<a class="btn btn-warning pull-left" style="margin-top: -10px;margin-bottom: 5px"
			href="{!! url('cache_clear') !!}">Clear Products</a>
	</h1>
</section>
<div class="content">
	<div class="clearfix"></div>
	<div class="pull-right" style="padding-bottom: 3%">
	<form action="/products/search" method="post">
	{{ csrf_field() }}

<input type="text" name="search" >
<input class="btn btn-primary pull-right" style="height: 70%" type="submit" name="submit" value="Search">
</form>
</div>

	@include('flash::message')
	<div class="form-group col-sm-12">
	<form action="/products/selectImages" method="post">
	{{ csrf_field() }}
		{!! Form::label('Products Images', 'Products Images:') !!}
        <select class="form-control" name="product_image_filter">
       
	  <option value="all">All Products</option>
	  <option value="0">All Products Without Images</option>
	  <option value="1">All Products With Images</option>
   
  </select>
  <div class="form-group col-sm-12">
		{!! Form::submit('Select', ['class' => 'btn btn-primary']) !!}
	
	
	</div>
	</form>
</div>

	<div class="clearfix"></div>
	<div class="box box-primary">
		<div class="box-body">
			@include('products.table')
		</div>
	</div>
	<div class="text-center">
            {{$products->links()}}
        </div>
</div>
@endsection