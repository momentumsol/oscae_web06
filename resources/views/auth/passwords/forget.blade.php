@extends('website.layouts.app')


<style>
    .login-page, .register-page {
    background: white;
    
}
.has-feedback .form-control {
   border-radius: 13px !important;
}
.login-box-msg{
font-size: 26px !important;
 color: #000 !important;
}

.login-box-body{
        width: 20% ;
    }
    .login-box-body input{
        width:100%
    }
@media only screen and (max-width: 600px) {
    .login-box-body{
        width: 80% !important;
    }
    .login-box-body input{
        width:100%
    }
}

</style>
@section('content')




<div class="login-box" style="background-color:white">
    @if (Session::has('msg'))
        <div id="alert-cart" class="alert alert-success" style="position: fixed ;z-index:9999" >
            {!! Session::has('msg') ? Session::get("msg") : '' !!}
        </div>
    @endif

    <!-- /.login-logo -->
    <div class="login-box-body" style="margin: 20px auto;border:1px solid #C53330;padding:20px;text-align:center" >
        <p class="login-box-msg">Enter You Email</p>

        <form method="post" action="{{ url('/forgot') }}">
            {!! csrf_field() !!}

            <input type="hidden" name="token" value="{{ url('/forgot') }}">

             <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div> 

            

           

            <div class="row" >
                <div class="col-md-12 " >
                    <button type="submit" class="btn qut-btn vs-btn shadow-none " style=" color:#fff; border-radius:20px !important;">
                        Reset Password
                    </button>
                </div>
            </div>
        </form>

    </div>

@endsection

@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/js/adminlte.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });

    $( document ).ready(function() {
   
        setTimeout(function() { 
         $('#alert-cart').fadeOut('fast'); 
        }, 5000);
    });
    
</script>
</body>
</html>
