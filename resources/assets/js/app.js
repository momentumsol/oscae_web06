
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

///new app
Vue.component('user-new-component', require('./components/UserNewComponent.vue'));
Vue.component('chat-component', require('./components/ChatComponent.vue'));
Vue.component('chat-messages-component', require('./components/ChatMessageComponent.vue'));
Vue.component('chat-form-component', require('./components/ChatFormComponent.vue'));
Vue.component('message-component', require('./components/MessageComponent.vue'));
Vue.component('user_info-component', require('./components/UserInfoComponent.vue'));
import { VBModal } from 'bootstrap-vue'
Vue.directive('b-modal', VBModal)
import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)
import NewChat from './components/ChatComponent.vue';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
const config = {
    headers: {
       'Authorization': "Bearer " + window.localStorage.getItem("token"),
      'Content-Type' : 'application/json'
    }
 }
 import Avatar from 'vue-avatar';
 import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    
    {
        name: 'chat',
        path: '/chat/:id/:customer/:client_id',
        component: NewChat
    },
    {
        name: 'home',
        path: '/users_chat/:customer',
        component: NewChat
    }
  ];
  
  const router = new VueRouter({ mode: 'history', routes: routes});
 

const app = new Vue({
    el: '#app',
    router,
    Avatar,
    props: ['customer'],

    data: {
    
        messages: []
    },
    
    created() {
    
        this.fetchMessages();
    },
    
    methods: {
      
        fetchMessages() {
          
            axios.post('/api/get_messages',{'channel_id':this.$route.params.id},config
            ).then(response => {
                this.messages = response.data;
            });
           
        },

   
    }
});
